//
//  LLWidgetSponsorModel.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 15.3.22.
//

import Foundation

internal struct LLWidgetSponsorModel: Decodable {
    let logoUrl: String
    let name: String

    enum CodingKeys: String, CodingKey {
        case logoUrl = "logo_url"
        case name
    }

    // Expected Payload
    // { logo_url: "https://example.com/logo.png", name: "Logo name" }
}
