//
//  LLWidgetBetDetailsModel.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 15.3.22.
//

import Foundation

/**
    Complementary data for the selection options
 */
internal struct LLWidgetBetDetailsModel: Decodable {
    let bet: String
    let description: String
    /**
        Text used to overwrite the selectable option image.
     */
    let imageOverwriter: String?

    // Expected Payload
    // { bet: "+2.5(-110)", description: "Astros", imageOverwriter: "X" }
}

/**
    The market presentation
 */
internal enum LLOddsBetMarketPresentation: String, Decodable {
    case horizontal
    case vertical
}

/**
    The state of the market
 */
internal enum LLMarketOptionState: String, Decodable {
    case live
    case suspended
    case correct
    case incorrect
}

/**
    Complementary data for the odds details
 */
internal struct LLOddsBetDetailsModel: Decodable {
    /**
        The name of the market
     */
    let marketName: String
    /**
        The ID of the market
     */
    let marketID: String
    /**
        The presentation of the market
     */
    let marketPresentation: LLOddsBetMarketPresentation
    /**
        An array that holds the market options
     */
    let marketOptions: [LLMarketOptionModel]

    enum CodingKeys: String, CodingKey {
        case description
        case marketID = "marketId"
        case marketPresentation = "presentation"
        case marketOptions
    }

    init(marketName: String, marketID: String, marketPresentation: LLOddsBetMarketPresentation, marketOptions: [LLMarketOptionModel]) {
        self.marketName = marketName
        self.marketID = marketID
        self.marketPresentation = marketPresentation
        self.marketOptions = marketOptions
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        marketName = try container.decode(String.self, forKey: .description)
        marketID = try container.decode(String.self, forKey: .marketID)
        marketPresentation = try container.decodeIfPresent(LLOddsBetMarketPresentation.self, forKey: .marketPresentation) ?? .horizontal
        marketOptions = try container.decode([LLMarketOptionModel].self, forKey: .marketOptions)
    }
}

/**
    Market option
 */
internal struct LLMarketOptionModel: Decodable {
    /**
        Market option image
     */
    let imageURL: URL?
    /**
        Market option description
     */
    let description: String?
    /**
        Market option odds
     */
    let odds: String?
    /**
        Market suspended flag,
        Will be deprecated in future versions.
     */
    private let _isSuspended: Bool?
    /**
        Market option outcome ID
     */
    let outcomeID: String
    /**
        Market option outcome URL
     */
    let url: URL?
    /**
        The state of the option
     */
    let state: LLMarketOptionState?

    /**
        Computed prop that returns true if the market is suspended
     */
    var isSuspended: Bool {
        return _isSuspended ?? (state == .suspended)
    }

    enum CodingKeys: String, CodingKey {
        case imageURL = "imageUrl"
        case description
        case odds
        case isSuspended
        case outcomeID = "outcomeId"
        case url
        case state
    }

    init(imageURL: URL?, description: String?, odds: String?, isSuspended: Bool? = nil, outcomeID: String, url: URL?, state: LLMarketOptionState?) {
        self.imageURL = imageURL
        self.description = description
        self.odds = odds
        _isSuspended = isSuspended
        self.outcomeID = outcomeID
        self.url = url
        self.state = state
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        imageURL = try container.decodeIfPresent(URL.self, forKey: .imageURL)
        description = try container.decodeIfPresent(String.self, forKey: .description)
        odds = try container.decodeIfPresent(String.self, forKey: .odds)
        _isSuspended = try container.decodeIfPresent(Bool.self, forKey: .isSuspended)
        outcomeID = try container.decode(String.self, forKey: .outcomeID)
        url = try? container.decodeIfPresent(URL.self, forKey: .url)
        state = try container.decodeIfPresent(LLMarketOptionState.self, forKey: .state)
    }

    /**
        Checks if objects are different
     */
    public func isDifferent(model: LLMarketOptionModel?) -> Bool {
        guard let model = model else { return true }

        if imageURL == model.imageURL,
           description == model.description,
           odds == model.odds,
           _isSuspended == model._isSuspended,
           outcomeID == model.outcomeID,
           url == model.url,
           state == model.state
        {
            return false
        }
        return true
    }
}

/**
    Object that keeps current and previous bet details
 */
internal struct LLOddsBetDetailsDiffModel {
    /**
        The name of the market
     */
    let marketName: String
    /**
        The ID of the market
     */
    let marketID: String
    /**
        The presentation of the market
     */
    let marketPresentation: LLOddsBetMarketPresentation
    /**
        The name of the market
     */
    let currentOptions: [LLMarketOptionModel]
    /**
        The ID of the market
     */
    let previousOptions: [LLMarketOptionModel]?

    /**
        Checks if refresh is required on this item
     */
    public func isDifferent(detailsModel: LLOddsBetDetailsModel) -> Bool {
        if detailsModel.marketID == marketID {
            return false
        }
        return true
    }

    /**
        Checks if refresh is required on this item
     */
    public func isRefreshRequired(detailsModel: LLOddsBetDetailsModel) -> Bool {
        if detailsModel.marketID == marketID,
           detailsModel.marketName == marketName,
           detailsModel.marketPresentation == marketPresentation,
           detailsModel.marketOptions.count == currentOptions.count
        {
            for (index, element) in currentOptions.enumerated() {
                if element.isDifferent(model: detailsModel.marketOptions[index]) {
                    return true
                }
            }

            return false
        }
        return true
    }
}
