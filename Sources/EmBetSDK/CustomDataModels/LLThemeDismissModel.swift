//
//  LLThemeDismissModel.swift
//
//
//  Created by Ljupco Nastevski on 27.5.22.
//

import UIKit

internal class LLThemeDismissModel: LLThemeViewModel {
    var color: UIColor?
    var size: Float?

    enum CodingKeys: String, CodingKey {
        case color
        case size
    }

    override init() { super.init() }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        if let hexColor = try container.decodeIfPresent(String.self, forKey: .color), let color = UIColor(hexaRGBA: hexColor) {
            self.color = color
        }

        if let size = try container.decodeIfPresent(Float.self, forKey: .size) {
            self.size = size
        } else {
            size = LLUIConstants.DismissButtonSize
        }

        try super.init(from: decoder)
    }
}
