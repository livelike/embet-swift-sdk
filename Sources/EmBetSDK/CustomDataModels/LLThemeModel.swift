//
//  LLTheme.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 28.2.22.
//

import Foundation
import UIKit

internal struct LLThemeModel: Decodable {
    let widgets: LLThemeWidgetsModel?
}

internal struct LLThemeWidgetsModel: Decodable {
    let prediction: LLThemeWidgetModel?
    let odds: LLThemeWidgetModel?
    let alert: LLThemeWidgetModel?
    let quiz: LLThemeWidgetModel?
}

internal struct LLThemeWidgetModel: Decodable {
    var root: LLThemeViewModel?
    var title: LLThemeViewModel?
    var header: LLThemeViewModel?
    var body: LLThemeViewModel?
    var footer: LLThemeViewModel?
    var insights: LLThemeViewModel?
    var betButton: LLThemeViewModel?

    var roundTimer: LLThemeTimerModel?
    var timer: LLThemeTimerModel?

    var dismiss: LLThemeDismissModel?

    var selectedOption: LLThemeViewModel?
    var selectedOptionBar: LLThemeViewModel?
    var selectedOptionDescription: LLThemeViewModel?
    var selectedOptionImage: LLThemeViewModel?
    var selectedOptionPercentage: LLThemeViewModel?

    var unselectedOption: LLThemeViewModel?
    var unselectedOptionBar: LLThemeViewModel?
    var unselectedOptionDescription: LLThemeViewModel?
    var unselectedOptionImage: LLThemeViewModel?
    var unselectedOptionPercentage: LLThemeViewModel?

    var insightBodyText: LLThemeViewModel?
    var insightTable: LLThemeViewModel?
    var insightTableCell: LLThemeViewModel?
    var insightTableCellHighlighted: LLThemeViewModel?
    var animatedBet: LLThemeViewModel?
    var sponsorText: LLThemeViewModel?
    var sponsor: LLThemeViewModel?

    var optionImageOverwriter: LLThemeViewModel?
    var optionRow: LLThemeViewModel?
    var optionRowVertical: LLThemeViewModel?

    var disclaimer: LLThemeViewModel?
    var disclaimerTitle: LLThemeViewModel?
    var disclaimerDescription: LLThemeViewModel?
    var teamName: LLThemeViewModel?
    var separator: LLThemeColorModel?
    var qrLabel: LLThemeViewModel?

    var accessoryButton: LLThemeDismissModel?
    var lockIcon: LLThemeColorModel?

    var information: LLThemeViewModel?

    var unselectedCorrectOption: LLThemeViewModel?
    var unselectedCorrectOptionBar: LLThemeViewModel?
    var unselectedCorrectDescription: LLThemeViewModel?
    var unselectedCorrectOptionPercentage: LLThemeViewModel?

    var selectedCorrectOption: LLThemeViewModel?
    var selectedCorrectOptionBar: LLThemeViewModel?
    var selectedCorrectDescription: LLThemeViewModel?
    var selectedCorrectOptionPercentage: LLThemeViewModel?

    var unselectedIncorrectOption: LLThemeViewModel?
    var unselectedIncorrectOptionBar: LLThemeViewModel?
    var unselectedIncorrectDescription: LLThemeViewModel?
    var unselectedIncorrectOptionPercentage: LLThemeViewModel?

    var selectedIncorrectOption: LLThemeViewModel?
    var selectedIncorrectOptionBar: LLThemeViewModel?
    var selectedIncorrectDescription: LLThemeViewModel?
    var selectedIncorrectOptionPercentage: LLThemeViewModel?

    var predictionCorrect: LLThemeViewModel?
    var predictionIncorrect: LLThemeViewModel?
    var predictionUnresolved: LLThemeViewModel?

    var resultDescription: LLThemeViewModel?
    var teamScore: LLThemeViewModel?
    var teamScoreWinner: LLThemeViewModel?
    var backButton: LLThemeViewModel?
}
