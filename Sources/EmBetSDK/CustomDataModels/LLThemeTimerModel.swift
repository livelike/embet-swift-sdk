//
//  LLThemeTimerModel.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 15.3.22.
//

import Foundation

internal class LLThemeTimerModel: LLThemeViewModel {
    var height: Double?
    var barWidth: Double?

    enum CodingKeys: String, CodingKey {
        case height
        case barWidth
    }

    override init() { super.init() }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        height = try container.decodeIfPresent(Double.self, forKey: .height)
        barWidth = try container.decodeIfPresent(Double.self, forKey: .barWidth)

        try super.init(from: decoder)
    }
}
