//
//  LLCustomWidgetPayload.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 23.2.22.
//

import Foundation
import LiveLikeSwift

/**
    Body variations of the widges
 */
internal enum LLWidgetBodyVariation: String, Decodable {
    case square
    case inline
    case bar
    case duel
    case squareFollowUp
}

/**
    Widget timer type
 */
internal enum LLWidgetTimerType: String, Decodable {
    case round
    case bar
}

/**
    Bet widget presentation type
 */
internal enum LLWidgetPresentationType {
    case insight
    case regular
}

/**
    Widget labels
 */
internal struct LLWidgetLabels: Decodable {
    let sponsoredBy: String?
    let widgetTitle: String?
    let numberOfVotes: String?
    let information: String?
    let disclaimerTitle: String?
    let disclaimerDescription: String?
    let disclaimerDescriptionCollapsed: String?
    let predictionCorrect: String?
    let predictionIncorrect: String?
    let predictionUnresolved: String?
    let qrBetMessage: String?
    let resultDescription: String?
}

/**
    Widget position in overlay
 */
internal enum LLWidgetPositionX: Decodable {
    case left
    case center
    case right
}

internal enum LLWidgetPositionY: Decodable {
    case top
    case center
    case bottom
}

/**
    Widget Alert widget media types
 */
internal enum AlertWidgetMediaType: String, Decodable {
    case image
    case gif
    case video
}

/**
    Alert widget text position.
    Determines the text position in relation to the media content.
 */
internal enum AlertWidgetTextPosition: String, Decodable {
    case bottom
    case top
    case left
    case right
}

// MARK: - Custom Widgets

/**
    Base custom widget payload protocol
 */
internal protocol LLBaseWidgetPayload: Any, Decodable {
    var widgetType: EmBetWidgetKind { get }
    var timer: LLWidgetTimerType? { get }
    var sponsors: [LLWidgetSponsorModel]? { get }
    var theme: LLThemeModel? { get }
    var legalLogoURL: URL? { get }
    var placeBetLabel: String? { get }
    var placeBetURL: URL? { get }
    var labels: LLWidgetLabels? { get }
    var position: LLPosition? { get }
    var openAnimation: LLWidgetAnimation? { get }
}

internal struct LLWidgetTypePayload: Decodable {
    let widgetType: EmBetWidgetKind
}

/**
    custom_data payload for Custom Alert Widget
 */
internal struct LLAlertWidgetPayload: LLBaseWidgetPayload, Decodable {
    let widgetType: EmBetWidgetKind
    let timer: LLWidgetTimerType?
    let sponsors: [LLWidgetSponsorModel]?
    let theme: LLThemeModel?
    let legalLogoURL: URL?
    let placeBetLabel: String?
    let placeBetURL: URL?
    let labels: LLWidgetLabels?
    let textPosition: AlertWidgetTextPosition
    let mediaType: AlertWidgetMediaType
    let position: LLPosition?
    let openAnimation: LLWidgetAnimation?

    enum CodingKeys: String, CodingKey {
        case widgetType
        case timer
        case sponsors
        case theme
        case legalLogoURL = "legalLogoSrc"
        case placeBetLabel
        case placeBetURL = "placeBetUrl"
        case labels
        case textPosition
        case mediaType
        case position
        case openAnimation
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        widgetType = try container.decode(EmBetWidgetKind.self, forKey: .widgetType)
        timer = try container.decodeIfPresent(LLWidgetTimerType.self, forKey: .timer)
        sponsors = try container.decodeIfPresent([LLWidgetSponsorModel].self, forKey: .sponsors)
        theme = try container.decodeIfPresent(LLThemeModel.self, forKey: .theme)
        legalLogoURL = try container.decodeIfPresent(URL.self, forKey: .legalLogoURL)
        placeBetLabel = try container.decodeIfPresent(String.self, forKey: .placeBetLabel)
        placeBetURL = try container.decodeIfPresent(URL.self, forKey: .placeBetURL)
        labels = try container.decodeIfPresent(LLWidgetLabels.self, forKey: .labels)
        textPosition = try container.decode(AlertWidgetTextPosition.self, forKey: .textPosition)
        mediaType = try container.decode(AlertWidgetMediaType.self, forKey: .mediaType)
        position = try container.decodeIfPresent(LLPosition.self, forKey: .position)
        openAnimation = try container.decodeIfPresent(LLWidgetAnimation.self, forKey: .openAnimation)
    }
}

/**
    custom_data payload for Custom Bet Widget
 */
internal struct LLBetWidgetPayload: LLBaseWidgetPayload, Decodable {
    let widgetType: EmBetWidgetKind
    let legalLogoURL: URL?

    let bodyText: String?
    let table: [LLWidgetTableElementModel]?

    let animationDelay: Double?
    let timeout: Double?
    let timer: LLWidgetTimerType?

    let widgetVariation: LLWidgetBodyVariation
    let sponsors: [LLWidgetSponsorModel]?
    let betDetails: [LLWidgetBetDetailsModel]
    let theme: LLThemeModel?

    let placeBetLabel: String?
    let placeBetURL: URL?

    let labels: LLWidgetLabels?
    let position: LLPosition?
    let openAnimation: LLWidgetAnimation?

    enum CodingKeys: String, CodingKey {
        case placeBetURL = "placeBetUrl"
        case widgetType
        case placeBetLabel
        case legalLogoURL = "legalLogoSrc"

        case bodyText
        case table

        case animationDelay
        case timeout
        case timer

        case widgetVariation
        case sponsors
        case betDetails
        case theme

        case labels
        case position
        case openAnimation
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        placeBetURL = try container.decodeIfPresent(URL.self, forKey: .placeBetURL)
        widgetType = try container.decode(EmBetWidgetKind.self, forKey: .widgetType)
        placeBetLabel = try container.decodeIfPresent(String.self, forKey: .placeBetLabel)
        legalLogoURL = try container.decodeIfPresent(URL.self, forKey: .legalLogoURL)
        bodyText = try container.decodeIfPresent(String.self, forKey: .bodyText)
        table = try container.decodeIfPresent([LLWidgetTableElementModel].self, forKey: .table)
        animationDelay = try container.decodeIfPresent(Double.self, forKey: .animationDelay)
        timeout = try container.decodeIfPresent(Double.self, forKey: .timeout)
        timer = try container.decodeIfPresent(LLWidgetTimerType.self, forKey: .timer)
        widgetVariation = try container.decode(LLWidgetBodyVariation.self, forKey: .widgetVariation)
        sponsors = try container.decodeIfPresent([LLWidgetSponsorModel].self, forKey: .sponsors)
        betDetails = try container.decode([LLWidgetBetDetailsModel].self, forKey: .betDetails)
        theme = try container.decodeIfPresent(LLThemeModel.self, forKey: .theme)
        labels = try container.decodeIfPresent(LLWidgetLabels.self, forKey: .labels)
        position = try container.decodeIfPresent(LLPosition.self, forKey: .position)
        openAnimation = try container.decodeIfPresent(LLWidgetAnimation.self, forKey: .openAnimation)
    }
}

/**
    custom_data payload for Custom Odds Widget
 */
internal struct LLOddsWidgetPayload: LLBaseWidgetPayload, Decodable {
    let widgetType: EmBetWidgetKind
    let timer: LLWidgetTimerType?
    let sponsors: [LLWidgetSponsorModel]?
    var legalLogoURL: URL?
    let theme: LLThemeModel?
    let placeBetLabel: String?
    let placeBetURL: URL?
    let labels: LLWidgetLabels?
    let betDetails: [LLOddsBetDetailsModel]?
    let sportsbookLogoURL: URL?
    let teams: [LLWidgetImageDetailsModel]?
    let position: LLPosition?
    let openAnimation: LLWidgetAnimation?
    let finalResult: LLFinalResultModel?

    enum CodingKeys: String, CodingKey {
        case widgetType
        case timer
        case sponsors
        case legalLogoURL = "legalLogoSrc"
        case theme
        case placeBetLabel
        case placeBetURL = "placeBetUrl"
        case labels
        case betDetails
        case sportsbookLogoURL = "sportsbookLogoUrl"
        case teams
        case position
        case openAnimation
        case finalResult
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        widgetType = try container.decode(EmBetWidgetKind.self, forKey: .widgetType)
        timer = try container.decodeIfPresent(LLWidgetTimerType.self, forKey: .timer)
        sponsors = try container.decodeIfPresent([LLWidgetSponsorModel].self, forKey: .sponsors)
        legalLogoURL = try container.decodeIfPresent(URL.self, forKey: .legalLogoURL)
        theme = try container.decodeIfPresent(LLThemeModel.self, forKey: .theme)
        placeBetLabel = try container.decodeIfPresent(String.self, forKey: .placeBetLabel)
        placeBetURL = try container.decodeIfPresent(URL.self, forKey: .placeBetURL)
        betDetails = try container.decode([LLOddsBetDetailsModel].self, forKey: .betDetails)
        labels = try container.decodeIfPresent(LLWidgetLabels.self, forKey: .labels)
        sportsbookLogoURL = try container.decodeIfPresent(URL.self, forKey: .sportsbookLogoURL)
        teams = try container.decodeIfPresent([LLWidgetImageDetailsModel].self, forKey: .teams)
        position = try container.decodeIfPresent(LLPosition.self, forKey: .position)
        openAnimation = try container.decodeIfPresent(LLWidgetAnimation.self, forKey: .openAnimation)
        finalResult = try container.decodeIfPresent(LLFinalResultModel.self, forKey: .finalResult)
    }
}

/**
    custom_data payload for Custom Quiz Widget
 */
internal struct LLQuizWidgetPayload: LLBaseWidgetPayload, Decodable {
    let widgetType: EmBetWidgetKind
    let timer: LLWidgetTimerType?
    let sponsors: [LLWidgetSponsorModel]?
    let theme: LLThemeModel?
    let legalLogoURL: URL?
    let placeBetLabel: String?
    let placeBetURL: URL?
    let labels: LLWidgetLabels?
    let position: LLPosition?
    let openAnimation: LLWidgetAnimation?

    enum CodingKeys: String, CodingKey {
        case widgetType
        case timer
        case sponsors
        case theme
        case legalLogoURL = "legalLogoSrc"
        case placeBetLabel
        case placeBetURL = "placeBetUrl"
        case labels
        case textPosition
        case mediaType
        case position
        case openAnimation
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        widgetType = try container.decode(EmBetWidgetKind.self, forKey: .widgetType)
        timer = try container.decodeIfPresent(LLWidgetTimerType.self, forKey: .timer)
        sponsors = try container.decodeIfPresent([LLWidgetSponsorModel].self, forKey: .sponsors)
        theme = try container.decodeIfPresent(LLThemeModel.self, forKey: .theme)
        legalLogoURL = try container.decodeIfPresent(URL.self, forKey: .legalLogoURL)
        placeBetLabel = try container.decodeIfPresent(String.self, forKey: .placeBetLabel)
        placeBetURL = try container.decodeIfPresent(URL.self, forKey: .placeBetURL)
        labels = try container.decodeIfPresent(LLWidgetLabels.self, forKey: .labels)
        position = try container.decodeIfPresent(LLPosition.self, forKey: .position)
        openAnimation = try container.decodeIfPresent(LLWidgetAnimation.self, forKey: .openAnimation)
    }
}

enum LLAdvertisementVariation: String, Decodable {
    case leaderboard
    case square
    case banner
    case skyscraper
}

/**
    custom_data payload for Custom Advertisement Widget
 */
internal struct LLAdvertisingWidgetPayload: LLBaseWidgetPayload, Decodable {
    let widgetType: EmBetWidgetKind
    let timer: LLWidgetTimerType?
    let sponsors: [LLWidgetSponsorModel]?
    let theme: LLThemeModel?
    let legalLogoURL: URL?
    let placeBetLabel: String?
    let placeBetURL: URL?
    let labels: LLWidgetLabels?
    let position: LLPosition?
    let openAnimation: LLWidgetAnimation?
    let variation: LLAdvertisementVariation
    let ctaUrl: URL?

    enum CodingKeys: String, CodingKey {
        case widgetType
        case timer
        case sponsors
        case theme
        case legalLogoURL = "legalLogoSrc"
        case placeBetLabel
        case placeBetURL = "placeBetUrl"
        case labels
        case textPosition
        case mediaType
        case position
        case openAnimation
        case variation = "widgetVariation"
        case ctaUrl
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        widgetType = try container.decode(EmBetWidgetKind.self, forKey: .widgetType)
        timer = try container.decodeIfPresent(LLWidgetTimerType.self, forKey: .timer)
        sponsors = try container.decodeIfPresent([LLWidgetSponsorModel].self, forKey: .sponsors)
        theme = try container.decodeIfPresent(LLThemeModel.self, forKey: .theme)
        legalLogoURL = try container.decodeIfPresent(URL.self, forKey: .legalLogoURL)
        placeBetLabel = try container.decodeIfPresent(String.self, forKey: .placeBetLabel)
        placeBetURL = try container.decodeIfPresent(URL.self, forKey: .placeBetURL)
        labels = try container.decodeIfPresent(LLWidgetLabels.self, forKey: .labels)
        position = try container.decodeIfPresent(LLPosition.self, forKey: .position)
        openAnimation = try container.decodeIfPresent(LLWidgetAnimation.self, forKey: .openAnimation)
        variation = try container.decode(LLAdvertisementVariation.self, forKey: .variation)
        ctaUrl = try container.decodeIfPresent(URL.self, forKey: .ctaUrl)
    }
}

/**
    custom_data payload for Custom Bet Widget
 */
internal struct LLPredictionWidgetPayload: LLBaseWidgetPayload, Decodable {
    let widgetType: EmBetWidgetKind
    let legalLogoURL: URL?
    let timer: LLWidgetTimerType?
    let sponsors: [LLWidgetSponsorModel]?
    let betDetails: [LLWidgetBetDetailsModel]?
    let theme: LLThemeModel?
    let placeBetLabel: String?
    let placeBetURL: URL?
    let labels: LLWidgetLabels?
    let position: LLPosition?
    let openAnimation: LLWidgetAnimation?

    enum CodingKeys: String, CodingKey {
        case placeBetURL = "placeBetUrl"
        case widgetType
        case placeBetLabel
        case legalLogoURL = "legalLogoSrc"
        case timer
        case sponsors
        case betDetails
        case theme
        case labels
        case position
        case openAnimation
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        placeBetURL = try container.decodeIfPresent(URL.self, forKey: .placeBetURL)
        widgetType = try container.decode(EmBetWidgetKind.self, forKey: .widgetType)
        placeBetLabel = try container.decodeIfPresent(String.self, forKey: .placeBetLabel)
        legalLogoURL = try container.decodeIfPresent(URL.self, forKey: .legalLogoURL)
        timer = try container.decodeIfPresent(LLWidgetTimerType.self, forKey: .timer)
        sponsors = try container.decodeIfPresent([LLWidgetSponsorModel].self, forKey: .sponsors)
        betDetails = try container.decodeIfPresent([LLWidgetBetDetailsModel].self, forKey: .betDetails)
        theme = try container.decodeIfPresent(LLThemeModel.self, forKey: .theme)
        labels = try container.decodeIfPresent(LLWidgetLabels.self, forKey: .labels)
        position = try container.decodeIfPresent(LLPosition.self, forKey: .position)
        openAnimation = try container.decodeIfPresent(LLWidgetAnimation.self, forKey: .openAnimation)
    }
}
