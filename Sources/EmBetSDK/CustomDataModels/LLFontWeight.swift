//
//  LLFontWeight.swift
//  EBDemo
//
//  Created by Ljupco Nastevski on 17.3.22.
//

import UIKit

internal enum LLFontWeight: String, Decodable {
    case light
    case normal
    case bold

    var uiFontWeight: UIFont.Weight {
        switch self {
        case .light:
            return .light
        case .normal:
            return .regular
        case .bold:
            return .bold
        }
    }
}
