//
//  LLWidgetImageDetailsModel.swift
//
//
//  Created by Ljupco Nastevski on 16.11.23.
//

import Foundation

internal struct LLWidgetImageDetailsModel: Decodable {
    /**
        Description
     */
    let description: String?
    /**
        Image Url
     */
    let logoURL: URL?

    enum CodingKeys: String, CodingKey {
        case description
        case logoURL = "logoUrl"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        description = try container.decodeIfPresent(String.self, forKey: .description)
        logoURL = try container.decodeIfPresent(URL.self, forKey: .logoURL)
    }
}
