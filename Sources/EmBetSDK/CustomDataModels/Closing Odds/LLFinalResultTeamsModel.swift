//
//  LLFinalResultTeamsModel.swift
//
//
//  Created by Ljupco Nastevski on 9.10.24.
//

import Foundation

internal struct LLFinalResultTeamsModel: Decodable {
    let description: String?
    let points: String?
    let isWinner: Bool?
}
