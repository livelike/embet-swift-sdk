//
//  LLFinalResultModel.swift
//
//
//  Created by Ljupco Nastevski on 10.10.24.
//

import Foundation

internal struct LLFinalResultModel: Decodable {
    let labels: LLWidgetLabels?
    let teams: [LLFinalResultTeamsModel]?
    let betDetails: [LLOddsBetDetailsModel]?
}
