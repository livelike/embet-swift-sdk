//
//  LLPoint.swift
//
//
//  Created by Ljupco Nastevski on 24.4.24.
//

import Foundation

/// A point representation
internal struct LLPoint {
    var x: Float
    var y: Float

    init?(floatArray: [Float]) {
        if floatArray.count == 2 {
            x = floatArray[0]
            y = floatArray[1]
        } else {
            return nil
        }
    }

    init(x: Float, y: Float) {
        self.x = x
        self.y = y
    }
}
