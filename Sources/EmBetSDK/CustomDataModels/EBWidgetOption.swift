//
//  EBWidgetOption.swift
//
//
//  Created by Ljupco Nastevski on 25.4.23.
//

import Foundation
import LiveLikeSwift

protocol EBWidgetOption {
    var id: String { get }
    var text: String { get }
    var imageURL: URL? { get }
    var voteCount: Int { get }
    var isOptionCorrect: Bool? { get }
}

extension PredictionWidgetModel.Option: EBWidgetOption {
    var isOptionCorrect: Bool? {
        return nil
    }
}

extension PredictionFollowUpWidgetModel.Option: EBWidgetOption {
    var isOptionCorrect: Bool? {
        return isCorrect
    }
}

extension QuizWidgetModel.Choice: EBWidgetOption {
    var isOptionCorrect: Bool? {
        return isCorrect
    }

    var voteCount: Int {
        return answerCount
    }
}
