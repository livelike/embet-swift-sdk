//
//  LLWidgetAnimation.swift
//
//
//  Created by Ljupco Nastevski on 23.4.24.
//

import Foundation

internal struct LLWidgetFadeAnimation: Decodable {
    static var name: String = "fade"
    var duration: Int
}

internal enum LLWidgetSlideOrigin: String, Decodable {
    case top
    case bottom
    case left
    case right
}

internal struct LLWidgetSlideProps: Decodable {
    let origin: LLWidgetSlideOrigin
}

internal struct LLWidgetSlideAnimation: Decodable {
    static var name: String = "slide"
    var duration: Int
    var props: LLWidgetSlideProps
}

internal enum LLWidgetAnimation: Decodable {
    enum CodingKeys: CodingKey {
        case name
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let animationName = try container.decode(String.self, forKey: .name)

        switch animationName {
        case LLWidgetSlideAnimation.name:
            self = try .slide(LLWidgetSlideAnimation(from: decoder))
        case LLWidgetFadeAnimation.name:
            self = try .fade(LLWidgetFadeAnimation(from: decoder))
        default:
            self = .none
        }
    }

    case slide(LLWidgetSlideAnimation)
    case fade(LLWidgetFadeAnimation)
    case none
}
