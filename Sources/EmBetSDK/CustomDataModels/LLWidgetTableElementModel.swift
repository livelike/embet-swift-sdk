//
//  LLWidgetTableElementModel.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 15.3.22.
//

import Foundation

internal struct LLWidgetTableElementModel: Decodable {
    let value: String
    let rowIndex: Int
    let columnIndex: Int
    let isHighlighted: Bool

    // Expected Payload:
    //  {"value": "",
    //  "rowIndex": 0,
    //  "columnIndex": 0,
    //  "isHighlighted": false}
}
