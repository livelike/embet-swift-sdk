//
//  LLThemeColorModel.swift
//
//
//  Created by Ljupco Nastevski on 28.11.23.
//

import UIKit

internal class LLThemeColorModel: LLThemeViewModel {
    var color: UIColor?

    enum CodingKeys: String, CodingKey {
        case color
    }

    override init() { super.init() }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        if let hexColor = try container.decodeIfPresent(String.self, forKey: .color), let color = UIColor(hexaRGBA: hexColor) {
            self.color = color
        }

        try super.init(from: decoder)
    }
}
