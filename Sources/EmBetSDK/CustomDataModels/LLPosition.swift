//
//  LLPosition.swift
//
//
//  Created by Ljupco Nastevski on 24.4.24.
//

import Foundation

internal enum LLHorizontalPositionComponent: String, Decodable {
    case left
    case center
    case right
}

internal enum LLVerticalPositionComponent: String, Decodable {
    case top
    case center
    case bottom
}

internal struct LLPosition: Decodable {
    var x: LLHorizontalPositionComponent
    var y: LLVerticalPositionComponent
    var offset: LLPoint?

    enum CodingKeys: String, CodingKey {
        case x
        case y
        case offset
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        x = try container.decode(LLHorizontalPositionComponent.self, forKey: .x)
        y = try container.decode(LLVerticalPositionComponent.self, forKey: .y)

        let offsetArray = try container.decodeIfPresent([Float].self, forKey: .offset)
        if let offsetArray = offsetArray {
            offset = LLPoint(floatArray: offsetArray)
        }
    }
}
