//
//  LLThemeViewModel.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 15.3.22.
//

import UIKit

internal class LLThemeViewModel: Decodable {
    var borderColor: UIColor?
    var fontWeight: LLFontWeight?
    var borderRadius: CornerRadii?
    var padding: Padding?
    var margin: Margin?
    var fontFamily: String?
    var fontSize: Double?
    var borderWidth: Double?
    var fontColor: UIColor?
    var background: LLThemeBackgroundModel?
    var textAlign: TextAlign?

    enum CodingKeys: String, CodingKey {
        case borderColor
        case fontWeight
        case borderRadius
        case padding
        case margin
        case fontFamily
        case fontSize
        case borderWidth
        case fontColor
        case background
        case textAlign
    }

    init() {}

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        if let hexColor = try? container.decodeIfPresent(String.self, forKey: .borderColor), let color = UIColor(hexaRGBA: hexColor) {
            borderColor = color
        }

        fontWeight = try? container.decodeIfPresent(LLFontWeight.self, forKey: .fontWeight)

        if let radii = try? container.decodeIfPresent([Double].self, forKey: .borderRadius) {
            borderRadius = CornerRadii(doubleArray: radii)
        }

        if let pad = try? container.decodeIfPresent([Double].self, forKey: .padding) {
            padding = Padding(doubleArray: pad)
        }

        if let mar = try? container.decodeIfPresent([Double].self, forKey: .margin) {
            margin = Margin(doubleArray: mar)
        }

        fontFamily = try? container.decodeIfPresent(String.self, forKey: .fontFamily)
        fontSize = try? container.decodeIfPresent(Double.self, forKey: .fontSize)

        borderWidth = try? container.decodeIfPresent(Double.self, forKey: .borderWidth)

        if let hexColor = try? container.decodeIfPresent(String.self, forKey: .fontColor), let color = UIColor(hexaRGBA: hexColor) {
            fontColor = color
        }

        background = try? container.decodeIfPresent(LLThemeBackgroundModel.self, forKey: .background)

        textAlign = try? container.decodeIfPresent(TextAlign.self, forKey: .textAlign)
    }
    // Example payload
    // "incorrectOptionBar": {
    //  "borderColor": "D93E34",
    //  "fontWeight": "normal",
    //  "borderRadius": [0, 0, 0, 0],
    //  "padding": [0, 0, 0, 0],
    //  "fontFamily": ["sans-serif"],
    //  "fontSize": 12,
    //  "borderWidth": 0,
    //  "background": {
//    "color": "6F48A7",
//    "format": "fill"
    //  },
    //  "fontColor": "6A5D08",
    //  "margin": [0, 0, 0, 0]
    // }
}
