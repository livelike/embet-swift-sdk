//
//  LLThemeBackgroundModel.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 15.3.22.
//

import UIKit

internal enum LLThemeBackgroundModel: Decodable {
    case fill(FillBackground)
    case uniformGradient(UniformGradientBackground)
    case unsupported

    private enum CodingKeys: String, CodingKey {
        case format
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let format = try container.decode(String.self, forKey: .format)

        switch format {
        case "fill":
            self = try .fill(FillBackground(from: decoder))
        case "uniformGradient":
            self = try .uniformGradient(UniformGradientBackground(from: decoder))
        default:
            self = .unsupported
        }
    }

    struct FillBackground: Decodable {
        var color: UIColor

        private enum CodingKeys: String, CodingKey {
            case color
        }

        init(color: UIColor) {
            self.color = color
        }

        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            let colorString = try container.decode(String.self, forKey: .color)
            color = UIColor(hexaRGBA: colorString) ?? .clear
        }
    }

    struct UniformGradientBackground: Decodable {
        var colors: [UIColor]
        var direction: Double

        private enum CodingKeys: String, CodingKey {
            case colors
            case direction
        }

        init(colors: [UIColor], direction: Double) {
            self.colors = colors
            self.direction = direction
        }

        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)

            let colorString = try container.decode([String].self, forKey: .colors)
            colors = colorString.map { UIColor(hexaRGBA: $0) ?? .clear }

            if let dir = try? container.decodeIfPresent(Double.self, forKey: .direction) {
                direction = dir
            } else {
                direction = LLUIConstants.DefaultBackgroundGradientDirection
            }
        }
    }
}
