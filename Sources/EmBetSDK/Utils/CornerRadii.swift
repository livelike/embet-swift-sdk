//
//  CornerRadii.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 17.3.22.
//

import UIKit

internal struct CornerRadii {
    let topLeft: CGFloat
    let topRight: CGFloat
    let bottomLeft: CGFloat
    let bottomRight: CGFloat

    static let zero = CornerRadii(topLeft: 0, topRight: 0, bottomLeft: 0, bottomRight: 0)

    init(doubleArray: [Double]) {
        if doubleArray.count >= 4 {
            topLeft = doubleArray[0]
            topRight = doubleArray[1]
            bottomLeft = doubleArray[2]
            bottomRight = doubleArray[3]
        } else {
            topLeft = 0
            topRight = 0
            bottomLeft = 0
            bottomRight = 0
        }
    }

    init(topLeft: Double, topRight: Double, bottomLeft: Double, bottomRight: Double) {
        self.topLeft = topLeft
        self.topRight = topRight
        self.bottomLeft = bottomLeft
        self.bottomRight = bottomRight
    }
}
