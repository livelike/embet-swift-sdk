//
//  Padding.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 17.3.22.
//

import UIKit

internal typealias Margin = Padding

internal struct Padding {
    let top: CGFloat
    let right: CGFloat
    let bottom: CGFloat
    let left: CGFloat

    static let zero = Padding(top: 0, right: 0, bottom: 0, left: 0)

    init(doubleArray: [Double]) {
        if doubleArray.count >= 4 {
            top = doubleArray[0]
            right = doubleArray[1]
            bottom = doubleArray[2]
            left = doubleArray[3]
        } else {
            top = 0
            right = 0
            bottom = 0
            left = 0
        }
    }

    init(top: Double, right: Double, bottom: Double, left: Double) {
        self.top = top
        self.right = right
        self.bottom = bottom
        self.left = left
    }

    func toEdgeInsets() -> UIEdgeInsets {
        return UIEdgeInsets(top: top, left: left, bottom: bottom, right: right)
    }

    @available(iOS 11, *)
    func toDirectionalInsets() -> NSDirectionalEdgeInsets {
        return NSDirectionalEdgeInsets(top: top, leading: left, bottom: bottom, trailing: right)
    }
}
