//
//  LLThemeBuilder.swift
//  EBDemo
//
//  Created by Ljupco Nastevski on 17.3.22.
//

import UIKit

internal class LLThemeBuilder {
    var themeModel: LLThemeWidgetModel

    init(themeModel: LLThemeWidgetModel) {
        self.themeModel = themeModel
    }

    /**
        Embed the view in corretct widget base view (header, footer)
     */
    func embedInHolder(themeType: LLThemableType, view: UIView, embedsInMarginView: Bool = true) -> UIView {
        view.translatesAutoresizingMaskIntoConstraints = false

        let holderView = LLThemableView(themeType: themeType)
        holderView.translatesAutoresizingMaskIntoConstraints = false
        holderView.addSubview(view)

        NSLayoutConstraint.activate([
            view.topAnchor.constraint(equalTo: holderView.layoutMarginsGuide.topAnchor),
            view.trailingAnchor.constraint(equalTo: holderView.layoutMarginsGuide.trailingAnchor),
            view.leadingAnchor.constraint(equalTo: holderView.layoutMarginsGuide.leadingAnchor),
            view.bottomAnchor.constraint(equalTo: holderView.layoutMarginsGuide.bottomAnchor),
        ])

        if embedsInMarginView == true {
            return embedInMarginView(themeType: themeType, view: holderView)
        } else {
            return holderView
        }
    }

    /**
        If the theme has margin, add the view into a margin view
     */
    func embedInMarginView(themeType: LLThemableType, view: UIView) -> UIView {
        if let margin = themableModelFrom(type: themeType)?.margin {
            view.translatesAutoresizingMaskIntoConstraints = false

            let marginView = UIView()
            marginView.translatesAutoresizingMaskIntoConstraints = false
            marginView.addSubview(view)

            NSLayoutConstraint.activate([
                view.topAnchor.constraint(equalTo: marginView.topAnchor, constant: margin.top),
                view.trailingAnchor.constraint(equalTo: marginView.trailingAnchor, constant: -margin.right),
                view.leadingAnchor.constraint(equalTo: marginView.leadingAnchor, constant: margin.left),
                view.bottomAnchor.constraint(equalTo: marginView.bottomAnchor, constant: -margin.bottom),
            ])

            return marginView
        }

        return view
    }

    func themableModelFrom(type: LLThemableType) -> LLThemeViewModel? {
        switch type {
        case .dismiss:
            return themeModel.dismiss
        case .timer:
            return themeModel.timer
        case .roundTimer:
            return themeModel.roundTimer
        case .root:
            return themeModel.root
        case .insights:
            return themeModel.insights
        case .title:
            return themeModel.title
        case .header:
            return themeModel.header
        case .body:
            return themeModel.body
        case .footer:
            return themeModel.footer
        case .betButton:
            return themeModel.betButton
        case .selectedOption:
            return themeModel.selectedOption
        case .unselectedOption:
            return themeModel.unselectedOption
        case .selectedOptionPercentage:
            return themeModel.selectedOptionPercentage
        case .selectedOptionBar:
            return themeModel.selectedOptionBar
        case .unselectedOptionPercentage:
            return themeModel.unselectedOptionPercentage
        case .unselectedOptionBar:
            return themeModel.unselectedOptionBar
        case .selectedOptionDescription:
            return themeModel.selectedOptionDescription
        case .unselectedOptionDescription:
            return themeModel.unselectedOptionDescription
        case .insightBodyText:
            return themeModel.insightBodyText
        case .insightTable:
            return themeModel.insightTable
        case .insightTableCell:
            return themeModel.insightTableCell
        case .insightTableCellHighlighted:
            return themeModel.insightTableCellHighlighted
        case .animatedBet:
            return themeModel.animatedBet
        case .sponsorText:
            return themeModel.sponsorText
        case .sponsor:
            return themeModel.sponsor
        case .optionImageOverwriter:
            return themeModel.optionImageOverwriter
        case .optionRow:
            return themeModel.optionRow
        case .optionRowVertical:
            return themeModel.optionRowVertical
        case .disclaimer:
            return themeModel.disclaimer
        case .disclaimerTitle:
            return themeModel.disclaimerTitle
        case .disclaimerDescription:
            return themeModel.disclaimerDescription
        case .teamName:
            return themeModel.teamName
        case .separator:
            return themeModel.separator
        case .qrLabel:
            return themeModel.qrLabel
        case .accessoryButton:
            return themeModel.accessoryButton
        case .lockIcon:
            return themeModel.lockIcon
        case .information:
            return themeModel.information
        case .unselectedCorrectOption:
            return themeModel.unselectedCorrectOption
        case .unselectedCorrectOptionBar:
            return themeModel.unselectedCorrectOptionBar
        case .unselectedCorrectOptionPercentage:
            return themeModel.unselectedCorrectOptionPercentage
        case .selectedCorrectOption:
            return themeModel.selectedCorrectOption
        case .selectedCorrectOptionBar:
            return themeModel.selectedCorrectOptionBar
        case .selectedCorrectOptionPercentage:
            return themeModel.selectedCorrectOptionPercentage
        case .unselectedIncorrectOption:
            return themeModel.unselectedIncorrectOption
        case .unselectedIncorrectOptionBar:
            return themeModel.unselectedIncorrectOptionBar
        case .unselectedIncorrectOptionPercentage:
            return themeModel.unselectedIncorrectOptionPercentage
        case .selectedIncorrectOption:
            return themeModel.selectedIncorrectOption
        case .selectedIncorrectOptionBar:
            return themeModel.selectedIncorrectOptionBar
        case .selectedIncorrectOptionPercentage:
            return themeModel.selectedIncorrectOptionPercentage
        case .predictionCorrect:
            return themeModel.predictionCorrect
        case .predictionIncorrect:
            return themeModel.predictionIncorrect
        case .predictionUnresolved:
            return themeModel.predictionUnresolved
        case .resultDescription:
            return themeModel.resultDescription
        case .teamScore:
            return themeModel.teamScore
        case .teamScoreWinner:
            return themeModel.teamScoreWinner
        case .backButton:
            return themeModel.backButton
        default:
            return nil
        }
    }

    /**
        Apply the theme to every subview of the superview
     */
    func applyThemeTo(superview: UIView) {
        if let superview = superview as? LLThemable {
            let theme: LLThemeViewModel? = themableModelFrom(type: superview.type)
            superview.applyTheme(theme: theme)

            if let interactive = superview as? LLThemableInteractive {
                interactive.unselectedTheme = theme
                interactive.selectedTheme = themableModelFrom(type: interactive.selectedType)
            }
        }

        for subview in superview.subviews {
            applyThemeTo(superview: subview)
        }
    }
}
