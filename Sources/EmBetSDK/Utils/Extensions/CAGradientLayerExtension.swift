//
//  CAGradientLayerExtension.swift
//
//
//  Created by Ljupco Nastevski on 18.3.22.
//

import QuartzCore

internal extension CAGradientLayer {
    /// Converts an angle to gradient start point and end point
    func calculatePoints(for angle: CGFloat) {
        var ang = (-angle).truncatingRemainder(dividingBy: 360)

        if ang < 0 { ang = 360 + ang }

        let n: CGFloat = 0.5

        switch ang {
        case 0 ... 45, 315 ... 360:
            let a = CGPoint(x: n * tanx(ang) + n, y: 1)
            let b = CGPoint(x: n * tanx(-ang) + n, y: 0)
            startPoint = a
            endPoint = b

        case 45 ... 135:
            let a = CGPoint(x: 1, y: n * tanx(-ang - 90) + n)
            let b = CGPoint(x: 0, y: n * tanx(ang - 90) + n)
            startPoint = a
            endPoint = b

        case 135 ... 225:
            let a = CGPoint(x: n * tanx(-ang) + n, y: 0)
            let b = CGPoint(x: n * tanx(ang) + n, y: 1)
            startPoint = a
            endPoint = b

        case 225 ... 315:
            let a = CGPoint(x: 0, y: n * tanx(ang - 90) + n)
            let b = CGPoint(x: 1, y: n * tanx(-ang - 90) + n)
            startPoint = a
            endPoint = b

        default:
            let a = CGPoint(x: 0, y: n)
            let b = CGPoint(x: 1, y: n)
            startPoint = a
            endPoint = b
        }
    }

    /// Private function to aid with the math when calculating the gradient angle
    private func tanx(_ 𝜽: CGFloat) -> CGFloat {
        return tan(𝜽 * CGFloat.pi / 180)
    }
}
