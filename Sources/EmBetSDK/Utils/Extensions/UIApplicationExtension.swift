//
//  File.swift
//
//
//  Created by Ljupco Nastevski on 23.3.22.
//

import UIKit

internal enum OpenError: Error {
    case invalidUrl
}

extension UIApplication {
    func openUrl(urlString: String) throws {
        if let url = URL(string: urlString) {
            open(url)
        } else {
            throw OpenError.invalidUrl
        }
    }
}
