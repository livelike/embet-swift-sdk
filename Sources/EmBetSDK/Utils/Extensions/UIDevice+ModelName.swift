//
//  UIDevice+ModelName.swift
//
//
//  Created by Ljupco Nastevski on 17.7.24.
//

import UIKit

extension UIDevice {
    /// https://stackoverflow.com/questions/26028918/how-to-determine-the-current-iphone-device-model
    static var modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        return machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
    }()
}
