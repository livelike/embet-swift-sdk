//
//  CGRectExtension.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 4.3.22.
//

import UIKit

internal extension CGRect {
    /**
        Returns a masked path for the provided corner radii
     */
    func maskPath(cornerRadii: CornerRadii) -> CGPath {
        let path = UIBezierPath()

        path.move(to: CGPoint(x: width - cornerRadii.topRight, y: 0))
        path.addLine(to: CGPoint(x: cornerRadii.topLeft, y: 0))
        path.addQuadCurve(to: CGPoint(x: 0, y: cornerRadii.topLeft), controlPoint: CGPoint.zero)
        path.addLine(to: CGPoint(x: 0, y: height - cornerRadii.bottomLeft))
        path.addQuadCurve(to: CGPoint(x: cornerRadii.bottomLeft, y: height), controlPoint: CGPoint(x: 0, y: height))
        path.addLine(to: CGPoint(x: width - cornerRadii.bottomRight, y: height))
        path.addQuadCurve(to: CGPoint(x: width, y: height - cornerRadii.bottomRight), controlPoint: CGPoint(x: width, y: height))
        path.addLine(to: CGPoint(x: width, y: cornerRadii.topRight))
        path.addQuadCurve(to: CGPoint(x: width - cornerRadii.topRight, y: 0), controlPoint: CGPoint(x: width, y: 0))

        path.close()

        return path.cgPath
    }
}
