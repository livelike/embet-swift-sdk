//
//  UIColorExtension.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 28.2.22.
//

import UIKit

internal extension UIColor {
    convenience init?(hexaRGBA: String?) {
        guard let hexaRGBA = hexaRGBA else { return nil }

        var cString: String = hexaRGBA.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if cString.hasPrefix("#") {
            cString.remove(at: cString.startIndex)
        }

        if cString.count == 8 {
            self.init(hexRGBA: cString)
        } else if cString.count == 6 {
            self.init(hexRGB: cString)
        } else {
            self.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 1)
        }
    }

    fileprivate convenience init?(hexRGB: String) {
        var rgbValue: UInt64 = 0
        Scanner(string: hexRGB).scanHexInt64(&rgbValue)

        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

    fileprivate convenience init?(hexRGBA: String) {
        var rgbValue: UInt64 = 0
        Scanner(string: hexRGBA).scanHexInt64(&rgbValue)

        self.init(
            red: CGFloat((rgbValue & 0xFF00_0000) >> 24) / 255.0,
            green: CGFloat((rgbValue & 0x00FF_0000) >> 16) / 255.0,
            blue: CGFloat((rgbValue & 0x0000_FF00) >> 8) / 255.0,
            alpha: CGFloat(rgbValue & 0x0000_00FF) / 255.0
        )
    }
}
