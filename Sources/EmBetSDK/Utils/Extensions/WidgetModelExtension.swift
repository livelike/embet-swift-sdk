//
//  WidgetModelExtension.swift
//
//
//  Created by Ljupco Nastevski on 12.4.22.
//

import Foundation
import LiveLikeSwift

internal extension WidgetModel {
    var customData: String? {
        let customData: String?

        switch self {
        case let .cheerMeter(cheerMeterWidgetModel):
            customData = cheerMeterWidgetModel.customData
        case let .alert(alertWidgetModel):
            customData = alertWidgetModel.customData
        case let .quiz(quizWidgetModel):
            customData = quizWidgetModel.customData
        case let .prediction(predictionWidgetModel):
            customData = predictionWidgetModel.customData
        case let .predictionFollowUp(predictionFollowUpWidgetModel):
            customData = predictionFollowUpWidgetModel.customData
        case let .poll(pollWidgetModel):
            customData = pollWidgetModel.customData
        case let .imageSlider(imageSliderWidgetModel):
            customData = imageSliderWidgetModel.customData
        case let .socialEmbed(socialEmbedWidgetModel):
            customData = socialEmbedWidgetModel.customData
        case let .videoAlert(videoAlertWidgetModel):
            customData = videoAlertWidgetModel.customData
        case let .textAsk(textAskWidgetModel):
            customData = textAskWidgetModel.customData
        case let .numberPrediction(numberPredictionWidgetModel):
            customData = numberPredictionWidgetModel.customData
        case let .numberPredictionFollowUp(numberPredictionFollowUpWidgetModel):
            customData = numberPredictionFollowUpWidgetModel.customData
        case let .richPost(richPostWidgetModel):
            customData = richPostWidgetModel.customData
        }

        return customData
    }
}

/**
    Make Widget model confirm to EmBet widget view controller building.
 */
extension WidgetModel: EmBetWidgetModel {
    public var isSuspended: Bool {
        guard let hidden = (baseWidgetModel.widgetAttributes.first(where: { $0.key == EmBetWidgetAttributeKeys.isSuspended })?.value as? NSString)?.boolValue else { return false }
        return hidden
    }

    public var isHidden: Bool {
        guard let hidden = (baseWidgetModel.widgetAttributes.first(where: { $0.key == EmBetWidgetAttributeKeys.isHidden })?.value as? NSString)?.boolValue else { return false }
        return hidden
    }

    public var isPersistent: Bool {
        guard let persistent = (baseWidgetModel.widgetAttributes.first(where: { $0.key == EmBetWidgetAttributeKeys.isPersistent })?.value as? NSString)?.boolValue else { return false }
        return persistent
    }

    private var baseWidgetModel: WidgetModelable {
        switch self {
        case let .alert(model):
            return model
        case let .cheerMeter(model):
            return model
        case let .quiz(model):
            return model
        case let .prediction(model):
            return model
        case let .predictionFollowUp(model):
            return model
        case let .poll(model):
            return model
        case let .imageSlider(model):
            return model
        case let .socialEmbed(model):
            return model
        case let .videoAlert(model):
            return model
        case let .textAsk(model):
            return model
        case let .numberPrediction(model):
            return model
        case let .numberPredictionFollowUp(model):
            return model
        case let .richPost(model):
            return model
        }
    }

    public var widgetKind: EmBetWidgetKind {
        if let data = customData?.data(using: .utf8),
           let widgetType = try? JSONDecoder().decode(LLWidgetTypePayload.self, from: data).widgetType
        {
            return widgetType
        }

        return .unsupported
    }
}
