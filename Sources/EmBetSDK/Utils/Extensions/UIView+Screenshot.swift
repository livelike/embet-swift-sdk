//
//  UIView+Screenshot.swift
//
//
//  Created by Ljupco Nastevski on 13.6.24.
//

import UIKit

extension UIView {
    /**
        Converts the view to an image, preserving transparency.
     */
    func toImage() -> UIImage {
        // Take transparent screenshot and draw it
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
        drawHierarchy(in: bounds, afterScreenUpdates: true)

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return image ?? UIImage()
    }
}
