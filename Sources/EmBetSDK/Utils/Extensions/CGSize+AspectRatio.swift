//
//  File.swift
//
//
//  Created by Ljupco Nastevski on 13.9.22.
//

import UIKit

extension CGSize {
    /**
        Retuns a 16:9 size of the size, preserving the width
     */
    func toSixteenByNinePreservingWidth() -> CGSize {
        return CGSize(width: width, height: (width / 16) * 9)
    }

    /**
        Returns a size that has the same width as the provided, but changes the height to preserve aspec ratio
     */
    func embedInSizePreservingWidth(parentSize: CGSize) -> CGSize {
        let ratio = parentSize.width / width
        return CGSize(width: width, height: height * ratio)
    }
}
