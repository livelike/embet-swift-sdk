//
//  UIFontExtension.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 11.3.22.
//

import UIKit

internal extension UIFont {
    static func loadFontFromTheme(familyName: String?, fontWeight: LLFontWeight?, size: Double?) -> UIFont {
        let size = size ?? LLUIConstants.DefaultFontSize
        guard let familyName = familyName, let fontWeight = fontWeight else {
            return UIFont.systemFont(ofSize: size, weight: fontWeight?.uiFontWeight ?? .regular)
        }

        let fontDescriptor = UIFontDescriptor(fontAttributes: [.family: familyName])

        var attributes = fontDescriptor.fontAttributes
        var traits = (attributes[.traits] as? [UIFontDescriptor.TraitKey: Any]) ?? [:]
        traits[.weight] = fontWeight.uiFontWeight
        attributes[.traits] = traits

        let finalFontDescriptor = UIFontDescriptor(fontAttributes: attributes)

        let font = UIFont(descriptor: finalFontDescriptor, size: CGFloat(size))
        return font
    }
}
