//
//  UIImage+Gif.swift
//
//
//  Created by Ljupco Nastevski on 7.9.22.
//
import ImageIO
import UIKit

// Reference:
// https://github.com/kiritmodi2702/GIF-Swift/blob/master/GIF-Swift/iOSDevCenters%2BGIF.swift
internal extension UIImage {
    static let ExpectedFrameDelay = 0.5

    class func gifImageWithData(data: NSData) -> UIImage? {
        guard let source = CGImageSourceCreateWithData(data, nil) else {
            log.error("The image does not exist")
            return nil
        }

        return UIImage.animatedImageWithSource(source: source)
    }

    class func gifImageWithURL(gifUrl: String) -> UIImage? {
        guard let bundleURL = NSURL(string: gifUrl)
        else {
            log.error("The image with url \"\(gifUrl)\" does not exist")
            return nil
        }
        guard let imageData = NSData(contentsOf: bundleURL as URL) else {
            log.error("Cannot turn image with url \"\(gifUrl)\" into NSData")
            return nil
        }

        return gifImageWithData(data: imageData)
    }

    class func gifImageWithName(name: String) -> UIImage? {
        guard let bundleURL = Bundle.main
            .url(forResource: name, withExtension: "gif")
        else {
            log.error("The image named \"\(name)\" does not exist")
            return nil
        }

        guard let imageData = NSData(contentsOf: bundleURL) else {
            log.error("Cannot turn image named \"\(name)\" into NSData")
            return nil
        }

        return gifImageWithData(data: imageData)
    }

    class func delayForImageAtIndex(index: Int, source: CGImageSource!) -> Double {
        let cfProperties = CGImageSourceCopyPropertiesAtIndex(source, index, nil)
        guard let mediaProperties = cfProperties as? [String: AnyObject],
              let gifProperties = mediaProperties[String(kCGImagePropertyGIFDictionary)] as? [String: AnyObject],
              let delay = gifProperties[String(kCGImagePropertyGIFDelayTime)] as? Double
        else { return ExpectedFrameDelay }
        return delay
    }

    class func gcdForPair(a: Int?, _ b: Int?) -> Int {
        var a = a
        var b = b
        if b == nil || a == nil {
            if b != nil {
                return b!
            } else if a != nil {
                return a!
            } else {
                return 0
            }
        }

        if a! < b! {
            let c = a!
            a = b!
            b = c
        }

        var rest: Int
        while true {
            rest = a! % b!

            if rest == 0 {
                return b!
            } else {
                a = b!
                b = rest
            }
        }
    }

    class func gcdForArray(array: [Int]) -> Int {
        if array.isEmpty {
            return 1
        }

        var gcd = array[0]

        for val in array {
            gcd = UIImage.gcdForPair(a: val, gcd)
        }

        return gcd
    }

    class func animatedImageWithSource(source: CGImageSource) -> UIImage? {
        let count = CGImageSourceGetCount(source)
        var images = [CGImage]()
        var delays = [Int]()

        for i in 0 ..< count {
            if let image = CGImageSourceCreateImageAtIndex(source, i, nil) {
                images.append(image)
            }

            let delaySeconds = UIImage.delayForImageAtIndex(index: Int(i), source: source)
            delays.append(Int(delaySeconds * 1000.0)) // Seconds to ms
        }

        let duration: Int = {
            var sum = 0

            for val: Int in delays {
                sum += val
            }

            return sum
        }()

        let gcd = gcdForArray(array: delays)
        var frames = [UIImage]()

        var frame: UIImage
        var frameCount: Int
        for i in 0 ..< count {
            frame = UIImage(cgImage: images[Int(i)])
            frameCount = Int(delays[Int(i)] / gcd)

            for _ in 0 ..< frameCount {
                frames.append(frame)
            }
        }

        let animation = UIImage.animatedImage(with: frames, duration: Double(duration) / 1000.0)

        return animation
    }
}
