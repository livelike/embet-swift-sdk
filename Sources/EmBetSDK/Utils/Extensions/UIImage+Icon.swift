//
//  Icon.swift
//
//
//  Created by Ljupco Nastevski on 14.9.22.
//

import UIKit

/**
    Images
 */
internal enum IconName: String {
    // The icon used for dismissing the widget
    case dismiss
    // The icon used for muting
    case mute
    // The icon used for unmuting
    case unmute
    // Chevron up icon
    case chevronUp = "chevron_up"
    // Chevron down icon
    case chevronDown = "chevron_down"
    // External link icon
    case externalLink = "external_link"
    // Lock icon
    case lock
    // Close icon
    case close
    // Checkmark icon
    case accept
    // Arrow up green icon
    case arrowUp = "arrow_up"
    // Arrow down red icon
    case arrowDown = "arrow_down"
}

extension UIImage {
    /**
        Loads SDK image from the resouce bundle
     */
    static func embetIconWithName(iconName: IconName) -> UIImage? {
        if #available(iOS 13.0, *) {
            return UIImage(named: iconName.rawValue, in: EmbetResource.resourceBundle, with: nil)
        } else {
            return UIImage(named: iconName.rawValue, in: EmbetResource.resourceBundle, compatibleWith: nil)
        }
    }
}
