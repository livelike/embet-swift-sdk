//
//  CGFloat+Percentage.swift
//
//
//  Created by Ljupco Nastevski on 24.6.24.
//

import Foundation

extension CGFloat {
    var percentageString: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.maximumFractionDigits = 0
        let percentageString = NSNumber(value: self)
        return formatter.string(from: percentageString) ?? "NAN"
    }
}
