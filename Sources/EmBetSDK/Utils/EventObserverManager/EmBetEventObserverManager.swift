//
//  EmBetEventObserverManager.swift
//
//
//  Created by Ljupco Nastevski on 30.11.23.
//

/**
    Class for tracking observers and the blocks they provide.
    The generic type is the blocks argument type.
 */
internal class EmBetEventObserverManager<T> {
    /**
        The type of the called block.
     */
    internal typealias EmBetEvent = (T) -> Void
    /**
        Tuple of observer hash and the block
     */
    typealias EmBetEventEntity = (objectHash: Int, block: EmBetEvent)
    /**
        Currently active observers
     */
    var activeListeners: [EmBetEventEntity] = []

    deinit {
        activeListeners.removeAll()
    }

    /**
        Adds an observer with a callback block
     */
    func addEventObserver(observer: AnyObject, block: @escaping EmBetEvent) {
        let objectHash = ObjectIdentifier(observer).hashValue
        activeListeners.append(EmBetEventEntity(objectHash, block))
    }

    /**
        Removes an observer
     */
    func removeEventObserver(observer: AnyObject) {
        let objectHash = ObjectIdentifier(observer).hashValue
        activeListeners.removeAll(where: { $0.objectHash == objectHash })
    }
}
