//
//  LLFontImporter.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 14.3.22.
//

import UIKit

internal class LLFontImporter {
    func loadCustomFonts() {
        let bundles: [Bundle] = [EmbetResource.resourceBundle]

        for bundle in bundles {
            loadCustomFont(from: bundle)
        }
    }

    private func loadCustomFont(from bundle: Bundle) {
        var fontsUrls: [URL] = []

        if let tffFonts = bundle.urls(forResourcesWithExtension: ".ttf", subdirectory: nil) {
            fontsUrls.append(contentsOf: tffFonts)
        }

        if let otfFonts = bundle.urls(forResourcesWithExtension: ".otf", subdirectory: nil) {
            fontsUrls.append(contentsOf: otfFonts)
        }

        // Custom fonts are only loaded for the duration of the process
        if #available(iOS 13.0, *) {
            CTFontManagerRegisterFontURLs(fontsUrls as CFArray, .process, true) { errors, done -> Bool in

                if done {
                    log.info("Fonts registered")
                }

                let errs = errors as Array

                if !errs.isEmpty {
                    log.info("Error while registering SDK fonts")
                }

                return true
            }

        } else {
            var errors: Unmanaged<CFArray>?

            let success = CTFontManagerRegisterFontsForURLs(fontsUrls as CFArray, .process, &errors)

            if success == true {
                log.info("Fonts registered")
            }

            if errors != nil {
                log.info("Error while registering SDK fonts")
            }
        }
    }
}
