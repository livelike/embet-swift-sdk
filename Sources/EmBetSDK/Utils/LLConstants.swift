//
//  LLConstants.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 25.2.22.
//

import Foundation
import LiveLikeSwift

/**
    A protocol for managing storage of the user’s access token.
 */
public typealias EBAccessTokenStorage = AccessTokenStorage

internal enum LLConstants {
    /// Default time in secconds that determines the delay before the widget is dismissed, after
    /// the timer has elapsed.
    static let DefaultResultsShowingDuration: TimeInterval = 5

    /// Max number of character when overwriting the selectable option image with text.
    static let MaxNumberOfOptionLogoViewCharacters: Int = 3

    // MARK: - Show results animation constants

    /// Fade out of selectable options content
    static let OptionFadeOutDuration: TimeInterval = 1.0

    /// Fade out delay
    static let OptionFadeOutDelay: TimeInterval = 0.0

    /// Fade in of the result duration
    static let OptionFadeInDuration: TimeInterval = 0.5

    /// Fade in delay
    static let OptionFadeInDelay: TimeInterval = 0.0

    /// Delay the result bar fill
    static let BarAnimationDelay: TimeInterval = OptionFadeInDelay + 0.0

    /// Default page size
    static let DefaultPageSize: Int = 10
}
