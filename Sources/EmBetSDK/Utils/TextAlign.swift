//
//  File.swift
//
//
//  Created by Ljupco Nastevski on 19.2.24.
//

import Foundation
import UIKit.UILabel

internal enum TextAlign: String, Decodable {
    case left
    case right
    case center

    var asNSTextAligment: NSTextAlignment {
        switch self {
        case .left:
            return .left
        case .right:
            return .right
        case .center:
            return .center
        }
    }
}
