//
//  Permitted.swift
//
//
//  Created by Ljupco Nastevski on 15.12.23.
//

import Foundation

@propertyWrapper
internal struct Permitted<T: Hashable> {
    /**
        Dynamic wrapper
     */
    public var wrappedValue: Set<T> {
        get { return value }
        set { value = permittedValues.intersection(newValue) }
    }

    /**
        The current value
     */
    private var value: Set<T>
    /**
        Set of permitted values
     */
    private var permittedValues: Set<T>

    init(wrappedValue: Set<T>, elements: Set<T>) {
        permittedValues = elements
        value = permittedValues.intersection(wrappedValue)
    }
}
