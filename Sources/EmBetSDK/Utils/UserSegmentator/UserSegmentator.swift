//
//  UserSegmentator.swift
//
//
//  Created by Ljupco Nastevski on 20.7.22.
//

import Foundation
import jsonlogic

/**
    User segmentator is a class that uses jsonLogic to match rules against data.
 */
internal class UserSegmentator {
    /**
        The rule string that needs to be matched against the user data.
     */
    var rule: String?

    /**
        The data that the rule will be applied to.
     */
    var userData: String?

    /**
        Reference to the SDK object.
        Needed to grab properties on how the Segmentatior will work.
        eg: isSegementationEnabled.
     */
    private let internalSDK: EmBetSDKInternalProtocol

    /**
        Required UserSegmentation initializer.
     */
    required init(internalSDK: EmBetSDKInternalProtocol) {
        self.internalSDK = internalSDK
    }

    /**
        Matches the rule agains the data object and returns.
        - Returns: A boolean determening if the userData passed the rule check.
     */
    func applyRuleToData() -> Bool {
        /*
            Check if segmentation is turned off.
         */
        guard internalSDK.isSegmentationEnabled == true else { return true }

        /*
            If the rule is missing, return true.
         */
        guard let rule = rule else { return true }
        var parsedRule = rule
        do {
            parsedRule = try extractSegmentationData(customData: rule)
        } catch {
            if let error = error as? SegmentatorErrors {
                if error == .dataMissing {
                    return true
                }
            }

            return false
        }

        /*
            Lastly, if the user is missing, fail the check.
            - Check should not pass if user is missing, but a rule is in place !!!
         */
        guard let userData = userData,
              let parsedUserData = try? extractSegmentationData(customData: userData) else { return false }

        do {
            let result: Bool = try jsonlogic.applyRule(parsedRule, to: parsedUserData)
            return result
        } catch {
            log.error("Error parsing segmentation object: \(error) - \(String(describing: userData))")
        }

        return false
    }

    /**
        Extracts the rule from a Json string and applies it to the userData property.
        Current userData is allways replaced.
     */
    private func extractSegmentationData(customData: String?) throws -> String {
        do {
            return try parseAndApply(customData: customData)
        } catch {
            throw error
        }
    }

    /**
        Parses the custom data and aplies it to the referenced obj.
     */
    private func parseAndApply(customData: String?) throws -> String {
        let data = SegmentatorParser.parseData(data: customData)
        switch data {
        case let .success(parsedData):
            return parsedData
        case let .failure(error):
            throw error
        }
    }
}
