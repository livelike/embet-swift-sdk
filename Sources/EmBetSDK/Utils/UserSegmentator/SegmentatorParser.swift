//
//  SegmentatorParser.swift
//
//
//  Created by Ljupco Nastevski on 25.7.22.
//

import Foundation

/**
    Errors for the segmentator object.
 */
enum SegmentatorErrors: Error {
    /**
        Unable to parse the data.
     */
    case unableToParseData
    /**
        Data is incomplete/missing.
     */
    case dataMissing
}

/**
    Parser for the segmentation feature.
    Parses user data and extracts the value for the "segmentation" key.
    Also preps data with "segmentation: _VALUE_" format.
 */
internal enum SegmentatorParser {
    fileprivate static let SegmentationKey = "segmentation"

    /**
        Extracts the rule from a Json string and applies it to the rule property.
        Current rule is allways replaced.
     */
    static func parseData(data: String?) -> Result<String, Error> {
        let dataObjectResponse = SegmentatorParser.parseToDataObject(data: data)

        switch dataObjectResponse {
        case let .success(dataObject):

            do {
                var writingOptions: JSONSerialization.WritingOptions
                if #available(iOS 13, *) {
                    writingOptions = .withoutEscapingSlashes
                } else {
                    writingOptions = .prettyPrinted
                }

                let jsonData = try JSONSerialization.data(withJSONObject: dataObject, options: writingOptions)
                if let segmentationRule = String(data: jsonData, encoding: .utf8) {
                    return .success(segmentationRule)
                }
                return .failure(SegmentatorErrors.unableToParseData)

            } catch {
                return .failure(error)
            }

        case let .failure(error):
            return .failure(error)
        }
    }

    /**
        Extracts the rule from a Json string and applies it to the rule property.
        Current rule is allways replaced.
     */
    static func parseToDataObject(data: String?) -> Result<[String: Any], Error> {
        if let data = data?.data(using: .utf8) {
            do {
                guard let jsonString = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
                    return .failure(SegmentatorErrors.unableToParseData)
                }
                guard let segmentation = jsonString[SegmentationKey] as? [String: Any] else {
                    return .failure(SegmentatorErrors.dataMissing)
                }
                return .success(segmentation)
            } catch {
                return .failure(SegmentatorErrors.unableToParseData)
            }
        }

        return .failure(SegmentatorErrors.dataMissing)
    }

    /**
        Extracts the rule from a Json string and applies it to the rule property.
        Current rule is allways replaced.
     */
    static func packData(data: [String: Any]?) -> Result<String, Error> {
        guard let customData = data else { return .failure(SegmentatorErrors.dataMissing) }

        do {
            var jsonDict: [String: Any] = [:]
            jsonDict[SegmentationKey] = customData

            let jsonData = try JSONSerialization.data(withJSONObject: jsonDict, options: [])
            if let json = String(data: jsonData, encoding: .utf8) {
                return .success(json)
            }
            return .failure(SegmentatorErrors.unableToParseData)
        } catch {
            return .failure(error)
        }
    }
}
