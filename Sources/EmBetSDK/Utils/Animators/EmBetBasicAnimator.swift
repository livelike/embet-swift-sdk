//
//  EmBetBasicAnimator.swift
//
//
//  Created by Ljupco Nastevski on 7.3.24.
//

import UIKit

internal enum EmBetBasicAnimator {
    /**
        Plays the default blink animation on the views layer.
     */
    static func playBlinkAnimation(view: UIView) {
        let flash = CABasicAnimation(keyPath: "opacity")
        flash.duration = 1
        flash.fromValue = 0
        flash.toValue = 1
        flash.timingFunction = .init(name: .easeInEaseOut)
        flash.autoreverses = true
        flash.repeatCount = 3
        view.layer.add(flash, forKey: nil)
    }

    /**
        Removes all animations in a views layer.
     */
    static func removeAllAnimations(view: UIView) {
        view.layer.removeAllAnimations()
    }

    static func performWidgetAnimation(widgetView: UIView, animation: LLWidgetAnimation?) {
        widgetView.layer.opacity = 0
        DispatchQueue.main.async {
            performAnimation(widgetView: widgetView, animation: animation)
        }
    }

    private static func performAnimation(widgetView: UIView, animation: LLWidgetAnimation?) {
        switch animation {
        case let .slide(slideAnimation):

            let durationToSec: Double = .init(slideAnimation.duration) / 1000

            var startPosition: CGFloat
            var keyPath: String

            switch slideAnimation.props.origin {
            case .bottom:
                startPosition = widgetView.bounds.height
                keyPath = "transform.translation.y"
            case .top:
                startPosition = -widgetView.bounds.height
                keyPath = "transform.translation.y"
            case .left:
                startPosition = -widgetView.bounds.width
                keyPath = "transform.translation.x"
            case .right:
                startPosition = widgetView.bounds.width
                keyPath = "transform.translation.x"
            }

            CATransaction.begin()

            // Adds a short opacity animation paired with the translation
            // to make the widget entrance animation smoother
            let animationOpacity = CABasicAnimation(keyPath: "opacity")
            animationOpacity.fromValue = 0
            animationOpacity.toValue = 1
            animationOpacity.duration = 0.1
            animationOpacity.timingFunction = CAMediaTimingFunction(name: .linear)
            widgetView.layer.add(animationOpacity, forKey: "opacity")

            let animation = CABasicAnimation(keyPath: keyPath)
            animation.fromValue = startPosition
            animation.duration = durationToSec
            animation.timingFunction = CAMediaTimingFunction(name: .linear)
            widgetView.layer.add(animation, forKey: keyPath)

            CATransaction.setCompletionBlock {
                widgetView.layer.opacity = 1
            }

            CATransaction.commit()
        case let .fade(fadeAnimation):

            CATransaction.begin()

            let durationToSec: Double = .init(fadeAnimation.duration) / 1000
            let animationOpacity = CABasicAnimation(keyPath: "opacity")
            animationOpacity.fromValue = 0
            animationOpacity.toValue = 1
            animationOpacity.duration = durationToSec
            animationOpacity.timingFunction = CAMediaTimingFunction(name: .linear)
            widgetView.layer.add(animationOpacity, forKey: "opacity")

            CATransaction.setCompletionBlock {
                widgetView.layer.opacity = 1
            }

            CATransaction.commit()

        default:
            widgetView.layer.opacity = 1
        }
    }

    private static func performPredictionSelectionInitial(view: UIView,
                                                          fromPosition: CGPoint,
                                                          toPosition: CGPoint,
                                                          completion: (() -> Void)? = nil)
    {
        CATransaction.begin()

        let translateX = CABasicAnimation(keyPath: "position")
        translateX.fromValue = [fromPosition.x, fromPosition.y]
        translateX.toValue = [toPosition.x, fromPosition.y]
        translateX.duration = 1.0
        translateX.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)

        CATransaction.setCompletionBlock {
            completion?()
        }

        view.layer.add(translateX, forKey: "positionX")
        CATransaction.commit()
    }

    public static func performPredictionSelectionFinal(view: UIView, complementaryView: UIView, fromPosition: CGPoint, toPosition: CGPoint, completion: (() -> Void)? = nil) {
        CATransaction.begin()

        let translateY = CABasicAnimation(keyPath: "position")
        translateY.duration = 1.0
        translateY.fromValue = [toPosition.x, fromPosition.y]
        translateY.toValue = [toPosition.x, toPosition.y]
        translateY.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)

        let revealAnimation = CABasicAnimation(keyPath: "opacity")
        revealAnimation.duration = 1.0
        revealAnimation.fromValue = 0
        revealAnimation.toValue = 1
        revealAnimation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)

        CATransaction.setCompletionBlock {
            complementaryView.alpha = 1
            completion?()
        }

        view.layer.add(translateY, forKey: "positionY")
        complementaryView.layer.add(revealAnimation, forKey: "opacity")

        CATransaction.commit()
    }

    public static func performPredictionSelection(view: UIView, complementaryView: UIView, fromPosition: CGPoint, toPosition: CGPoint, completion: (() -> Void)? = nil) {
        CATransaction.begin()

        let translateY = CAKeyframeAnimation(keyPath: "position")
        translateY.duration = 2.0
        translateY.values = [
            [fromPosition.x, fromPosition.y],
            [toPosition.x, fromPosition.y],
            [toPosition.x, toPosition.y],
        ]
        translateY.keyTimes = [0, 0.6, 1.0]
        translateY.timingFunction = CAMediaTimingFunction(name: .linear)

        let revealAnimation = CABasicAnimation(keyPath: "opacity")
        revealAnimation.duration = 1.0
        revealAnimation.beginTime = CACurrentMediaTime() + 1
        revealAnimation.fromValue = 0
        revealAnimation.toValue = 1
        revealAnimation.timingFunction = CAMediaTimingFunction(name: .linear)

        CATransaction.setCompletionBlock {
            complementaryView.alpha = 1
            completion?()
        }

        view.layer.add(translateY, forKey: "positionY")
        complementaryView.layer.add(revealAnimation, forKey: "opacity")

        CATransaction.commit()
    }
}
