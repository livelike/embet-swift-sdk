//
//  EmBetChannelListeners.swift
//
//
//  Created by Ljupco Nastevski on 23.1.24.
//

import Foundation
import LiveLikeCore
import PubNub

/// A thread safe class for managing channel listeners.
///
/// A strong reference is maintained for each listener. Therefore for each
/// ```
/// addListener(_ listener: WidgetProxyInput, forChannel channel: String)
/// ```
/// a corresponding
/// ```
/// removeListener(_ listener: WidgetProxyInput, forChannel channel: String)
/// ```
/// is required, otherwise a memory leak could occur
class EmBetChannelListeners {
    private var channelListeners = [String: Listener<EmbetWidgetProxyInput>]()
    private let synchronizingQueue = DispatchQueue(label: "com.livelike.embet.clientListenerSynchronizer", attributes: .concurrent)

    /**
        Adds a listener to a channel.
     */
    func addListener(_ listener: EmbetWidgetProxyInput, forChannel channel: String) {
        synchronizingQueue.async(flags: .barrier) { [weak self] in
            guard let self = self else { return }
            if self.channelListeners[channel] == nil {
                self.channelListeners[channel] = Listener()
            }
            self.channelListeners[channel]?.addListener(listener)
        }
    }

    func removeListener(_ listener: EmbetWidgetProxyInput, forChannel channel: String) {
        synchronizingQueue.async(flags: .barrier) { [weak self] in
            guard let self = self else { return }
            self.channelListeners[channel]?.removeListener(listener)
        }
    }

    /**
        Checks if the channel has no listerners
        - Parameters:
            - channel: The name of the channel
        - Returns: A value that determines if the channel has no listeners
     */
    func isEmpty(forChannel channel: String) -> Bool {
        var isEmpty = true
        synchronizingQueue.sync { [weak self] in
            guard let self = self else { return }
            if let listeners = self.channelListeners[channel] {
                isEmpty = listeners.isEmpty()
            }
        }
        return isEmpty
    }

    /**
        Removes all listeners.
     */
    func removeAll() {
        synchronizingQueue.async(flags: .barrier) { [weak self] in
            self?.channelListeners.removeAll()
        }
    }

    func publish(channel: String?, _ invocation: (EmbetWidgetProxyInput) -> Void) {
        synchronizingQueue.sync { [weak self] in
            guard let self = self else { return }
            if let channel = channel {
                guard let inputs = self.channelListeners[channel] else { return }
                inputs.publish(invocation)
            } else {
                channelListeners.values.forEach { $0.publish(invocation) }
            }
        }
    }
}
