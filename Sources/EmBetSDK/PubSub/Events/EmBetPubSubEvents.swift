//
//  EmBetPubSubEvents.swift
//
//
//  Created by Ljupco Nastevski on 23.1.24.
//

import Foundation

internal enum EmBetPubSubEvents: Decodable {
    /// Enum outlining the valid event names we can receive from
    /// the messaging service (e.g. PubNub)
    enum EventName: String, Codable {
        // Widget Events
        case oddsUpdate = "odds_update"
        case widgetUpdate = "widget_update"
    }

    enum CodingKeys: CodingKey {
        case event
        case payload
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let event = try container.decode(EventName.self, forKey: .event)

        switch event {
        case .oddsUpdate:
            self = try .oddsUpdate(container.decode(EmBetPubSubOddsUpdateModel.self, forKey: .payload))
        case .widgetUpdate:
            self = try .widgetUpdate(container.decode(EmBetPubSubWidgetUpdateModel.self, forKey: .payload))
        }
    }

    case oddsUpdate(EmBetPubSubOddsUpdateModel)
    case widgetUpdate(EmBetPubSubWidgetUpdateModel)
}
