//
//  PubSubClient.swift
//
//
//  Created by Ljupco Nastevski on 9.1.24.
//

import Foundation
import LiveLikeCore
import PubNub

/// Reasons a `WidgetView` won't get displayed
enum DiscardedReason {
    // published during a paused state.
    case paused
    // published too far in the past.
    case invalidPublishDate
    // loading remote images or animations failed.
    case invalidResources
    // no recorded vote for follow up widgets.
    case noVote
}

typealias EmBetWidgetProxy = EmbetWidgetProxyInput & EmBetWidgetProxyOutput

struct WidgetProxyPublishData {
    var clientEvent: EmBetPubSubEvents
}

protocol EmbetWidgetProxyInput: AnyObject {
    func publish(event: WidgetProxyPublishData)
    func discard(event: WidgetProxyPublishData, reason: DiscardedReason)
    func connectionStatusDidChange(_ status: ConnectionStatus)
    func error(_ error: Error)
}

protocol EmBetWidgetProxyOutput: AnyObject {
    var downStreamProxyInput: EmbetWidgetProxyInput? { get set }
}

extension EmbetWidgetProxyInput where Self: EmBetWidgetProxyOutput {
    func error(_ error: Error) {
        downStreamProxyInput?.error(error)
    }

    func discard(event: WidgetProxyPublishData, reason: DiscardedReason) {
        downStreamProxyInput?.discard(event: event, reason: reason)
    }

    func connectionStatusDidChange(_ status: ConnectionStatus) {
        downStreamProxyInput?.connectionStatusDidChange(status)
    }
}

extension EmBetWidgetProxyOutput {
    func addProxy(_ proxy: () -> EmBetWidgetProxy) -> EmBetWidgetProxy {
        let input = proxy()
        downStreamProxyInput = input
        return input
    }
}

//=============
protocol PubSubWidgetClient: AnyObject {
    /// An instance `ChannelListeners` to manage channel listeners
    var widgetListeners: EmBetChannelListeners { get }
    /// Add a listener for a specific channel
    ///
    /// - Parameters:
    ///   - listener: the `WidgetProxyInput` that will receive `channel` events
    ///   - channel: channel name on which client should try to subscribe
    func addListener(_ listener: EmbetWidgetProxyInput, toChannel channel: String)

    /// Remove a listener for a specific channel
    ///
    /// - Parameters:
    ///   - listener: the `WidgetProxyInput` that will receive `channel` events
    ///   - channel: channel name on which client should try to subscribe
    func removeListener(_ listener: EmbetWidgetProxyInput, fromChannel channel: String)

    func unsubscribe(fromChannel channel: String)

    /// Unsubscribe from all channels
    func removeAllListeners()
}

/// Concrete implementation of `WidgetMessagingClient`
/// based on [PubNub](https://www.pubnub.com/docs/ios-objective-c/pubnub-objective-c-sdk)
class EmBetInternalPubSubWidgetClient: NSObject, PubSubWidgetClient {
    /// Internal
    var widgetListeners = EmBetChannelListeners()

    /// Private
    private let subscribeKey: String
    private let origin: String?
    private let userID: String
    private let heartbeatInterval: Int
    private let heartbeatTimeout: Int

    private let listener = SubscriptionListener(
        queue: DispatchQueue(label: "com.livelike.embet.pubnub")
    )

    private lazy var client: PubNub = {
        log.info("PubNub is being initialized with userID '\(userID)' and subscribeKey '\(subscribeKey)'.")
        var config = PubNubConfiguration(
            publishKey: nil,
            subscribeKey: self.subscribeKey,
            userId: userID
        )
        if let origin = origin {
            config.origin = origin
        }
        // Configure Presence heartbeat
        config.heartbeatInterval = UInt(heartbeatInterval)
        config.durationUntilTimeout = UInt(heartbeatTimeout)
        config.automaticRetry = AutomaticRetry(
            retryLimit: 10,
            policy: .exponential(minDelay: 3, maxDelay: 60)
        )

        let pubnub = PubNub(configuration: config)

        listener.didReceiveMessage = { [weak self] message in
            guard let self = self else { return }
            do {
                let clientEvent = try self.processMessage(message: message)
                self.widgetListeners.publish(channel: message.channel) {
                    $0.publish(event: WidgetProxyPublishData(clientEvent: clientEvent))
                }
            } catch {
                log.error(error)
                self.widgetListeners.publish(channel: message.channel) { $0.error(error) }
            }
        }

        listener.didReceiveStatus = { (status: SubscriptionListener.StatusEvent) in
            log.info("[PubNub] Status: \(status)")
        }

        pubnub.add(listener)
        return pubnub
    }()

    init(
        subscribeKey: String,
        origin: String?,
        userID: String,
        heartbeatTimeout: Int,
        heartbeatInterval: Int
    ) {
        self.subscribeKey = subscribeKey
        self.origin = origin
        self.userID = userID
        self.heartbeatTimeout = heartbeatTimeout
        self.heartbeatInterval = heartbeatInterval
        super.init()
    }

    func addListener(_ listener: EmbetWidgetProxyInput, toChannel channel: String) {
        widgetListeners.addListener(listener, forChannel: channel)
        client.subscribe(to: [channel], withPresence: false)
        log.debug("[PubNub] Connected to PubNub channel. Connected to \(client.subscriptionCount) channels.")
    }

    func removeListener(_ listener: EmbetWidgetProxyInput, fromChannel channel: String) {
        widgetListeners.removeListener(listener, forChannel: channel)
        unsubscribe(fromChannel: channel)
    }

    /// Unsubscribes from a channel if there are no more listeners
    func unsubscribe(fromChannel channel: String) {
        if widgetListeners.isEmpty(forChannel: channel) {
            client.unsubscribe(from: [channel])
            log.debug("[PubNub] Disconnected from PubNub channel. Connected to \(client.subscriptionCount) channels.")
        }
    }

    func removeAllListeners() {
        widgetListeners.removeAll()
        client.unsubscribeAll()
    }
}

private extension EmBetInternalPubSubWidgetClient {
    /// We are expecting all messages to have the following schema
    ///
    /// ```
    ///    {
    ///      event = "the_event_name" // predefined `EventName`
    ///      payload = {
    ///       ...
    ///      }
    ///    }
    /// ```
    func processMessage(message: PubNubMessage) throws -> EmBetPubSubEvents {
        // Updates are sent as array with one element
        guard let message = (message.payload as? AnyJSON)?.arrayOptional?.first else {
            throw MessagingClientError.invalidEvent(event: "The message is empty")
        }

        let jsonData = try JSONSerialization.data(withJSONObject: message, options: .prettyPrinted)
        let decoder = LLJSONDecoder()

        return try decoder.decode(EmBetPubSubEvents.self, from: jsonData)
    }
}
