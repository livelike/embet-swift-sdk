//
//  EmBetPubSubWidgetUpdateModel.swift
//
//
//  Created by Ljupco Nastevski on 23.1.24.
//

import Foundation

/**
    Object that updates the widget attributes on specific widgets
 */
internal class EmBetPubSubWidgetUpdateModel: Decodable {
    /**
        List of widget IDs
     */
    let widgetIds: [String]
    /**
        List of widgets attributes.
     */
    let widgetAttributes: [EmBetWidgetAttribute]
}
