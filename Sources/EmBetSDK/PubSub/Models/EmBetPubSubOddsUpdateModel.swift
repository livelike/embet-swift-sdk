//
//  EmBetPubSubOddsUpdateModel.swift
//
//
//  Created by Ljupco Nastevski on 23.1.24.
//

import Foundation

/**
    Object that updates the Odds widgets data.
 */
internal class EmBetPubSubOddsUpdateModel: Decodable {
    /**
        List of widgets IDs
     */
    let widgetIds: [String]
    /**
        The final result of the match
     */
    let finalResult: LLFinalResultModel?
    /**
        List of bet details models for the Odds Widget
     */
    let betDetails: [LLOddsBetDetailsModel]
}
