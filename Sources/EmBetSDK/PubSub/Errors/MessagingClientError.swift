//
//  MessagingClientError.swift
//
//
//  Created by Ljupco Nastevski on 5.2.24.
//

import Foundation

enum MessagingClientError: Error, LocalizedError {
    case invalidEvent(event: String)
    var errorDescription: String? {
        switch self {
        case let .invalidEvent(event):
            return event
        }
    }
}
