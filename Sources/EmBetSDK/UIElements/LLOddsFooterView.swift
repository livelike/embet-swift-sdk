//
//  LLOddsFooterView.swift
//
//
//  Created by Ljupco Nastevski on 13.11.23.
//

import UIKit

internal class LLOddsFooterView: LLDisclaimerView {
    #if os(tvOS)
        override var canBecomeFocused: Bool {
            return true
        }

        let focusLayer: CALayer = .init()

        override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
            if context.nextFocusedView == self {
                coordinator.addCoordinatedAnimations({ () in
                    self.focusLayer.backgroundColor = LLUIConstants.FocusColor.cgColor
                }, completion: nil)

            } else if context.previouslyFocusedView == self {
                // Collapse the disclaimer on leaving focus
                if chevronButton.isSelected {
                    didPressExpandButton()
                }
                coordinator.addCoordinatedAnimations({ () in
                    self.focusLayer.backgroundColor = UIColor.clear.cgColor
                }, completion: nil)
            }
        }
    #endif
    /**
        Expand/contract button
     */
    private lazy var chevronButton: UIButton = {
        let button = LLThemableDismissButton.makeChevronButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(didPressExpandButton), for: .primaryActionTriggered)
        return button
    }()

    var didPressExpandHandler: ((_ shouldExpand: Bool) -> Void)?

    override func layoutSubviews() {
        super.layoutSubviews()
        // Change size of focus layer acordingly
        // Animations are dissabled
        #if os(tvOS)
            CATransaction.begin()
            CATransaction.setDisableActions(true)
            focusLayer.frame = layer.bounds
            CATransaction.commit()
        #endif
    }

    override func commonInit() {
        #if os(tvOS)
            focusLayer.frame = layer.bounds
            layer.addSublayer(focusLayer)
        #endif
        super.commonInit()
        // Adds button to the view if both descriptions are different
        if descriptionText != descriptionCollapsedText,
           descriptionText != nil, descriptionCollapsedText != nil
        {
            addChevronButton()
            // Add focus layer for tvOS
            #if os(tvOS)
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didPressExpandButton))
                addGestureRecognizer(tapGesture)
            #endif
        }
    }

    private func addChevronButton() {
        addSubview(chevronButton)
        // Lays out constraints
        NSLayoutConstraint.activate([
            chevronButton.trailingAnchor.constraint(equalTo: layoutMarginsGuide.trailingAnchor),
            chevronButton.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor),
            chevronButton.heightAnchor.constraint(equalToConstant: 16.0),
            chevronButton.widthAnchor.constraint(equalTo: chevronButton.heightAnchor),
        ])
    }

    @objc
    private func didPressExpandButton() {
        let chevronButtonSelected = !chevronButton.isSelected
        chevronButton.isSelected = chevronButtonSelected

        if chevronButtonSelected {
            chevronButton.setImage(.embetIconWithName(iconName: .chevronDown), for: .normal)
        } else {
            chevronButton.setImage(.embetIconWithName(iconName: .chevronUp), for: .normal)
        }

        // If the button is marked as selected, shouldExpand will be true
        setupSelfFor(chevronButtonSelected: chevronButtonSelected)

        didPressExpandHandler?(chevronButton.isSelected)
    }

    /**
        Setup diferently for selected or unselected chevron button
     */
    public func setupSelfFor(chevronButtonSelected: Bool) {
        if chevronButtonSelected == false {
            willCollapse()
        } else {
            willExpand()
        }
    }
}
