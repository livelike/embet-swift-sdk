//
//  LLCountdownLabel.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 9.3.22.
//

import UIKit

/**
    LLThemable label subclass.
 */
internal class LLCountdownLabel: LLThemableLabel {
    /**
        The property to determine when the timer was started.
        When a user puts the app in the background, timers are not reliable
        so a continuation mechanism is needed.
     */
    private var timerStartDate: Date?

    private weak var timer: Timer?

    private var themeType: LLThemableType

    var duration: TimeInterval = 0

    private var secondsRemaining: TimeInterval = 0

    deinit {
        self.timer?.invalidate()
    }

    init(duration: TimeInterval, themeType: LLThemableType) {
        self.duration = duration
        self.themeType = themeType
        secondsRemaining = self.duration
        super.init(themeType: self.themeType)
        initialSetup()
    }

    override func applyTheme(theme: LLThemeViewModel?) {
        super.applyTheme(theme: theme)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetup()
    }

    private func initialSetup() {
        text = getSecondsRemainingString()

        textAlignment = .center
        translatesAutoresizingMaskIntoConstraints = false
        adjustsFontSizeToFitWidth = true

        NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification, object: nil, queue: .main) { [weak self] _ in
            guard let self = self else { return }
            self.secondsRemaining = self.duration + (self.timerStartDate?.timeIntervalSinceNow ?? 0.0)
            self.updateCountdownLabel()
            self.startCountdownTimer()
        }

        NotificationCenter.default.addObserver(forName: UIApplication.didEnterBackgroundNotification, object: nil, queue: .main) { [weak self] _ in
            self?.timer?.invalidate()
        }
    }

    /**
        Invalidates the active timer, if present.
     */
    func invalidateCountdownTimer() {
        timer?.invalidate()
    }

    /**
        Starts the countdown timer immediately.
        Invalidate previuos timers.
        If there already is a started timer, the function returns.
     */
    func startCountdownTimer() {
        invalidateCountdownTimer()

        if timerStartDate == nil {
            timerStartDate = Date()
        }

        guard timer == nil else { return }

        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { [weak self] _ in

            guard let self = self else { return }
            self.secondsRemaining -= 1
            self.updateCountdownLabel()

        })

        timer?.tolerance = 0
    }

    private func updateCountdownLabel() {
        if secondsRemaining <= 0 {
            timer?.invalidate()
            DispatchQueue.main.async { [weak self] in
                // After timer reaches "0", remove text
                self?.text = " "
            }
            return
        }

        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.text = self.getSecondsRemainingString()
        }
    }

    private func getSecondsRemainingString() -> String {
        return secondsRemaining < 100 ? "\(Int(secondsRemaining))" : " "
    }
}
