//
//  LLAsyncImageView.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 1.3.22.
//

import UIKit

/**
    A UIImageView subclass, for asynchronous loading of images.
 */
internal class LLAsyncImageView: UIImageView {
    /**
        The active URLSessionTask.
        Reference is hold for canceling.
     */
    private weak var task: URLSessionTask?

    override init(frame: CGRect) {
        super.init(frame: frame)
        contentMode = .scaleAspectFit
        setContentHuggingPriority(.defaultHigh, for: .horizontal)
    }

    init() {
        super.init(frame: .zero)
        contentMode = .scaleAspectFit
        setContentHuggingPriority(.defaultHigh, for: .horizontal)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /**
        Starts loading the image data async.
     */
    func loadAsyncImage(string: String?) {
        guard let string = string,
              let url = URL(string: string) else { return }

        loadAsyncImage(url: url)
    }

    /**
        Starts loading the image data async.
     */
    func loadAsyncImage(url: URL?) {
        guard let url = url else { return }

        task = URLSession.shared.dataTask(with: url) { [weak self] data, _, _ in

            if let data = data, let image = UIImage(data: data) {
                DispatchQueue.main.async {
                    self?.image = image
                }
            }
        }

        task?.resume()
    }

    /**
        Cancels the active URLSessionTask if present.
        Always called right before `loadAsyncImage`.
     */
    func cancelPreviousTask() {
        task?.cancel()
    }

    /*
        Recalculating the intrinsicContentSize. scaleAspectFit does not work with dynamic caclulations as the
        intristic size depends on image.size.
     */
    override var intrinsicContentSize: CGSize {
        if contentMode == .scaleAspectFit {
            guard let image = image else { return frame.size.toSixteenByNinePreservingWidth() }
            return image.size.embedInSizePreservingWidth(parentSize: frame.size)
        }

        return super.intrinsicContentSize
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        invalidateIntrinsicContentSize()
    }
}
