//
//  LLDescriptiveMarketOptionView.swift
//
//
//  Created by Ljupco Nastevski on 21.10.24.
//

import UIKit

class LLDescriptiveMarketOptionView: UIView {
    /**
        Imageview that shows the options image
     */
    private let optionImageView: LLAsyncImageView = {
        let imageView = LLAsyncImageView(frame: .zero)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor).isActive = true
        return imageView
    }()

    /**
        Imageview that shows the options image
     */
    private lazy var optionImageHolder: UIView = {
        let view = UIView(frame: .zero)
        view.addSubview(optionImageView)
        NSLayoutConstraint.activate([
            view.leadingAnchor.constraint(equalTo: optionImageView.leadingAnchor),
            view.trailingAnchor.constraint(equalTo: optionImageView.trailingAnchor),
            view.topAnchor.constraint(equalTo: optionImageView.topAnchor, constant: -6),
            view.bottomAnchor.constraint(equalTo: optionImageView.bottomAnchor, constant: 6),
        ])
        return view
    }()

    /**
        Lock icon to the left side of the market odds, shown when state is correct or incorrect
     */
    private let marketSuspendedImageView: UIImageView = {
        let imageView = LLThemableImageView(type: .lockIcon)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.image = .embetIconWithName(iconName: .lock)
        imageView.widthAnchor.constraint(equalToConstant: 20.0).isActive = true
        imageView.alpha = 0
        return imageView
    }()

    /**
        The content stack view
     */
    private let contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = 8
        return stackView
    }()

    /**
        Label that shows the options name
     */
    private lazy var optionNameLabel: LLThemableLabel = .init(themeType: .optionRowVertical)

    /**
        The market details model
     */
    private var market: LLMarketOptionModel

    /**
        Bool that indicates if the oods should be suspended
     */
    var forceSuspension: Bool = false

    /**
        The left option view for this market, that will present the odds
     */
    public let optionView: LLMarketOptionView = .init()

    /**
        BetDetails model
     */
    var betDetails: LLOddsBetDetailsDiffModel

    /**
        Focus guide is needed because the selectable option
        is at the right edge of the screen, so if above/below elemet has focus
        on the left side, the focus guide will catch it
     */
    #if os(tvOS)
        private let focusGuide = UIFocusGuide()
    #endif
    /**
        Required init.
     */
    required init(market: LLMarketOptionModel,
                  betDetails: LLOddsBetDetailsDiffModel,
                  forceSuspernsion: Bool)
    {
        self.betDetails = betDetails
        self.market = market
        forceSuspension = forceSuspernsion
        super.init(frame: .zero)
        commonInit()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /**
        Sets up the view
     */
    private func commonInit() {
        optionNameLabel.text = market.description

        // If imageUrl is present, show the market, if it is nil, market should be hidden
        if market.imageURL == nil {
            optionImageHolder.isHidden = true
        } else {
            optionImageView.loadAsyncImage(url: market.imageURL)
        }

        optionView.shouldHideDescription = true
        optionView.marketID = betDetails.marketID
        optionView.forceSuspension = forceSuspension
        optionView.setMarketOptions(current: market, previous: nil)

        let infoStackView = UIStackView(arrangedSubviews: [optionImageHolder, optionNameLabel])
        infoStackView.spacing = 8

        optionView.translatesAutoresizingMaskIntoConstraints = false

        contentStackView.addArrangedSubview(infoStackView)
        contentStackView.addArrangedSubview(marketSuspendedImageView)
        contentStackView.addArrangedSubview(optionView)
        addSubview(contentStackView)

        NSLayoutConstraint.activate([
            contentStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            contentStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            contentStackView.topAnchor.constraint(equalTo: topAnchor),
            contentStackView.bottomAnchor.constraint(equalTo: bottomAnchor),

            optionView.widthAnchor.constraint(equalToConstant: 72.0),
            optionView.heightAnchor.constraint(equalToConstant: 50.0),
        ])

        #if os(tvOS)
            addLayoutGuide(focusGuide)
            NSLayoutConstraint.activate([
                focusGuide.rightAnchor.constraint(equalTo: optionView.leftAnchor),
                focusGuide.topAnchor.constraint(equalTo: optionView.topAnchor),
                focusGuide.bottomAnchor.constraint(equalTo: optionView.bottomAnchor),
                focusGuide.leftAnchor.constraint(equalTo: leftAnchor),
            ])
        #endif
        // Shows the lock image if conditions are met
        if market.state == .correct || market.state == .incorrect, forceSuspension == false {
            if marketSuspendedImageView.alpha == 0 {
                marketSuspendedImageView.alpha = 1
            }
        }
    }

    public func setModel(market: LLMarketOptionModel,
                         betDetails: LLOddsBetDetailsDiffModel)
    {
        if market.state == .correct || market.state == .incorrect {
            if marketSuspendedImageView.alpha == 0 {
                marketSuspendedImageView.alpha = 1
            }
        } else {
            if marketSuspendedImageView.alpha == 1 {
                marketSuspendedImageView.alpha = 0
            }
        }

        if market.description != self.market.description {
            optionNameLabel.text = market.description
        }

        // If imageUrl is present, show the market, if it is nil, market should be hidden
        if market.imageURL == nil, optionImageHolder.isHidden == false {
            optionImageHolder.isHidden = true
        } else if market.imageURL != nil, optionImageHolder.isHidden == true {
            optionImageHolder.isHidden = false
        }

        if market.imageURL != self.market.imageURL {
            optionImageView.loadAsyncImage(url: market.imageURL)
        }

        optionView.marketID = betDetails.marketID
        optionView.forceSuspension = forceSuspension
        optionView.setMarketOptions(current: market, previous: betDetails.previousOptions?.first(where: { $0.outcomeID == market.outcomeID }))
        self.market = market
        self.betDetails = betDetails
    }
}
