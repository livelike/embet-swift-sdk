//
//  LLOddsBodyView.swift
//
//
//  Created by Ljupco Nastevski on 13.11.23.
//

import UIKit

internal protocol LLOddsBodyViewSelectionDelegate: AnyObject {
    func oddsTableViewDidSelect(selection: LLOddsSelection)
    func oddsTableViewDidDeselect(selection: LLOddsSelection)
}

internal class LLOddsBodyView: LLThemableView {
    public weak var delegate: LLOddsBodyViewSelectionDelegate?

    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [tableView])
        stackView.insetsLayoutMarginsFromSafeArea = false
        stackView.spacing = 8
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.setContentHuggingPriority(.defaultLow, for: .vertical)

        return stackView
    }()

    /**
        The tableview that lists the markets
     */
    private let tableView: LLTableView = {
        let tableView = LLTableView()
        tableView.bounces = false
        tableView.backgroundColor = .clear
        tableView.setContentHuggingPriority(.defaultLow, for: .vertical)
        tableView.showsVerticalScrollIndicator = false
        tableView.heightAnchor.constraint(greaterThanOrEqualToConstant: 160).isActive = true
        return tableView
    }()

    /**
        Results description label that is shown with the closing odds
     */
    private let gameOddslabel: LLThemableLabel = {
        let label = LLThemableLabel(themeType: .title)
        label.numberOfLines = 0
        return label
    }()

    /**
        Results description label that is shown with the closing odds
     */
    private let resultDescriptionLabel: LLThemableLabel = {
        let label = LLThemableLabel(themeType: .resultDescription)
        label.numberOfLines = 0
        return label
    }()

    /**
        The teams option model
     */
    private var teams: [LLWidgetImageDetailsModel]?

    /**
        The bet details model, that holds the market odds data
     */
    private var betDetails: [LLOddsBetDetailsDiffModel] = []
    /**
        The theme builder object responsible for applying the theme
     */
    private let themeBuilder: LLThemeBuilder

    /**
        The widgets labels object.
     */
    private let widgetLabels: LLWidgetLabels?

    /**
        Ref to the table header view
     */
    private weak var tableHeader: LLOddsBodyInfoView?

    /**
        Bool that indicates if the oods should be suspended
     */
    var forceSuspension: Bool = false

    var selectionCoordinator = LLBaseSelectableViewCoordinator()

    /**
        The active selection object that holds the currentluy selected data.
     */
    public var activeSelection: LLOddsSelection?

    private var placeBetURL: URL?

    required init(themeBuilder: LLThemeBuilder,
                  teams: [LLWidgetImageDetailsModel]?,
                  widgetLabels: LLWidgetLabels?,
                  placeBetURL: URL?)
    {
        self.teams = teams
        self.themeBuilder = themeBuilder
        self.widgetLabels = widgetLabels
        self.placeBetURL = placeBetURL
        selectionCoordinator.themeBulder = self.themeBuilder
        super.init(themeType: .body)
        commonInit()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /**
        Extra init setup
     */
    private func commonInit() {
        #if os(tvOS)
            if let information = widgetLabels?.information {
                gameOddslabel.text = information
                contentStackView.insertArrangedSubview(gameOddslabel, at: 0)
            }
        #endif
        // Setup subviews
        addSubview(contentStackView)
        // Lays out constraints
        NSLayoutConstraint.activate([
            contentStackView.topAnchor.constraint(equalTo: topAnchor),
            contentStackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            contentStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            contentStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
        ])
        // Adds separator at top
        addTableHeader()
        recreateTableView()
    }

    /**
        Sets the bet details
     */
    public func setBetDetails(betDetails: [LLOddsBetDetailsModel]?) {
        var details: [LLOddsBetDetailsDiffModel] = self.betDetails

        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }

            if let betDetails = betDetails {
                var index = 0
                for element in betDetails {
                    var didFoundPrevElement = false
                    // Rearange previous elements
                    for (prevIndex, prevElement) in details.enumerated() {
                        // Checks if the models are the same
                        if prevElement.isDifferent(detailsModel: element) == false {
                            // If variation is different, recreate the view
                            if prevElement.marketPresentation != element.marketPresentation {
                                break
                            }
                            // Checks if index are different, and moves item to
                            // proper index
                            if index != prevIndex {
                                details.insert(prevElement, at: index)
                                details.remove(at: prevIndex + 1)
                                self.tableView.moveView(from: prevIndex, to: index)
                            }
                            // Updates the existing element, if required
                            if prevElement.isRefreshRequired(detailsModel: element) {
                                let updatedElement = LLOddsBetDetailsDiffModel(marketName: element.marketName, marketID: element.marketID, marketPresentation: element.marketPresentation, currentOptions: element.marketOptions, previousOptions: prevElement.currentOptions)
                                details.remove(at: index)
                                details.insert(updatedElement, at: index)
                                // Update the UI
                                let cell = self.tableView.view(for: index) as? LLOddsTableCell
                                cell?.setModel(model: updatedElement)
                            }

                            didFoundPrevElement = true
                            break
                        }
                    }

                    // This is a new element, should be inseted at proper index
                    if didFoundPrevElement == false {
                        let detail = LLOddsBetDetailsDiffModel(marketName: element.marketName, marketID: element.marketID, marketPresentation: element.marketPresentation, currentOptions: element.marketOptions, previousOptions: nil)
                        details.insert(detail, at: index)
                        self.tableView.insert(view: self.getMarketView(betDetail: detail), at: index)
                    }

                    index += 1
                }

                // Cut the tail, remove extra elements that were not found
                // in the new array
                while betDetails.count < details.count {
                    details.removeLast()
                    self.tableView.removeLast()
                }
            }

            self.betDetails = details
        }
    }

    private func getMarketView(betDetail: LLOddsBetDetailsDiffModel) -> UIView {
        switch betDetail.marketPresentation {
        case .horizontal:
            let cell = LLHorizontalOddsTableCell(betDetails: betDetail,
                                                 forceSuspension: forceSuspension)
            cell.selectionCoordinator = selectionCoordinator
            cell.heightAnchor.constraint(equalToConstant: 66.0).isActive = true
            cell.delegate = self
            themeBuilder.applyThemeTo(superview: cell)
            return cell

        case .vertical:
            let cell = LLVerticalOddsTableCell(betDetails: betDetail,
                                               selectionCoordinator: selectionCoordinator,
                                               forceSuspension: forceSuspension)
            cell.delegate = self
            themeBuilder.applyThemeTo(superview: cell)
            return cell
        }
    }

    /**
        Recreates the table view cells, manages selection - prepares the table view data.
     */
    private func recreateTableView(forceSuspension: Bool = false) {
        // If suspension on the widget is forced, make deselection
        if forceSuspension == true {
            selectionCoordinator.deselectAll()
        }
        // Clear selection coordinator selection
        let selectedID = selectionCoordinator.getSelected?.uniqueID
        selectionCoordinator.views = []
        // Removing all the cells so we can repopulate the list as we are working with static cells
        var cells: [LLOddsTableCell] = []

        // Create new cells
        for betDetail in betDetails {
            switch betDetail.marketPresentation {
            case .horizontal:
                let cell = LLHorizontalOddsTableCell(betDetails: betDetail,
                                                     forceSuspension: forceSuspension)
                cell.selectionCoordinator = selectionCoordinator
                cell.heightAnchor.constraint(equalToConstant: 66.0).isActive = true
                cell.delegate = self
                cells.append(cell)
                themeBuilder.applyThemeTo(superview: cell)

            case .vertical:
                let cell = LLVerticalOddsTableCell(betDetails: betDetail,
                                                   selectionCoordinator: selectionCoordinator,
                                                   forceSuspension: forceSuspension)
                cell.delegate = self
                cells.append(cell)
                themeBuilder.applyThemeTo(superview: cell)
            }
        }
        // Reload the table view
        tableView.reload(elements: cells)

        if let selectedID = selectedID {
            let elements = selectedID.components(separatedBy: .whitespaces)
            if let hit = cells.first(where: { $0.getMarketID() == elements[0] }) {
                DispatchQueue.main.async {
                    hit.selectOptionWithID(option: nil, uniqueID: selectedID)
                }
            } else {
                activeSelection = nil
            }
        } else {
            activeSelection = nil
        }
    }

    /**
        Adds the table header at top of the table view
     */
    private func addTableHeader() {
        let headerView = LLStandardBodyInfoView()
        headerView.configureInfoView(teams: teams, widgetLabels: widgetLabels)
        #if os(tvOS)
            headerView.isHidden = true
        #endif
        contentStackView.insertArrangedSubview(headerView, at: 0)
        tableHeader = headerView
    }

    /**
        Adds the results description label with the correct text
     */
    private func addResultDescription(resultDescription: String) {
        resultDescriptionLabel.text = resultDescription
        contentStackView.insertArrangedSubview(resultDescriptionLabel, at: 1)
    }

    /**
        Should be called when closing odds are received to update the body element
     */
    func setFinalResult(finalResult: LLFinalResultModel?) {
        guard let finalResult = finalResult else { return }

        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }

            self.tableHeader?.setFinalResult(finalResult: finalResult)
            if let resultDescription = finalResult.labels?.resultDescription {
                addResultDescription(resultDescription: resultDescription)
            }
            // Updates the headers theme
            if let tableHeader = self.tableHeader {
                self.themeBuilder.applyThemeTo(superview: tableHeader)
                self.themeBuilder.applyThemeTo(superview: resultDescriptionLabel)
            }

            #if os(tvOS)
                if let tableHeader = self.tableHeader, tableHeader.isHidden == true {
                    self.tableHeader?.isHidden = false
                }
            #endif
        }
    }

    /**
        Returns the selected market option model if an option has been selected.
     */
    func getSelectedMarketOption() -> LLMarketOptionModel? {
        return selectionCoordinator.getSelected?.marketOption
    }

    func setupSuspendedState(isSuspended: Bool) {
        forceSuspension = isSuspended
        DispatchQueue.main.async { [weak self] in
            self?.recreateTableView(forceSuspension: isSuspended)
        }
    }

    /**
        Forces a deselection if any selection is currently active
     */
    func forceDeselect() {
        selectionCoordinator.deselectAll()
        if let activeSelection = activeSelection {
            oddsCellDidDeselectOption(selection: activeSelection)
        }
    }
}

extension LLOddsBodyView: LLOddsTableViewCellDelegate {
    func oddsCellDidSelectOption(selection: LLOddsSelection) {
        // Blocks the method being called upon update, this can happen when odds are updated and selection is kept
        if let activeSelectionID = activeSelection?.uniqueID, selection.uniqueID == activeSelectionID {
            return
        }
        activeSelection = selection
        delegate?.oddsTableViewDidSelect(selection: selection)
    }

    func oddsCellDidDeselectOption(selection: LLOddsSelection) {
        activeSelection = nil
        delegate?.oddsTableViewDidDeselect(selection: selection)
    }
}
