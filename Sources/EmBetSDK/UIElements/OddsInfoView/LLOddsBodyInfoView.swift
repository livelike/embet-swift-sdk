//
//  LLOddsBodyInfoView.swift
//
//
//  Created by Ljupco Nastevski on 29.10.24.
//

import UIKit

/**
    View that is shown on top of the body element of the OddsWidget controller
 */
internal protocol LLOddsBodyInfoView: UIView {
    func configureInfoView(teams: [LLWidgetImageDetailsModel]?, widgetLabels: LLWidgetLabels?)
    func setFinalResult(finalResult: LLFinalResultModel)
}
