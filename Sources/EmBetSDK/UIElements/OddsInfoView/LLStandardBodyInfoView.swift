//
//  LLStandardBodyInfoView.swift
//
//
//  Created by Ljupco Nastevski on 29.10.24.
//

import UIKit

internal class LLStandardBodyInfoView: UIView, LLOddsBodyInfoView {
    private let titleLabel = LLThemableLabel(themeType: .title)

    private var teamViews: [LLTeamView] = []

    required init() {
        super.init(frame: .zero)
        addBottomSeparator()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func addBottomSeparator() {
        let separator = LLThemableSeparator(themeType: .separator)
        separator.translatesAutoresizingMaskIntoConstraints = false

        addSubview(separator)

        NSLayoutConstraint.activate([
            separator.leadingAnchor.constraint(equalTo: leadingAnchor),
            separator.trailingAnchor.constraint(equalTo: trailingAnchor),
            separator.bottomAnchor.constraint(equalTo: bottomAnchor),
            separator.heightAnchor.constraint(equalToConstant: 1.0),
        ])
    }

    func configureInfoView(teams: [LLWidgetImageDetailsModel]?, widgetLabels: LLWidgetLabels?) {
        let headerStackView = UIStackView()
        headerStackView.translatesAutoresizingMaskIntoConstraints = false
        headerStackView.axis = .horizontal
        headerStackView.distribution = .equalSpacing

        if let teams = teams, !teams.isEmpty {
            for (index, team) in teams.enumerated() {
                let teamView = LLTeamView(team: team)
                // First item should have points to the right, others to left
                index == 0 ? teamView.setPointsAsPostfix() : teamView.setPointsAsPrefix()
                headerStackView.addArrangedSubview(teamView)
                teamViews.append(teamView)
            }
        }

        // As information is optional, the header label element should be only shown if
        // information is present
        if let informationText = widgetLabels?.information {
            titleLabel.numberOfLines = 0
            titleLabel.text = informationText
            // Inserts at position 1 if teams are provided
            let insertIndex = headerStackView.arrangedSubviews.count > 0 ? 1 : 0
            headerStackView.insertArrangedSubview(titleLabel, at: insertIndex)
        }

        // Inserts at 0
        addSubview(headerStackView)

        NSLayoutConstraint.activate([
            headerStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            headerStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            headerStackView.topAnchor.constraint(equalTo: topAnchor),
            headerStackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -1),
        ])
    }

    func setFinalResult(finalResult: LLFinalResultModel) {
        var didFirstWon = false
        var didLastWin = false

        if let teams = finalResult.teams {
            if let firstTeam = finalResult.teams?.first {
                if firstTeam.isWinner == true {
                    didFirstWon = true
                }
            }

            if let lastTeam = finalResult.teams?.last {
                if lastTeam.isWinner == true {
                    didLastWin = true
                }
            }

            teamViews.forEach { teamView in
                if let hit = teams.first(where: { $0.description == teamView.team.description }),
                   let points = hit.points,
                   let isWinner = hit.isWinner
                {
                    teamView.setPoints(string: points, isWinner: isWinner)
                }
            }
        }

        if let information = finalResult.labels?.information {
            let fullString = NSMutableAttributedString()

            if didFirstWon == true {
                let imageAttachment = NSTextAttachment()
                imageAttachment.image = UIImage(systemName: "arrowtriangle.left.fill")?.withTintColor(titleLabel.textColor)
                fullString.append(NSAttributedString(attachment: imageAttachment))
                fullString.append(NSAttributedString(string: " "))
            }

            fullString.append(NSAttributedString(string: information))

            if didLastWin == true {
                let imageAttachment = NSTextAttachment()
                imageAttachment.image = UIImage(systemName: "arrowtriangle.right.fill")?.withTintColor(titleLabel.textColor)
                fullString.append(NSAttributedString(string: " "))
                fullString.append(NSAttributedString(attachment: imageAttachment))
            }

            titleLabel.attributedText = fullString
        }
    }
}
