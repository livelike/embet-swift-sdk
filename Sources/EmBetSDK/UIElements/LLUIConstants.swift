//
//  LLUIConstants.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 10.3.22.
//

import UIKit

/**
    Initial widget UI - constants.
    Default widget UI - constants.
 */
internal enum LLUIConstants {
    // MARK: - Root Constants

    /// Size of the dismiss button
    static let DismissButtonSize: Float = 21.0

    /// Default font size for all elements
    static let DefaultFontSize: CGFloat = 16

    /// The default text color for all elements
    static let DefaultTextFontColor: UIColor = .init(hexaRGBA: "#FFFFFF") ?? .white

    /// The size of the countdown timer view
    static let CountdownTimerViewSize: CGSize = .init(width: 30, height: 30)

    /// The top padding to the round timer
    static let FloatingElementsRootPadding: CGFloat = 12

    /// The leading padding to the round timer
    static let FloatingElementsRootLeadingPadding: CGFloat = 12

    /// Round timer shadow color
    static let RoundTimerShadowColor: UIColor = .init(hexaRGBA: "#58585896") ?? .black

    /// The dismiss button top padding
    static let DismissButtonDefaultMargin: Margin = .init(top: 16, right: 12, bottom: 0, left: 0)

    /// The default timer color
    static let DefaultTimerColor: LLThemeBackgroundModel = .fill(.init(color: .white))

    /// The default dismiss button color
    static let DefaultDismissButtonColor: UIColor = .init(hexaRGBA: "#979797") ?? .gray

    /// Accent color
    static let DefaultAccentColor: UIColor = .init(hexaRGBA: "#00fa00B2") ?? .green

    /// Default background object
    static let DefaultBackground: LLThemeBackgroundModel = .fill(.init(color: .black))

    /// Default root border radius
    static let DefaultRootBorderRadius: CornerRadii = .init(doubleArray: [6, 6, 6, 6])

    /// Default round timer diameter
    static let DefaultTimerHeight: CGFloat = 30

    /// Default bar timer bar width
    static let DefaultTimerBarWidth: CGFloat = 3

    /// Size of the dismiss button
    static let DefaultBackgroundGradientDirection: Double = 0

    // MARK: - Header Constants

    /// Header stack view element spacing
    static let HeaderElementsSpacing: CGFloat = 10

    /// Default header content padding
    static let DefaultHeaderPadding: Padding = .init(doubleArray: [16, 16, 8, 16])

    /// Default header elements font size
    static let DefaultHeaderFontSize: CGFloat = 24

    // MARK: - Body Constants

    /// Default body content padding
    static let DefaultBodyPadding: Padding = .init(doubleArray: [0, 0, 8, 0])

    /// The image size for the bar bet widget variation
    static let BarSelectableViewImageSize: CGSize = .init(width: 40, height: 40)

    /// The image size for the square bet widget variation
    static let SquareSelectableViewImageSize: CGSize = .init(width: 48, height: 48)

    /// The image size for the inline bet widget variation
    static let InlineSelectableViewImageSize: CGSize = .init(width: 56, height: 56)

    /// The bar variation option content padding
    static let BarSelectableViewDefaultPadding: CGFloat = 8.0

    /// The square variation option content padding
    static let SquareSelectableViewDefaultPadding: CGFloat = 8.0

    /// Inline variation horizontal content padding
    static let InlineSelectableViewPaddingHorizontalPadding: CGFloat = 12.0

    /// Inline variation vertical content padding
    static let InlineSelectableViewPaddingVerticalPadding: CGFloat = 8.0

    /// Default body items content padding
    static let BodyItemsHorizontalPadding: CGFloat = 16

    /// Default spacing between body elements
    static let BodyElementsSpacing: CGFloat = 12

    /// Default body options spacing
    static let SelectableOptionsSpacing: CGFloat = 12

    /// Inline variation spacing between images and options view holder
    static let BodyInlineImageSpacing: CGFloat = 8

    /// Spacing between the elements in the alert widget
    static let AlertBodyElementsSpacing: CGFloat = 8

    /// Bet results overlay background color
    static let BetResultsOverlayBackgroundColor: UIColor = .init(hexaRGBA: "#323232DC") ?? .gray

    /// Default alert body padding
    static let DefaultAlertBodyPadding = Padding(doubleArray: [0, 0, 0, 0])

    /// Default media width when the textPosition property of the alert widget is either .left or .right.
    static let DefaultAlertHorizontalPositionMediaWidth: Double = 0.4

    /// Default elements vertical spacing in duel variation.
    static let DuelVariationBodyElementSpacing: CGFloat = 16

    /// Default duel variation text
    static let DuelVariationVSLabelText: String = "VS"

    /// Default duel variation text size.
    static let DuelVariationVSLabelFontSize: CGFloat = 20

    // MARK: - Insights

    /// Default padding between insight table cell
    static let InsightTableCellPadding: Padding = .init(doubleArray: [2, 0, 2, 0])

    /// Default padding of insights content view
    static let DefaultInsightsPadding: Padding = .init(doubleArray: [8, 16, 8, 16])

    /// Default padding of insight individual elements
    static let InsightComponentsPadding: Padding = .init(doubleArray: [0, 0, 8, 0])

    /// Default insight background
    static let DefaultInsightsBackground: LLThemeBackgroundModel = .fill(.init(color: .white.withAlphaComponent(0.3)))

    // MARK: Selectable options

    /// Selectable option border color when selected
    static let SelectedColorBorder: UIColor = .init(hexaRGBA: "#00fA00B2") ?? .green.withAlphaComponent(0.7)

    /// Selectable option bar color when selected
    static let SelectedBarColor: UIColor = .init(hexaRGBA: "#00E100B2") ?? .green.withAlphaComponent(0.7)

    /// Selectable bar background when selected
    static let SelectedBarBackground: LLThemeBackgroundModel = .fill(.init(color: SelectedBarColor))

    /// Selectable option bar color when unselected
    static let UnselectedColorBorder: UIColor = .init(hexaRGBA: "#BEBEBEB2") ?? .gray.withAlphaComponent(0.7)

    /// Selectable option bar color when unselected
    static let UnselectedBarColor: UIColor = .init(hexaRGBA: "#A0A0A0B2") ?? .gray.withAlphaComponent(0.7)

    /// Selectable bar background when unselected
    static let UnselectedBarBackground: LLThemeBackgroundModel = .fill(.init(color: UnselectedBarColor))

    /// Default border radius of the selectable option
    static let DefaultSelectableOptionBorderRadius: CornerRadii = .init(doubleArray: [6, 6, 6, 6])

    /// Default border width of the selectable option
    static let DefaultSelectableOptionBorderWidth: Double = 3.0

    // MARK: - Footer constants

    /// Footer elements spacing
    static let FooterElementsSpacing: CGFloat = 4

    /// The default font weigth of the CTA button
    static let BetButonFontWeight: LLFontWeight = .bold

    /// The height of the sponsor image
    static let SponsorImageHeight: CGFloat = 40.0

    /// Legal advice image height
    static let LegalAdviceImageViewHeight: CGFloat = 40.0

    /// Legal advice width to parent.width ratio
    static let LegalAdviceImageEmbededWidthRatio: CGFloat = 0.5

    /// Single footer element (sponsor or CTA) width to parent.width ratio
    static let SingleFooterTopElementRatio: CGFloat = 0.5

    /// Default alert footer elements padding
    static let DefaultAlertFooterPadding = Padding(doubleArray: [0, 16, 12, 16])

    /// Default footer content padding
    static let DefaultFooterPadding: Padding = .init(doubleArray: [8, 16, 8, 16])

    /// Default CTA button border radius
    static let DefaultBetButtonBorderRadius: CornerRadii = .init(doubleArray: [19, 19, 19, 19])

    /// Default CTA button content padding
    static let DefaultBetButtonPadding: Padding = .init(doubleArray: [10, 4, 10, 4])

    /// Default CTA button text color
    static let DefaultBetButtonFontColor: UIColor = .black

    /// Default bet button background
    static let DefaultBetButtonBackground: LLThemeBackgroundModel = .fill(.init(color: .white))

    /// `Sponsored by` text font size
    static let DefaultSponsoredByLabelFontSize: CGFloat = 12.0

    /// `Sponsored by` text font color
    static let DefaultSponsoredByLabelFontColor: UIColor = .init(hexaRGBA: "#999999") ?? .gray

    /// Sponsor image border width
    static let DefaultSponsoredImageBorderWidth: CGFloat = 2.0

    /// Sponsor image border color
    static let DefaultSponsoredImageBorderColor: UIColor = .init(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.2)

    /// Sponsor image content padding
    static let DefaultSponsoredImagePadding: Padding = .init(doubleArray: [0, 6, 0, 6])

    // MARK: - Image Overwriter

    /// Default size of the text that overwrites the selectable option images.
    static let DefaultImageOverwriterFontSize: CGFloat = 40.0

    #if os(tvOS)

        // MARK: - tvOS Constants

        /// Default size of the text that overwrites the selectable option images.
        static let FocusColor: UIColor = UIColor.white.withAlphaComponent(0.4)
    #endif
}
