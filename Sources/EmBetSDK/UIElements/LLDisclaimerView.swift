//
//  LLDisclaimerView.swift
//
//
//  Created by Ljupco Nastevski on 13.11.23.
//

import UIKit

internal class LLDisclaimerView: LLThemableView {
    /**
        The title label
     */
    private lazy var titleLabel: LLThemableLabel = {
        let label = LLThemableLabel(themeType: .disclaimerTitle)
        label.setContentHuggingPriority(.defaultHigh, for: .vertical)
        label.numberOfLines = 0
        return label
    }()

    /**
        The description label
     */
    private lazy var descriptionLabel: LLThemableLabel = {
        let label = LLThemableLabel(themeType: .disclaimerDescription)
        label.numberOfLines = 0
        return label
    }()

    /**
        Holder stack view for the views elements
     */
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 4.0
        return stackView
    }()

    /**
        The scrollview that holds the content collectionview
     */
    internal lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()

    private var frameLayoutHeight: NSLayoutConstraint?

    private var scrollViewHeightAnchor: NSLayoutConstraint?

    /**
        The disclaimer title
     */
    private(set) var title: String?

    /**
        The disclaimer description
     */
    private(set) var descriptionText: String?

    /**
        The disclaimer description colapsed
     */
    private(set) var descriptionCollapsedText: String?

    required init(themeType: LLThemableType, title: String?, description: String?, descriptionCollapsed: String?) {
        super.init(themeType: themeType)
        self.title = title
        descriptionText = description
        descriptionCollapsedText = descriptionCollapsed
        commonInit()
    }

    /**
        View setup
     */
    internal func commonInit() {
        // Add content stack view subviews - title and description
        if let title = title {
            contentStackView.addArrangedSubview(titleLabel)
            titleLabel.text = title
        }

        if descriptionText != nil || descriptionCollapsedText != nil {
            contentStackView.addArrangedSubview(descriptionLabel)
            descriptionLabel.text = descriptionCollapsedText ?? descriptionText
        }

        // Add subviews
        scrollView.addSubview(contentStackView)
        addSubview(scrollView)

        // Adds contraint to subviews
        NSLayoutConstraint.activate([
            // Content stack view constraints
            contentStackView.leadingAnchor.constraint(equalTo: scrollView.contentLayoutGuide.leadingAnchor),
            contentStackView.trailingAnchor.constraint(equalTo: scrollView.contentLayoutGuide.trailingAnchor),
            contentStackView.topAnchor.constraint(equalTo: scrollView.contentLayoutGuide.topAnchor),
            contentStackView.bottomAnchor.constraint(equalTo: scrollView.contentLayoutGuide.bottomAnchor),
            contentStackView.heightAnchor.constraint(equalTo: scrollView.contentLayoutGuide.heightAnchor),

            // Scrollview constraints
            scrollView.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: layoutMarginsGuide.trailingAnchor),
            scrollView.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor),

            scrollView.contentLayoutGuide.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
            scrollView.contentLayoutGuide.heightAnchor.constraint(equalTo: scrollView.heightAnchor),
        ])
    }

    public func willExpand() {
        if descriptionText != nil || descriptionCollapsedText != nil {
            descriptionLabel.text = descriptionText
        }

        // Recalculate the height of the content
        setNeedsLayout()
        layoutIfNeeded()

        // Add a content height constraint if not added
        if scrollViewHeightAnchor == nil {
            scrollViewHeightAnchor = scrollView.contentLayoutGuide.heightAnchor.constraint(equalToConstant: scrollView.contentSize.height)
        }
        scrollViewHeightAnchor?.isActive = true

        // Enables scroll
        scrollView.isScrollEnabled = true
    }

    public func willCollapse() {
        // Dissable height constraint
        scrollViewHeightAnchor?.isActive = false

        if descriptionText != nil || descriptionCollapsedText != nil {
            descriptionLabel.text = descriptionCollapsedText ?? descriptionText
        }
        scrollView.isScrollEnabled = false
    }
}
