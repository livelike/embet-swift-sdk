//
//  LLLayerThemable.swift
//
//
//  Created by Ljupco Nastevski on 16.11.22.
//

import Foundation

/// Layer theming protocol
protocol LLLayerThemable {
    /**
        Objects implementing this protocol should start
        the theming process hare.
     */
    func setTheme(theme: LLThemeViewModel?)
}
