//
//  LLThemableLayer.swift
//
//
//  Created by Ljupco Nastevski on 16.11.22.
//

import UIKit

/**
    Themable layer, used by themable views.
    Used to setup the background and border.
    Default implementation.
 */
internal class LLThemableLayer: CAGradientLayer, LLLayerThemable {
    internal weak var borderLayer: CAShapeLayer?

    internal var theme: LLThemeViewModel?

    override init() { super.init() }

    override init(layer: Any) {
        super.init(layer: layer)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSublayers() {
        super.layoutSublayers()
        setupLayer()
        updateLayerFrames()
    }

    // updates the frames, without animating
    internal func updateLayerFrames() {
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        borderLayer?.frame = bounds
        CATransaction.commit()
    }

    // Setup Layer/border
    internal func setupLayer() {
        let path = bounds.maskPath(cornerRadii: theme?.borderRadius ?? .zero)

        let maskShape = CAShapeLayer()
        maskShape.path = path
        mask = maskShape

        guard let borderWidth = theme?.borderWidth,
              let borderColor = theme?.borderColor
        else {
            borderLayer?.removeFromSuperlayer()
            return
        }

        if let borderLayer = borderLayer {
            borderLayer.path = path
            borderLayer.frame = borderLayer.superlayer?.bounds ?? .zero
            borderLayer.lineWidth = borderWidth
            borderLayer.strokeColor = borderColor.cgColor
            borderLayer.fillColor = UIColor.clear.cgColor
        } else {
            let borderLayer = CAShapeLayer()
            borderLayer.frame = bounds
            borderLayer.path = path
            borderLayer.lineWidth = CGFloat(borderWidth)
            borderLayer.strokeColor = borderColor.cgColor
            borderLayer.fillColor = UIColor.clear.cgColor
            self.borderLayer = borderLayer

            insertSublayer(borderLayer, at: 0)
        }
    }

    func setTheme(theme: LLThemeViewModel?) {
        self.theme = theme

        borderLayer?.removeFromSuperlayer()
        switch theme?.background {
        case let .fill(fill):
            backgroundColor = fill.color.cgColor
        case let .uniformGradient(gradient):
            colors = gradient.colors.map { $0.cgColor }
            calculatePoints(for: gradient.direction)
        case .none:
            break
        case .some(.unsupported):
            break
        }
        DispatchQueue.main.async { [weak self] in
            self?.setupLayer()
            self?.updateLayerFrames()
        }
    }
}
