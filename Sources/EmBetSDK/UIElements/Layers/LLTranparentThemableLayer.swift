//
//  LLTranparentThemableLayer.swift
//
//
//  Created by Ljupco Nastevski on 17.11.22.
//

import UIKit

/**
    Themable layer, that skips background theming
 */
internal class LLTranparentThemableLayer: LLThemableLayer {
    override func setTheme(theme: LLThemeViewModel?) {
        self.theme = theme
        DispatchQueue.main.async { [weak self] in
            self?.setupLayer()
            self?.updateLayerFrames()
        }
    }
}
