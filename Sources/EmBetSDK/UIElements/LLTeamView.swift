//
//  LLTeamView.swift
//
//
//  Created by Ljupco Nastevski on 29.10.24.
//

import UIKit

/**
    View with an imageview at top and label at bottom.
 */
internal class LLTeamView: UIView {
    let team: LLWidgetImageDetailsModel

    private let pointsLabel: LLThemableLabel = .init(themeType: .teamScore)

    private let contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .center
        stackView.axis = .horizontal
        return stackView
    }()

    public init(team: LLWidgetImageDetailsModel) {
        self.team = team
        super.init(frame: .zero)
        setupSelf()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupSelf() {
        let teamLogoImageView = LLAsyncImageView()
        teamLogoImageView.translatesAutoresizingMaskIntoConstraints = false
        teamLogoImageView.loadAsyncImage(url: team.logoURL)
        teamLogoImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        teamLogoImageView.widthAnchor.constraint(equalToConstant: 72.0).isActive = true

        let teamDescriptionLabel = LLThemableLabel(themeType: .teamName)
        teamDescriptionLabel.textAlignment = .center
        teamDescriptionLabel.text = team.description

        let teamStackView = UIStackView(arrangedSubviews: [teamLogoImageView, teamDescriptionLabel])
        teamStackView.translatesAutoresizingMaskIntoConstraints = false
        teamStackView.axis = .vertical
        teamStackView.spacing = 8

        contentStackView.addArrangedSubview(teamStackView)

        addSubview(contentStackView)

        NSLayoutConstraint.activate([
            contentStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            contentStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            contentStackView.topAnchor.constraint(equalTo: topAnchor),
            contentStackView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }

    func setPointsAsPostfix() {
        contentStackView.addArrangedSubview(pointsLabel)
    }

    func setPointsAsPrefix() {
        contentStackView.insertArrangedSubview(pointsLabel, at: 0)
    }

    func setPoints(string: String, isWinner: Bool) {
        pointsLabel.type = isWinner == true ? .teamScoreWinner : .teamScore
        pointsLabel.text = string
    }
}
