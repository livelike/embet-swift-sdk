//
//  WidgetOptionLogoViewFactory.swift
//
//
//  Created by Ljupco Nastevski on 5.9.22.
//

import UIKit

/**
    Class that decides to either return imageview with the logo or label with provided text for the selectable options.
 */
class ImageOverwriter {
    /**
        Rule: Always return the label if text is present.
        If text is not present, return ImageView
     */
    static func overwriteImage(imageUrl: URL?, text: String?) -> UIView {
        if let textOverwrite = text {
            let lbl = LLThemableLabel(themeType: .optionImageOverwriter)
            lbl.textAlignment = .center
            lbl.adjustsFontSizeToFitWidth = true
            lbl.text = textOverwrite.prefix(LLConstants.MaxNumberOfOptionLogoViewCharacters).uppercased()
            return lbl
        }

        let imageView = LLAsyncImageView()
        imageView.loadAsyncImage(url: imageUrl)
        return imageView
    }
}
