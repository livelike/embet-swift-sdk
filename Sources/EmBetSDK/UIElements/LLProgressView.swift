//
//  LLProgressView.swift
//
//
//  Created by Ljupco Nastevski on 24.3.22.
//

import UIKit

internal enum LLProgressViewDirection {
    case leftToRight
    case bottomToTop
}

internal class LLProgressView: LLThemableView {
    private let progressLayer = CALayer()

    private let maskLayer = CAShapeLayer()

    private weak var backgroundLayer: LLBackgroundLayer?

    var progress: CGFloat = 0.0 {
        didSet {
            setNeedsDisplay()
        }
    }

    var direction: LLProgressViewDirection = .leftToRight

    var padding = Padding.zero {
        didSet {
            setNeedsDisplay()
        }
    }

    var cornerRadii = CornerRadii.zero {
        didSet {
            self.setNeedsDisplay()
        }
    }

    required init() {
        super.init(themeType: .unselectedOptionBar)
        progressLayer.mask = maskLayer
        layer.addSublayer(progressLayer)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        setNeedsDisplay()
    }

    override func draw(_ rect: CGRect) {
        let paddedRect = CGRect(x: rect.origin.x + padding.left, y: rect.origin.y + padding.top, width: rect.width - (padding.left + padding.right), height: rect.height - (padding.top + padding.bottom))

        var progressRect: CGRect!

        switch direction {
        case .leftToRight:
            progressRect = CGRect(origin: paddedRect.origin, size: CGSize(width: paddedRect.width * progress, height: paddedRect.height))
        case .bottomToTop:
            progressRect = CGRect(origin: CGPoint(x: paddedRect.origin.x, y: padding.top + (paddedRect.height - (paddedRect.height * progress))), size: CGSize(width: paddedRect.width, height: paddedRect.height * progress))
        }

        let mask = paddedRect.maskPath(cornerRadii: cornerRadii)
        maskLayer.path = mask

        progressLayer.frame = progressRect
        maskLayer.frame = progressLayer.bounds

        if backgroundLayer == nil {
            let bglayer = LLBackgroundLayer()
            bglayer.frame = progressLayer.bounds
            progressLayer.addSublayer(bglayer)
            backgroundLayer = bglayer
        } else {
            backgroundLayer?.frame = progressLayer.bounds
        }
    }

    override func applyTheme(theme: LLThemeViewModel?) {
        backgroundLayer?.theme = theme
        cornerRadii = theme?.borderRadius ?? .zero
        padding = theme?.padding ?? .zero
    }
}

class LLBackgroundLayer: CALayer {
    private weak var backgroundLayer: CALayer?

    var theme: LLThemeViewModel? {
        didSet {
            self.initalSetup()
        }
    }

    override func layoutSublayers() {
        super.layoutSublayers()
        updateLayerFrames()
    }

    private func initalSetup() {
        backgroundLayer?.removeFromSuperlayer()
        setupBackground()
    }

    private func updateLayerFrames() {
        CATransaction.begin()
        backgroundLayer?.frame = bounds
        CATransaction.commit()
    }

    private func setupBackground() {
        backgroundLayer?.removeFromSuperlayer()

        switch theme?.background {
        case let .fill(fill):

            let backgroundLayer = CALayer()
            backgroundLayer.frame = bounds
            backgroundLayer.backgroundColor = fill.color.cgColor
            self.backgroundLayer = backgroundLayer
            insertSublayer(backgroundLayer, at: 0)

        case let .uniformGradient(gradient):

            let gradientLayer = CAGradientLayer()
            gradientLayer.frame = bounds
            gradientLayer.colors = gradient.colors.map { $0.cgColor }
            gradientLayer.calculatePoints(for: gradient.direction)

            backgroundLayer = gradientLayer
            insertSublayer(gradientLayer, at: 0)

        case .none:
            break
        case .some(.unsupported):
            break
        }
    }
}
