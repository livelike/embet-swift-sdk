//
//  LLSquareFollowUpSelectableView.swift
//
//
//  Created by Ljupco Nastevski on 7.6.24.
//

import UIKit

internal class LLSquareFollowUpSelectableView: LLSelectableView {
    private let optionLbl: LLThemableLabel = {
        let lbl = LLThemableLabel(themeType: .unselectedOption)
        lbl.textAlignment = .center
        lbl.setContentHuggingPriority(.defaultHigh, for: .vertical)
        return lbl
    }()

    private let percentageLbl: LLThemableLabel = {
        let lbl = LLThemableLabel(themeType: .unselectedOption)
        lbl.setContentHuggingPriority(.defaultHigh, for: .vertical)
        lbl.textAlignment = .center
        return lbl
    }()

    private var followUpResultsView: LLSelectableFollowUpResultsOverlay? {
        return voteResultsView as? LLSelectableFollowUpResultsOverlay
    }

    required init(option: EBWidgetOption,
                  betDetails: LLWidgetBetDetailsModel?,
                  selectedThemeType: LLThemableType,
                  unselectedThemeType: LLThemableType,
                  numberOfVotesText: String?,
                  voteResultsView _: LLOptionResultsOverlay = LLSelectableResultsOverlay())
    {
        super.init(option: option,
                   betDetails: betDetails,
                   selectedThemeType: selectedThemeType,
                   unselectedThemeType: unselectedThemeType,
                   numberOfVotesText: numberOfVotesText,
                   voteResultsView: LLSelectableFollowUpResultsOverlay())
    }

    override func initialSetup() {
        super.initialSetup()

        optionLbl.text = option.text
        percentageLbl.text = " "

        let imageView = ImageOverwriter.overwriteImage(imageUrl: option.imageURL, text: betDetails?.imageOverwriter)
        imageView.translatesAutoresizingMaskIntoConstraints = false

        let imageHolderView = UIView()
        imageHolderView.translatesAutoresizingMaskIntoConstraints = false
        imageHolderView.addSubview(imageView)

        let contentStackView = UIStackView(arrangedSubviews: [optionLbl, imageHolderView, percentageLbl])
        contentStackView.spacing = 4.0
        contentStackView.translatesAutoresizingMaskIntoConstraints = false
        contentStackView.axis = .vertical
        addSubview(contentStackView)

        let defPadding: CGFloat = 4.0

        NSLayoutConstraint.activate([
            imageView.widthAnchor.constraint(lessThanOrEqualTo: imageHolderView.widthAnchor),
            imageView.heightAnchor.constraint(lessThanOrEqualTo: imageHolderView.heightAnchor),
            imageView.centerXAnchor.constraint(equalTo: imageHolderView.centerXAnchor),
            imageView.centerYAnchor.constraint(equalTo: imageHolderView.centerYAnchor),

            imageView.heightAnchor.constraint(equalToConstant: LLUIConstants.SquareSelectableViewImageSize.height),

            contentStackView.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor, constant: defPadding),
            contentStackView.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor, constant: -defPadding),
            contentStackView.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor, constant: defPadding),
            contentStackView.trailingAnchor.constraint(equalTo: layoutMarginsGuide.trailingAnchor, constant: -defPadding),
        ])

        addResultsOverlayView(asFirstSubview: true)
        selectionChanged()
        voteResultsView.setProgressBarDirection(direction: .vertical)
    }

    override func selectionChanged() {
        if isSelected {
            if option.isOptionCorrect == true {
                optionLbl.type = .selectedCorrectOption
                percentageLbl.type = .selectedCorrectOptionPercentage
                type = .selectedCorrectOption
                followUpResultsView?.optionState = .selectedCorrect
            } else {
                optionLbl.type = .selectedIncorrectOption
                percentageLbl.type = .selectedIncorrectOptionPercentage
                type = .selectedIncorrectOption
                followUpResultsView?.optionState = .selectedIncorrect
            }
        } else {
            if option.isOptionCorrect == true {
                optionLbl.type = .unselectedCorrectOption
                percentageLbl.type = .unselectedCorrectOptionPercentage
                type = .unselectedCorrectOption
                followUpResultsView?.optionState = .unselectedCorrect
            } else {
                optionLbl.type = .unselectedIncorrectOption
                percentageLbl.type = .unselectedIncorrectOptionPercentage
                type = .unselectedIncorrectOption
                followUpResultsView?.optionState = .unselectedIncorrect
            }
        }
        voteResultsView.setSelected(selected: isSelected)
    }

    override public func setVoteResultPercentage(percentage: CGFloat?) {
        guard let percentage = percentage else {
            voteResultsView.removeFromSuperview()
            return
        }

        percentageLbl.text = "\(percentage.percentageString)"
        voteResultsView.setPercentage(percentage: percentage)
    }

    private func applyCorrectTheme() {}
}
