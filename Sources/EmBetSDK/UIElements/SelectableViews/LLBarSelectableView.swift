//
//  LLBarSelectableView.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 7.3.22.
//

import UIKit

internal class LLBarSelectableView: LLSelectableView {
    private let optionLbl: LLThemableLabel = .init(themeType: .unselectedOption)

    private let detailsLbl: LLThemableLabel = {
        let lbl = LLThemableLabel(themeType: .unselectedOption)
        lbl.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        lbl.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        return lbl
    }()

    override func initialSetup() {
        super.initialSetup()

        let imageView = ImageOverwriter.overwriteImage(imageUrl: option.imageURL, text: betDetails?.imageOverwriter)
        imageView.translatesAutoresizingMaskIntoConstraints = false

        optionLbl.text = option.text
        detailsLbl.text = betDetails?.bet

        let contentStackView = UIStackView(arrangedSubviews: [imageView, optionLbl, detailsLbl])
        contentStackView.translatesAutoresizingMaskIntoConstraints = false
        contentStackView.spacing = 8
        contentStackView.axis = .horizontal
        addSubview(contentStackView)

        let defPadding = LLUIConstants.BarSelectableViewDefaultPadding

        NSLayoutConstraint.activate([
            imageView.widthAnchor.constraint(equalToConstant: LLUIConstants.BarSelectableViewImageSize.width),
            imageView.heightAnchor.constraint(equalToConstant: LLUIConstants.BarSelectableViewImageSize.height),

            contentStackView.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor, constant: defPadding),
            contentStackView.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor, constant: -defPadding),
            contentStackView.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor, constant: defPadding),
            contentStackView.trailingAnchor.constraint(equalTo: layoutMarginsGuide.trailingAnchor, constant: -defPadding),
        ])

        addResultsOverlayView()
        voteResultsView.setProgressBarDirection(direction: .horizontal)
    }

    override func selectionChanged() {
        super.selectionChanged()

        if isSelected {
            optionLbl.type = .selectedOption
            detailsLbl.type = .selectedOption
        } else {
            optionLbl.type = .unselectedOption
            detailsLbl.type = .unselectedOption
        }
    }
}
