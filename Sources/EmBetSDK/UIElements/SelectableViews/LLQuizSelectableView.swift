//
//  LLQuizSelectableView.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 7.3.22.
//

import UIKit

internal class LLQuizSelectableView: LLSelectableView {
    private let optionLbl: LLThemableLabel = .init(themeType: .unselectedOption)

    private var icon: UIImage? {
        if let isCorrect = option.isOptionCorrect,
           isCorrect == true
        {
            return .embetIconWithName(iconName: .accept)
        }
        return .embetIconWithName(iconName: .close)
    }

    override func initialSetup() {
        super.initialSetup()
        voteResultsView = LLSelectableQuizResultsOverlay()
        optionLbl.text = option.text

        let imageView = UIImageView(image: icon)
        imageView.translatesAutoresizingMaskIntoConstraints = false

        let contentStackView = UIStackView(arrangedSubviews: [optionLbl, imageView])
        contentStackView.translatesAutoresizingMaskIntoConstraints = false
        contentStackView.spacing = 8
        contentStackView.axis = .horizontal
        addSubview(contentStackView)

        let defPadding = LLUIConstants.BarSelectableViewDefaultPadding

        NSLayoutConstraint.activate([
            imageView.widthAnchor.constraint(equalToConstant: 30),
            imageView.heightAnchor.constraint(equalToConstant: 30),

            contentStackView.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor, constant: defPadding),
            contentStackView.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor, constant: -defPadding),
            contentStackView.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor, constant: defPadding),
            contentStackView.trailingAnchor.constraint(equalTo: layoutMarginsGuide.trailingAnchor, constant: -defPadding),
        ])

        addResultsOverlayView(asFirstSubview: true)

        voteResultsView.addSubview(imageView)

        NSLayoutConstraint.activate([
            imageView.widthAnchor.constraint(equalToConstant: 30),
            imageView.heightAnchor.constraint(equalToConstant: 30),

            imageView.topAnchor.constraint(equalTo: voteResultsView.layoutMarginsGuide.topAnchor, constant: defPadding),
            imageView.bottomAnchor.constraint(equalTo: voteResultsView.layoutMarginsGuide.bottomAnchor, constant: -defPadding),
            imageView.trailingAnchor.constraint(equalTo: voteResultsView.layoutMarginsGuide.trailingAnchor, constant: -defPadding - 48),
        ])

        voteResultsView.setProgressBarDirection(direction: .horizontal)
    }

    override func selectionChanged() {
        super.selectionChanged()

        if isSelected {
            optionLbl.type = .selectedOption
        } else {
            optionLbl.type = .unselectedOption
        }
    }
}
