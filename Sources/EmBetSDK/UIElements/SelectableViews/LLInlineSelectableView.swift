//
//  LLInlineSelectableView.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 7.3.22.
//

import UIKit

internal class LLInlineSelectableView: LLSelectableView {
    private let optionLbl: LLThemableLabel = .init(themeType: .unselectedOption)

    private let detailsLbl: LLThemableLabel = {
        let lbl = LLThemableLabel(themeType: .unselectedOption)
        lbl.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        lbl.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        return lbl
    }()

    override func initialSetup() {
        super.initialSetup()

        optionLbl.text = option.text
        detailsLbl.text = betDetails?.bet

        let contentStackView = UIStackView(arrangedSubviews: [optionLbl, detailsLbl])
        contentStackView.translatesAutoresizingMaskIntoConstraints = false
        contentStackView.spacing = 8
        contentStackView.axis = .horizontal
        addSubview(contentStackView)

        NSLayoutConstraint.activate([
            contentStackView.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor, constant: LLUIConstants.InlineSelectableViewPaddingHorizontalPadding),
            contentStackView.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor, constant: -LLUIConstants.InlineSelectableViewPaddingHorizontalPadding),
            contentStackView.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor, constant: LLUIConstants.InlineSelectableViewPaddingVerticalPadding),
            contentStackView.trailingAnchor.constraint(equalTo: layoutMarginsGuide.trailingAnchor, constant: -LLUIConstants.InlineSelectableViewPaddingVerticalPadding),
        ])

        setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)

        addResultsOverlayView()
        voteResultsView.setProgressBarDirection(direction: .horizontal)
    }

    override func selectionChanged() {
        super.selectionChanged()

        if isSelected {
            optionLbl.type = .selectedOption
            detailsLbl.type = .selectedOption
        } else {
            optionLbl.type = .unselectedOption
            detailsLbl.type = .unselectedOption
        }
    }
}
