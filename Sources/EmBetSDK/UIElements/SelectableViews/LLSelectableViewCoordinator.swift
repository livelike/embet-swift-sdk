//
//  LLSelectableViewCoordinator.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 28.2.22.
//

import CoreGraphics
import UIKit

// Determines the state of the initial animation
internal enum InitialAnimationState {
    // Animation is idle and has not been started
    case idle

    // Initial animation is in progress
    case inProgress

    // Animation has finished
    case finished
}

/**
    Coordinates the selection of multiple SelectableViews
 */
internal class LLSelectableViewCoordinator {
    weak var themeBulder: LLThemeBuilder?

    /**
     If this value is set to `true` results will be shown without
     animation.
     */
    var shouldShowInstantResults: Bool = false

    /**
        Setting the property to `false` will not show the results
        upon making a selection.
     */
    var showsResultsOnSelection: Bool = true

    var views: [LLSelectableView] = []

    /**
     Returns the selected LLSelectableView
     */
    var getSelected: LLSelectableView? {
        return views.first(where: { $0.isSelected == true })
    }

    /**
     The current state of the animation
     */
    private var initialAnimationState: InitialAnimationState = .idle

    /**
     Initial results to show after the animation has finished
     */
    private var initialResults: [String: Int] = [:]

    /**
     Deselect selected LLSelectableView
     */
    func deselectAll(exludingID: String) {
        for v in views.filter({ $0.isSelected == true }) where exludingID != v.option.id {
            v.isSelected = false
            themeBulder?.applyThemeTo(superview: v)
        }
    }

    /**
     Select element with id
     */
    public func didSelect(id: String) {
        if showsResultsOnSelection == true {
            performInitialResultsReveal()
        }

        if let hit = views.first(where: { $0.option.id == id }) {
            hit.isSelected = true
            themeBulder?.applyThemeTo(superview: hit)
        }

        deselectAll(exludingID: id)
    }

    public func performInitialResultsReveal() {
        // allows for only one animation
        if initialAnimationState != .idle { return }
        initialAnimationState = .inProgress

        if shouldShowInstantResults == true {
            performInstantResultsReveal()
        } else {
            performAnimatedResultsReveal()
        }
    }

    // Initial animation block should only be shown for the first time
    public func performAnimatedResultsReveal() {
        for view in views {
            UIView.animate(withDuration: LLConstants.OptionFadeOutDuration, delay: LLConstants.OptionFadeOutDelay, options: [.curveEaseIn]) {
                // Fades in the results overlay
                view.voteResultsView.alpha = 1
            } completion: { finished in

                if finished {
                    let calculatedPercentage = self.calculatePercentage(view: view)
                    self.revealResultsForView(view: view, calculatedPercentage: calculatedPercentage)

                    UIView.animate(withDuration: LLConstants.OptionFadeInDuration, delay: LLConstants.OptionFadeInDelay, options: [.curveEaseIn]) {} completion: { [weak self] completed in
                        self?.animateBarForView(view: view, calculatedPercentage: calculatedPercentage)
                        if completed == true {
                            // Initial animation has completed
                            self?.initialAnimationState = .finished
                        }
                    }
                }
            }
        }
    }

    public func performInstantResultsReveal() {
        for view in views {
            view.voteResultsView.alpha = 1
            let calculatedPercentage = calculatePercentage(view: view)
            revealResultsForView(view: view, calculatedPercentage: calculatedPercentage)
            animateBarForView(view: view, calculatedPercentage: calculatedPercentage)
        }
        initialAnimationState = .finished
    }

    func didReceiveUpdateForOption(optionID: String, votes: Int, totalVotes: Int) {
        if initialAnimationState == .finished {
            // if initial animation has finished, update as results arrive
            if let hit = views.first(where: { $0.option.id == optionID }) {
                hit.setVoteResultPercentage(percentage: CGFloat(votes) / CGFloat(totalVotes))
                hit.updateResults()
            }

        } else {
            // prepare results to show for initial animation
            initialResults[optionID] = votes
        }
    }

    private func calculatePercentage(view: LLSelectableView) -> CGFloat? {
        let totalVotes = initialResults.map { $0.value }.reduce(0, +)
        let res = initialResults.first(where: { $0.key == view.option.id })?.value ?? 0 // If there are no results before the animation finished, set everything to 0

        // Don't divide by zero
        // If total votes is 0, set option percentage to 0.0
        return (totalVotes == 0 ? 0.0 : CGFloat(res) / CGFloat(totalVotes))
    }

    private func revealResultsForView(view: LLSelectableView, calculatedPercentage: CGFloat?) {
        // Apply theme to views and update only the title element
        DispatchQueue.main.asyncAfter(deadline: .now()) { [weak self] in
            self?.themeBulder?.applyThemeTo(superview: view)
            view.setVoteResultPercentage(percentage: calculatedPercentage)
            view.updateVotesLabel()
        }
    }

    private func animateBarForView(view: LLSelectableView, calculatedPercentage: CGFloat?) {
        // Animate in the title element
        view.voteResultsView.showResults()

        // After a delay, start animating the bar
        DispatchQueue.main.asyncAfter(deadline: .now() + LLConstants.BarAnimationDelay) {
            view.setVoteResultPercentage(percentage: calculatedPercentage)
            view.updateBar()
        }
    }
}

internal class LLBaseSelectableViewCoordinator {
    weak var themeBulder: LLThemeBuilder?

    var views: [LLMarketOptionView] = []

    /**
        Returns the selected LLSelectableView
     */
    weak var getSelected: LLMarketOptionView? {
        return views.first(where: { $0.isSelected == true })
    }

    /**
        Deselect selected LLSelectableView
     */
    func deselectAll(exludingID: String) {
        for v in views.filter({ $0.isSelected == true }) where exludingID != v.uniqueID {
            v.isSelected = false
            themeBulder?.applyThemeTo(superview: v)
        }
    }

    /**
        Deselect selected LLSelectableView
     */
    func deselectAll() {
        for v in views.filter({ $0.isSelected == true }) {
            v.isSelected = false
            themeBulder?.applyThemeTo(superview: v)
        }
    }

    /**
        Select element with id
     */
    func didSelect(id: String) {
        if let hit = views.first(where: { $0.uniqueID == id }), getSelected?.uniqueID != id {
            hit.isSelected = true
            themeBulder?.applyThemeTo(superview: hit)
            deselectAll(exludingID: id)
        } else {
            deselectAll()
        }
    }
}

internal class LLQuizSelectionCoordinator: LLSelectableViewCoordinator {
    override func performInitialResultsReveal() {
        DispatchQueue.main.async {
            super.performInitialResultsReveal()
        }
    }

    override func didSelect(id: String) {
        performInitialResultsReveal()

        if let hit = views.first(where: { $0.option.id == id }) {
            hit.isSelected = true
            DispatchQueue.main.asyncAfter(deadline: .now()) { [weak self] in
                self?.themeBulder?.applyThemeTo(superview: hit)
            }
        }
        deselectAll(exludingID: id)
    }
}
