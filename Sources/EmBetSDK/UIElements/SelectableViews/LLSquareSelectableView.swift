//
//  LLSquareSelectableView.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 7.3.22.
//

import UIKit

internal class LLSquareSelectableView: LLSelectableView {
    private let optionLbl: LLThemableLabel = {
        let lbl = LLThemableLabel(themeType: .unselectedOption)
        lbl.textAlignment = .center
        lbl.setContentHuggingPriority(.defaultHigh, for: .vertical)
        return lbl
    }()

    private let detailsLbl: LLThemableLabel = {
        let lbl = LLThemableLabel(themeType: .unselectedOption)
        lbl.setContentHuggingPriority(.defaultHigh, for: .vertical)
        lbl.textAlignment = .center
        return lbl
    }()

    override func initialSetup() {
        super.initialSetup()

        optionLbl.text = option.text
        detailsLbl.text = betDetails?.bet

        let imageView = ImageOverwriter.overwriteImage(imageUrl: option.imageURL, text: betDetails?.imageOverwriter)
        imageView.translatesAutoresizingMaskIntoConstraints = false

        let imageHolderView = UIView()
        imageHolderView.translatesAutoresizingMaskIntoConstraints = false
        imageHolderView.addSubview(imageView)

        let contentStackView = UIStackView(arrangedSubviews: [imageHolderView, optionLbl, detailsLbl])
        contentStackView.translatesAutoresizingMaskIntoConstraints = false
        contentStackView.axis = .vertical
        addSubview(contentStackView)

        let defPadding = LLUIConstants.SquareSelectableViewDefaultPadding

        NSLayoutConstraint.activate([
            imageView.widthAnchor.constraint(lessThanOrEqualTo: imageHolderView.widthAnchor),
            imageView.heightAnchor.constraint(lessThanOrEqualTo: imageHolderView.heightAnchor),
            imageView.centerXAnchor.constraint(equalTo: imageHolderView.centerXAnchor),
            imageView.centerYAnchor.constraint(equalTo: imageHolderView.centerYAnchor),

            imageView.heightAnchor.constraint(equalToConstant: LLUIConstants.SquareSelectableViewImageSize.height),

            contentStackView.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor, constant: defPadding),
            contentStackView.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor),
            contentStackView.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor, constant: defPadding),
            contentStackView.trailingAnchor.constraint(equalTo: layoutMarginsGuide.trailingAnchor, constant: -defPadding),
        ])

        addResultsOverlayView()
        voteResultsView.setProgressBarDirection(direction: .vertical)
    }

    override func selectionChanged() {
        super.selectionChanged()

        if isSelected {
            optionLbl.type = .selectedOption
            detailsLbl.type = .selectedOption
        } else {
            optionLbl.type = .unselectedOption
            detailsLbl.type = .unselectedOption
        }
    }
}
