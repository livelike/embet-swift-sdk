//
//  LLMarketOptionView.swift
//
//
//  Created by Ljupco Nastevski on 13.11.23.
//

import UIKit

protocol LLMarketOptionViewDelegate: AnyObject {
    func didSelectOption(option: LLMarketOptionModel, uniqueID: String)
    func didDeselectOption(option: LLMarketOptionModel, uniqueID: String)
}

internal class LLMarketOptionView: LLThemableView, LLThemableInteractive {
    private lazy var suspendedImageView: UIImageView = {
        let imageView = LLThemableImageView(type: .lockIcon)
        imageView.image = .embetIconWithName(iconName: .lock)
        imageView.frame = CGRect(x: 0, y: bounds.height / 2 - 10, width: bounds.width, height: 20)
        imageView.autoresizingMask = [.flexibleHeight]
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    var selectedType: LLThemableType = .selectedOption

    var selectedTheme: LLThemeViewModel?

    var unselectedType: LLThemableType = .unselectedOption

    var unselectedTheme: LLThemeViewModel?

    var isSelected = false {
        didSet {
            self.selectionChanged()
        }
    }

    weak var delegate: LLMarketOptionViewDelegate?

    var marketID: String?

    /**
        Default value is nil.
        When this property is set to true, the description is hidden.
     */
    var shouldHideDescription: Bool = false

    #if os(tvOS)
        override var canBecomeFocused: Bool {
            return true
        }

        let focusLayer: CALayer = .init()

        override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
            if context.nextFocusedView == self {
                coordinator.addCoordinatedAnimations({ () in
                    self.focusLayer.backgroundColor = LLUIConstants.FocusColor.cgColor
                }, completion: nil)

            } else if context.previouslyFocusedView == self {
                coordinator.addCoordinatedAnimations({ () in
                    self.focusLayer.backgroundColor = UIColor.clear.cgColor
                }, completion: nil)
            }
        }
    #endif

    private(set) var marketOption: LLMarketOptionModel? {
        didSet {
            didSetMarketOption()
        }
    }

    var forceSuspension: Bool = false

    /**
        Compound ID, createad by combining market ID and outcomeID
     */
    var uniqueID: String {
        if let marketID = marketID, let outcomeID = marketOption?.outcomeID {
            return "\(marketID) \(outcomeID)"
        }
        return ""
    }

    private let changeIndicatorImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        // Start with layer with 0 opacity so we can animate it
        imageView.layer.opacity = 0
        return imageView
    }()

    /**
        Description label (top)
     */
    private let descriptionLabel: LLThemableLabel = {
        let label = LLThemableLabel(themeType: .unselectedOptionDescription)
        label.textAlignment = .center
        label.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
        return label
    }()

    /**
        Odss label (bottom)
     */
    private let oddsLabel: LLThemableLabel = {
        let label = LLThemableLabel(themeType: .unselectedOption)
        label.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
        label.textAlignment = .center
        return label
    }()

    /**
        Content holder stack view
     */
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [oddsLabel, descriptionLabel])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.distribution = .fillProportionally
        stackView.alignment = .center
        stackView.axis = .vertical
        return stackView
    }()

    required init() {
        super.init(themeType: .unselectedOption)
        // Add focus layer for tvOS
        #if os(tvOS)
            focusLayer.frame = layer.bounds
            layer.addSublayer(focusLayer)
        #endif
        commonInit()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /**
        Extra init setup
     */
    private func commonInit() {
        addSubview(contentStackView)
        addSubview(changeIndicatorImageView)
        addSubview(suspendedImageView)

        NSLayoutConstraint.activate([
            contentStackView.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor),
            contentStackView.trailingAnchor.constraint(equalTo: layoutMarginsGuide.trailingAnchor),
            contentStackView.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor),
            contentStackView.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor),

            changeIndicatorImageView.heightAnchor.constraint(equalToConstant: 8),
            changeIndicatorImageView.widthAnchor.constraint(equalTo: changeIndicatorImageView.heightAnchor),
        ])

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapOnSelf))
        addGestureRecognizer(tapGesture)
    }

    @objc
    private func didTapOnSelf() {
        guard let marketOption = marketOption else { return }

        if isSelected {
            delegate?.didDeselectOption(option: marketOption, uniqueID: uniqueID)
            return
        }
        delegate?.didSelectOption(option: marketOption, uniqueID: uniqueID)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        suspendedImageView.frame = CGRect(x: 0, y: bounds.height / 2 - 10, width: bounds.width, height: 20)
        // Change size of focus layer acordingly
        #if os(tvOS)
            focusLayer.frame = layer.bounds
        #endif
    }

    /**
        Market option setter
     */
    private func didSetMarketOption() {
        // If the market description is missing
        // odds should be applied to the market description label
        // instead of the odds label
        if let description = marketOption?.description, shouldHideDescription == false {
            descriptionLabel.text = description
            oddsLabel.text = marketOption?.odds
            NSLayoutConstraint.activate([
                changeIndicatorImageView.leadingAnchor.constraint(equalTo: descriptionLabel.trailingAnchor, constant: 4),
                changeIndicatorImageView.centerYAnchor.constraint(equalTo: descriptionLabel.centerYAnchor),

            ])
        } else {
            oddsLabel.text = marketOption?.odds
            NSLayoutConstraint.activate([
                changeIndicatorImageView.leadingAnchor.constraint(equalTo: oddsLabel.trailingAnchor, constant: 4),
                changeIndicatorImageView.centerYAnchor.constraint(equalTo: oddsLabel.centerYAnchor),
            ])
        }

        setupSuspendedState()
    }

    private func setupSuspendedState() {
        let isSuspended = marketOption?.isSuspended ?? false

        if isSuspended == true || forceSuspension == true {
            // Configures elements for suspended state
            suspendedImageView.isHidden = false
            contentStackView.isHidden = true
            changeIndicatorImageView.isHidden = true
            isUserInteractionEnabled = false
            return
        }

        // Configures elements for regular state
        suspendedImageView.isHidden = true
        contentStackView.isHidden = false
        changeIndicatorImageView.isHidden = false
        isUserInteractionEnabled = true
    }

    internal func selectionChanged() {
        if isSelected {
            type = .selectedOption
            descriptionLabel.type = .selectedOptionDescription
            oddsLabel.type = .selectedOption
        } else {
            type = .unselectedOption
            descriptionLabel.type = .unselectedOptionDescription
            oddsLabel.type = .unselectedOption
        }
    }

    private func setState(state: LLMarketOptionState, previousState: LLMarketOptionState?) {
        if forceSuspension == true { return }
        // Recover from incorrect state
        if let previousState = previousState,
           previousState != state, previousState == .incorrect
        {
            alpha = 1
        }

        switch state {
        case .correct:
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.oddsLabel.textColor = LLUIConstants.DefaultAccentColor
                self.descriptionLabel.textColor = LLUIConstants.DefaultAccentColor
            }
            isUserInteractionEnabled = false
        case .incorrect:
            alpha = 0.5
            isUserInteractionEnabled = false
        case .suspended:
            isUserInteractionEnabled = false
        default:
            isUserInteractionEnabled = true
        }
    }

    public func setMarketOptions(current: LLMarketOptionModel?, previous: LLMarketOptionModel?) {
        marketOption = current

        if let newState = current?.state {
            setState(state: newState, previousState: previous?.state)
        }

        if let newOddsString = current?.odds,
           let newOdds = Int(newOddsString),
           let oldOddsString = previous?.odds,
           let oldOdds = Int(oldOddsString)
        {
            // Don't animate if markets are suspended
            let isSuspended = marketOption?.isSuspended ?? false
            if isSuspended == true || forceSuspension == true {
                return
            }

            if oldOdds > newOdds {
                changeIndicatorImageView.image = UIImage.embetIconWithName(iconName: .arrowDown)
                EmBetBasicAnimator.playBlinkAnimation(view: changeIndicatorImageView)
            } else if oldOdds < newOdds {
                changeIndicatorImageView.image = UIImage.embetIconWithName(iconName: .arrowUp)
                EmBetBasicAnimator.playBlinkAnimation(view: changeIndicatorImageView)
            }
        }
    }
}
