//
//  LLSelectableFollowUpResultsOverlay.swift
//
//
//  Created by Ljupco Nastevski on 24.6.24.
//

import UIKit

internal class LLSelectableFollowUpResultsOverlay: LLThemableView {
    enum OptionState {
        case selectedIncorrect
        case selectedCorrect
        case unselectedIncorrect
        case unselectedCorrect
    }

    override class var layerClass: AnyClass {
        return LLTranparentThemableLayer.self
    }

    private var didShowInitialResults: Bool = false

    private var percentage: CGFloat = 0

    private var numberOfVotesText: String?

    public var optionState: OptionState = .unselectedIncorrect

    private var isSelected: Bool = false {
        didSet {
            switch optionState {
            case .selectedIncorrect:
                progressView.type = .selectedIncorrectOptionBar
                type = .selectedIncorrectOption
            case .selectedCorrect:
                progressView.type = .selectedCorrectOptionBar
                type = .selectedCorrectOption
            case .unselectedIncorrect:
                progressView.type = .unselectedIncorrectOptionBar
                type = .unselectedIncorrectOption
            case .unselectedCorrect:
                progressView.type = .unselectedCorrectOptionBar
                type = .unselectedCorrectOption
            }
        }
    }

    private var barDirection: LLOptionResultsOverlayBarDirection = .horizontal {
        didSet {
            switch barDirection {
            case .horizontal:
                progressView.direction = .leftToRight
            case .vertical:
                progressView.direction = .bottomToTop
            }
        }
    }

    private let progressView: LLProgressView = {
        let pv = LLProgressView()
        pv.translatesAutoresizingMaskIntoConstraints = false
        return pv
    }()

    required init() {
        super.init(themeType: .unselectedCorrectOptionBar)
        backgroundColor = LLUIConstants.BetResultsOverlayBackgroundColor
        commonInit()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func applyTheme(theme: LLThemeViewModel?) {
        // Override with static color
        if type == .unselectedOption, didShowInitialResults == true {
            theme?.borderColor = LLUIConstants.UnselectedColorBorder
        }
        super.applyTheme(theme: theme)
    }

    private func commonInit() {
        addSubview(progressView)

        NSLayoutConstraint.activate([
            progressView.leadingAnchor.constraint(equalTo: leadingAnchor),
            progressView.trailingAnchor.constraint(equalTo: trailingAnchor),
            progressView.topAnchor.constraint(equalTo: topAnchor),
            progressView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
}

extension LLSelectableFollowUpResultsOverlay: LLOptionResultsOverlay {
    func showResults() {
        didShowInitialResults = true
    }

    func setProgressBarDirection(direction: LLOptionResultsOverlayBarDirection) {
        barDirection = direction
    }

    func setSelected(selected: Bool) {
        isSelected = selected
    }

    func setPercentage(percentage: CGFloat) {
        self.percentage = percentage
    }

    func setNumberOfVotesText(text: String?) {
        numberOfVotesText = text
    }

    func updateVotesLabel() {}

    func updateBar() {
        DispatchQueue.main.async { [weak self] in
            self?.progressView.progress = self?.percentage ?? 0
        }
    }
}
