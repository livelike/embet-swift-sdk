//
//  LLSelectableResultsOverlay.swift
//
//
//  Created by Ljupco Nastevski on 24.3.22.
//

import UIKit

internal class LLSelectableResultsOverlay: LLThemableView {
    override class var layerClass: AnyClass {
        return LLTranparentThemableLayer.self
    }

    private var didShowInitialResults: Bool = false

    private var percentage: CGFloat = 0

    private var numberOfVotesText: String?

    private var isSelected: Bool = false {
        didSet {
            if isSelected == true {
                votesLbl.type = .selectedOptionPercentage
                progressView.type = .selectedOptionBar
                type = .selectedOption
            } else {
                votesLbl.type = .unselectedOptionPercentage
                progressView.type = .unselectedOptionBar
                type = .unselectedOption
            }
        }
    }

    private var barDirection: LLOptionResultsOverlayBarDirection = .horizontal {
        didSet {
            switch barDirection {
            case .horizontal:
                progressView.direction = .leftToRight
            case .vertical:
                progressView.direction = .bottomToTop
            }
        }
    }

    private lazy var votesLbl: LLThemableLabel = {
        let lbl = LLThemableLabel(themeType: .unselectedOptionPercentage)
        lbl.textAlignment = .center
        lbl.textColor = .white
        lbl.numberOfLines = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()

    private let progressView: LLProgressView = {
        let pv = LLProgressView()
        pv.translatesAutoresizingMaskIntoConstraints = false
        return pv
    }()

    required init() {
        super.init(themeType: .unselectedOptionBar)
        backgroundColor = LLUIConstants.BetResultsOverlayBackgroundColor
        commonInit()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func applyTheme(theme: LLThemeViewModel?) {
        // Override with static color
        if type == .unselectedOption, didShowInitialResults == true {
            theme?.borderColor = LLUIConstants.UnselectedColorBorder
        }
        super.applyTheme(theme: theme)
    }

    private func commonInit() {
        votesLbl.alpha = 0

        addSubview(progressView)
        addSubview(votesLbl)

        NSLayoutConstraint.activate([
            votesLbl.leadingAnchor.constraint(equalTo: leadingAnchor),
            votesLbl.trailingAnchor.constraint(equalTo: trailingAnchor),
            votesLbl.topAnchor.constraint(equalTo: topAnchor),
            votesLbl.bottomAnchor.constraint(equalTo: bottomAnchor),
            progressView.leadingAnchor.constraint(equalTo: leadingAnchor),
            progressView.trailingAnchor.constraint(equalTo: trailingAnchor),
            progressView.topAnchor.constraint(equalTo: topAnchor),
            progressView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
}

extension LLSelectableResultsOverlay: LLOptionResultsOverlay {
    func showResults() {
        didShowInitialResults = true
        votesLbl.alpha = 1
    }

    func setProgressBarDirection(direction: LLOptionResultsOverlayBarDirection) {
        barDirection = direction
    }

    func setSelected(selected: Bool) {
        isSelected = selected
    }

    func setPercentage(percentage: CGFloat) {
        self.percentage = percentage
    }

    func setNumberOfVotesText(text: String?) {
        numberOfVotesText = text
    }

    func updateVotesLabel() {
        guard let numberOfVotesText = numberOfVotesText else {
            votesLbl.text = "\(percentage.percentageString)"
            return
        }

        switch barDirection {
        case .horizontal:
            votesLbl.text = "\(percentage.percentageString) \(numberOfVotesText)"
        case .vertical:
            votesLbl.text = "\(percentage.percentageString)\n\(numberOfVotesText)"
        }
    }

    func updateBar() {
        DispatchQueue.main.async { [weak self] in
            self?.progressView.progress = self?.percentage ?? 0
        }
    }
}
