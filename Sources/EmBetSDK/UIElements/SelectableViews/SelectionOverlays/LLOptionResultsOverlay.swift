//
//  LLOptionResultsOverlay.swift
//
//
//  Created by Ljupco Nastevski on 8.2.24.
//

import Foundation

internal enum LLOptionResultsOverlayBarDirection {
    /**
        Horizontal - left to right
     */
    case horizontal
    /**
        Vertical - bottom to top
     */
    case vertical
}

internal protocol LLOptionResultsOverlay: LLThemableView {
    /**
        Reveals the results
     */
    func showResults()
    /**
        The bars drawing direction
     */
    func setProgressBarDirection(direction: LLOptionResultsOverlayBarDirection)
    /**
        The bars drawing direction
     */
    func setSelected(selected: Bool)
    /**
        The percentage of votes
     */
    func setPercentage(percentage: CGFloat)
    /**
        Sets the number of votes text
     */
    func setNumberOfVotesText(text: String?)
    /**
        Updates the votes label text
     */
    func updateVotesLabel()
    /**
        Updates the votes bar
     */
    func updateBar()
}
