//
//  VideoPlaybackView.swift
//
//
//  Created by Ljupco Nastevski on 13.9.22.
//

import AVFoundation
import UIKit

/**
    VideoPlaybackView is used for video playback.
    AutoRepeats videos.
 */
internal class VideoPlaybackView: UIView {
    private let videoUrl: URL

    private lazy var asset: AVAsset = .init(url: videoUrl)

    private lazy var playerItem: AVPlayerItem = .init(asset: asset)

    private lazy var player: AVQueuePlayer = .init(playerItem: playerItem)

    private var looper: AVPlayerLooper?

    private lazy var videoLayer: AVPlayerLayer = .init(player: player)

    // Key-value observing context
    private var observer: NSKeyValueObservation?

    private lazy var muteButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(.embetIconWithName(iconName: .mute), for: .normal)
        button.addTarget(self, action: #selector(didPressMuteButton), for: .touchUpInside)
        return button
    }()

    deinit {
        observer?.invalidate()
    }

    required init(videoUrl: URL) {
        self.videoUrl = videoUrl
        super.init(frame: .zero)
        initialSetup()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /*
        Recalculating the intrinsicContentSize to have a properly drawn player.
     */
    override var intrinsicContentSize: CGSize {
        if videoLayer.videoRect != .zero {
            return videoLayer.videoRect.size.embedInSizePreservingWidth(parentSize: frame.size)
        }
        return frame.size.toSixteenByNinePreservingWidth()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        videoLayer.frame = bounds
        invalidateIntrinsicContentSize()
    }

    private func initialSetup() {
        layer.addSublayer(videoLayer)
        looper = AVPlayerLooper(player: player, templateItem: playerItem)
        player.isMuted = true

        // https://developer.apple.com/documentation/avfoundation/media_playback/observing_playback_state
        observer = player.observe(\.status, options: [.initial]) { [weak self] mediaPlayer, _ in
            // Switch over status value
            switch mediaPlayer.status {
            case .readyToPlay:
                self?.player.play()
                self?.invalidateIntrinsicContentSize()
                self?.addPlayerControlls()
            default:
                break
            }
        }
    }

    private func addPlayerControlls() {
        addSubview(muteButton)
        NSLayoutConstraint.activate([
            muteButton.widthAnchor.constraint(equalTo: muteButton.heightAnchor),
            muteButton.widthAnchor.constraint(equalToConstant: 24),
            trailingAnchor.constraint(equalTo: muteButton.trailingAnchor, constant: 8),
            bottomAnchor.constraint(equalTo: muteButton.bottomAnchor, constant: 8),
        ])
    }

    @objc private func didPressMuteButton() {
        player.isMuted = !player.isMuted
        muteButton.setImage(
            player.isMuted == true ? .embetIconWithName(iconName: .mute) : .embetIconWithName(iconName: .unmute),
            for: .normal
        )
    }
}
