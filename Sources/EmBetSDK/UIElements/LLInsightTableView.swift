//
//  LLInsightTableView.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 10.3.22.
//

import UIKit

internal class LLInsightTableView: LLThemableView {
    let elements: [LLWidgetTableElementModel]

    private weak var themeBuilder: LLThemeBuilder?

    init(elements: [LLWidgetTableElementModel], themeBuilder: LLThemeBuilder) {
        self.elements = elements
        super.init(themeType: .insightTable)
        self.themeBuilder = themeBuilder
        commonInit()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }

    private func commonInit() {
        asPlainTableView()
    }

    func asPlainTableView() {
        var prevRow = 0
        var tableViews: [UIView] = []

        while true {
            let rows = elements.filter { $0.rowIndex == prevRow }.sorted(by: { $0.columnIndex < $1.columnIndex })

            if rows.count > 0 {
                prevRow += 1

                let views = rows.map { elem -> UIView in

                    let lblTheme: LLThemableType = elem.isHighlighted ? .insightTableCellHighlighted : .insightTableCell
                    let lbl = LLThemableLabel(themeType: lblTheme)
                    lbl.textAlignment = .center
                    lbl.text = elem.value

                    return themeBuilder?.embedInHolder(themeType: lblTheme, view: lbl) ?? UIView()
                }

                let rowStackView = UIStackView(arrangedSubviews: views)
                rowStackView.axis = .horizontal
                rowStackView.distribution = .fillEqually

                tableViews.append(rowStackView)

                // Add horisontal line between the first and second row
                if prevRow == 1 {
                    let view = UIView()
                    view.translatesAutoresizingMaskIntoConstraints = false
                    view.backgroundColor = .white
                    view.heightAnchor.constraint(equalToConstant: 1).isActive = true
                    tableViews.append(view)
                }

            } else {
                break
            }
        }

        let tableStackView = UIStackView(arrangedSubviews: tableViews)
        tableStackView.translatesAutoresizingMaskIntoConstraints = false
        tableStackView.distribution = .fill
        tableStackView.axis = .vertical

        addSubview(tableStackView)

        NSLayoutConstraint.activate([
            tableStackView.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor),
            tableStackView.trailingAnchor.constraint(equalTo: layoutMarginsGuide.trailingAnchor),
            tableStackView.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor),
            tableStackView.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor),
        ])
    }
}
