//
//  WidgetCollectionViewCell.swift
//
//
//  Created by Ljupco Nastevski on 9.10.23.
//

import UIKit

internal class WidgetCollectionViewCell: UICollectionViewCell {
    var widget: UIViewController?

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func prepareForReuse() {
        widget?.willMove(toParent: nil)
        widget?.removeFromParent()
        contentView.subviews.first?.removeFromSuperview()
        widget = nil
    }
}
