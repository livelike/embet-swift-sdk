//
//  LLVerticalOddsTableCell.swift
//
//
//  Created by Ljupco Nastevski on 29.8.24.
//

import UIKit

/**
    Vertical market representation
 */
internal class LLVerticalOddsTableCell: UIView, LLOddsTableCell {
    public weak var delegate: LLOddsTableViewCellDelegate?
    /**
        Separator view
     */
    public lazy var separatorView: UIView = {
        let separator = LLThemableSeparator(themeType: .separator)
        separator.frame = CGRect(x: 0, y: 0, width: bounds.width, height: 1.0)
        separator.autoresizingMask = [.flexibleWidth]
        return separator
    }()

    /**
        The base view of the cell, this is the main themable UI object
     */
    private let baseView: LLThemableView = {
        let baseView = LLThemableView(themeType: .optionRowVertical)
        baseView.translatesAutoresizingMaskIntoConstraints = false
        return baseView
    }()

    /**
        Content stack view used for creating the UI
     */
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [marketLabel, optionTable])
        stackView.spacing = 8
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    /**
        Content stack view used for creating the UI
     */
    private let optionTable: LLTableView = {
        let optionTable = LLTableView()
        optionTable.extendsToContentHeight()
        optionTable.setCellSpacing(spacing: 8)
        return optionTable
    }()

    /**
        Bool that indicates if the oods should be suspended
     */
    var forceSuspension: Bool = false

    /**
        The label that holds the name of the market
     */
    private let marketLabel: LLThemableLabel = {
        let label = LLThemableLabel(themeType: .title)
        label.textAlignment = .left
        return label
    }()

    /**
        The bet details model for the current cell
     */
    private var betDetails: LLOddsBetDetailsDiffModel

    let selectionCoordinator: LLBaseSelectableViewCoordinator?

    required init(betDetails: LLOddsBetDetailsDiffModel,
                  selectionCoordinator: LLBaseSelectableViewCoordinator?,
                  forceSuspension: Bool = false)
    {
        self.betDetails = betDetails
        self.selectionCoordinator = selectionCoordinator
        self.forceSuspension = forceSuspension
        super.init(frame: .zero)
        commonInit()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /**
        Default cell setup
     */
    private func commonInit() {
        backgroundColor = .clear
        // Adding subviews
        addSubview(baseView)
        baseView.addSubview(contentStackView)
        // Setting up constraints
        NSLayoutConstraint.activate([
            // Background constraints
            baseView.leadingAnchor.constraint(equalTo: leadingAnchor),
            baseView.trailingAnchor.constraint(equalTo: trailingAnchor),
            baseView.topAnchor.constraint(equalTo: topAnchor),
            baseView.bottomAnchor.constraint(equalTo: bottomAnchor),
            // Content stack view constraints
            contentStackView.leadingAnchor.constraint(equalTo: baseView.leadingAnchor),
            contentStackView.trailingAnchor.constraint(equalTo: baseView.trailingAnchor),
            contentStackView.topAnchor.constraint(equalTo: baseView.topAnchor, constant: 8),
            contentStackView.bottomAnchor.constraint(equalTo: baseView.bottomAnchor, constant: -8),
        ])

        // Add the separator view
        #if os(iOS)
            addSubview(separatorView)
        #endif
        setModel(model: betDetails)
    }

    /**
        Sets the `LLOddsBetDetailsModel`
     */
    func setModel(model: LLOddsBetDetailsDiffModel) {
        betDetails = model

        // Sets up the market name
        if marketLabel.text != model.marketName {
            marketLabel.text = betDetails.marketName
        }

        if let previousOptions = model.previousOptions,
           previousOptions.count == betDetails.currentOptions.count,
           !self.forceSuspension,
           optionTable.elements.count != 0
        {
            for (index, option) in model.currentOptions.enumerated() {
                let cell = optionTable.view(for: index) as? LLDescriptiveMarketOptionView
                cell?.setModel(market: option, betDetails: model)
            }

            return
        }

        var cells: [LLDescriptiveMarketOptionView] = []

        for option in betDetails.currentOptions {
            let cell = LLDescriptiveMarketOptionView(market: option, betDetails: betDetails, forceSuspernsion: forceSuspension)
            cells.append(cell)
        }

        cells.forEach { marketView in
            guard let selectionCoordinator = selectionCoordinator else { return }
            if !selectionCoordinator.views.contains(marketView.optionView) {
                selectionCoordinator.views.append(marketView.optionView)
            }
            marketView.optionView.delegate = self
        }

        optionTable.reload(elements: cells)
    }

    /**
        Returns the market ID.
     */
    func getMarketID() -> String {
        return betDetails.marketID
    }

    /**
        Selects an option with compound id
     */
    func selectOptionWithID(option: LLMarketOptionModel?, uniqueID: String) {
        // If market is suspended, dont make a selection
        if let selectedOption = selectionCoordinator?.views.first(where: { $0.uniqueID == uniqueID }),
           let isSuspended = selectedOption.marketOption?.isSuspended,
           isSuspended == true
        {
            return
        }
        // Only call the delegate if an option parameter was sent
        if let option = option {
            let market = LLOddsBetDetailsModel(marketName: betDetails.marketName, marketID: betDetails.marketID, marketPresentation: betDetails.marketPresentation, marketOptions: betDetails.currentOptions)
            delegate?.oddsCellDidSelectOption(selection: LLOddsSelection(option: option, market: market, uniqueID: uniqueID))
        }
        selectionCoordinator?.didSelect(id: uniqueID)
    }

    /**
        Selects an option with compound id
     */
    func deselectOptionWithID(option: LLMarketOptionModel? = nil, uniqueID: String) {
        // Only call the delegate if an option parameter was sent
        if let option = option {
            let market = LLOddsBetDetailsModel(marketName: betDetails.marketName, marketID: betDetails.marketID, marketPresentation: betDetails.marketPresentation, marketOptions: betDetails.currentOptions)
            delegate?.oddsCellDidDeselectOption(selection: LLOddsSelection(option: option, market: market, uniqueID: uniqueID))
        }
        selectionCoordinator?.deselectAll()
    }
}

extension LLVerticalOddsTableCell: LLMarketOptionViewDelegate {
    func didSelectOption(option: LLMarketOptionModel, uniqueID: String) {
        selectOptionWithID(option: option, uniqueID: uniqueID)
    }

    func didDeselectOption(option: LLMarketOptionModel, uniqueID: String) {
        deselectOptionWithID(option: option, uniqueID: uniqueID)
    }
}
