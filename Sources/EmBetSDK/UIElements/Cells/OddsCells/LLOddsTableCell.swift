//
//  LLOddsTableCell.swift
//
//
//  Created by Ljupco Nastevski on 30.8.24.
//

import UIKit

internal struct LLOddsSelection {
    let option: LLMarketOptionModel
    let market: LLOddsBetDetailsModel
    let uniqueID: String
}

internal protocol LLOddsTableViewCellDelegate: AnyObject {
    func oddsCellDidSelectOption(selection: LLOddsSelection)
    func oddsCellDidDeselectOption(selection: LLOddsSelection)
}

internal protocol LLOddsTableCell: UIView {
    func getMarketID() -> String
    func setModel(model: LLOddsBetDetailsDiffModel)
    func selectOptionWithID(option: LLMarketOptionModel?, uniqueID: String)
}
