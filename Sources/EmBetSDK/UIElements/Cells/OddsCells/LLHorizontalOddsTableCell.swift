//
//  LLHorizontalOddsTableCell.swift
//
//
//  Created by Ljupco Nastevski on 13.11.23.
//

import UIKit

/**
    Horizontal market representation
 */
internal class LLHorizontalOddsTableCell: UIView, LLOddsTableCell {
    public weak var delegate: LLOddsTableViewCellDelegate?
    /**
        Separator view
     */
    private lazy var separatorView: UIView = {
        let separator = LLThemableSeparator(themeType: .separator)
        separator.frame = CGRect(x: 0, y: 0, width: bounds.width, height: 1.0)
        separator.autoresizingMask = [.flexibleWidth]
        return separator
    }()

    /**
        The base view of the cell, this is the main themable UI object
     */
    private let baseView: LLThemableView = {
        let baseView = LLThemableView(themeType: .optionRow)
        baseView.translatesAutoresizingMaskIntoConstraints = false
        return baseView
    }()

    /**
        Content stack view used for creating the UI
     */
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [leftMarketOptionStackView, marketLabel, rightMarketOptionStackView])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = 8
        return stackView
    }()

    /**
        The left option view for this market, that will present the odds
     */
    lazy var leftOddsOption: LLMarketOptionView = .init()

    /**
        StackView that holds the market option view and lock icon
     */
    private lazy var leftMarketOptionStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [leftOddsOption, leftMarketSuspendedImageView])
        stackView.setContentHuggingPriority(.defaultLow, for: .horizontal)
        stackView.spacing = 8
        return stackView
    }()

    private let leftMarketSuspendedImageView: UIImageView = {
        let imageView = LLThemableImageView(type: .lockIcon)
        imageView.alpha = 0
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.image = .embetIconWithName(iconName: .lock)
        imageView.widthAnchor.constraint(equalToConstant: 20.0).isActive = true
        return imageView
    }()

    /**
        The right option view for this market, that will present the odds
     */
    lazy var rightOddsOption: LLMarketOptionView = .init()

    /**
        StackView that holds the market option view and lock icon
     */
    private lazy var rightMarketOptionStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [rightMarketSuspendedImageView, rightOddsOption])
        stackView.setContentHuggingPriority(.defaultLow, for: .horizontal)
        stackView.spacing = 8
        return stackView
    }()

    private let rightMarketSuspendedImageView: UIImageView = {
        let imageView = LLThemableImageView(type: .lockIcon)
        imageView.alpha = 0
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.image = .embetIconWithName(iconName: .lock)
        imageView.widthAnchor.constraint(equalToConstant: 20.0).isActive = true
        return imageView
    }()

    /**
        Bool that indicates if the oods should be suspended
     */
    var forceSuspension: Bool = false

    /**
        The label that holds the name of the market
     */
    private let marketLabel: LLThemableLabel = {
        let label = LLThemableLabel(themeType: .optionRow)
        label.textAlignment = .center
        return label
    }()

    /**
        The bet details model for the current cell
     */
    private var betDetails: LLOddsBetDetailsDiffModel

    var selectionCoordinator: LLBaseSelectableViewCoordinator? {
        didSet {
            selectionCoordinator?.views.append(contentsOf: [leftOddsOption, rightOddsOption])
        }
    }

    required init(betDetails: LLOddsBetDetailsDiffModel,
                  forceSuspension: Bool = false)
    {
        self.betDetails = betDetails
        self.forceSuspension = forceSuspension
        super.init(frame: .zero)
        commonInit()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /**
        Default cell setup
     */
    private func commonInit() {
        // Odds deelgates
        leftOddsOption.delegate = self
        leftOddsOption.forceSuspension = forceSuspension
        rightOddsOption.delegate = self
        rightOddsOption.forceSuspension = forceSuspension

        backgroundColor = .clear
        // Adding subviews
        addSubview(baseView)
        baseView.addSubview(contentStackView)
        // Setting up constraints
        NSLayoutConstraint.activate([
            // Background constraints
            baseView.leadingAnchor.constraint(equalTo: leadingAnchor),
            baseView.trailingAnchor.constraint(equalTo: trailingAnchor),
            baseView.topAnchor.constraint(equalTo: topAnchor),
            baseView.bottomAnchor.constraint(equalTo: bottomAnchor),
            // Content stack view constraints
            contentStackView.leadingAnchor.constraint(equalTo: baseView.leadingAnchor),
            contentStackView.trailingAnchor.constraint(equalTo: baseView.trailingAnchor),
            contentStackView.topAnchor.constraint(equalTo: baseView.topAnchor, constant: 8),
            contentStackView.bottomAnchor.constraint(equalTo: baseView.bottomAnchor, constant: -8),
            // Odd option equal width
            leftOddsOption.widthAnchor.constraint(equalTo: rightOddsOption.widthAnchor),
            leftOddsOption.widthAnchor.constraint(equalToConstant: 72.0),
        ])

        // Add the separator view
        #if os(iOS)
            addSubview(separatorView)
        #endif
        configureMarket(betDetails: betDetails)
    }

    /**
        Sets the `LLOddsBetDetailsModel`
     */
    private func configureMarket(betDetails: LLOddsBetDetailsDiffModel) {
        self.betDetails = betDetails
        // Sets up the market name
        marketLabel.text = betDetails.marketName

        if forceSuspension == true { return }

        // Sets up left and right market option
        leftOddsOption.marketID = betDetails.marketID
        leftOddsOption.setMarketOptions(current: betDetails.currentOptions.first, previous: nil)
        if let marketOptionState = betDetails.currentOptions.first?.state,
           marketOptionState == .correct || marketOptionState == .incorrect
        {
            leftMarketSuspendedImageView.alpha = 1.0
        }

        rightOddsOption.marketID = betDetails.marketID
        rightOddsOption.setMarketOptions(current: betDetails.currentOptions.last, previous: nil)
        if let marketOptionState = betDetails.currentOptions.last?.state,
           marketOptionState == .correct || marketOptionState == .incorrect
        {
            rightMarketSuspendedImageView.alpha = 1.0
        }
    }

    func setModel(model: LLOddsBetDetailsDiffModel) {
        if forceSuspension == true { return }

        if betDetails.marketName != model.marketName {
            marketLabel.text = model.marketName
        }

        if let newMarketOption = model.currentOptions.first,
           newMarketOption.isDifferent(model: leftOddsOption.marketOption)
        {
            leftOddsOption.setMarketOptions(current: newMarketOption, previous: leftOddsOption.marketOption)
            if let marketOptionState = newMarketOption.state,
               marketOptionState == .correct || marketOptionState == .incorrect
            {
                leftMarketSuspendedImageView.alpha = 1.0
            }
        }

        if let newMarketOption = model.currentOptions.last,
           newMarketOption.isDifferent(model: rightOddsOption.marketOption)
        {
            rightOddsOption.setMarketOptions(current: newMarketOption, previous: rightOddsOption.marketOption)
            if let marketOptionState = newMarketOption.state,
               marketOptionState == .correct || marketOptionState == .incorrect
            {
                rightMarketSuspendedImageView.alpha = 1.0
            }
        }

        betDetails = model
    }

    /**
        Returns the market ID.
     */
    func getMarketID() -> String {
        return betDetails.marketID
    }

    /**
        Selects an option with compound id
     */
    func selectOptionWithID(option: LLMarketOptionModel? = nil, uniqueID: String) {
        // If market is suspended, dont make a selection
        if let selectedOption = [leftOddsOption, rightOddsOption].first(where: { $0.uniqueID == uniqueID }),
           let isSuspended = selectedOption.marketOption?.isSuspended,
           isSuspended == true
        {
            return
        }
        // Only call the delegate if an option parameter was sent
        if let option = option {
            let market = LLOddsBetDetailsModel(marketName: betDetails.marketName, marketID: betDetails.marketID, marketPresentation: betDetails.marketPresentation, marketOptions: betDetails.currentOptions)
            delegate?.oddsCellDidSelectOption(selection: LLOddsSelection(option: option, market: market, uniqueID: uniqueID))
        }
        selectionCoordinator?.didSelect(id: uniqueID)
    }

    /**
        Selects an option with compound id
     */
    func deselectOptionWithID(option: LLMarketOptionModel? = nil, uniqueID: String) {
        // Only call the delegate if an option parameter was sent
        if let option = option {
            let market = LLOddsBetDetailsModel(marketName: betDetails.marketName, marketID: betDetails.marketID, marketPresentation: betDetails.marketPresentation, marketOptions: betDetails.currentOptions)
            delegate?.oddsCellDidDeselectOption(selection: LLOddsSelection(option: option, market: market, uniqueID: uniqueID))
        }
        selectionCoordinator?.deselectAll()
    }
}

extension LLHorizontalOddsTableCell: LLMarketOptionViewDelegate {
    func didSelectOption(option: LLMarketOptionModel, uniqueID: String) {
        selectOptionWithID(option: option, uniqueID: uniqueID)
    }

    func didDeselectOption(option: LLMarketOptionModel, uniqueID: String) {
        deselectOptionWithID(option: option, uniqueID: uniqueID)
    }
}
