//
//  LLTimerAnimatableView.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 7.3.22.
//

import UIKit

internal enum LLTimerAnimationType {
    case round
    case bar
}

internal class LLTimerAnimatableView: UIView {
    /**
        The property to determine when the timer was started.
        When a user puts the app in the background, timers are not reliable
        so a continuation mechanism is needed.
     */
    private var animationStartDate: Date?

    /**
        Animation layer
     */
    private weak var animationLayer: CALayer?

    /**
        Shadow layer.
        Present only of the animation is emptying
     */
    private weak var shadowLayer: CALayer?

    /**
        Animation layer
     */
    private var animationOffset: Double = 0

    /**
        Bar Width
     */
    var barWidth: CGFloat = 1.0

    /**
        Animation duration.
     */
    var duration: TimeInterval = 10.0

    /**
        Animation Type.
     */
    var animationType: LLTimerAnimationType = .round

    /**
        Animation stroke color
     */
    var timerColor: LLThemeBackgroundModel = LLThemeBackgroundModel.fill(.init(color: .darkGray))

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    init(barWidth: CGFloat, frame: CGRect) {
        self.barWidth = barWidth
        super.init(frame: frame)
        commonInit()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func commonInit() {
        backgroundColor = .clear

        NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification, object: nil, queue: .main) { [weak self] _ in

            guard let self = self else { return }

            if let dateStarted = self.animationStartDate {
                let elapsedTime = dateStarted.timeIntervalSinceNow
                self.animationOffset = abs(elapsedTime)
                self.startAnimating()
            }
        }
    }

    /**
        Starts the animation
     */
    func startAnimating(offsetValue: Double = 0) {
        if animationStartDate == nil {
            animationStartDate = Date()
        }

        switch animationType {
        case .round:
            animateWithCircularAnimation(offsetValue: offsetValue)
        case .bar:
            animateWithBarAnimation(offsetValue: offsetValue)
        }
    }

    /**
        Starts animation with circular timer animation.
     */
    private func animateWithCircularAnimation(offsetValue: Double) {
        let path = UIBezierPath(arcCenter: CGPoint(x: bounds.midX, y: bounds.midY), radius: frame.width / 2 - barWidth / 2, startAngle: (3 * .pi) / 2, endAngle: -.pi / 2, clockwise: false)

        animateStrokeAlong(path: path.cgPath, offsetValue: offsetValue)
    }

    /**
        Starts animation with progress bar timer animation.
     */
    private func animateWithBarAnimation(offsetValue: Double) {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: bounds.midY))
        path.addLine(to: CGPoint(x: bounds.maxX, y: bounds.midY))

        animateStrokeAlong(path: path.cgPath, fills: true, offsetValue: offsetValue)
    }

    /**
        Executes annimation along the provided path.
        Fills property determines if the animation value will start at 0 and go to 1.
     */
    private func animateStrokeAlong(path: CGPath, fills: Bool = false, offsetValue _: Double) {
        CATransaction.begin()

        let gradientLayer = CAGradientLayer()
        gradientLayer.masksToBounds = false
        gradientLayer.frame = bounds

        switch timerColor {
        case let .fill(fill):
            gradientLayer.backgroundColor = fill.color.cgColor
        case let .uniformGradient(gradient):
            gradientLayer.colors = gradient.colors.map { $0.cgColor }
            gradientLayer.calculatePoints(for: gradient.direction)
        default: return
        }

        let layer = CAShapeLayer()
        layer.strokeColor = UIColor.black.cgColor
        layer.lineWidth = barWidth
        layer.fillColor = UIColor.clear.cgColor
        layer.masksToBounds = false
        layer.path = path

        gradientLayer.mask = layer

        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.isRemovedOnCompletion = false

        animation.duration = duration - animationOffset

        CATransaction.setCompletionBlock { [weak self] in
            self?.shadowLayer?.removeFromSuperlayer()
            gradientLayer.removeFromSuperlayer()
        }

        if fills {
            animation.fromValue = 0 + (animationOffset / duration)
            animation.toValue = 1.0
        } else {
            animation.fromValue = 1 - (animationOffset / duration)
            animation.toValue = 0.0

            // Shadow layer when timer is emptying
            let shadowLayer = CAShapeLayer()
            shadowLayer.strokeColor = LLUIConstants.RoundTimerShadowColor.cgColor
            shadowLayer.lineWidth = barWidth
            shadowLayer.fillColor = UIColor.clear.cgColor
            shadowLayer.masksToBounds = false
            shadowLayer.path = path
            self.shadowLayer = shadowLayer
            self.layer.addSublayer(shadowLayer)
        }

        layer.add(animation, forKey: nil)

        CATransaction.commit()

        self.layer.addSublayer(gradientLayer)
        animationLayer = layer
    }
}
