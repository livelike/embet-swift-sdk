//
//  LLQRView.swift
//
//
//  Created by Ljupco Nastevski on 19.7.24.
//

import CoreImage.CIFilterBuiltins
import UIKit

/**
    UIView that shows an image view with a QR code.
 */
internal class LLQRView: UIView {
    /**
        The last code generated
     */
    private var lastGeneratedCode: String?

    private let qrImageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    required init() {
        super.init(frame: .zero)

        addSubview(qrImageView)
        NSLayoutConstraint.activate([
            qrImageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            qrImageView.trailingAnchor.constraint(equalTo: trailingAnchor),
            qrImageView.topAnchor.constraint(equalTo: topAnchor),
            qrImageView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])

        layoutIfNeeded()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /**
        Updates the QR code according to the string data.
        Sending `Nil` removes the already generated QR code.
     */
    public func generateCode(string: String?) {
        // Does not allow for duplicate generation
        if string == lastGeneratedCode {
            return
        }

        guard let string = string else {
            qrImageView.image = nil
            lastGeneratedCode = nil
            return
        }

        // Stores the code so subsequent calls with same
        // data dont trigger generation.
        lastGeneratedCode = string

        DispatchQueue.main.async { [weak self] in
            self?.generate(from: string) { image in
                if let image = image {
                    self?.qrImageView.image = image
                }
            }
        }
    }

    private func generate(from string: String, completion: (UIImage?) -> Void) {
        guard let data = string.data(using: String.Encoding.ascii) else {
            completion(nil)
            return
        }

        let qrCodeGenerator = CIFilter.qrCodeGenerator()
        qrCodeGenerator.message = data
        qrCodeGenerator.correctionLevel = "H"

        if let outputImage = qrCodeGenerator.outputImage {
            let scaleX = qrImageView.frame.size.width / outputImage.extent.size.width
            let scaleY = qrImageView.frame.size.height / outputImage.extent.size.height

            let transform = CGAffineTransform(scaleX: scaleX, y: scaleY)

            completion(UIImage(ciImage: outputImage.transformed(by: transform)))
            return
        }

        completion(nil)
    }
}
