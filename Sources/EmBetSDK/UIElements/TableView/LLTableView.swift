//
//  LLTableView.swift
//
//
//  Created by Ljupco Nastevski on 19.8.24.
//

import UIKit
/**
    Custom list view
 */
internal class LLTableView: UIScrollView {
    /**
        Stack view that aranges the cells verticaly
     */
    private let cellStackView = UIStackView(frame: .zero)

    /**
        Table view data source
     */
    public private(set) var elements: [UIView] = []

    /**
        Required init
     */
    required init() {
        super.init(frame: .zero)
        configureSelf()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /**
        Configures the stack view to be scrollable
     */
    private func configureSelf() {
        configureStackView()
        addSubview(cellStackView)

        NSLayoutConstraint.activate([
            // Set up the frame constraints
            cellStackView.leadingAnchor.constraint(equalTo: frameLayoutGuide.leadingAnchor),
            cellStackView.trailingAnchor.constraint(equalTo: frameLayoutGuide.trailingAnchor),
            cellStackView.topAnchor.constraint(equalTo: frameLayoutGuide.topAnchor),
            // Set up the content constraints
            cellStackView.leadingAnchor.constraint(equalTo: contentLayoutGuide.leadingAnchor),
            cellStackView.trailingAnchor.constraint(equalTo: contentLayoutGuide.trailingAnchor),
            cellStackView.topAnchor.constraint(equalTo: contentLayoutGuide.topAnchor),
            cellStackView.bottomAnchor.constraint(equalTo: contentLayoutGuide.bottomAnchor),
        ])
    }

    /**
        Cofigures the stack view
     */
    private func configureStackView() {
        cellStackView.translatesAutoresizingMaskIntoConstraints = false
        cellStackView.axis = .vertical
    }

    /**
        Sets the cell spacing
     */
    func setCellSpacing(spacing: CGFloat) {
        cellStackView.spacing = spacing
    }

    /**
        Reloads the data by removing all subview and calling the datasource to setup the new data
     */
    func reload(elements: [UIView]) {
        self.elements = elements
        cellStackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
        for element in elements {
            cellStackView.addArrangedSubview(element)
        }
    }

    /**
        Insert view at index
     */
    func insert(view: UIView, at index: Int) {
        cellStackView.insertArrangedSubview(view, at: index)
        elements.insert(view, at: index)
    }

    /**
        Switch index
     */
    func moveView(from index: Int, to toIndex: Int) {
        let viewToRemove = cellStackView.arrangedSubviews[index]
        cellStackView.insertArrangedSubview(viewToRemove, at: toIndex)
        let element = elements.remove(at: index)
        elements.insert(element, at: index)
    }

    /**
        Remove view at index
     */
    func remove(at index: Int) {
        let viewToRemove = cellStackView.arrangedSubviews[index]
        cellStackView.removeArrangedSubview(viewToRemove)
        elements.remove(at: index)
        viewToRemove.removeFromSuperview()
    }

    /**
        Remove view at index
     */
    func removeLast() {
        let lastIndex = cellStackView.arrangedSubviews.count - 1
        let viewToRemove = cellStackView.arrangedSubviews[lastIndex]
        cellStackView.removeArrangedSubview(viewToRemove)
        elements.removeLast()
        viewToRemove.removeFromSuperview()
    }

    /**
        Update view at index
     */
    func update(view: UIView, at index: Int) {
        remove(at: index)
        insert(view: view, at: index)
    }

    func view(for index: Int) -> UIView {
        return cellStackView.arrangedSubviews[index]
    }

    func extendsToContentHeight() {
        frameLayoutGuide.heightAnchor.constraint(equalTo: contentLayoutGuide.heightAnchor).isActive = true
    }
}
