//
//  Widget.swift
//
//
//  Created by Ljupco Nastevski on 17.7.24.
//

import LiveLikeSwift
import UIKit

public class Widget: UIViewController, WidgetViewModel {
    // MARK: Public Properties

    public let id: String
    public let kind: WidgetKind
    public let widgetTitle: String?

    internal let createdAt: Date
    internal let publishedAt: Date?
    internal let interactionTimeInterval: TimeInterval?
    internal let customData: String?
    internal let widgetModel: WidgetModel?
    internal let options: Set<WidgetOption>?
    internal var previousState: WidgetState?
    internal var currentState: WidgetState
    internal weak var delegate: WidgetViewDelegate?
    open var userDidInteract: Bool
    open var dismissSwipeableView: UIView {
        return view
    }

    // MARK: Internal Properties

    let widgetLink: URL?
    let optionsArray: [WidgetOption]?
    var interactionCount: Int = 0
    var timeOfLastInteraction: Date?
    // var interactableState: InteractableState = .closedToInteraction
    private var interactivityExpirationTimer: Timer?

    public init() {
        id = ""
        kind = .alert
        widgetTitle = nil
        createdAt = Date()
        publishedAt = nil
        interactionTimeInterval = nil
        optionsArray = []
        options = Set()
        customData = nil
        previousState = nil
        currentState = .ready
        userDidInteract = false
        widgetLink = nil
        widgetModel = nil
        super.init(nibName: nil, bundle: nil)
    }

    deinit {
        interactivityExpirationTimer?.invalidate()
        interactivityExpirationTimer = nil
    }

    init(
        id: String,
        kind: WidgetKind,
        widgetTitle: String?,
        createdAt: Date,
        publishedAt: Date?,
        interactionTimeInterval: TimeInterval?,
        options: [WidgetOption]
    ) {
        self.id = id
        self.kind = kind
        self.widgetTitle = widgetTitle
        self.createdAt = createdAt
        self.publishedAt = publishedAt
        self.interactionTimeInterval = interactionTimeInterval
        optionsArray = options
        self.options = Set(options)
        customData = nil
        previousState = nil
        currentState = .ready
        userDidInteract = false
        widgetLink = nil
        widgetModel = nil
        super.init(nibName: nil, bundle: nil)
    }

    public init(model: CheerMeterWidgetModel) {
        id = model.id
        kind = model.kind
        widgetTitle = model.title
        createdAt = model.createdAt
        publishedAt = model.publishedAt
        interactionTimeInterval = model.interactionTimeInterval
        currentState = .ready
        userDidInteract = false
        let opts = model.options.map {
            WidgetOption(
                id: $0.id,
                voteURL: nil,
                text: $0.text,
                image: nil,
                voteCount: $0.voteCount
            )
        }
        optionsArray = opts
        options = Set(opts)
        customData = nil
        widgetLink = nil
        widgetModel = .cheerMeter(model)
        super.init(nibName: nil, bundle: nil)
    }

    public init(model: AlertWidgetModel) {
        id = model.id
        kind = model.kind
        widgetTitle = model.title
        createdAt = model.createdAt
        publishedAt = model.publishedAt
        interactionTimeInterval = model.interactionTimeInterval
        currentState = .ready
        userDidInteract = false
        optionsArray = nil
        options = nil
        customData = model.customData
        widgetLink = model.linkURL
        widgetModel = .alert(model)
        super.init(nibName: nil, bundle: nil)
    }

    public init(model: VideoAlertWidgetModel) {
        id = model.id
        kind = model.kind
        widgetTitle = model.title
        createdAt = model.createdAt
        publishedAt = model.publishedAt
        interactionTimeInterval = model.interactionTimeInterval
        currentState = .ready
        userDidInteract = false
        optionsArray = nil
        options = nil
        customData = model.customData
        widgetLink = model.linkURL
        widgetModel = .videoAlert(model)
        super.init(nibName: nil, bundle: nil)
    }

    public init(model: QuizWidgetModel) {
        id = model.id
        kind = model.kind
        widgetTitle = model.question
        createdAt = model.createdAt
        publishedAt = model.publishedAt
        interactionTimeInterval = model.interactionTimeInterval
        currentState = .ready
        userDidInteract = false
        let opts = model.choices.map {
            WidgetOption(
                id: $0.id,
                voteURL: nil,
                text: $0.text,
                image: nil,
                imageURL: $0.imageURL,
                isCorrect: $0.isCorrect,
                voteCount: $0.answerCount
            )
        }
        optionsArray = opts
        options = Set(opts)
        customData = model.customData
        widgetLink = nil
        widgetModel = .quiz(model)
        super.init(nibName: nil, bundle: nil)
    }

    public init(model: PredictionWidgetModel) {
        id = model.id
        kind = model.kind
        widgetTitle = model.question
        createdAt = model.createdAt
        publishedAt = model.publishedAt
        interactionTimeInterval = model.interactionTimeInterval
        currentState = .ready
        userDidInteract = false
        let opts = model.options.map {
            WidgetOption(
                id: $0.id,
                voteURL: nil,
                text: $0.text,
                image: nil,
                imageURL: $0.imageURL,
                isCorrect: nil,
                voteCount: $0.voteCount
            )
        }
        optionsArray = opts
        options = Set(opts)
        customData = model.customData
        widgetLink = nil
        widgetModel = .prediction(model)
        super.init(nibName: nil, bundle: nil)
    }

    public init(model: PredictionFollowUpWidgetModel) {
        id = model.id
        kind = model.kind
        widgetTitle = model.question
        createdAt = model.createdAt
        publishedAt = model.publishedAt
        interactionTimeInterval = model.interactionTimeInterval
        currentState = .ready
        userDidInteract = false
        let opts = model.options.map {
            WidgetOption(
                id: $0.id,
                voteURL: nil,
                text: $0.text,
                image: nil,
                imageURL: $0.imageURL,
                isCorrect: nil,
                voteCount: $0.voteCount
            )
        }
        optionsArray = opts
        options = Set(opts)
        customData = model.customData
        widgetLink = nil
        widgetModel = .predictionFollowUp(model)
        super.init(nibName: nil, bundle: nil)
    }

    public init(model: PollWidgetModel) {
        id = model.id
        kind = model.kind
        widgetTitle = model.question
        createdAt = model.createdAt
        publishedAt = model.publishedAt
        interactionTimeInterval = model.interactionTimeInterval
        currentState = .ready
        userDidInteract = false
        let opts = model.options.map {
            WidgetOption(
                id: $0.id,
                voteURL: nil,
                text: $0.text,
                image: nil,
                imageURL: $0.imageURL,
                voteCount: $0.voteCount
            )
        }
        optionsArray = opts
        options = Set(opts)
        customData = model.customData
        widgetLink = nil
        widgetModel = .poll(model)
        super.init(nibName: nil, bundle: nil)
    }

    public init(model: ImageSliderWidgetModel) {
        id = model.id
        kind = model.kind
        widgetTitle = model.question
        createdAt = model.createdAt
        publishedAt = model.publishedAt
        interactionTimeInterval = model.interactionTimeInterval
        currentState = .ready
        userDidInteract = false
        let opts = model.options.map {
            WidgetOption(
                id: $0.id,
                voteURL: nil,
                text: nil,
                image: nil,
                imageURL: $0.imageURL,
                voteCount: nil
            )
        }
        optionsArray = opts
        options = Set(opts)
        customData = model.customData
        widgetLink = nil
        widgetModel = .imageSlider(model)
        super.init(nibName: nil, bundle: nil)
    }

    public init(model: SocialEmbedWidgetModel) {
        id = model.id
        kind = model.kind
        widgetTitle = model.comment
        createdAt = model.createdAt
        publishedAt = model.publishedAt
        interactionTimeInterval = model.interactionTimeInterval
        currentState = .ready
        userDidInteract = false
        optionsArray = nil
        options = nil
        customData = model.customData
        widgetLink = nil
        widgetModel = .socialEmbed(model)
        super.init(nibName: nil, bundle: nil)
    }

    public init(model: TextAskWidgetModel) {
        id = model.id
        kind = model.kind
        widgetTitle = model.title
        createdAt = model.createdAt
        publishedAt = model.publishedAt
        interactionTimeInterval = model.interactionTimeInterval
        currentState = .ready
        userDidInteract = false
        optionsArray = nil
        options = nil
        customData = model.customData
        widgetLink = nil
        widgetModel = .textAsk(model)
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    public required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    open func moveToNextState() {}
    func addCloseButton(_: @escaping (WidgetViewModel) -> Void) {}
    func addTimer(seconds _: TimeInterval, completion _: @escaping (WidgetViewModel) -> Void) {}
    func cancelTimer() {}

    func beginInteractiveUntilTimer(completion: @escaping () -> Void) {
        guard let interactivityTimeRemaining = widgetModel?.getSecondsUntilInteractivityExpiration() else {
            return
        }
        interactivityExpirationTimer = Timer.scheduledTimer(
            withTimeInterval: interactivityTimeRemaining,
            repeats: false
        ) { _ in
            completion()
        }
    }
}
