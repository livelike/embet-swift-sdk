//
//  WidgetOption.swift
//
//
//  Created by Ljupco Nastevski on 17.7.24.
//

import LiveLikeSwift
import UIKit

/**
    WidgetOption is a class which represents an option a widget can have
 */
internal class WidgetOption: Hashable {
    public let id: String
    let voteURL: URL? = nil
    public let text: String?
    public let image: UIImage?
    public var imageUrl: URL?
    public let isCorrect: Bool?
    public var voteCount: Int?

    init(
        id: String,
        voteURL _: URL?,
        text: String? = nil,
        image: UIImage? = nil,
        imageURL: URL? = nil,
        isCorrect: Bool? = nil,
        voteCount: Int? = nil
    ) {
        self.id = id
        self.text = text
        self.image = image
        imageUrl = imageURL
        self.isCorrect = isCorrect
        self.voteCount = voteCount
    }

    static func == (lhs: WidgetOption, rhs: WidgetOption) -> Bool {
        return lhs.id == rhs.id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
