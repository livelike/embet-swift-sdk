//
//  EngagementSDK.swift
//
//
//  Created by Ljupco Nastevski on 8.7.24.
//

import LiveLikeCore
import LiveLikeSwift
import UIKit

/**
  The entry point for all interaction with the EngagementSDK.

 - Important: Concurrent instances of the EngagementSDK is not supported; Only one instance should exist at any time.
 */
internal class EngagementSDK: LiveLike {
    static let version: String = livelikeversion

    override public init(config: LiveLikeConfig) {
        LLCore.networking.userAgent += "[EngagementSDK/\(UIDevice.modelName)/\(UIDevice.current.systemVersion)]"
        super.init(config: config)
    }

    /// Loads a `Widget` using  the `DefaultWidgetFactory` by an id and kind
    func getWidget(
        id: String,
        kind: WidgetKind,
        completion: @escaping (Result<EmBetWidget, Error>) -> Void
    ) {
        getWidgetModel(
            id: id,
            kind: kind
        ) { result in
            switch result {
            case let .failure(error):
                completion(.failure(error))
            case let .success(widgetModel):
                guard let widget = EBWidgetFactory.defaultWidgetFor(widgetModel: widgetModel) else {
                    return completion(.failure(EBWidgetFactory.Error.failedToBuildUI))
                }
                completion(.success(widget))
            }
        }
    }

    /// Loads a `Widget` using  the `DefaultWidgetFactory` by a widget json
    func createWidget(
        withJSONObject jsonObject: Any,
        completion: @escaping (Result<EmBetWidget, Error>) -> Void
    ) {
        createWidgetModel(withJSONObject: jsonObject) { result in
            switch result {
            case let .failure(error):
                completion(.failure(error))
            case let .success(widgetModel):
                guard let widget = EBWidgetFactory.defaultWidgetFor(widgetModel: widgetModel) else {
                    return completion(.failure(EBWidgetFactory.Error.failedToBuildUI))
                }
                completion(.success(widget))
            }
        }
    }
}
