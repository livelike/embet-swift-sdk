//
//  WidgetViewDelegate.swift
//
//
//  Created by Ljupco Nastevski on 17.7.24.
//

import Foundation
import LiveLikeSwift

/// Methods for managing a Widget lifecyle and other events
internal protocol WidgetViewDelegate: AnyObject {
    /// The widget entered a new WidgetState
    func widgetDidEnterState(widget: WidgetViewModel, state: WidgetState)

    /// The widget has completed all delayed processes of the WidgetState (i.e. animations, http requests, etc.)
    func widgetStateCanComplete(widget: WidgetViewModel, state: WidgetState)

    /// The current user has interacted with the widget
    func userDidInteract(_ widget: WidgetViewModel)
}
