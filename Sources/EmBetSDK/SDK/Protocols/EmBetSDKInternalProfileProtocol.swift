//
//  EmBetSDKInternalProfileProtocol.swift
//
//
//  Created by Ljupco Nastevski on 4.11.22.
//

import Foundation
import LiveLikeSwift

internal protocol EmBetSDKInternalProfileProtocol {
    /**
        The current profile object.
     */
    var current: UserProfile? { get set }

    /**
        Updates the current user data.
     */
    func updateProfileData(data: [String: Any], completion: @escaping (_ success: Bool, _ error: Error?) -> Void)

    /**
        Gets the current user profile data.
     */
    func getProfileData(completion: @escaping (_ profileData: [String: Any]?, _ error: Error?) -> Void)

    /**
        Updates the current user nickname.
     */
    func updateUserDisplayName(displayName: String, completion: @escaping (Bool, Error?) -> Void)

    /**
        Gets the current user nickname.
     */
    func getUserDisplayName(completion: @escaping (String?, Error?) -> Void)

    /**
        Updates the local user reference.
     */
    func updateUser()
}
