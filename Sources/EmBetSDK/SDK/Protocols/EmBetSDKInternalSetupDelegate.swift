//
//  EmBetSDKInternalSetupDelegate.swift
//
//
//  Created by Ljupco Nastevski on 4.11.22.
//

import Foundation

/**
    A delegate to pass the event of `EngagementSDK` to `EmBetSDK`.
 */
internal protocol EmBetSDKInternalSetupDelegate: AnyObject {
    /**
        Creation of the `sdk` was successfull.
     */
    func didFinishSetupWithSuccess()

    /**
        Called when the given `sdk` has failed to setup properly

        Upon receiving this call, the `sdk` should be considered invalid and unuseable.
        If caused by some transient failure like a poor network, a new `EngagementSDK` should
        be created.

        - parameter error: The error encountered, possibly with information about how to resolve the issue.
     */
    func didFinishSetupWithError(error: NSError)
}
