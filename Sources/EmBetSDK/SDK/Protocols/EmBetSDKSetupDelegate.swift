//
//  EmBetSDKSetupDelegate.swift
//
//
//  Created by Ljupco Nastevski on 4.11.22.
//

import Foundation

/**
    A delegate which the `EmBetSDK` will inform about important events, such as setup errors.
 */
@objc
public protocol EmBetSDKSetupDelegate: AnyObject {
    /**
        Creation of the `sdk` was successfull.
     */
    func didFinishSetupWithSuccess(sdk: EmBetSDK)

    /**
        Called when the given `sdk` has failed to setup properly

        Upon receiving this call, the `sdk` should be considered invalid and unuseable.
        If caused by some transient failure like a poor network, a new `EmBetSDK` should
        be created.

        - parameter error: The error encountered, possibly with information about how to resolve the issue.
     */
    func didFinishSetupWithError(sdk: EmBetSDK, error: NSError)
}
