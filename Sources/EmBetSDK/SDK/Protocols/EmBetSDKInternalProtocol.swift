//
//  EmBetSDKInternalProtocol.swift
//
//
//  Created by Ljupco Nastevski on 5.10.22.
//

import Foundation
import LiveLikeSwift

/**
    Internal SDK interface.
 */
internal protocol EmBetSDKInternalProtocol {
    /**
        SDK Setup delegate
     */
    var setupDelegate: EmBetSDKInternalSetupDelegate? { get set }

    /**
        The user object.
     */
    var profile: EmBetSDKInternalProfileProtocol { get }

    /**
        The session object.
     */
    var session: EmBetSDKInternalSessionProtocol { get }

    /**
        The session object.
     */
    var eventManger: EmBetSDKInternalEventManagerProtocol { get set }

    /**
        Current segmentation status.
     */
    var isSegmentationEnabled: Bool { get }

    /**
        Configures the internal SDK instance.
     */
    func configureInternalSDK(setupDelegate: EmBetSDKInternalSetupDelegate)

    /**
        Sets the status of the segmentation feature.
     */
    func enableSegmentation(isEnabled: Bool)

    /**
        Creates and configures the session.
     */
    func configureSession(configuration: EmBetSessionConfiguration)

    /**
        Gets the program ID for the custom ID.
     */
    func getProgramID(customID: String, completion: @escaping (String?, Error?) -> Void)

    /**
        Gets PubSub connection details.
     */
    func getPubSubConnectionDetails(completion: @escaping (Result<PubSubConnectionDetails, Error>) -> Void)

    /**
        Creates a widget from jsonObject.
     */
    func createWidget(jsonObject: Any, completion: @escaping (Result<EmBetWidget, Error>) -> Void)
}
