//
//  EmBetUIComponentsProtocol.swift
//
//
//  Created by Ljupco Nastevski on 30.10.23.
//

import Foundation

public protocol EmBetUIComponentsProtocol: AnyObject {
    func instantiateTimelineController() -> EmBetTimelineViewController

    func instantiatePopoverController() -> EBWidgetPopupViewController
}
