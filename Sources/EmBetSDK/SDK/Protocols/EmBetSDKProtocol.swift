//
//  EmBetSDKProtocols.swift
//
//
//  Created by Ljupco Nastevski on 25.7.22.
//

import Foundation

/**
    Determines the SDK interface.
 */
internal protocol EmBetSDKProtocol {
    /**
        SDK Setup delegate
     */
    var setupDelegate: EmBetSDKSetupDelegate? { get set }
    /**
        Session APIs
     */
    var session: EmBetSDKSessionProtocol { get }

    /**
        Session APIs
     */
    var profile: EmBetSDKProfileProtocol { get }

    /**
        Event Manager
     */
    var eventManager: EmBetSDKEventManagerProtocol { get }

    /**
        Sets the status of the segmentation feature.
     */
    func enableSegmentation(isEnabled: Bool)

    /**
        Gets the program ID for the custom ID.
     */
    func getProgramID(customID: String, completion: @escaping (String?, Error?) -> Void)

    /**
        Creates and configures the session.
     */
    func configureSession(configuration: EmBetSessionConfiguration)
}
