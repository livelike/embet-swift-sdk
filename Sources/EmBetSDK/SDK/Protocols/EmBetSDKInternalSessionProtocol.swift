//
//  EmBetSDKInternalSessionProtocol.swift
//
//
//  Created by Ljupco Nastevski on 4.7.23.
//

import Foundation
import LiveLikeSwift

/**
    Internal session protocol
 */
internal protocol EmBetSDKInternalSessionProtocol {
    /**
        Widget events
     */
    var widgetEventsManager: EmBetEventObserverManager<WidgetModel> { get }
    /**
        Engagement session
     */
    var internalSession: ContentSession? { get }
    /**
        Creates a session with `EmBetSessionConfiguration`
     */
    func create(configuration: EmBetSessionConfiguration)
    /**
        Pauses the active session
     */
    func pause()
    /**
        Resumes the active session
     */
    func resume()
    /**
        Closes the active session
     */
    func close()
}
