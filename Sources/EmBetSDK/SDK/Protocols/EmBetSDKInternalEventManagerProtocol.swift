//
//  EmBetSDKInternalEventManagerProtocol.swift
//
//
//  Created by Ljupco Nastevski on 25.3.24.
//

import Foundation

internal protocol EmBetSDKInternalEventManagerProtocol {
    /**
        Event listerner for emBet events
     */
    var eventListener: ((EmBetEvent) -> Void)? { get set }
    /**
        New event
     */
    func newEvent(event: EmBetEvent)
}
