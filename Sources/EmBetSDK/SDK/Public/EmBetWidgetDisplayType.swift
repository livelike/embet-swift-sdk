//
//  EmBetWidgetDisplayType.swift
//
//
//  Created by Ljupco Nastevski on 15.12.22.
//

import Foundation

/**
    The widget interaction mode.
 */
@objc
public enum EmBetWidgetDisplayType: UInt8 {
    /**
        Regular representation of the widget.
     */
    case regular
    /**
        Hides the dismiss button.
     */
    case forceHideCloseButton
}
