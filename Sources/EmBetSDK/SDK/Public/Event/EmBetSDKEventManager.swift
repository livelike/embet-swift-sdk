//
//  FileEmBetSDKEventManagerswift
//
//
//  Created by Ljupco Nastevski on 25.3.24.
//

import Foundation

internal class EmBetSDKEventManager: EmBetSDKEventManagerProtocol {
    weak var sdk: EmBetSDK?

    required init(sdk: EmBetSDK) {
        self.sdk = sdk
    }

    // Should be implemented by integrator
    public var eventListener: ((EmBetEvent) -> Void)? {
        didSet {
            sdk?.internalSDK.eventManger.eventListener = eventListener
        }
    }
}
