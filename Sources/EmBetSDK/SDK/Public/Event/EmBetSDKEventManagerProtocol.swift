//
//  EmBetSDKEventManagerProtocol.swift
//
//
//  Created by Ljupco Nastevski on 25.3.24.
//

import Foundation

public protocol EmBetSDKEventManagerProtocol {
    /**
        Event listerner for emBet events
     */
    var eventListener: ((EmBetEvent) -> Void)? { get set }
}
