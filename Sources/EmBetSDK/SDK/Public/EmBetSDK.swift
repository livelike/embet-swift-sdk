import Foundation
import LiveLikeCore
import LiveLikeSwift

/**
  The entry point for all interaction with the emBetSDK.

 - Important: Concurrent instances of the emBetSDK is not supported; Only one instance should exist at any time.
 */
@objcMembers
public class EmBetSDK: NSObject, EmBetSDKProtocol {
    /**
        Internal SDK Communication
     */
    internal var internalSDK: EmBetSDKInternalProtocol

    /**
        Setup Delegate
     */
    public weak var setupDelegate: EmBetSDKSetupDelegate?

    /**
        Session APIs
     */
    public lazy var session: EmBetSDKSessionProtocol = EmBetSDKSession(sdk: self)

    /**
        Profile APIs
     */
    public lazy var profile: EmBetSDKProfileProtocol = EmBetSDKProfile(sdk: self)

    /**
        Event Manager
     */
    public lazy var eventManager: EmBetSDKEventManagerProtocol = EmBetSDKEventManager(sdk: self)

    /**
        Required instantiation of the emBetSDK.
        - Parameter config: `EBSDKConfiguration` instance.
     */
    public required init(config: EmBetSDKConfiguration) {
        internalSDK = EmBetSDKInternal(config: config)
        super.init()
        internalSDK.configureInternalSDK(setupDelegate: self)
    }

    /**
        Sets the segmentation feature to enabled/dissabled.
        The emBet documentation provides more details on this feature and how to use it.
     */
    public func enableSegmentation(isEnabled: Bool) {
        internalSDK.enableSegmentation(isEnabled: isEnabled)
    }

    /**
        Gets the program ID for the custom ID.
     */
    public func getProgramID(customID: String, completion: @escaping (String?, Error?) -> Void) {
        internalSDK.getProgramID(customID: customID, completion: completion)
    }

    /**
        Configures the SDKs session with the specific configuration
     */
    public func configureSession(configuration: EmBetSessionConfiguration) {
        internalSDK.configureSession(configuration: configuration)
    }

    /**
        `logLevel` is  the level of detail to be logged to Apple's unified logging system.

        Defaults to `none`.
     */
    public static var logLevel: LogLevel {
        get { return Logger.LoggingLevel }
        set {
            Logger.LoggingLevel = newValue
            LiveLike.logLevel = newValue
        }
    }
}

extension EmBetSDK: EmBetSDKInternalSetupDelegate {
    func didFinishSetupWithError(error: NSError) {
        setupDelegate?.didFinishSetupWithError(sdk: self, error: error)
    }

    func didFinishSetupWithSuccess() {
        setupDelegate?.didFinishSetupWithSuccess(sdk: self)
    }
}
