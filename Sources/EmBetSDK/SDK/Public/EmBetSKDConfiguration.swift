//
//  EmBetSDKConfiguration.swift
//
//
//  Created by Ljupco Nastevski on 9.5.22.
//

import Foundation

/**
    An `EmBetSDKConfiguration` instance is a configuration that is used to configure the `EmBetSDK` instance.
 */
@objcMembers
public class EmBetSDKConfiguration: NSObject {
    /**
          The client id used to link the EmBetSKD to the CMS
     */
    public let clientID: String

    /**
          The access token storage object.
     */
    public var accessTokenStorage: EBAccessTokenStorage = EBDefaultTokenStorage()

    /**
         Instantiates the configuration with the provided clientID.
     */
    public required init(clientID: String) {
        self.clientID = clientID
    }
}
