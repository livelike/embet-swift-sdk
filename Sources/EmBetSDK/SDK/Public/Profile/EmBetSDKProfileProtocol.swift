//
//  EmBetSDKProfileProtocol.swift
//
//
//  Created by Ljupco Nastevski on 30.10.23.
//

import Foundation

public protocol EmBetSDKProfileProtocol {
    /**
        Updates the current user data.
     */
    func updateProfileData(data: [String: Any], completion: @escaping (_ success: Bool, _ error: Error?) -> Void)
    /**
        Gets the current user profile data.
     */
    func getProfileData(completion: @escaping (_ profileData: [String: Any]?, _ error: Error?) -> Void)
    /**
        Updates the current user nickname.
     */
    func updateUserDisplayName(displayName: String, completion: @escaping (Bool, Error?) -> Void)
    /**
        Gets the current user nickname.
     */
    func getUserDisplayName(completion: @escaping (String?, Error?) -> Void)
}
