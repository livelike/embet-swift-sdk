//
//  EmBetSDKProfile.swift
//
//
//  Created by Ljupco Nastevski on 30.10.23.
//

import Foundation

class EmBetSDKProfile: EmBetSDKProfileProtocol {
    weak var sdk: EmBetSDK?

    required init(sdk: EmBetSDK) {
        self.sdk = sdk
    }

    func updateProfileData(data: [String: Any], completion: @escaping (Bool, Error?) -> Void) {
        sdk?.internalSDK.profile.updateProfileData(data: data, completion: completion)
    }

    func getProfileData(completion: @escaping ([String: Any]?, Error?) -> Void) {
        sdk?.internalSDK.profile.getProfileData(completion: completion)
    }

    func updateUserDisplayName(displayName: String, completion: @escaping (Bool, Error?) -> Void) {
        sdk?.internalSDK.profile.updateUserDisplayName(displayName: displayName, completion: completion)
    }

    func getUserDisplayName(completion: @escaping (String?, Error?) -> Void) {
        sdk?.internalSDK.profile.getUserDisplayName(completion: completion)
    }
}
