//
//  EmBetSession.swift
//
//
//  Created by Ljupco Nastevski on 20.7.22.
//

import Foundation

internal class EmBetSDKSession: EmBetSDKSessionProtocol {
    weak var sdk: EmBetSDK?

    required init(sdk: EmBetSDK) {
        self.sdk = sdk
    }

    public func pause() {
        sdk?.internalSDK.session.pause()
    }

    public func resume() {
        sdk?.internalSDK.session.resume()
    }

    public func close() {
        sdk?.internalSDK.session.close()
    }
}
