//
//  EmBetSDKSessionProtocol.swift
//
//
//  Created by Ljupco Nastevski on 30.10.23.
//

import Foundation

/**
    Public session protocol
 */
public protocol EmBetSDKSessionProtocol {
    /**
        Pauses the active session
     */
    func pause()
    /**
        Resumes the active session
     */
    func resume()
    /**
        Closes the active session
     */
    func close()
}
