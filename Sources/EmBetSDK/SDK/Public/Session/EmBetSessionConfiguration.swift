//
//  File.swift
//
//
//  Created by Ljupco Nastevski on 20.7.22.
//

import Foundation

/**
 A `EBSessionConfiguration` instance is a configuration that is used to configure a `EBWidgetPopupViewController`.
 */
@objcMembers
public class EmBetSessionConfiguration: NSObject {
    /**
          The program id used to link the session to the specific program.
     */
    public let programID: String

    /**
         Instantiates the configuration with the provided client and program id.
     */
    public required init(programID: String) {
        self.programID = programID
    }
}
