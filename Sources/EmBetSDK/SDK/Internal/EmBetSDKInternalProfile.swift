//
//  EmBetSDKInternalProfile.swift
//
//
//  Created by Ljupco Nastevski on 4.11.22.
//

import Foundation
import LiveLikeSwift

internal class EmBetSDKInternalProfile: EmBetSDKInternalProfileProtocol {
    /**
        The current profile
     */
    var current: UserProfile?

    /**
         Engagement reference.
         - Internal API Calls
     */
    let engagement: EngagementSDK

    required init(engagementSDK: EngagementSDK) {
        engagement = engagementSDK
    }

    /**
        Update profile data internal implementation
     */
    func updateProfileData(data: [String: Any], completion: @escaping (Bool, Error?) -> Void) {
        let parsedDataResult = SegmentatorParser.packData(data: data)

        switch parsedDataResult {
        case let .success(parsedData):

            engagement.setUserCustomData(data: parsedData) { [weak self] results in
                self?.updateUser()
                switch results {
                case .success:
                    completion(true, nil)
                case let .failure(error):
                    completion(false, error)
                }
            }

        case let .failure(error):
            completion(false, error)
        }
    }

    /**
        Get profile data internal implementation.
     */
    func getProfileData(completion: @escaping ([String: Any]?, Error?) -> Void) {
        engagement.getCurrentUserProfile { results in
            switch results {
            case let .success(profile):
                self.current = profile
                let result = SegmentatorParser.parseToDataObject(data: profile.customData)
                switch result {
                case let .success(data):
                    completion(data, nil)
                case .failure:
                    completion(nil, nil)
                }
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
        Update user display name internal implementation.
     */
    func updateUserDisplayName(displayName: String, completion: @escaping (Bool, Error?) -> Void) {
        engagement.setUserDisplayName(displayName, completion: completion)
    }

    /**
        Get user display internal implementation.
     */
    func getUserDisplayName(completion: @escaping (String?, Error?) -> Void) {
        engagement.getUserDisplayName { result in
            switch result {
            case let .success(name):
                completion(name, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
        Updates the local user reference.
     */
    func updateUser() {
        getProfile(completion: { [weak self] userProfile in
            self?.current = userProfile
        })
    }

    /**
        Gets the current user profile.
     */
    private func getProfile(completion: @escaping (UserProfile?) -> Void) {
        engagement.getCurrentUserProfile { result in
            switch result {
            case let .success(userProfile):
                completion(userProfile)
            default:
                completion(nil)
            }
        }
    }
}
