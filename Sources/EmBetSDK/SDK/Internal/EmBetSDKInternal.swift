//
//  File.swift
//
//
//  Created by Ljupco Nastevski on 25.7.22.
//

import Foundation
import LiveLikeSwift

/**
    Internal implementation of the SDKInternal interface.
 */
internal class EmBetSDKInternal: EmBetSDKInternalProtocol {
    /**
         Engagement reference.
         - Internal API Calls
     */
    let engagement: EngagementSDK

    /**
        A delegate which the `EmBetSDK` will inform about important setup events.
     */
    weak var setupDelegate: EmBetSDKInternalSetupDelegate?

    /**
        Profile APIs
     */
    lazy var profile: EmBetSDKInternalProfileProtocol = EmBetSDKInternalProfile(engagementSDK: engagement)

    /**
        Session APIs
     */
    lazy var session: EmBetSDKInternalSessionProtocol = EmBetSDKInternalSession(engagement: engagement)

    /**
        Internal event manager
     */
    lazy var eventManger: EmBetSDKInternalEventManagerProtocol = EmBetSDKInternalEventManager()

    /**
        Flag that determines wether the segmentation feature should be used.
     */
    var isSegmentationEnabled = true

    required init(config: EmBetSDKConfiguration) {
        var internalConfig = LiveLikeConfig(clientID: config.clientID)
        internalConfig.accessTokenStorage = config.accessTokenStorage
        engagement = EngagementSDK(config: internalConfig)
        engagement.delegate = self
        loadFonts()
    }

    func configureInternalSDK(setupDelegate: EmBetSDKInternalSetupDelegate) {
        self.setupDelegate = setupDelegate
    }

    /**
        Enable segmentation internal implementation
     */
    func enableSegmentation(isEnabled: Bool) {
        isSegmentationEnabled = isEnabled
        log.debug("Segmentation status changed, is segmentation enabled: \(isEnabled)")
    }

    /**
        Create session internal implementation
     */
    func configureSession(configuration: EmBetSessionConfiguration) {
        session = EmBetSDKInternalSession(engagement: engagement)
        session.create(configuration: configuration)
    }

    /**
        Get profile id internal implementation.
     */
    func getProfileID(completion: @escaping (String?) -> Void) {
        engagement.getCurrentUserProfileID { result in
            switch result {
            case let .success(id):
                completion(id)
            default:
                completion(nil)
            }
        }
    }

    /**
        Gets the program ID for the custom ID.
     */
    func getProgramID(customID: String, completion: @escaping (String?, Error?) -> Void) {
        engagement.getProgramID(customID: customID) { result in
            switch result {
            case let .success(programID):
                completion(programID, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
        Gets PubSub connection details.
     */
    func getPubSubConnectionDetails(completion: @escaping (Result<PubSubConnectionDetails, Error>) -> Void) {
        engagement.getPubSubConnectionDetails { result in
            completion(result)
        }
    }

    /**
        Creates, if possible a widget instance that would be shown on the view controller.
     */
    func createWidget(jsonObject: Any, completion: @escaping (Result<EmBetWidget, Error>) -> Void) {
        engagement.createWidget(withJSONObject: jsonObject) { result in
            completion(result)
        }
    }

    /**
        Loads fonts.
     */
    private func loadFonts() {
        LLFontImporter().loadCustomFonts()
    }
}

/**
    Transforms the Engagements SDK setup into EmBet SDK Setup delegate.
 */
extension EmBetSDKInternal: EngagementSDKDelegate {
    func sdk(setupCompleted engagementSDK: LiveLike) {
        engagementSDK.getCurrentUserProfile(completion: { [weak self] result in
            switch result {
            case let .success(currentProfile):
                self?.profile.current = currentProfile
                log.debug("Successfully loaded user with nickname: \(currentProfile.nickname) and custom data: \(currentProfile.customData ?? "_no_data_"))")
            case let .failure(failure):
                log.error("Failed to get profile at initialization: \(failure.localizedDescription)")
            }
        })
        setupDelegate?.didFinishSetupWithSuccess()
    }

    func sdk(_: LiveLike, setupFailedWithError error: Error) {
        setupDelegate?.didFinishSetupWithError(error: error as NSError)
    }
}
