//
//  EmBetSDKInternalEventManager.swift
//
//
//  Created by Ljupco Nastevski on 25.3.24.
//

import Foundation

internal class EmBetSDKInternalEventManager: EmBetSDKInternalEventManagerProtocol {
    // Should be implemented by integrator
    public var eventListener: ((EmBetEvent) -> Void)?

    // Should be called from the widget/reporting object
    public func newEvent(event: EmBetEvent) {
        eventListener?(event)
    }
}
