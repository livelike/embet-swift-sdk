//
//  EmBetSDKInternalSession.swift
//
//
//  Created by Ljupco Nastevski on 4.7.23.
//

import Foundation
import LiveLikeSwift

/**
    Internal session implementation
 */
class EmBetSDKInternalSession: EmBetSDKInternalSessionProtocol {
    /**
         Engagement reference.
         - Internal API Calls
     */
    private let engagement: EngagementSDK

    var widgetEventsManager: EmBetEventObserverManager<WidgetModel> = .init()
    /**
        EmBet Session object
     */
    var internalSession: ContentSession?

    init(engagement: EngagementSDK) {
        self.engagement = engagement
    }

    /**
        Creates a session. If a session was already active, it closes that session.
     */
    func create(configuration: EmBetSessionConfiguration) {
        if internalSession != nil {
            close()
            internalSession = nil
        }
        internalSession = engagement.contentSession(
            config: SessionConfiguration(programID: configuration.programID)
        )
        internalSession?.delegate = self
    }

    /**
        Pauses the currently active session
     */
    func pause() {
        internalSession?.pause()
    }

    /**
        Resumes the currently active session
     */
    func resume() {
        internalSession?.resume()
    }

    /**
        Closes the currently active session
     */
    func close() {
        internalSession?.close()
        internalSession = nil
    }
}

extension EmBetSDKInternalSession: ContentSessionDelegate {
    func playheadTimeSource(_: ContentSession) -> Date? {
        return nil
    }

    func session(_: ContentSession, didChangeStatus _: SessionStatus) {}

    func session(_: ContentSession, didReceiveError _: Error) {}

    func chat(session _: ContentSession, roomID _: String, newMessage _: ChatMessage) {}

    func contentSession(_: ContentSession, didReceiveWidget widgetModel: WidgetModel) {
        widgetEventsManager.activeListeners.forEach { $0.block(widgetModel) }
    }
}
