//
//  LLThemableButton.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 4.3.22.
//

import UIKit

/**
    Base class for themable button.
    Sublasses should override applyTheme.
 */
internal class LLThemableButton: UIButton, LLThemable {
    var type: LLThemableType

    var theme: LLThemeViewModel?

    override class var layerClass: AnyClass {
        return LLThemableLayer.self
    }

    init(themeType: LLThemableType) {
        type = themeType
        super.init(frame: .zero)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func applyTheme(theme: LLThemeViewModel?) {
        guard let theme = theme else {
            return
        }

        self.theme = theme

        let font = UIFont.loadFontFromTheme(familyName: theme.fontFamily, fontWeight: theme.fontWeight, size: theme.fontSize)
        let fontColor = theme.fontColor ?? .white

        if #available(iOS 15.0, tvOS 15.0, *) {
            var configuration = UIButton.Configuration.plain()
            configuration.contentInsets = theme.padding?.toDirectionalInsets() ?? .zero
            configuration.attributedTitle?.font = font

            var container = AttributeContainer()
            container.font = font
            configuration.attributedTitle = AttributedString(self.title(for: .normal) ?? "", attributes: container)

            self.setTitle(nil, for: .normal)

            self.configuration = configuration

            self.configurationUpdateHandler = { button in

                switch button.state {
                case .normal:
                    self.titleLabel?.font = font
                    self.tintColor = fontColor
                default:
                    self.tintColor = fontColor.withAlphaComponent(0.6)
                }
            }

        } else {
            contentEdgeInsets = theme.padding?.toEdgeInsets() ?? .zero
            titleLabel?.font = font

            setTitleColor(fontColor, for: .normal)
            setTitleColor(fontColor.withAlphaComponent(0.6), for: .highlighted)
            setTitleColor(fontColor.withAlphaComponent(0.6), for: .disabled)
        }

        DispatchQueue.main.async { [weak self] in
            self?.setupBackground()
            self?.layer.setNeedsLayout()
        }
    }

    // Resets the background acording to the theme
    private func setupBackground() {
        guard let layer = layer as? LLLayerThemable else { return }
        layer.setTheme(theme: theme)
    }
}
