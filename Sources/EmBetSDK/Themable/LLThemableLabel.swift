//
//  LLThemableLabel.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 4.3.22.
//

import UIKit

internal class LLThemableLabel: UILabel, LLThemable {
    var type: LLThemableType

    var theme: LLThemeViewModel?

    init(themeType: LLThemableType) {
        type = themeType
        super.init(frame: .zero)
        commonInit()
    }

    private func commonInit() {
        font = .systemFont(ofSize: LLUIConstants.DefaultFontSize)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
    }

    func applyTheme(theme: LLThemeViewModel?) {
        guard let theme = theme else {
            return
        }

        // Text Alignment is only applied if value is present
        if let newTextAlignment = theme.textAlign?.asNSTextAligment {
            textAlignment = newTextAlignment
        }

        textColor = theme.fontColor ?? .white
        font = UIFont.loadFontFromTheme(familyName: theme.fontFamily, fontWeight: theme.fontWeight, size: theme.fontSize)
    }
}
