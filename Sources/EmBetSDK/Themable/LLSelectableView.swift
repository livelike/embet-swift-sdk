//
//  LLSelectableView.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 28.2.22.
//

import LiveLikeSwift
import UIKit

protocol LLSelectableViewDelegate: AnyObject {
    func didSelectOptionWithID(id: String)
}

/**
    Selectable view
 */
internal class LLSelectableView: LLThemableView, LLThemableInteractive {
    internal let option: EBWidgetOption

    internal let betDetails: LLWidgetBetDetailsModel?

    private var numberOfVotesText: String?

    var selectedType: LLThemableType
    var selectedTheme: LLThemeViewModel?
    var unselectedType: LLThemableType
    var unselectedTheme: LLThemeViewModel?

    #if os(tvOS)
        override var canBecomeFocused: Bool {
            return true
        }

        let focusLayer: CALayer = .init()

        override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
            if context.nextFocusedView == self {
                coordinator.addCoordinatedAnimations({ () in
                    self.focusLayer.backgroundColor = LLUIConstants.FocusColor.cgColor
                }, completion: nil)

            } else if context.previouslyFocusedView == self {
                coordinator.addCoordinatedAnimations({ () in
                    self.focusLayer.backgroundColor = UIColor.clear.cgColor
                }, completion: nil)
            }
        }
    #endif

    var isSelected: Bool = false {
        didSet {
            self.selectionChanged()
        }
    }

    weak var delegate: LLSelectableViewDelegate?

    var voteResultsView: LLOptionResultsOverlay = LLSelectableResultsOverlay()

    required init(
        option: EBWidgetOption,
        betDetails: LLWidgetBetDetailsModel?,
        selectedThemeType _: LLThemableType,
        unselectedThemeType _: LLThemableType,
        numberOfVotesText: String?,
        voteResultsView: LLOptionResultsOverlay = LLSelectableResultsOverlay()
    ) {
        self.option = option
        self.betDetails = betDetails

        selectedType = .selectedOption
        unselectedType = .unselectedOption

        self.numberOfVotesText = numberOfVotesText

        self.voteResultsView = voteResultsView

        super.init(themeType: .unselectedOption)

        #if os(tvOS)
            focusLayer.frame = layer.bounds
            layer.addSublayer(focusLayer)
        #endif

        initialSetup()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    internal func initialSetup() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapOnSelf))
        addGestureRecognizer(tapGesture)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        // Change size of focus layer acordingly
        #if os(tvOS)
            focusLayer.frame = layer.bounds
        #endif
    }

    internal func addResultsOverlayView(asFirstSubview: Bool = false) {
        voteResultsView.alpha = 0
        voteResultsView.type = .unselectedOption
        voteResultsView.translatesAutoresizingMaskIntoConstraints = false
        voteResultsView.frame = bounds
        voteResultsView.setNumberOfVotesText(text: numberOfVotesText)

        if asFirstSubview {
            insertSubview(voteResultsView, at: 0)
        } else {
            addSubview(voteResultsView)
        }

        NSLayoutConstraint.activate([
            voteResultsView.leadingAnchor.constraint(equalTo: leadingAnchor),
            voteResultsView.trailingAnchor.constraint(equalTo: trailingAnchor),
            voteResultsView.topAnchor.constraint(equalTo: topAnchor),
            voteResultsView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])

        voteResultsView.setSelected(selected: isSelected)
        voteResultsView.setPercentage(percentage: 0.0)
    }

    @objc func didTapOnSelf() {
        if isSelected {
            return
        }

        delegate?.didSelectOptionWithID(id: option.id)
    }

    internal func selectionChanged() {
        if isSelected {
            type = .selectedOption
        } else {
            type = .unselectedOption
        }

        voteResultsView.setSelected(selected: isSelected)
    }

    func setVoteResultPercentage(percentage: CGFloat?) {
        guard let percentage = percentage else {
            voteResultsView.removeFromSuperview()
            return
        }

        voteResultsView.setPercentage(percentage: percentage)
    }

    /**
        Update both the bar and label
     */
    func updateResults() {
        updateVotesLabel()
        updateBar()
    }

    /**
        Update only the bar
     */
    func updateBar() {
        voteResultsView.updateBar()
    }

    /**
        Update the title label
     */
    func updateVotesLabel() {
        voteResultsView.updateVotesLabel()
    }
}
