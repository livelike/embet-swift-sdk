//
//  LLThemableView.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 4.3.22.
//

import UIKit

internal class LLThemableView: UIView, LLThemable {
    var type: LLThemableType

    var theme: LLThemeViewModel?

    override class var layerClass: AnyClass {
        return LLThemableLayer.self
    }

    init(themeType: LLThemableType) {
        type = themeType
        super.init(frame: .zero)
        // Since the widget use directional layout guides
        // `insetsLayoutMarginsFromSafeArea` should be dissabled
        // to ignore safe area insets.
        insetsLayoutMarginsFromSafeArea = false
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func applyTheme(theme: LLThemeViewModel?) {
        guard let theme = theme else {
            layer.mask = nil
            return
        }

        self.theme = theme

        if let padding = theme.padding {
            layoutMargins = padding.toEdgeInsets()
        } else {
            layoutMargins = .zero
        }

        DispatchQueue.main.async { [weak self] in
            self?.setupBackground()
            self?.layer.setNeedsLayout()
        }
    }

    // Resets the background acording to the theme
    private func setupBackground() {
        guard let layer = layer as? LLLayerThemable else { return }
        layer.setTheme(theme: theme)
    }
}
