//
//  LLThemable.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 28.2.22.
//

import UIKit

/**
    Theme type elemets that reflect the part of theme they will conform to.
 */
@objc
internal enum LLThemableType: Int {
    case none
    case dismiss
    case root
    case title
    case header
    case body
    case footer
    case timer
    case insights
    case betButton
    case roundTimer
    case selectedOption
    case selectedOptionBar
    case selectedOptionDescription
    case selectedOptionImage
    case selectedOptionPercentage
    case unselectedOption
    case unselectedOptionBar
    case unselectedOptionDescription
    case unselectedOptionImage
    case unselectedOptionPercentage
    case insightBodyText
    case insightTable
    case insightTableCell
    case insightTableCellHighlighted
    case animatedBet
    case sponsorText
    case sponsor
    case optionImageOverwriter
    case optionRow
    case optionRowVertical
    case disclaimer
    case disclaimerTitle
    case disclaimerDescription
    case teamName
    case separator
    case qrLabel
    case accessoryButton
    case lockIcon
    case information
    // Unselected Correct Option
    case unselectedCorrectOption
    case unselectedCorrectOptionBar
    case unselectedCorrectOptionPercentage
    // Selected Correct Option
    case selectedCorrectOption
    case selectedCorrectOptionBar
    case selectedCorrectOptionPercentage
    // Unselected Incorrect Option
    case unselectedIncorrectOption
    case unselectedIncorrectOptionBar
    case unselectedIncorrectOptionPercentage
    // Selected Incorrect Option
    case selectedIncorrectOption
    case selectedIncorrectOptionBar
    case selectedIncorrectOptionPercentage
    // Prediction follow up theme
    case predictionCorrect
    case predictionIncorrect
    case predictionUnresolved
    // Closing odds
    case resultDescription
    case teamScore
    case teamScoreWinner
    // Back Button
    case backButton
}

/**
    UIElements that confirm and implement this protocol
    will be subjected to theming.
 */
protocol LLThemable: UIView {
    /**
        The theming type the
     */
    var type: LLThemableType { get }

    /**
        Called by the widget. Any updates to the view should happen in this function.
     */
    func applyTheme(theme: LLThemeViewModel?)
}

/**
    UIElements that confirm and implement this protocol
    will be subjected to theming.
 */
protocol LLThemableInteractive: UIView {
    var selectedType: LLThemableType { get }

    var selectedTheme: LLThemeViewModel? { get set }

    var unselectedType: LLThemableType { get }

    var unselectedTheme: LLThemeViewModel? { get set }
}
