//
//  LLThemableImageView.swift
//
//
//  Created by Ljupco Nastevski on 28.11.23.
//

import UIKit

internal class LLThemableImageView: UIImageView, LLThemable {
    var type: LLThemableType

    required init(type: LLThemableType) {
        self.type = type
        super.init(frame: .zero)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func applyTheme(theme: LLThemeViewModel?) {
        guard let theme = theme else {
            return
        }

        if let theme = theme as? LLThemeColorModel,
           let color = theme.color
        {
            tintColor = color
        }
    }
}
