//
//  LLThemableSeparator.swift
//
//
//  Created by Ljupco Nastevski on 28.11.23.
//

import UIKit

internal class LLThemableSeparator: LLThemableView {
    override func applyTheme(theme: LLThemeViewModel?) {
        guard let theme = theme else { return }
        self.theme = theme
        if let theme = theme as? LLThemeColorModel, let color = theme.color {
            backgroundColor = color
            return
        }
    }
}
