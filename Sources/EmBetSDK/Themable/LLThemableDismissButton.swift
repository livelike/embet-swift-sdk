//
//  LLThemableDismissButton.swift
//
//
//  Created by Ljupco Nastevski on 27.5.22.
//

import UIKit

class LLThemableDismissButton: LLThemableButton {
    static func makeDismissButton() -> LLThemableDismissButton {
        let button = LLThemableDismissButton(themeType: .dismiss)
        button.setImage(.embetIconWithName(iconName: .dismiss), for: .normal)
        return button
    }

    static func makeChevronButton() -> LLThemableDismissButton {
        let button = LLThemableDismissButton(themeType: .accessoryButton)
        button.setImage(.embetIconWithName(iconName: .chevronUp), for: .normal)
        return button
    }

    override func applyTheme(theme: LLThemeViewModel?) {
        guard let theme = theme else {
            return
        }
        self.theme = theme
        if let dismissTheme = theme as? LLThemeDismissModel, let color = dismissTheme.color {
            tintColor = color
            return
        }
    }

    #if os(tvOS)
        override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
            if context.nextFocusedView == self {
                coordinator.addCoordinatedAnimations({ () in
                    self.layer.backgroundColor = LLUIConstants.FocusColor.cgColor
                }, completion: nil)

            } else if context.previouslyFocusedView == self {
                coordinator.addCoordinatedAnimations({ () in
                    self.layer.backgroundColor = UIColor.clear.cgColor
                }, completion: nil)
            }
        }
    #endif
}
