//
//  EBWidgetViewController.swift
//
//
//  Created by Ljupco Nastevski on 1.8.22.
//

import LiveLikeSwift
import UIKit

/**
    View controller used for showing widgets loaded as json objects.
 */
public class EBWidgetViewController: UIViewController {
    /**
        Currently visible widget widget
     */
    private weak var currentWidgetVC: EmBetWidget?

    private let sdk: EmBetSDKInternalProtocol
    /**
        Widgets state controller
     */
    private lazy var widgetViewDelegate: EBDefaultWidgetStateController = .init(closeButtonAction: { [weak self] in
        self?.removeCurrentWidget()
    }, widgetFinishedCompletion: { [weak self] widget in
        guard widget.id == self?.currentWidgetVC?.widgetID else { return }
        self?.removeCurrentWidget()
    })

    /**
        Sets the widget display type.
     */
    public var widgetDisplayType: EmBetWidgetDisplayType = .regular

    public required init(clientID: String) {
        sdk = EmBetSDKInternal(config: EmBetSDKConfiguration(clientID: clientID))
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /**
        Loads a widget from json object
     */
    public func loadWidgetFromJsonObject(jsonObject: Any) {
        removeCurrentWidget()
        setupWidget(jsonObject: jsonObject)
    }

    /**
        Removes the currently visible widget.
     */
    private func removeCurrentWidget() {
        guard let currentWidgetVC = currentWidgetVC else {
            log.error("Unable to remove widget as no widget is currently presented")
            return
        }
        currentWidgetVC.willMove(toParent: nil)
        currentWidgetVC.view.removeFromSuperview()
        currentWidgetVC.removeFromParent()
    }

    /**
        Creates, if possible a widget instance that would be shown on the view controller.
     */
    private func setupWidget(jsonObject: Any) {
        sdk.createWidget(jsonObject: jsonObject) { [weak self] result in
            switch result {
            case let .success(widget):
                widget.displayType = self?.widgetDisplayType ?? .regular
                self?.prepareWidgetForShowing(widgetViewController: widget)
            case let .failure(error):
                log.error("Unable to load widget from data: \(error)")
                self?.currentWidgetVC = nil
            }
        }
    }

    /**
        Prepares the newly created widget and adds it it to the widget view controller
     */
    private func prepareWidgetForShowing(widgetViewController: EmBetWidget?) {
        guard let nextWidgetVC = widgetViewController else {
            currentWidgetVC = nil
            return
        }

        let options = nextWidgetVC.options?.map { $0.id }

        switch nextWidgetVC.widgetModel {
        case let .prediction(predictionModel):
            if let predicitionWidget = nextWidgetVC as? LLBetWidgetViewController {
                setupDummyVotes(predictionWidgetModel: predictionModel, widgetVC: predicitionWidget, optionIDs: options!)
            }
        case let .quiz(quizModel):
            guard let quizWidget = nextWidgetVC as? LLQuizWidgetViewController else { return }
            setupQuizDummyVotes(quizWidgetModel: quizModel, widgetVC: quizWidget, optionIDs: options!)
        default: break
        }
        nextWidgetVC.delegate = widgetViewDelegate
        nextWidgetVC.moveToNextState()

        addChild(nextWidgetVC)
        nextWidgetVC.didMove(toParent: self)

        view.addSubview(nextWidgetVC.view)

        nextWidgetVC.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            nextWidgetVC.view.topAnchor.constraint(equalTo: view.topAnchor),
            nextWidgetVC.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            nextWidgetVC.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
        ])

        currentWidgetVC = nextWidgetVC
    }

    /**
        Fires a delegate call to create dummy votes after a seccond
     */
    private func setupDummyVotes(predictionWidgetModel: PredictionWidgetModel, widgetVC: LLBetWidgetViewController, optionIDs: [String]) {
        let totalVotesCount = 99

        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            var votesLeft = totalVotesCount

            for optionID in optionIDs {
                var votes = Int.random(in: 0 ..< votesLeft)
                votesLeft -= votes

                if optionID == optionIDs.last! {
                    votes += votesLeft
                }

                widgetVC.predictionWidgetModel(predictionWidgetModel, voteCountDidChange: votes, forOption: optionID)
            }
        }
    }

    /**
        Fires a delegate call to create dummy votes after a seccond
     */
    private func setupQuizDummyVotes(quizWidgetModel: QuizWidgetModel, widgetVC: LLQuizWidgetViewController, optionIDs: [String]) {
        let totalVotesCount = 100
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            var votesLeft = totalVotesCount
            for optionID in optionIDs {
                var votes = Int.random(in: 0 ..< votesLeft)
                votesLeft -= votes
                if optionID == optionIDs.last! {
                    votes += votesLeft
                }
                widgetVC.quizWidgetModel(quizWidgetModel, answerCountDidChange: votes, forChoice: optionID)
            }
        }
    }
}
