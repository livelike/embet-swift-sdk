//
//  EmBetTimelineViewControllerTraits.swift
//
//
//  Created by Ljupco Nastevski on 19.12.23.
//

import Foundation

/**
    PostedWidgets controller traits
 */
public struct EmBetTimelineViewControllerTraits: OptionSet {
    public let rawValue: Int8
    /**
        Trait that configures the timeline to show only persistent widgets.
     */
    public static let persistentWidgetsOnly = EmBetTimelineViewControllerTraits(rawValue: 1 << 0) // 00000001
    /**
        Allows new widgets to be added.
     */
    public static let addsNewWidgets = EmBetTimelineViewControllerTraits(rawValue: 1 << 1) // 00000010
    /**
        Changes the flow to vertical.
     */
    public static let usesVerticalFlow = EmBetTimelineViewControllerTraits(rawValue: 1 << 2) // 00000100
    /**
        Enables widgets real-time update.
     */
    public static let realtimeUpdates = EmBetTimelineViewControllerTraits(rawValue: 1 << 3) // 00001000

    public init(rawValue: Int8) {
        self.rawValue = rawValue
    }
}
