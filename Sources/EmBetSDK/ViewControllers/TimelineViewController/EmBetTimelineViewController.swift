//
//  EmBetTimelineViewController.swift.swift
//
//
//  Created by Ljupco Nastevski on 3.7.23.
//

import LiveLikeCore
import LiveLikeSwift
import UIKit

/**
    Timeline view controller delegate
 */
public protocol EmBetTimelineViewControllerDelegate: AnyObject {
    /**
        Provide a view to be shown if there are no widgets to be
        shown in the timeline view.
     */
    func timelineEmptyView() -> UIView?
}

public final class EmBetTimelineViewController: UIViewController {
    // MARK: - Public variables

    public weak var delegate: EmBetTimelineViewControllerDelegate?
    /**
        Timeline traits - options set that configures the timeline.
     */
    public var traits: EmBetTimelineViewControllerTraits = [.addsNewWidgets, .realtimeUpdates]
    /**
        Timeline widget kinds. Determines the kind of widgets to be listed.
     */
    public var widgetKinds: Set<EmBetWidgetKind> {
        get { _widgetKinds }
        set { _widgetKinds = newValue }
    }

    // MARK: - Private variables

    /**
        Widget kind private ivar.
        To not expose `@Permited` wraper publicly
     */
    @Permitted(elements: EmBetWidgetKind.permitedTimelineWidgetKinds)
    private var _widgetKinds: Set<EmBetWidgetKind> = EmBetWidgetKind.permitedTimelineWidgetKinds

    /**
        `EmBetPostedWidgetsViewController` flow direction.
     */
    private enum EBTimelineFlowType {
        case vertical
        case horizontal
    }

    /**
        The flow direction of the collection view
     */
    private var flowType: EBTimelineFlowType = .horizontal

    /**
        The collectionviews flow layout
     */
    private var collectionViewFlowLayout: UICollectionViewFlowLayout = EBHorizontalTimelineFlowLayout()
    /**
        Page control, shown when flow layout direction is horizontal
     */
    private lazy var pageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.isUserInteractionEnabled = false
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        pageControl.numberOfPages = 0
        return pageControl
    }()

    #if os(iOS)
        /**
            Enables pull to refresh on the timeline collection view
         */
        private lazy var refreshControl: UIRefreshControl = {
            let refreshControll = UIRefreshControl()
            refreshControll.addTarget(self, action: #selector(refreshControllerPulled), for: .valueChanged)
            return refreshControll
        }()

    #endif

    /**
        The timeline collection view
     */
    internal lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewFlowLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.allowsSelection = false
        collectionView.backgroundColor = .clear
        collectionView.isHidden = true
        collectionView.contentInsetAdjustmentBehavior = .never
        collectionView.register(WidgetCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        #if os(iOS)
            collectionView.refreshControl = self.refreshControl
            if flowType == .horizontal { collectionView.isPagingEnabled = true }
        #endif
        return collectionView
    }()

    private let emptyTableLoadingIndicator: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView()
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.hidesWhenStopped = true
        spinner.startAnimating()
        return spinner
    }()

    /**
        The empty data view to be shown when the timeline is empty
     */
    private var emptyTimelineView: UIView?

    /**
        SDK instance
     */
    private let sdk: EmBetSDK

    /**
        Presenter
     */
    internal lazy var presenter: EmBetPostedWidgetsPresenter = EmBetInternalPostedWidgetsPresenter()

    // MARK: - Lifecycle methods

    public init(sdk: EmBetSDK) {
        self.sdk = sdk
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    public required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override public func viewDidLoad() {
        super.viewDidLoad()

        sdk.profile.getProfileData { [weak self] _, _ in
            self?.setupSelf()
        }

        if traits.contains(.usesVerticalFlow) {
            collectionViewFlowLayout = EBVerticalTimelineFlowLayout()
            flowType = .vertical
        }

        view.addSubview(emptyTableLoadingIndicator)
        view.addSubview(collectionView)

        NSLayoutConstraint.activate([
            emptyTableLoadingIndicator.topAnchor.constraint(equalTo: view.topAnchor),
            emptyTableLoadingIndicator.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            emptyTableLoadingIndicator.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            emptyTableLoadingIndicator.bottomAnchor.constraint(equalTo: view.bottomAnchor),

            collectionView.topAnchor.constraint(equalTo: view.topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])

        if flowType == .horizontal {
            collectionView.showsHorizontalScrollIndicator = false
            collectionView.showsVerticalScrollIndicator = false
            view.addSubview(pageControl)
            pageControl.pageIndicatorTintColor = .lightGray
            pageControl.currentPageIndicatorTintColor = .black
            NSLayoutConstraint.activate([
                pageControl.heightAnchor.constraint(equalToConstant: 36),
                pageControl.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                pageControl.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                pageControl.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            ])
        }

        collectionView.dataSource = self
        collectionView.delegate = self
    }

    /**
        Sets up the view controller
     */
    private func setupSelf() {
        presenter.traits = traits
        presenter.configure(delegate: self, sdk: sdk)
        presenter.widgetKinds = widgetKinds
        presenter.loadData()
    }

    /// A factory method that is called when a widget is ready to be displayed.
    /// - Parameter EmBetWidgetModel: The `EmBetWidgetModel` to be displayed
    /// - Returns: A `UIViewController` that represents the Widget. Return `nil` to ignore this widget.
    private func makeWidget(_ widgetModel: EmBetWidgetModel) -> UIViewController? {
        if let widget = EBWidgetFactory.defaultWidgetFor(widgetModel: widgetModel) {
            widget.prepareAsTimelineWidget()
            return widget
        }
        return nil
    }

    /**
        Creates the footer view
     */
    private func createLoadingFooterView() -> UIView {
        let loadingView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 44))
        let spinner = UIActivityIndicatorView()
        spinner.center = loadingView.center
        loadingView.addSubview(spinner)
        spinner.startAnimating()

        return loadingView
    }
}

// MARK: - UITableViewDelegate

/// :nodoc:
extension EmBetTimelineViewController: UITableViewDelegate {
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if flowType == .horizontal {
            let offSet = scrollView.contentOffset.x
            let width = scrollView.frame.width
            let horizontalCenter = width / 2
            DispatchQueue.main.async {
                self.pageControl.currentPage = Int(offSet + horizontalCenter) / Int(width)
            }
        }
    }
}

// MARK: - UICollectionView

extension EmBetTimelineViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    public func collectionView(_: UICollectionView, numberOfItemsInSection _: Int) -> Int {
        return presenter.widgetModels.count
    }

    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? WidgetCollectionViewCell else {
            return UICollectionViewCell()
        }

        let widgetModel = presenter.widgetModels[indexPath.row]

        if let widget = makeWidget(widgetModel) {
            addChild(widget)
            widget.didMove(toParent: self)
            let widgetView: UIView! = widget.view
            widgetView.translatesAutoresizingMaskIntoConstraints = false
            cell.contentView.addSubview(widgetView)

            if flowType == .vertical {
                NSLayoutConstraint.activate([
                    widgetView.leadingAnchor.constraint(equalTo: cell.contentView.leadingAnchor),
                    widgetView.trailingAnchor.constraint(equalTo: cell.contentView.trailingAnchor),
                    widgetView.topAnchor.constraint(equalTo: cell.contentView.topAnchor),
                    widgetView.bottomAnchor.constraint(equalTo: cell.contentView.bottomAnchor),
                    widgetView.widthAnchor.constraint(equalToConstant: collectionView.collectionViewLayout.collectionViewContentSize.width - 16.0),
                ])
            } else if flowType == .horizontal {
                NSLayoutConstraint.activate([
                    widgetView.leadingAnchor.constraint(equalTo: cell.contentView.leadingAnchor),
                    widgetView.trailingAnchor.constraint(equalTo: cell.contentView.trailingAnchor),
                    widgetView.centerYAnchor.constraint(equalTo: cell.contentView.centerYAnchor),
                ])
            }
            cell.widget = widget
        }

        return cell
    }
}

// MARK: - Actions

extension EmBetTimelineViewController {
    /**
        Action called when the the user pulls to refresh
     */
    @objc
    private func refreshControllerPulled() {
        presenter.loadData()
    }
}

// MARK: - Empty collection view

extension EmBetTimelineViewController {
    /**
        Shows/creates the empty view.
        Tries to load it from the delegate, if no view is provided, it loads a default empty view.
     */
    private func showEmptyView() {
        if emptyTimelineView == nil {
            let emptyView = delegate?.timelineEmptyView() ?? getDefaultEmptyView()
            emptyView.translatesAutoresizingMaskIntoConstraints = false
            emptyTimelineView = emptyView

            if let emptyTimelineView = emptyTimelineView {
                view.addSubview(emptyTimelineView)

                NSLayoutConstraint.activate([
                    emptyTimelineView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                    emptyTimelineView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
                    emptyTimelineView.topAnchor.constraint(greaterThanOrEqualTo: view.topAnchor),
                    emptyTimelineView.leadingAnchor.constraint(greaterThanOrEqualTo: view.leadingAnchor),
                    emptyTimelineView.bottomAnchor.constraint(lessThanOrEqualTo: view.bottomAnchor),
                    emptyTimelineView.trailingAnchor.constraint(lessThanOrEqualTo: view.trailingAnchor),
                ])
            }
        } else {
            emptyTimelineView?.isHidden = false
        }
    }

    /**
        Hides the empty timeline view.
     */
    private func hideEmptyView() {
        emptyTimelineView?.isHidden = true
    }

    /**
        Returns a default implementation fot he empty timeline view
     */
    private func getDefaultEmptyView() -> UIView {
        let emptyDefaultView = UILabel()
        emptyDefaultView.translatesAutoresizingMaskIntoConstraints = false
        emptyDefaultView.textAlignment = .center
        emptyDefaultView.numberOfLines = 0
        emptyDefaultView.text = "There currently are no\n widgets, please try again later"
        return emptyDefaultView
    }
}

public extension EmBetTimelineViewController {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        /// Handle infinite scroll
        if flowType == .vertical {
            let offsetY = scrollView.contentOffset.y
            let contentHeight = scrollView.contentSize.height

            if offsetY > contentHeight - scrollView.frame.height, !presenter.isLoading {
                presenter.loadMoreData(force: false)
            }
        } else {
            let offsetY = scrollView.contentOffset.x
            let contentWidth = scrollView.contentSize.width

            if offsetY > contentWidth - scrollView.frame.width, !presenter.isLoading {
                presenter.loadMoreData(force: false)
            }
        }
    }
}

extension EmBetTimelineViewController: EmBetPostedWidgetsPresenterDelegate {
    func presenterDidFinishLoading() {
        DispatchQueue.main.async { [weak self] in
            #if os(iOS)
                self?.refreshControl.endRefreshing()
            #endif
            self?.emptyTableLoadingIndicator.stopAnimating()
        }
    }

    func presenterShouldHideEmptyDataView() {
        DispatchQueue.main.async { [weak self] in
            self?.hideEmptyView()
        }
    }

    func presenterShouldShowEmptyDataView() {
        DispatchQueue.main.async { [weak self] in
            self?.showEmptyView()
        }
    }

    func presenterDidLoadPageWithNewItems(insertIndexes _: [IndexPath]) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.pageControl.numberOfPages = self.presenter.widgetModels.count
            self.collectionView.reloadData()
            self.collectionView.isHidden = false
        }
    }

    func presenterDidReceiveNewUpdate(pubsubUpdate _: EmBetPubSubEvents) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.pageControl.numberOfPages = self.presenter.widgetModels.count
            self.collectionView.reloadData()
        }
    }
}
