//
//  EBVerticalTimelineFlow.swift
//
//
//  Created by Ljupco Nastevski on 5.7.23.
//

import UIKit

internal final class EBVerticalTimelineFlowLayout: UICollectionViewFlowLayout {
    override public func prepare() {
        super.prepare()

        estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        sectionInset = UIEdgeInsets(top: minimumInteritemSpacing, left: 0.0, bottom: 0.0, right: 0.0)
        sectionInsetReference = .fromSafeArea

        register(HorizontalLineDecorationView.self, forDecorationViewOfKind: HorizontalLineDecorationView.decorationViewKind)
    }

    override public func initialLayoutAttributesForAppearingDecorationElement(ofKind _: String, at _: IndexPath) -> UICollectionViewLayoutAttributes? {
        return nil
    }

    override public func finalLayoutAttributesForDisappearingDecorationElement(ofKind _: String, at _: IndexPath) -> UICollectionViewLayoutAttributes? {
        return nil
    }

    override public func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let layoutAttributes = super.layoutAttributesForElements(in: rect) ?? []
        let lineWidth = minimumLineSpacing

        var decorationAttributes: [UICollectionViewLayoutAttributes] = []

        // Create layout attributes, skiping the first item
        // As separator is not neededn at the top of the first item
        for layoutAttribute in layoutAttributes where layoutAttribute.indexPath.item > 0 {
            let separatorAttribute = UICollectionViewLayoutAttributes(forDecorationViewOfKind: HorizontalLineDecorationView.decorationViewKind,
                                                                      with: layoutAttribute.indexPath)
            let cellFrame = layoutAttribute.frame
            separatorAttribute.frame = CGRect(x: cellFrame.origin.x,
                                              y: cellFrame.origin.y - lineWidth,
                                              width: cellFrame.size.width,
                                              height: lineWidth)
            separatorAttribute.zIndex = 0
            decorationAttributes.append(separatorAttribute)
        }

        return layoutAttributes + decorationAttributes
    }
}

class HorizontalLineDecorationView: UICollectionReusableView {
    let horizontalInset: CGFloat = 18

    let lineWidth: CGFloat = 1.0

    static let decorationViewKind = "HorizontalLineDecorationView"

    let lineView = UIView()

    override init(frame: CGRect) {
        super.init(frame: frame)

        lineView.backgroundColor = .lightGray.withAlphaComponent(0.7)

        addSubview(lineView)

        lineView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            lineView.heightAnchor.constraint(equalToConstant: lineWidth),
            lineView.leftAnchor.constraint(equalTo: leftAnchor, constant: horizontalInset),
            lineView.rightAnchor.constraint(equalTo: rightAnchor, constant: -horizontalInset),
            lineView.centerYAnchor.constraint(equalTo: centerYAnchor),
        ])
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
