//
//  EBHorizontalTimelineFlowLayout.swift
//
//
//  Created by Ljupco Nastevski on 4.8.23.
//

import UIKit

internal final class EBHorizontalTimelineFlowLayout: UICollectionViewFlowLayout {
    override public init() {
        super.init()
        scrollDirection = .horizontal
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override public func prepare() {
        super.prepare()

        guard let collectionView = collectionView else { return }
        collectionView.alwaysBounceVertical = false

        let availableWidth = collectionView.bounds.inset(by: collectionView.layoutMargins).width
        let availableHeight = collectionView.bounds.inset(by: collectionView.layoutMargins).height

        itemSize = CGSize(width: availableWidth, height: availableHeight)
        minimumLineSpacing = collectionView.bounds.width - availableWidth
        sectionInsetReference = .fromSafeArea

        sectionInset = UIEdgeInsets(top: 0, left: minimumLineSpacing / 2, bottom: 0, right: minimumLineSpacing / 2)
    }
}
