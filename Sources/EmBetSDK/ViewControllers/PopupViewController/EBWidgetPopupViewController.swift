//
//  EBWidgetPopupViewController.swift
//
//
//  Created by Ljupco Nastevski on 22.3.22.
//

import LiveLikeSwift
import UIKit

/**
 A `EBWidgetPopupViewController` instance represents a view controller that handles widgets for the `EmBetSDK`.

 Once an instance of `EBWidgetPopupViewController` has been created, a `EBSDKConfiguration` object needs to be set to link the `EBWidgetPopupViewController` with the program/CMS.

 The `EBWidgetPopupViewController` can be presented as-is or placed inside a `UIView` as a child UIViewController. See [Apple Documentation](https://developer.apple.com/library/archive/featuredarticles/ViewControllerPGforiPhoneOS/ImplementingaContainerViewController.html#//apple_ref/doc/uid/TP40007457-CH11-SW1) for more information.
 */
@objc
public class EBWidgetPopupViewController: UIViewController {
    private lazy var widgetViewController = EBBaseWidgetPopupViewController(sdk: sdk)

    private var sdk: EmBetSDK

    private lazy var segmentator: UserSegmentator = .init(internalSDK: self.sdk.internalSDK)

    @objc
    public weak var delegate: EBWidgetPopupViewControllerDelegate?

    @objc
    public required init(sdk: EmBetSDK) {
        self.sdk = sdk
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
        setupWidgetViewController()
    }

    override public func loadView() {
        view = PassthroughView()
        view.backgroundColor = .clear
        view.clipsToBounds = true
    }

    private func setupWidgetViewController() {
        widgetViewController.widgetStateController = EBDefaultWidgetStateController(closeButtonAction: { [weak self] in
            self?.widgetViewController.dismissWidget(direction: .up)
        }, widgetFinishedCompletion: { [weak self] widget in
            guard widget.id == self?.widgetViewController.currentWidget?.id else { return }
            self?.widgetViewController.dismissWidget(direction: .up)
        })

        addChild(widgetViewController)
        widgetViewController.didMove(toParent: self)
        widgetViewController.delegate = self

        widgetViewController.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(widgetViewController.view)
        NSLayoutConstraint.activate([
            widgetViewController.view.topAnchor.constraint(equalTo: view.topAnchor),
            widgetViewController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            widgetViewController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            widgetViewController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
    }
}

extension EBWidgetPopupViewController: EBBaseWidgetPopupViewControllerDelegate {
    func widgetViewController(didDismiss widget: Widget) {
        guard let emBetWidget = widget as? EmBetWidget else { return }
        delegate?.widgetViewController(didDismiss: emBetWidget)
    }

    func widgetViewController(didDisplay widget: Widget) {
        guard let emBetWidget = widget as? EmBetWidget else { return }
        delegate?.widgetViewController(didDisplay: emBetWidget)
    }

    func widgetViewController(willDismiss widget: Widget) {
        guard let emBetWidget = widget as? EmBetWidget else { return }
        delegate?.widgetViewController(willDismiss: emBetWidget)
    }

    func widgetViewController(willDisplay widget: Widget) {
        guard let emBetWidget = widget as? EmBetWidget else { return }
        delegate?.widgetViewController(willDisplay: emBetWidget)
    }

    func widgetViewController(willEnqueueWidget widgetModel: WidgetModel) -> Widget? {
        let emBetWidget = widgetModel as EmBetWidgetModel

        // Filter only permited widgets
        if !EmBetWidgetKind.permitedLiveWidgetKinds.contains(emBetWidget.widgetKind) {
            return nil
        }

        // Persistent widgets should not be shown in popup controller
        if widgetModel.isPersistent == true {
            return nil
        }

        // Check if the user passes the segmentator rules
        segmentator.userData = sdk.internalSDK.profile.current?.customData
        segmentator.rule = widgetModel.customData
        guard segmentator.applyRuleToData() == true else { return nil }

        // Create the widget
        let widget = EBWidgetFactory.defaultWidgetFor(widgetModel: emBetWidget, eventManger: sdk.internalSDK.eventManger)

        // Checks if the optional delegate function is implemented.
        if delegate?.widgetViewController != nil {
            widget?.interactionDelegate = self
        }
        return widget
    }
}

/**
    Widget interaction delegate
 */
extension EBWidgetPopupViewController: EmBetWidgetInteractionDelegate {
    public func widget(didPressCTAWith event: EmBetCTAEvent) {
        guard let embetWidget = widgetViewController.currentWidget as? EmBetWidget else { return }
        delegate?.widgetViewController?(current: embetWidget, didPressCTAButtonWithEvent: event)
    }
}
