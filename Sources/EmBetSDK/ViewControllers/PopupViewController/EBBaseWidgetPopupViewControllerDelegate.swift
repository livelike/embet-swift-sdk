//
//  EBBaseWidgetPopupViewControllerDelegate.swift
//
//
//  Created by Ljupco Nastevski on 1.11.23.
//

import Foundation
import LiveLikeSwift

/// Delegate methods to the WidgetPopupViewController
internal protocol EBBaseWidgetPopupViewControllerDelegate: AnyObject {
    /// Called when a Widget is about to animate into the view
    func widgetViewController(willDisplay widget: Widget)
    /// Called immediately after a Widget has finished animating into the view
    func widgetViewController(didDisplay widget: Widget)
    /// Called when a Widget is about to animate out of the view
    func widgetViewController(willDismiss widget: Widget)
    /// Called immediately after a Widget has finished animating out of the view
    func widgetViewController(didDismiss widget: Widget)
    /// Called when the WidgetPopupViewController receives a widget and will enqueue it to be displayed
    /// Return nil to not display the Widget
    func widgetViewController(willEnqueueWidget widgetModel: WidgetModel) -> Widget?
}
