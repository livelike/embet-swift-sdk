//
//  EBWidgetPopupViewControllerDelegate.swift
//
//
//  Created by Ljupco Nastevski on 29.11.22.
//

import Foundation
import LiveLikeSwift

/**
    Widget Popup View Controller events
 */
@objc
public protocol EBWidgetPopupViewControllerDelegate: AnyObject {
    /**
        A widget will be displayed
     */
    @objc
    func widgetViewController(willDisplay widget: EmBetWidget)
    /**
        A widget was displayed
     */
    @objc
    func widgetViewController(didDisplay widget: EmBetWidget)
    /**
        A widget will be dismissed
     */
    @objc
    func widgetViewController(willDismiss widget: EmBetWidget)
    /**
        A widget was dismissed
     */
    @objc
    func widgetViewController(didDismiss widget: EmBetWidget)
    /**
        The current widget CTA button was pressed
     */
    @objc
    optional func widgetViewController(current widget: EmBetWidget, didPressCTAButtonWithEvent widgetEvent: EmBetCTAEvent)
}
