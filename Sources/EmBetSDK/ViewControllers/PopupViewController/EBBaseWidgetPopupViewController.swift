//
//  EBBaseWidgetPopupViewController.swift
//
//
//  Created by Ljupco Nastevski on 1.11.23.
//

import LiveLikeSwift
import UIKit

/// Direction with the widget
internal enum Direction: Int {
    /// Left direction
    case left = 0
    /// Right direction
    case right
    /// Up direction
    case up
    /// Down direction
    case down
}

/**
    Base class for Widget popover controller
 */
internal class EBBaseWidgetPopupViewController: UIViewController {
    // MARK: Properties

    /**
        EBBaseWidgetPopupViewControllerDelegate used for widget events
     */
    public weak var delegate: EBBaseWidgetPopupViewControllerDelegate?
    /**
        The `widgetStateController`used for defining how the widgets will act in each state
     */
    public lazy var widgetStateController: WidgetViewDelegate = DefaultWidgetStateController(
        closeButtonAction: { [weak self] in
            self?.dismissWidget(direction: .up)
        },
        widgetFinishedCompletion: { [weak self] widget in
            guard widget.id == self?.currentWidget?.id else { return }
            self?.dismissWidget(direction: .up)
        }
    )
    /**
        The `Widget` currently displayed in the view (if any)
     */
    public internal(set) var currentWidget: Widget?
    /**
        Queue for displaying widgets
     */
    private var widgetsToDisplayQueue: Queue<WidgetModel> = Queue()
    /**
        A container view for handling animations and swipe gesture
     */
    private let widgetContainer: UIView = .init()
    /**
        Curently displaying widget center x display
     */
    private var widgetContainerXConstraint: NSLayoutConstraint!
    /**
        Curently displaying widget top anchor
     */
    private var widgetContainerTopAnchorConstraint: NSLayoutConstraint!
    /**
        Swipe to dismiss gesture recognizer
     */
    private var swipeGesture: UISwipeGestureRecognizer?
    /**
        SDK instance
     */
    private let sdk: EmBetSDK

    // MARK: Init Functions

    deinit {
        self.sdk.internalSDK.session.widgetEventsManager.removeEventObserver(observer: self)
    }

    required init(sdk: EmBetSDK) {
        self.sdk = sdk
        super.init(nibName: nil, bundle: nil)
        sdk.internalSDK.session.widgetEventsManager.addEventObserver(observer: self) { [weak self] widgetModel in
            self?.widgetsToDisplayQueue.enqueue(element: widgetModel)
            self?.showNextWidgetInQueue()
        }
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /// :nodoc:
    override func loadView() {
        view = PassthroughView()
        view.backgroundColor = .clear
        view.clipsToBounds = true
    }

    /// :nodoc:
    override func viewDidLoad() {
        super.viewDidLoad()

        widgetContainer.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(widgetContainer)

        widgetContainerTopAnchorConstraint = widgetContainer.topAnchor.constraint(equalTo: view.topAnchor, constant: 16)
        widgetContainerXConstraint = widgetContainer.centerXAnchor.constraint(equalTo: view.centerXAnchor)

        NSLayoutConstraint.activate([
            widgetContainer.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -32),
            widgetContainerXConstraint,
            widgetContainerTopAnchorConstraint,
        ])
    }

    /// :nodoc:
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        clearDisplayedWidget()
    }

    private func addSwipeToDismissGesture(to view: UIView) {
        let swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(sender:)))
        swipeGesture.delegate = self
        view.addGestureRecognizer(swipeGesture)
        self.swipeGesture = swipeGesture
    }

    @objc
    private func handleSwipe(sender _: UISwipeGestureRecognizer) {
        dismissWidget(direction: .right)
    }

    @objc
    private func scrollViewDidChange(_ scrollView: UIScrollView) {
        swipeGesture?.isEnabled = scrollView.contentOffset.x <= 1.0
    }

    /**
        Clears the curently displayed widget and removes it from superview.
     */
    private func clearDisplayedWidget() {
        guard let displayedWidget = currentWidget else { return }

        displayedWidget.delegate = nil
        displayedWidget.view.removeFromSuperview()
        displayedWidget.removeFromParent()
        currentWidget = nil
        delegate?.widgetViewController(didDismiss: displayedWidget)
    }

    /**
        Dismisses the current widget, if present
     */
    func dismissWidget(direction: Direction) {
        guard let currentWidget = currentWidget else { return }
        delegate?.widgetViewController(willDismiss: currentWidget)

        animateOut(
            direction: direction,
            completion: { [weak self] in
                guard let self = self else { return }
                self.clearDisplayedWidget()
                // immediately show next widget if any in queue
                self.showNextWidgetInQueue()
            }
        )
    }

    /**
        Shows the next widget in queue
     */
    private func showNextWidgetInQueue() {
        guard sdk.internalSDK.session.internalSession != nil else { return }

        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            if
                self.currentWidget == nil,
                let nextWidgetModel = self.widgetsToDisplayQueue.dequeue(),
                let nextWidget = self.makeWidget(from: nextWidgetModel)
            {
                self.clearDisplayedWidget()
                self.delegate?.widgetViewController(willDisplay: nextWidget)
                self.currentWidget = nextWidget

                nextWidget.view.translatesAutoresizingMaskIntoConstraints = false
                self.addChild(nextWidget)
                self.widgetContainer.addSubview(nextWidget.view)
                nextWidget.didMove(toParent: self)

                nextWidget.view.constraintsFill(to: self.widgetContainer)

                self.animateIn { [weak self] in
                    guard let self = self else { return }
                    nextWidget.delegate = self.widgetStateController
                    nextWidget.moveToNextState()
                    self.addSwipeToDismissGesture(to: nextWidget.dismissSwipeableView)
                    self.delegate?.widgetViewController(didDisplay: nextWidget)
                }
            }
        }
    }

    /**
        Animates in the current widget
     */
    private func animateIn(completion: (() -> Void)? = nil) {
        widgetContainerTopAnchorConstraint.constant = -widgetContainer.bounds.height
        widgetContainerXConstraint.constant = 0
        view.layoutIfNeeded()

        UIView.animate(
            withDuration: 0.98,
            delay: 0,
            usingSpringWithDamping: 0.5,
            initialSpringVelocity: 0,
            options: .curveEaseInOut,
            animations: {
                self.widgetContainerTopAnchorConstraint.constant = 16
                self.view.layoutIfNeeded()
            }, completion: { _ in
                if let completion = completion {
                    completion()
                }
            }
        )
    }

    /**
        Animates out the current widget
     */
    private func animateOut(direction: Direction, completion: @escaping (() -> Void) = {}) {
        let constraint: NSLayoutConstraint
        let multiplier: Int
        let offset: CGFloat

        switch direction {
        case .up, .down:
            constraint = widgetContainerTopAnchorConstraint
            offset = widgetContainer.bounds.height
        case .left, .right:
            constraint = widgetContainerXConstraint
            offset = (view.bounds.width / 2) + widgetContainer.bounds.width
        }

        switch direction {
        case .right, .down:
            multiplier = 1
        case .up, .left:
            multiplier = -1
        }

        UIView.animate(
            withDuration: 0.33,
            delay: 0,
            options: [.curveEaseInOut],
            animations: {
                constraint.constant = offset * CGFloat(multiplier)
                self.view.layoutIfNeeded()
            }, completion: { _ in

                completion()
            }
        )
    }

    /**
        Makes a widget from the delegate or from the DefaultWidgetFactory
     */
    private func makeWidget(from widgetModel: WidgetModel) -> Widget? {
        if let delegate = delegate {
            return delegate.widgetViewController(willEnqueueWidget: widgetModel)
        } else {
            return EBWidgetFactory.defaultWidgetFor(widgetModel: widgetModel, eventManger: sdk.internalSDK.eventManger)
        }
    }
}

// MARK: - UIGestureRecognizerDelegate

extension EBBaseWidgetPopupViewController: UIGestureRecognizerDelegate {
    public func gestureRecognizer(_: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith _: UIGestureRecognizer) -> Bool {
        return true
    }
}

private extension UIView {
    func constraintsFill(to parentView: UIView, offset: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        let constraints = [
            parentView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: offset),
            parentView.topAnchor.constraint(equalTo: topAnchor, constant: -offset),
            parentView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: offset),
            parentView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: -offset),
        ]

        NSLayoutConstraint.activate(constraints)
    }
}
