//
//  File.swift
//
//
//  Created by Ljupco Nastevski on 27.6.22.
//

import Foundation
import LiveLikeSwift

/**
    Internal definition of the widget state controller
 */
internal class EBDefaultWidgetStateController {
    private let closeButtonAction: () -> Void
    private let widgetFinishedCompletion: (WidgetViewModel) -> Void

    public init(
        closeButtonAction: @escaping () -> Void,
        widgetFinishedCompletion: @escaping (WidgetViewModel) -> Void
    ) {
        self.closeButtonAction = closeButtonAction
        self.widgetFinishedCompletion = widgetFinishedCompletion
    }
}

extension EBDefaultWidgetStateController: WidgetViewDelegate {
    public func widgetDidEnterState(widget: WidgetViewModel, state: WidgetState) {
        log.info("Widget Did Enter State: \(String(describing: state))")
        switch state {
        case .ready:
            break
        case .interacting:

            weak var weakWidget = widget
            // Call timer and close button action when widget is ready
            widget.addTimer(seconds: widget.interactionTimeInterval ?? 5) { _ in
                weakWidget?.moveToNextState()
            }
            widget.addCloseButton { [weak self] _ in
                self?.closeButtonAction()
            }

        case .results:
            break
        case .finished:

            // Dismiss the widget after a delay to allow the user to see the final results
            if widget.kind.isOf(.imagePrediction,
                                .textPrediction,
                                .textQuiz)
            {
                DispatchQueue.main.asyncAfter(deadline: .now() + LLConstants.DefaultResultsShowingDuration) { [weak self] in
                    self?.widgetFinishedCompletion(widget)
                }
            } else {
                // Dismiss widget immediately
                widgetFinishedCompletion(widget)
            }
        }
    }

    public func widgetStateCanComplete(widget: WidgetViewModel, state: WidgetState) {
        log.info("Widget State Can Complete: \(String(describing: state))")
        switch state {
        case .ready:
            break
        case .interacting:
            break
        case .results:
            if widget.kind.isOf(
                .imagePredictionFollowUp,
                .textPredictionFollowUp,
                .imageNumberPredictionFollowUp
            ) {
                weak var wWidget = widget
                widget.addTimer(seconds: widget.interactionTimeInterval ?? LLConstants.DefaultResultsShowingDuration) { _ in
                    wWidget?.moveToNextState()
                }
            } else {
                widget.moveToNextState()
            }
        case .finished:
            break
        }
    }

    public func userDidInteract(_ widget: WidgetViewModel) {
        log.info("\(widget.kind.displayName) Widget Did Recieve Interaction")
    }
}

extension WidgetKind {
    /// Checks whether the WidgetKind is contained in elements
    func isOf(_ elements: WidgetKind...) -> Bool {
        return elements.contains(self)
    }
}
