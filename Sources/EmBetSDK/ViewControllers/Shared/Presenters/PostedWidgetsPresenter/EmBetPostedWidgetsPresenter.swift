//
//  EmBetPostedWidgetsPresenter.swift
//
//
//  Created by Ljupco Nastevski on 23.11.23.
//

import Foundation

internal protocol EmBetPostedWidgetsPresenter {
    /**
        The loaded widget models
     */
    var widgetModels: [EmBetWidgetModel] { get }
    /**
        Value that determines if the presenter is currently loading data
     */
    var isLoading: Bool { get }
    /**
        Optional configuration - set of widget kinds to fetch
     */
    var widgetKinds: Set<EmBetWidgetKind> { get set }
    /**
        Optional configuration - additional traits for the presenter
     */
    var traits: EmBetTimelineViewControllerTraits { get set }
    /**
        Required configuration for the presenter
     */
    func configure(delegate: EmBetPostedWidgetsPresenterDelegate, sdk: EmBetSDK)
    /**
        Starts loading data
     */
    func loadData()
    /**
        Request loading more data
     */
    func loadMoreData(force: Bool)
}
