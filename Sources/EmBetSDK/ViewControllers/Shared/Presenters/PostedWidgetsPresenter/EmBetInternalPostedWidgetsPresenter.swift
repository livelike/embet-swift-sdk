//
//  EmBetInternalPostedWidgetsPresenter.swift
//
//
//  Created by Ljupco Nastevski on 23.11.23.
//

import LiveLikeSwift
import PubNub
import UIKit

internal class EmBetInternalPostedWidgetsPresenter: EmBetPostedWidgetsPresenter {
    /**
        Page size for getting new widgets.
        Pagesize is greater than 20 because we expect some widgets to
        be hidden during segmentation that is done localy, as well filtering
        widgets by their `isHidden` attribute.
     */
    private let TimelinePageSize = 50

    /**
        The widget kind to filter
     */
    internal var widgetKinds: Set<EmBetWidgetKind> = []

    /**
        The widget kind to filter
     */
    internal var traits: EmBetTimelineViewControllerTraits = []

    /**
        A bool value determening if more data should be loaded
     */
    private var shouldLoadMore: Bool = true

    /**
        Determines whether fetching data from backend is in progress
     */
    internal var isLoading: Bool = true {
        didSet {
            if isLoading == false {
                delegate?.presenterDidFinishLoading()
            }
        }
    }

    /**
        `EmBetPostedWidgetsPresenter` protocol property.
     */
    internal var widgetModels: [EmBetWidgetModel] {
        return filteredWidgetModels
    }

    /**
        Determines whether fetching data from backend is in progress
     */
    private var _widgetModels: [EmBetWidgetModel] = []

    /**
        Filtered models
     */
    private var filteredWidgetModels: [EmBetWidgetModel] {
        return _widgetModels.filter { widgetModel in

            if let hit = liveAttributes[widgetModel.id],
               let hidden = hit.first(where: { $0.key == EmBetWidgetAttributeKeys.isHidden })
            {
                if hidden.value == "false" {
                    return true
                } else {
                    return false
                }
            }

            if widgetModel.isHidden == false {
                return true
            }

            return false
        }
    }

    /**
        Live attrbutes is provided as a TEMP solution to filtering widgets
        It holds widget attributes as value for the widget ID
     */
    private var liveAttributes: [String: [EmBetWidgetAttribute]] = [:]

    /**
        The current content session
     */
    private weak var session: ContentSession?

    /**
        The presenters delegate
     */
    private weak var delegate: EmBetPostedWidgetsPresenterDelegate?

    /**
        SDK reference
     */
    private weak var sdk: EmBetSDK?

    /**
        Widget attributes
     */
    private var attributes: [Attribute] {
        var attr: [Attribute] = []
        if traits.contains(.persistentWidgetsOnly) {
            attr.append(Attribute(key: EmBetWidgetAttributeKeys.isPersistent, value: "true"))
        }
        return attr
    }

    /**
        User Segmentator instance
     */
    private lazy var segmentator: UserSegmentator? = {
        guard let sdk = sdk else { return nil }
        return UserSegmentator(internalSDK: sdk.internalSDK)
    }()

    /**
        PubSub Client
     */
    private var pubSubClient: EmBetInternalPubSubWidgetClient?

    deinit {
        sdk?.internalSDK.session.widgetEventsManager.removeEventObserver(observer: self)
        pubSubClient?.removeAllListeners()
    }

    internal func configure(delegate: EmBetPostedWidgetsPresenterDelegate, sdk: EmBetSDK) {
        self.delegate = delegate
        self.sdk = sdk
        session = sdk.internalSDK.session.internalSession
        // Checks if the timeline should observe for new widgets
        if traits.contains(.addsNewWidgets) == true {
            configureWidgetEventsManager(sdk: sdk)
        }
        // Checks if the timeline should listen for real-time updates.
        if traits.contains(.realtimeUpdates) == true {
            setupPubSubClient()
        }
    }

    private func configureWidgetEventsManager(sdk: EmBetSDK) {
        sdk.internalSDK.session.widgetEventsManager.addEventObserver(observer: self) { [weak self] widgetModel in
            guard let self = self else { return }

            // Create the widget
            let emBetWidgetModel = widgetModel as EmBetWidgetModel

            if self.traits.contains(.persistentWidgetsOnly), emBetWidgetModel.isPersistent == false {
                return
            }

            // Check if the user passes the segmentator rules
            self.segmentator?.userData = sdk.internalSDK.profile.current?.customData
            self.segmentator?.rule = widgetModel.customData
            guard self.segmentator?.applyRuleToData() == true else { return }

            self._widgetModels.insert(emBetWidgetModel, at: 0)

            let customIndexPaths: [IndexPath] = [IndexPath(row: 0, section: 0)]
            self.delegate?.presenterDidLoadPageWithNewItems(insertIndexes: customIndexPaths)
        }
    }

    internal func loadData() {
        if widgetKinds.count == 0 {
            shouldLoadMore = false
            isLoading = false
            delegate?.presenterShouldShowEmptyDataView()
            return
        }

        guard let sdk = sdk, let segmentator = segmentator else { return }

        shouldLoadMore = true
        delegate?.presenterShouldHideEmptyDataView()

        let selectedWidgetKinds = EmBetWidgetKind.toEngagementWidgetKind(embetWidgetKinds: widgetKinds)
        let options = GetWidgetModelsRequestOptions(widgetStatus: .published,
                                                    widgetKind: selectedWidgetKinds,
                                                    widgetOrdering: .recent,
                                                    widgetAttributes: attributes,
                                                    pageSize: LLConstants.DefaultPageSize)

        session?.getWidgetModels(page: .first, options: options, completion: { [weak self] result in
            guard let self = self else { return }
            self._widgetModels.removeAll()
            self.liveAttributes.removeAll()
            // Result
            switch result {
            case let .success(models):

                if models.count == 0 {
                    self.shouldLoadMore = false
                    self.isLoading = false
                    self.delegate?.presenterShouldShowEmptyDataView()
                    return
                }

                // Filters the widgets using segmentation.
                var segmentedWidgets: [WidgetModel] = []
                if sdk.internalSDK.isSegmentationEnabled == true {
                    segmentedWidgets = models.filter { widgetModel in
                        segmentator.userData = sdk.internalSDK.profile.current?.customData
                        segmentator.rule = widgetModel.customData
                        return segmentator.applyRuleToData()
                    }
                } else {
                    segmentedWidgets = models
                }

                // Allows the integrator to filter widgets.
                let filteredModels = segmentedWidgets

                // Get next page if the number of filtered results is less than 20.
                // This can happen if there are no triggered widgets or
                // due to segmentation, count is 0.
                self._widgetModels = filteredModels
                if filteredModels.count < 20 {
                    self.loadMoreData(force: true)
                    return
                }
                self.isLoading = false
                self.delegate?.presenterDidLoadPageWithNewItems(insertIndexes: [])
            case let .failure(error):
                self.isLoading = false
                log.error(error)
            }
        })
    }

    /**
        Gets the next timeline widget page
     */
    internal func loadMoreData(force: Bool = false) {
        guard let sdk = sdk, let segmentator = segmentator else { return }

        if force == false, shouldLoadMore == false || isLoading == true {
            return
        }
        isLoading = true

        let selectedWidgetKinds = EmBetWidgetKind.toEngagementWidgetKind(embetWidgetKinds: widgetKinds)
        let options = GetWidgetModelsRequestOptions(widgetStatus: .published,
                                                    widgetKind: selectedWidgetKinds,
                                                    widgetOrdering: .recent,
                                                    widgetAttributes: attributes,
                                                    pageSize: LLConstants.DefaultPageSize)

        session?.getWidgetModels(page: .next, options: options, completion: { [weak self] result in
            guard let self = self else { return }
            defer {
                self.isLoading = false
            }
            switch result {
            case let .success(models):
                if models.count == 0 {
                    // If number of total widgets is equal to 0
                    // and there is no next page, show the empty view.
                    if self._widgetModels.count == 0 {
                        self.delegate?.presenterShouldShowEmptyDataView()
                    } else {
                        self.delegate?.presenterDidLoadPageWithNewItems(insertIndexes: [])
                    }
                    self.shouldLoadMore = false
                    return
                }

                // Filters the widgets using segmentation.
                var segmentedWidgets: [WidgetModel] = []
                if sdk.internalSDK.isSegmentationEnabled == true {
                    segmentedWidgets = models.filter { widgetModel in
                        segmentator.userData = sdk.internalSDK.profile.current?.customData
                        segmentator.rule = widgetModel.customData
                        return segmentator.applyRuleToData()
                    }
                } else {
                    segmentedWidgets = models
                }

                let filteredModels = segmentedWidgets

                let insertionRange = Range(
                    uncheckedBounds: (
                        self._widgetModels.count,
                        self._widgetModels.count + filteredModels.count
                    )
                )

                self._widgetModels.append(contentsOf: filteredModels)

                let indexesOfNewSections = IndexSet(insertionRange)
                let customIndexPaths: [IndexPath] = indexesOfNewSections.map { IndexPath(row: $0, section: 0) }
                self.delegate?.presenterDidLoadPageWithNewItems(insertIndexes: customIndexPaths)
                if self._widgetModels.count < 20 {
                    self.loadMoreData(force: true)
                }
            case let .failure(error):
                if self._widgetModels.count == 0 {
                    self.delegate?.presenterShouldShowEmptyDataView()
                } else {
                    self.delegate?.presenterDidLoadPageWithNewItems(insertIndexes: [])
                }
                self.shouldLoadMore = false
                log.error(error)
            }
        })
    }

    /**
        Sets up the PubSub client
     */
    private func setupPubSubClient() {
        sdk?.internalSDK.getPubSubConnectionDetails { [weak self] result in
            switch result {
            case let .success(success):
                self?.pubSubClient = EmBetInternalPubSubWidgetClient(subscribeKey: success.subscribeKey, origin: success.origin, userID: success.userID, heartbeatTimeout: success.heartbeatTimeout, heartbeatInterval: success.heartbeatInterval)
                self?.setupPubSubListeners()
            case let .failure(error):
                log.error("Failed to establish connection: \(error)")
            }
        }
    }

    /**
        Adds `self` as listener to channels
     */
    internal func setupPubSubListeners() {
        guard let programID = sdk?.internalSDK.session.internalSession?.programID else { return }

        pubSubClient?.addListener(self, toChannel: "embet.\(programID).data_update")
        pubSubClient?.addListener(self, toChannel: "embet.\(programID).widget_update")
    }

    private func updateWidget(widgetUpdate: EmBetPubSubWidgetUpdateModel) {
        widgetUpdate.widgetIds.forEach { widgetID in
            if _widgetModels.first(where: { $0.id == widgetID }) != nil {
                self.liveAttributes[widgetID] = widgetUpdate.widgetAttributes
            }
        }
    }
}

extension EmBetInternalPostedWidgetsPresenter: EmbetWidgetProxyInput {
    func publish(event: WidgetProxyPublishData) {
        switch event.clientEvent {
        case let .widgetUpdate(widgetUpdateModel):
            updateWidget(widgetUpdate: widgetUpdateModel)
        default:
            break
        }
        delegate?.presenterDidReceiveNewUpdate(pubsubUpdate: event.clientEvent)
    }

    func discard(event _: WidgetProxyPublishData, reason: DiscardedReason) {
        log.dev("[PubNub] Event discarded with reason: \(reason)")
    }

    func connectionStatusDidChange(_ connectionStatus: ConnectionStatus) {
        log.dev("[PubNub] Connection status changed: \(connectionStatus)")
    }

    func error(_ error: Error) {
        log.error("[PubNub] Error occured: \(error)")
    }
}
