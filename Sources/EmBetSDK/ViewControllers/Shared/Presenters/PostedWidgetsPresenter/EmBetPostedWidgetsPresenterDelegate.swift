//
//  EmBetPostedWidgetsPresenterDelegate.swift
//
//
//  Created by Ljupco Nastevski on 23.11.23.
//

import Foundation

internal protocol EmBetPostedWidgetsPresenterDelegate: AnyObject {
    /**
        Called once a loading is finished.
     */
    func presenterDidFinishLoading()
    /**
        Called once a loading is finished and there is response data
     */
    func presenterShouldHideEmptyDataView()
    /**
        Called once a loading is finished and there is `NOT`response data
     */
    func presenterShouldShowEmptyDataView()
    /**
        Called once more data is loaded
     */
    func presenterDidLoadPageWithNewItems(insertIndexes: [IndexPath])
    /**
        Did receive new update
     */
    func presenterDidReceiveNewUpdate(pubsubUpdate: EmBetPubSubEvents)
}
