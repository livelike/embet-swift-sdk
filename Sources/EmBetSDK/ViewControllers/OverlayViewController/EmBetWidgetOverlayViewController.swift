//
//  EmBetWidgetOverlayViewController.swift
//
//
//  Created by Ljupco Nastevski on 15.4.24.
//

import LiveLikeSwift
import UIKit

/**
    The view of the widget view controller should be used as an overlay on top of video players.
    Idealy, the frame should match the parent view. Any live widget trigger should be shown here.
 */
public class EmBetWidgetOverlayViewController: UIViewController {
    // MARK: Properties

    /**
        Width is applied when a widget is created.
     */
    public var widgetWidth: CGFloat = 370
    /**
        Scale factor of the widget. Scale is applied on `widgetWidth` when a widget is created.
     */
    public var widgetScaleFactor: CGFloat = 1
    /**
        `EmBetWidgetOverlayDelegate` used for widget events
     */
    public weak var delegate: EmBetWidgetOverlayDelegate?
    /**
        `EmBetWidgetInteractionDelegate` used for widget interaction events
     */
    public weak var interationDelegate: EmBetWidgetInteractionDelegate?
    /**
        The `widgetStateController`used for defining how the widgets will act in each state
     */
    private lazy var widgetStateController: WidgetViewDelegate = EBDefaultWidgetStateController(closeButtonAction: { [weak self] in
        self?.dismissWidget()
    }, widgetFinishedCompletion: { [weak self] widget in
        guard widget.id == self?.currentWidget?.widgetID else { return }
        self?.dismissWidget()
    })

    internal let permitedWidgets: Set<EmBetWidgetKind> = EmBetWidgetKind.permitedLiveWidgetKinds

    /**
        The `Widget` currently displayed in the view (if any)
     */
    public internal(set) var currentWidget: EmBetWidget?
    /**
        The `Widget` contrainer view
     */
    private var widgetContrainer: UIView = {
        var contrainer = UIView()
        contrainer.translatesAutoresizingMaskIntoConstraints = false
        return contrainer
    }()

    /**
        Queue for displaying widgets
     */
    private var widgetsToDisplayQueue: Queue<WidgetModel> = Queue()

    /**
        Swipe to dismiss gesture recognizer
     */
    private var swipeGesture: UISwipeGestureRecognizer?
    /**
        SDK instance
     */
    private let sdk: EmBetSDK
    /**
        User segmentator instance
     */
    private lazy var segmentator: UserSegmentator = .init(internalSDK: self.sdk.internalSDK)

    // MARK: Init Functions

    deinit {
        self.sdk.internalSDK.session.widgetEventsManager.removeEventObserver(observer: self)
    }

    /**
        Initializes the widget overlay view with an SDK instance.
     */
    public required init(sdk: EmBetSDK) {
        self.sdk = sdk

        super.init(nibName: nil, bundle: nil)
        sdk.internalSDK.session.widgetEventsManager.addEventObserver(observer: self) { [weak self] widgetModel in
            self?.widgetsToDisplayQueue.enqueue(element: widgetModel)
            self?.showNextWidgetInQueue()
        }
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /// :nodoc:
    override public func loadView() {
        view = PassthroughView()
        view.backgroundColor = .clear
        view.clipsToBounds = true
    }

    /// :nodoc:
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        clearDisplayedWidget()
    }

    /**
        Adds the swipe to dismiss gesture to the view.
     */
    private func addSwipeToDismissGesture(to view: UIView) {
        let swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(sender:)))
        swipeGesture.delegate = self
        view.addGestureRecognizer(swipeGesture)
        self.swipeGesture = swipeGesture
    }

    @objc
    private func handleSwipe(sender _: UISwipeGestureRecognizer) {
        dismissWidget()
    }

    /**
        Clears the curently displayed widget and removes it from superview.
     */
    private func clearDisplayedWidget() {
        guard let displayedWidget = currentWidget else { return }
        delegate?.widgetOverlay(willDismiss: displayedWidget)
        displayedWidget.delegate = nil
        widgetContrainer.removeFromSuperview()
        displayedWidget.willMove(toParent: nil)
        displayedWidget.removeFromParent()
        displayedWidget.view.removeFromSuperview()
        currentWidget = nil
        delegate?.widgetOverlay(didDismiss: displayedWidget)
    }

    /**
        Dismisses the current widget, if present
     */
    private func dismissWidget() {
        if currentWidget == nil { return }
        // Clear current widget
        clearDisplayedWidget()
        // immediately show next widget if any in queue
        showNextWidgetInQueue()
    }

    /**
        Shows the next widget in queue
     */
    private func showNextWidgetInQueue() {
        guard sdk.internalSDK.session.internalSession != nil else { return }

        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            if
                self.currentWidget == nil,
                let nextWidgetModel = self.widgetsToDisplayQueue.dequeue(),
                let nextWidget = self.makeWidget(from: nextWidgetModel)
            {
                self.clearDisplayedWidget()

                self.delegate?.widgetOverlay(willDisplay: nextWidget)
                self.currentWidget = nextWidget

                nextWidget.view.transform = CGAffineTransform(scaleX: self.widgetScaleFactor, y: self.widgetScaleFactor)
                nextWidget.view.translatesAutoresizingMaskIntoConstraints = false

                self.addChild(nextWidget)
                nextWidget.didMove(toParent: self)
                self.widgetContrainer.addSubview(nextWidget.view)
                self.view.addSubview(self.widgetContrainer)

                NSLayoutConstraint.activate([
                    nextWidget.view.centerXAnchor.constraint(equalTo: widgetContrainer.centerXAnchor),
                    nextWidget.view.centerYAnchor.constraint(equalTo: widgetContrainer.centerYAnchor),

                    widgetContrainer.widthAnchor.constraint(equalTo: nextWidget.view.widthAnchor, multiplier: self.widgetScaleFactor),
                    widgetContrainer.heightAnchor.constraint(equalTo: nextWidget.view.heightAnchor, multiplier: self.widgetScaleFactor),
                ])

                // Sets the position of the widget from the widget payload
                setPosition(widgetView: self.widgetContrainer, position: (nextWidget as? LLBaseWidget)?.customDataPayload.position)

                // Performs the widget entrance animation
                EmBetBasicAnimator.performWidgetAnimation(widgetView: self.widgetContrainer, animation: (nextWidget as? LLBaseWidget)?.customDataPayload.openAnimation)

                nextWidget.delegate = self.widgetStateController
                nextWidget.moveToNextState()
                self.addSwipeToDismissGesture(to: nextWidget.dismissSwipeableView)
                self.delegate?.widgetOverlay(didDisplay: nextWidget)
            }
        }
    }

    /**
        Sets the position of the widget view inside the overlay view controller.
     */
    private func setPosition(widgetView: UIView, position: LLPosition?) {
        // Sets the default widget width
        let multiplied = widgetWidth * widgetScaleFactor
        widgetView.widthAnchor.constraint(equalToConstant: multiplied).isActive = true

        // Default position is top left
        guard let position = position else {
            // Default position is top left
            NSLayoutConstraint.activate([
                widgetView.topAnchor.constraint(equalTo: view.topAnchor),
                widgetView.leftAnchor.constraint(equalTo: view.leftAnchor),
            ])
            return
        }

        var horizontalAnchor: NSLayoutConstraint
        var verticalAnchor: NSLayoutConstraint

        switch position.x {
        case .left:
            horizontalAnchor = widgetView.leftAnchor.constraint(equalTo: view.leftAnchor)
        case .center:
            horizontalAnchor = widgetView.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        case .right:
            horizontalAnchor = widgetView.rightAnchor.constraint(equalTo: view.rightAnchor)
        }

        switch position.y {
        case .top:
            verticalAnchor = widgetView.topAnchor.constraint(equalTo: view.topAnchor)
        case .center:
            verticalAnchor = widgetView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        case .bottom:
            verticalAnchor = widgetView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        }

        // Applies offset to the position, idealy values of offset.x and offset.y
        if let offset = position.offset {
            horizontalAnchor.constant = view.frame.width * CGFloat(offset.x)
            verticalAnchor.constant = view.frame.height * CGFloat(offset.y)
        }

        NSLayoutConstraint.activate([
            horizontalAnchor,
            verticalAnchor,
        ])
    }

    /**
        Makes a widget from the delegate or from the `EBWidgetFactory`
     */
    private func makeWidget(from widgetModel: WidgetModel) -> EmBetWidget? {
        let emBetWidget = widgetModel as EmBetWidgetModel

        // Filter only permited widgets
        if !EmBetWidgetKind.permitedLiveWidgetKinds.contains(emBetWidget.widgetKind) {
            return nil
        }

        // Persistent widgets should not be shown in overlay controller
        if widgetModel.isPersistent == true {
            return nil
        }

        // Check if the user passes the segmentator rules
        segmentator.userData = sdk.internalSDK.profile.current?.customData
        segmentator.rule = widgetModel.customData
        guard segmentator.applyRuleToData() == true else { return nil }

        // Create the widget
        let widget = EBWidgetFactory.defaultWidgetFor(widgetModel: emBetWidget, eventManger: sdk.internalSDK.eventManger)

        // Adds the interaction delegate
        if let interactionDelegate = interationDelegate {
            widget?.interactionDelegate = interactionDelegate
        }
        return widget
    }
}

// MARK: - UIGestureRecognizerDelegate

extension EmBetWidgetOverlayViewController: UIGestureRecognizerDelegate {
    public func gestureRecognizer(_: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith _: UIGestureRecognizer) -> Bool {
        return true
    }
}
