//
//  EmBetWidgetOverlayDelegate.swift
//
//
//  Created by Ljupco Nastevski on 31.5.24.
//

import Foundation

/// Delegate methods to the `EmBetWidgetOverlayViewController`
public protocol EmBetWidgetOverlayDelegate: AnyObject {
    /// Called when a Widget is about to animate into the view
    func widgetOverlay(willDisplay widget: EmBetWidget)
    /// Called immediately after a Widget has finished animating into the view
    func widgetOverlay(didDisplay widget: EmBetWidget)
    /// Called when a Widget is about to animate out of the view
    func widgetOverlay(willDismiss widget: EmBetWidget)
    /// Called immediately after a Widget has finished animating out of the view
    func widgetOverlay(didDismiss widget: EmBetWidget)
}
