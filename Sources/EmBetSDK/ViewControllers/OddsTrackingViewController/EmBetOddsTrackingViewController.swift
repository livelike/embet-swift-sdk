//
//  EmBetOddsTrackingViewController.swift
//
//
//  Created by Ljupco Nastevski on 17.11.23.
//

import LiveLikeSwift
import UIKit

/**
    View controller that embeds the currently active Odds Widget
 */
public final class EmBetOddsTrackingViewController: UIViewController {
    /**
        EmBet SDK instance
     */
    private let sdk: EmBetSDK

    /**
        The widget that is currently showing
     */
    private weak var currentWidget: EmBetWidget?

    /**
        The empty data view to be shown when there are no odds
     */
    private var emptyOddsTrackingView: UIView?

    /**
        The view controllers presenter
     */
    private lazy var presenter: EmBetPostedWidgetsPresenter = EmBetInternalPostedWidgetsPresenter()

    /**
        `EmBetOddsTrackingViewController` delegate
     */
    public weak var delegate: EmBetOddsTrackingViewControllerDelegate?

    public required init(sdk: EmBetSDK) {
        self.sdk = sdk
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
        startPresenter()
    }

    /**
        Configures and notifies the presenter to start loading data
     */
    private func startPresenter() {
        presenter.widgetKinds = [.odds]
        presenter.traits = [.realtimeUpdates, .addsNewWidgets]
        presenter.configure(delegate: self, sdk: sdk)
        presenter.loadData()
    }

    /**
        Removes the currently presented widget, if any, from the controller
     */
    private func removeCurrentWidget() {
        guard let currentWidget = currentWidget else { return }
        currentWidget.willMove(toParent: nil)
        currentWidget.view.removeFromSuperview()
        currentWidget.removeFromParent()
        self.currentWidget = nil
    }

    /**
        Shows/creates the empty view.
        Tries to load it from the delegate, if no view is provided, it loads a default empty view.
     */
    private func showEmptyView() {
        if emptyOddsTrackingView == nil {
            let emptyView = delegate?.emptyDataView() ?? getDefaultEmptyView()
            emptyView.translatesAutoresizingMaskIntoConstraints = false
            emptyOddsTrackingView = emptyView

            if let emptyOddsTrackingView = emptyOddsTrackingView {
                view.addSubview(emptyOddsTrackingView)

                NSLayoutConstraint.activate([
                    emptyOddsTrackingView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                    emptyOddsTrackingView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
                    emptyOddsTrackingView.topAnchor.constraint(greaterThanOrEqualTo: view.topAnchor),
                    emptyOddsTrackingView.leadingAnchor.constraint(greaterThanOrEqualTo: view.leadingAnchor),
                    emptyOddsTrackingView.bottomAnchor.constraint(lessThanOrEqualTo: view.bottomAnchor),
                    emptyOddsTrackingView.trailingAnchor.constraint(lessThanOrEqualTo: view.trailingAnchor),
                ])
            }
        } else {
            emptyOddsTrackingView?.isHidden = false
        }
    }

    /**
        Returns a default implementation fot he empty timeline view
     */
    private func getDefaultEmptyView() -> UIView {
        let emptyDefaultView = UILabel()
        emptyDefaultView.translatesAutoresizingMaskIntoConstraints = false
        emptyDefaultView.textAlignment = .center
        emptyDefaultView.numberOfLines = 0
        emptyDefaultView.text = "There currently are no\n odds, please try again later"
        return emptyDefaultView
    }

    private func setupOddsWidget() {
        // Adds the widget to the view controller
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }

            // Do not show the same widget twice.
            // Temporary solution as better can be found.
            if self.currentWidget?.widgetID == self.presenter.widgetModels.first?.id {
                return
            }

            // Removes existing widget
            self.currentWidget?.view.removeFromSuperview()
            self.currentWidget?.willMove(toParent: nil)
            self.currentWidget?.removeFromParent()
            self.currentWidget = nil

            guard let widgetModel = self.presenter.widgetModels.first,
                  let widget = EBWidgetFactory.defaultWidgetFor(widgetModel: widgetModel, eventManger: sdk.internalSDK.eventManger) else { return }

            self.currentWidget = widget

            self.addChild(widget)
            widget.view.frame = self.view.bounds
            widget.view.translatesAutoresizingMaskIntoConstraints = false
            self.view.addSubview(widget.view)
            widget.didMove(toParent: self)
            NSLayoutConstraint.activate([
                widget.view.topAnchor.constraint(equalTo: self.view.topAnchor),
                widget.view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
                widget.view.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
                widget.view.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            ])
        }
    }

    #if os(tvOS)
        override public var preferredFocusEnvironments: [UIFocusEnvironment] {
            return currentWidget?.preferredFocusEnvironments ?? super.preferredFocusEnvironments
        }
    #endif
}

extension EmBetOddsTrackingViewController: EmBetPostedWidgetsPresenterDelegate {
    func presenterDidFinishLoading() {}

    func presenterShouldHideEmptyDataView() {}

    func presenterShouldShowEmptyDataView() {
        DispatchQueue.main.async { [weak self] in
            self?.showEmptyView()
        }
    }

    func presenterDidLoadPageWithNewItems(insertIndexes _: [IndexPath]) {
        setupOddsWidget()
    }

    func presenterDidReceiveNewUpdate(pubsubUpdate: EmBetPubSubEvents) {
        switch pubsubUpdate {
        case let .oddsUpdate(emBetPubSubOddsUpdateModel):
            // Update current odd widget
            guard let currentWidgetID = currentWidget?.widgetID,
                  let currentOddsWidget = currentWidget as? LLOddsWidgetViewController,
                  emBetPubSubOddsUpdateModel.widgetIds.contains(currentWidgetID)
            else { return }
            currentOddsWidget.setBetDetails(betDetails: emBetPubSubOddsUpdateModel.betDetails)
            currentOddsWidget.setFinalResult(finalResult: emBetPubSubOddsUpdateModel.finalResult)
        case let .widgetUpdate(widgetUpdate):
            guard let currentWidgetID = currentWidget?.widgetID,
                  let currentOddsWidget = currentWidget as? LLBaseWidget,
                  let isSuspendedString = widgetUpdate.widgetAttributes.first(where: { $0.key == EmBetWidgetAttributeKeys.isSuspended })?.value as? NSString,
                  widgetUpdate.widgetIds.contains(currentWidgetID)
            else { return }
            let isSuspended = isSuspendedString.boolValue
            currentOddsWidget.setupSuspendedState(isSuspended: isSuspended)
        }
    }
}
