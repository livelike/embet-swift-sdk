//
//  EmBetOddsTrackingViewControllerDelegate.swift
//
//
//  Created by Ljupco Nastevski on 19.12.23.
//

import UIKit

/**
    Odds tracking view controller delegate
 */
public protocol EmBetOddsTrackingViewControllerDelegate: AnyObject {
    /**
        Provide a view to be shown if there is no active odds widget
     */
    func emptyDataView() -> UIView?
}
