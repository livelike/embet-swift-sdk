//
//  EmBetEvents.swift
//
//
//  Created by Ljupco Nastevski on 25.3.24.
//

import Foundation

public enum EmBetWidgetInteractionName {
    static let WidgetCtaPressed = "widget_cta_pressed"
    static let WidgetDisclaimerExpanded = "widget_disclaimer_expanded"
    static let WidgetDisclaimerCollapsed = "widget_disclaimer_collapsed"
    static let WidgetOptionSelected = "widget_option_selected"
    static let WidgetOptionUnselected = "widget_option_unselected"
    static let WidgetQRViewDismissed = "widget_qrview_dismissed"
    static let WidgetViewingTime = "widget_viewing_time"
}

public struct EmBetWidgetInteractionEvent {
    // The interaction type
    let eventName: String
    // The ID of the widget
    let widgetID: String
    // Optional property where we store option ID or any ID if tied to an action
    let payload: [String: Any]?
}

public enum EmBetEvent {
    case widgetInteraction(EmBetWidgetInteractionEvent)

    public func toDictionary() -> [String: Any] {
        switch self {
        case let .widgetInteraction(emBetWidgetInteractionEvent):

            var retDictionary: [String: Any] = [:]

            retDictionary["eventName"] = emBetWidgetInteractionEvent.eventName
            retDictionary["widgetID"] = emBetWidgetInteractionEvent.widgetID

            if let payload = emBetWidgetInteractionEvent.payload {
                retDictionary["payload"] = payload
            }

            return retDictionary
        }
    }
}
