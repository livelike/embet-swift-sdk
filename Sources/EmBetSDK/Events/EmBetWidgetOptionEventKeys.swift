//
//  EmBetWidgetOptionEventKeys.swift
//
//
//  Created by Ljupco Nastevski on 12.4.24.
//

import Foundation

/**
    Payload keys for option event
 */
internal enum EmBetWidgetOptionEventKeys {
    static let Market = "market"
    static let Team = "team"
    static let Description = "description"
    static let Odds = "odds"
    static let OptionID = "optionId"
    static let OutcomeID = "outcomeId"
}

internal enum EmBetWidgetViewingTimeKeys {
    static let Duration = "duration"
}
