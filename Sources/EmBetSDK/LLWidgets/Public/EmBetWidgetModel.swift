//
//  File.swift
//
//
//  Created by Ljupco Nastevski on 6.12.22.
//

import Foundation

/**
    Representation of the EmBet Widget model
 */
public protocol EmBetWidgetModel {
    /**
        The id of the widget
     */
    var id: String { get }
    /**
        Checks if a widget is marked as hidden.
     */
    var isHidden: Bool { get }
    /**
        Determines if a widget is persistent.
     */
    var isPersistent: Bool { get }
    /**
        The date untill the widget is interactable
     */
    var interactiveUntil: Date? { get }
    /**
        The widgets model kind
     */
    var widgetKind: EmBetWidgetKind { get }
}
