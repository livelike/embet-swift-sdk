//
//  EmBetWidget.swift
//
//
//  Created by Ljupco Nastevski on 7.12.22.
//

import UIKit

/**
    Representation of EmBetWidget as UIVIewController.
 */
public typealias EmBetWidget = EmBetWidgetPublicProtocol & Widget

/**
    Protocol representing the EmBet widget
 */
@objc
public protocol EmBetWidgetPublicProtocol {
    /**
        The widget ID.
     */
    @objc
    var widgetID: String { get }

    /**
        Override the widget displayType.
     */
    @objc
    var displayType: EmBetWidgetDisplayType { get set }

    /**
        Widget interaction delegate.
     */
    @objc
    weak var interactionDelegate: EmBetWidgetInteractionDelegate? { get set }

    func prepareAsTimelineWidget()
}
