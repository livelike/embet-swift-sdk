//
//  EmBetCTAEvent.swift
//
//
//  Created by Ljupco Nastevski on 7.12.22.
//

import Foundation

/**
    Object that is sent when the user interacts with the widgets CTA button.
 */
@objcMembers
public class EmBetCTAEvent: NSObject {
    /**
        The widgets place bet URL.
     */
    public let placeBetURL: URL?
    /**
        The selected option ID of the widget, in the time the event was raised.
     */
    public let selectedOptionID: String?

    required init(url: URL?, selectedOptionID: String?) {
        placeBetURL = url
        self.selectedOptionID = selectedOptionID
        super.init()
    }
}
