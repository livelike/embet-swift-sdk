//
//  EmBetWidgetInteractionDelegate.swift
//
//
//  Created by Ljupco Nastevski on 7.12.22.
//

import Foundation

/**
    Widget interaction delegate.
 */
@objc
public protocol EmBetWidgetInteractionDelegate {
    /**
        If the delegate function is not implemented, the default behaviour will be to open the url out of app.
        Custom implementation can allow the the integrator to capture the press of the CTA button and add
        custom action.
     */
    @objc
    optional func widget(widget: EmBetWidget, didPressCTAWith event: EmBetCTAEvent)
}
