//
//  LLPredictionWidgetViewController.swift
//
//
//  Created by Ljupco Nastevski on 7.6.24.
//

import LiveLikeSwift
import UIKit

/**
    Container view controller (widget), that prepares itself acording to data.
 */
internal class LLPredictionWidgetViewController: LLWidget {
    var betWidgetCustomData: LLPredictionWidgetPayload! {
        return (customDataPayload as? LLPredictionWidgetPayload)
    }

    private let model: PredictionWidgetModel

    private var selectionCoordinator: LLSelectableViewCoordinator?

    /// The body content view of the widget
    private weak var bodyContentView: UIView?

    override var currentState: WidgetState {
        willSet {
            previousState = currentState
        }
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.delegate?.widgetDidEnterState(widget: self, state: self.currentState)
                switch self.currentState {
                case .ready:
                    break
                case .interacting:
                    self.enterInteractingState()
                case .results:
                    self.enterResultsState()
                case .finished:
                    self.enterFinishedState()
                }
            }
        }
    }

    private var selectedOptionID: String?

    private lazy var informationLabel: UILabel = {
        let label = LLThemableLabel(themeType: .information)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = customDataPayload?.labels?.information ?? "Prediction placed,\nstay tuned for the results!"
        label.alpha = 0
        return label
    }()

    override required init?(safelyWith model: PredictionWidgetModel) {
        self.model = model
        super.init(predictionSafelyWith: model)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        model.registerImpression()
        // On load check if interactivity period has expired
        if model.isInteractivityExpired() {
            bodyContentView?.isUserInteractionEnabled = false
        } else {
            model.markAsInteractive()
        }
    }

    private func enterInteractingState() {
        model.markAsInteractive()
        delegate?.widgetStateCanComplete(widget: self, state: .interacting)
    }

    private func enterResultsState() {
        // Dissable user interaction
        bodyContentView?.isUserInteractionEnabled = false
        hideCountdownAndTimer()

        delegate?.widgetStateCanComplete(widget: self, state: .results)
    }

    private func enterFinishedState() {
        delegate?.widgetStateCanComplete(widget: self, state: .finished)
    }
}

// MARK: - Widget body delegate

extension LLPredictionWidgetViewController: LLWidgetBodyViewDelegate {
    func widgetBodyDidSelectOptionWithID(id: String) {
        // Dissable further interactions
        view.isUserInteractionEnabled = false

        // On selection check if interactivity period has expired
        if model.isInteractivityExpired() {
            return
        }

        // Send event for selection
        let payload: [String: Any?] = [
            EmBetWidgetOptionEventKeys.Description: options?.first(where: { $0.id == id })?.text,
            EmBetWidgetOptionEventKeys.OptionID: id,
        ]
        super.addOptionSelectedEvent(payload: payload.compactMapValues { $0 })

        // Mark user interacted
        userDidInteract = true
        delegate?.userDidInteract(self)

        selectedOptionID = id
        selectionCoordinator?.didSelect(id: id)
        didMakeSelection()

        model.submitVote(optionID: id) { res in
            switch res {
            case let .failure(error):
                log.info("Error submitting vote: \(error.localizedDescription)")
            default:
                break
            }
        }
    }
}

// MARK: Widget protocol conformation

extension LLPredictionWidgetViewController {
    func widgetBuilderHeaderContentView() -> UIView? {
        let widgetTitle = customDataPayload.labels?.widgetTitle ?? betWidgetCustomData.widgetType.descriptive

        let headerStackView = UIStackView()
        headerStackView.translatesAutoresizingMaskIntoConstraints = false
        headerStackView.spacing = LLUIConstants.HeaderElementsSpacing
        headerStackView.axis = .vertical

        let headerLbl = LLThemableLabel(themeType: .header)
        headerLbl.numberOfLines = 0
        headerLbl.textAlignment = .center
        headerLbl.text = widgetTitle
        headerStackView.addArrangedSubview(headerLbl)

        return headerStackView
    }

    func widgetBuilderBodyContentView() -> UIView? {
        let bodyStackView = UIStackView()
        bodyStackView.translatesAutoresizingMaskIntoConstraints = false
        bodyStackView.spacing = LLUIConstants.BodyElementsSpacing
        bodyStackView.axis = .vertical

        let titleLbl = LLThemableLabel(themeType: .title)
        titleLbl.numberOfLines = 0
        titleLbl.textAlignment = .center
        titleLbl.text = model.question
        bodyStackView.addArrangedSubview(themeBuilder.embedInHolder(themeType: .title, view: titleLbl))

        let bodyVC = LLWidgetBodyViewController(variation: .square,
                                                options: model.options,
                                                betDetails: betWidgetCustomData.betDetails ?? [],
                                                numberOfVotesText: customDataPayload.labels?.numberOfVotes)
        selectionCoordinator = bodyVC.selectionCoordinator
        selectionCoordinator?.showsResultsOnSelection = false
        selectionCoordinator?.themeBulder = themeBuilder
        bodyVC.delegate = self
        addChild(bodyVC)
        bodyVC.didMove(toParent: self)

        bodyStackView.addArrangedSubview(bodyVC.view)

        bodyContentView = bodyStackView

        return bodyStackView
    }

    func widgetBuilderFooterContentView() -> UIView? {
        let footerContent = LLWidgetFooter(customData: customDataPayload, themeBuilder: themeBuilder)
        footerContent?.interactionAction = { [weak self] in
            guard let self = self else { return }
            // Submit payload
            let payload = EmBetCTAEvent(url: self.betWidgetCustomData.placeBetURL, selectedOptionID: self.selectedOptionID)
            self.submitPressed(payload: payload)
        }

        return footerContent
    }

    func widgetBuilderWillSetupWidgetView() {
        LLBaseWidgetProperties.applyDefaultPredictionWidgetProperties(to: &themeBuilder.themeModel)
    }

    func widgetBuilderPrepareWidgetForTimeline() {
        currentState = .finished
    }

    private func didMakeSelection() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            if let selectedView = self.selectionCoordinator?.getSelected,
               let newPoints = selectedView.superview?.convert(selectedView.center, to: view.coordinateSpace)
            {
                let screenShot = selectedView.toImage()

                let imageView = UIImageView()
                imageView.alpha = 0
                imageView.translatesAutoresizingMaskIntoConstraints = false
                imageView.image = screenShot
                self.view.addSubview(imageView)

                NSLayoutConstraint.activate([
                    imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                    imageView.centerYAnchor.constraint(equalTo: view.topAnchor, constant: newPoints.y - screenShot.size.height / 2),
                ])

                let predictionPlacedView = self.informationLabel
                themeBuilder.applyThemeTo(superview: self.informationLabel)
                self.view.addSubview(predictionPlacedView)

                NSLayoutConstraint.activate([
                    predictionPlacedView.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 12),
                    predictionPlacedView.bottomAnchor.constraint(lessThanOrEqualTo: view.bottomAnchor, constant: -12),
                    predictionPlacedView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 12),
                    predictionPlacedView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -12),
                ])

                // Start animation with delay
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.hideWidgetViews()

                    let fromPosition = newPoints
                    let toPosition = CGPoint(x: self.view.center.x, y: newPoints.y - screenShot.size.height / 2)

                    EmBetBasicAnimator.performPredictionSelection(view: imageView,
                                                                  complementaryView: predictionPlacedView,
                                                                  fromPosition: fromPosition,
                                                                  toPosition: toPosition)
                    {
                        // Once animation is finished, move to `finished` state
                        self.moveToNextState()
                    }

                    imageView.alpha = 1
                    selectedView.alpha = 0
                }
            }
        }
    }
}
