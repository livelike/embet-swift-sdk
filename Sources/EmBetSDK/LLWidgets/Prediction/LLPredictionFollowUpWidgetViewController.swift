//
//  LLPredictionFollowUpWidgetViewController.swift
//
//
//  Created by Ljupco Nastevski on 7.6.24.
//

import LiveLikeSwift
import UIKit

/**
    Container view controller (widget), that prepares itself acording to data.
 */
internal class LLPredictionFollowUpWidgetViewController: LLWidget {
    var followUpWidgetCustomData: LLPredictionWidgetPayload! {
        return (customDataPayload as? LLPredictionWidgetPayload)
    }

    private let model: PredictionFollowUpWidgetModel

    private var selectionCoordinator: LLSelectableViewCoordinator?

    /// The body content view of the widget
    private weak var bodyContentView: UIView?

    override var currentState: WidgetState {
        willSet {
            previousState = currentState
        }
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.delegate?.widgetDidEnterState(widget: self, state: self.currentState)
                switch self.currentState {
                case .ready:
                    break
                case .interacting:
                    self.enterInteractingState()
                case .results:
                    self.enterResultsState()
                case .finished:
                    self.enterFinishedState()
                }
            }
        }
    }

    private var selectedOptionID: String?

    override required init?(safelyWith model: PredictionFollowUpWidgetModel) {
        self.model = model
        super.init(safelyWith: model)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        model.registerImpression()

        if let selectionID = model.mostRecentVote?.optionID {
            selectedOptionID = selectionID
            selectionCoordinator?.didSelect(id: selectionID)
        }
        selectionCoordinator?.performInitialResultsReveal()

        if let claimableVote = model.userVotes.first(where: { $0.claimToken != nil }) {
            model.claimRewards(vote: claimableVote)
        }

        let totalPoints = model.options.map { $0.voteCount }.reduce(0, +)
        for option in model.options {
            selectionCoordinator?.didReceiveUpdateForOption(optionID: option.id, votes: option.voteCount, totalVotes: totalPoints)
        }

        bodyContentView?.isUserInteractionEnabled = false
    }

    private func enterInteractingState() {
        model.markAsInteractive()
        delegate?.widgetStateCanComplete(widget: self, state: .interacting)
    }

    private func enterResultsState() {
        // Dissable user interaction
        bodyContentView?.isUserInteractionEnabled = false
        hideCountdownAndTimer()

        delegate?.widgetStateCanComplete(widget: self, state: .results)
    }

    private func enterFinishedState() {
        delegate?.widgetStateCanComplete(widget: self, state: .finished)
    }
}

// MARK: - Widget body delegate

extension LLPredictionFollowUpWidgetViewController: LLWidgetBodyViewDelegate {
    func widgetBodyDidSelectOptionWithID(id _: String) {}
}

// MARK: Widget protocol conformation

extension LLPredictionFollowUpWidgetViewController {
    func widgetBuilderHeaderContentView() -> UIView? {
        let widgetTitle = customDataPayload.labels?.widgetTitle ?? followUpWidgetCustomData.widgetType.descriptive

        let headerStackView = UIStackView()
        headerStackView.translatesAutoresizingMaskIntoConstraints = false
        headerStackView.spacing = LLUIConstants.HeaderElementsSpacing
        headerStackView.axis = .vertical

        let headerLbl = LLThemableLabel(themeType: .header)
        headerLbl.numberOfLines = 0
        headerLbl.textAlignment = .center
        headerLbl.text = widgetTitle
        headerStackView.addArrangedSubview(headerLbl)

        return headerStackView
    }

    func widgetBuilderBodyContentView() -> UIView? {
        let bodyStackView = UIStackView()
        bodyStackView.translatesAutoresizingMaskIntoConstraints = false
        bodyStackView.spacing = LLUIConstants.BodyElementsSpacing
        bodyStackView.axis = .vertical

        let titleLbl = LLThemableLabel(themeType: .title)
        titleLbl.numberOfLines = 0
        titleLbl.textAlignment = .center
        titleLbl.text = model.question
        bodyStackView.addArrangedSubview(themeBuilder.embedInHolder(themeType: .title, view: titleLbl))

        let bodyVC = LLWidgetBodyViewController(variation: .squareFollowUp,
                                                options: model.options,
                                                betDetails: [],
                                                numberOfVotesText: customDataPayload.labels?.numberOfVotes)

        selectionCoordinator = bodyVC.selectionCoordinator
        selectionCoordinator?.showsResultsOnSelection = false
        selectionCoordinator?.themeBulder = themeBuilder
        bodyVC.delegate = self
        addChild(bodyVC)
        bodyVC.didMove(toParent: self)
        bodyStackView.addArrangedSubview(bodyVC.view)

        bodyStackView.addArrangedSubview(getPredictionView())

        bodyContentView = bodyStackView

        return bodyStackView
    }

    /**
        Returns the prediction label, themed correctly and with the correct text added
     */
    private func getPredictionView() -> UIView {
        var themeType: LLThemableType = .predictionUnresolved
        var predictionText: String? = followUpWidgetCustomData.labels?.predictionUnresolved

        if model.options.contains(where: { $0.isCorrect == true }) == true {
            if let selectedOptionID = model.mostRecentVote?.optionID,
               let isCorrect = model.options.first(where: { $0.id == selectedOptionID })?.isCorrect
            {
                if isCorrect == true {
                    themeType = .predictionCorrect
                    predictionText = followUpWidgetCustomData.labels?.predictionCorrect
                } else {
                    themeType = .predictionIncorrect
                    predictionText = followUpWidgetCustomData.labels?.predictionIncorrect
                }
            }
        }

        let predictionLbl = LLThemableLabel(themeType: themeType)
        predictionLbl.numberOfLines = 0
        predictionLbl.textAlignment = .center
        predictionLbl.text = predictionText
        return themeBuilder.embedInHolder(themeType: themeType, view: predictionLbl)
    }

    func widgetBuilderFooterContentView() -> UIView? {
        let footerContent = LLWidgetFooter(customData: customDataPayload, themeBuilder: themeBuilder)
        footerContent?.interactionAction = { [weak self] in
            guard let self = self else { return }
            // Submit payload
            let payload = EmBetCTAEvent(url: self.followUpWidgetCustomData.placeBetURL, selectedOptionID: self.selectedOptionID)
            self.submitPressed(payload: payload)
        }

        return footerContent
    }

    func widgetBuilderWillSetupWidgetView() {
        LLBaseWidgetProperties.applyDefaultPredictionFollowUpWidgetProperties(to: &themeBuilder.themeModel)
    }

    func widgetBuilderPrepareWidgetForTimeline() {
        currentState = .finished
    }
}
