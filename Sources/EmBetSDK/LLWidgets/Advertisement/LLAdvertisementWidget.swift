//
//  LLAdvertisementWidget.swift
//
//
//  Created by Ljupco Nastevski on 27.5.24.
//

import LiveLikeSwift
import UIKit

internal class LLAdvertisementWidget: LLWidget {
    var advertisementWidgetCustomData: LLAdvertisingWidgetPayload! {
        return (customDataPayload as? LLAdvertisingWidgetPayload)
    }

    private lazy var ctaButton: UIButton = {
        let btn = UIButton()
        btn.setTitle("", for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()

    override func moveToNextState() {
        switch currentState {
        case .ready:
            currentState = .interacting
        case .interacting:
            currentState = .finished
        case .results:
            break
        case .finished:
            break
        }
    }

    override var currentState: WidgetState {
        willSet {
            previousState = currentState
        }
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.delegate?.widgetDidEnterState(widget: self, state: self.currentState)
                switch self.currentState {
                case .interacting:
                    self.enterInteractingState()
                case .finished:
                    self.enterFinishedState()
                default:
                    break
                }
            }
        }
    }

    private let model: AlertWidgetModel

    override required init?(safelyWith model: AlertWidgetModel) {
        self.model = model
        super.init(advertisimentSafelyWith: model)
    }

    private func enterInteractingState() {
        delegate?.widgetStateCanComplete(widget: self, state: .interacting)
    }

    private func enterFinishedState() {
        hideCountdownAndTimer()
        delegate?.widgetStateCanComplete(widget: self, state: .finished)
    }

    @objc
    private func didTapWidget() {
        let payload = EmBetCTAEvent(url: advertisementWidgetCustomData.ctaUrl, selectedOptionID: nil)
        submitPressed(payload: payload)
    }
}

extension LLAdvertisementWidget {
    func widgetBuilderBodyContentView() -> UIView? {
        let imgView = LLAsyncImageView()
        imgView.contentMode = .scaleToFill
        imgView.loadAsyncImage(url: model.imageURL)

        // Add tap gesture recognizer on the image view
        // If CTA URL is present
        if advertisementWidgetCustomData.ctaUrl != nil {
            let tap = UITapGestureRecognizer(target: self, action: #selector(didTapWidget))
            imgView.addGestureRecognizer(tap)
            imgView.isUserInteractionEnabled = true
        }

        return imgView
    }

    func widgetBuilderWillSetupWidgetView() {
        LLBaseWidgetProperties.applyDefaultAlertProperties(to: &themeBuilder.themeModel)
    }

    func widgetBuilderFooterContentView() -> UIView? { return nil }

    func widgetBuilderHeaderContentView() -> UIView? { return nil }
}
