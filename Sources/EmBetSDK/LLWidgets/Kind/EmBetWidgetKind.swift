//
//  EmBetWidgetKind.swift
//
//
//  Created by Ljupco Nastevski on 14.12.23.
//

import Foundation
import LiveLikeSwift

public enum EmBetWidgetKind: String, Decodable {
    case liveAtBat = "live-at-bat"
    case liveStatsOdds = "live-stats-odds"
    case liveSpread = "live-spread"
    case liveMoneyline = "live-moneyline"
    case liveTotal = "live-total"
    case alert = "embet-alert"
    case odds = "live-odds"
    case quiz = "embet-quiz"
    case advertisement = "embet-advertisement"
    case prediction = "embet-prediction"
    case predictionFollowUp = "embet-prediction-follow-up"
    case unsupported

    /**
        The betting widget kinds
     */
    internal static let bettingWidgetKinds: Set<EmBetWidgetKind> = [.liveAtBat, .liveStatsOdds, .liveSpread, .liveMoneyline, .liveTotal]

    /**
        Permited widget kinds for timeline presentation
     */
    internal static let permitedTimelineWidgetKinds: Set<EmBetWidgetKind> = [.liveAtBat, .liveStatsOdds, .liveSpread, .liveMoneyline, .liveTotal, .alert, .quiz]

    /**
        Permited widget kinds for live presentation
     */
    internal static let permitedLiveWidgetKinds: Set<EmBetWidgetKind> = [.liveAtBat, .liveStatsOdds, .liveSpread, .liveMoneyline, .liveTotal, .alert, .quiz, .prediction, .predictionFollowUp]

    /**
        Permited advertisement widget kinds
     */
    internal static let permitedAdvertisementWidgetKinds: Set<EmBetWidgetKind> = [.advertisement]

    /**
        Converts EmBet widget kind to Engagement widget kind
     */
    internal static func toEngagementWidgetKind(embetWidgetKinds: Set<EmBetWidgetKind>) -> Set<WidgetKind> {
        return Set(embetWidgetKinds.compactMap { toEngagementWidgetKind(embetWidgetKind: $0) })
    }

    /**
        Converts EmBet widget kind to Engagement widget kind
     */
    internal static func toEngagementWidgetKind(embetWidgetKind: EmBetWidgetKind) -> WidgetKind? {
        switch embetWidgetKind {
        case .liveAtBat, .liveStatsOdds, .liveSpread, .liveMoneyline, .liveTotal, .prediction:
            return .imagePrediction
        case .predictionFollowUp:
            return .imagePredictionFollowUp
        case .alert, .advertisement:
            return .alert
        case .odds:
            return .textPrediction
        case .quiz:
            return .textQuiz
        case .unsupported:
            return nil
        }
    }

    var presentationType: LLWidgetPresentationType {
        switch self {
        case .liveAtBat, .liveStatsOdds:
            return .insight
        default:
            return .regular
        }
    }

    var descriptive: String {
        switch self {
        case .liveAtBat:
            return "LIVE AT BAT"
        case .liveStatsOdds:
            return "LIVE STATS ODDS"
        case .liveSpread:
            return "LIVE SPREAD"
        case .liveMoneyline:
            return "LIVE MONEYLINE"
        case .liveTotal:
            return "LIVE TOTAL"
        case .alert:
            return "ALERT"
        case .quiz:
            return "TRIVIA"
        case .odds:
            return "ODDS"
        case .advertisement:
            return "AD"
        case .prediction:
            return "Prediction"
        case .predictionFollowUp:
            return "Prediction FollowUp"
        case .unsupported:
            return "unsupported"
        }
    }
}
