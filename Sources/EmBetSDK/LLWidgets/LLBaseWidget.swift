//
//  LLBaseWidget.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 2.3.22.
//

import LiveLikeSwift
import UIKit

internal typealias LLWidget = LLBaseWidget & LLWidgetBuildable

@objc
internal protocol LLWidgetBuildable {
    /**
        Header view
     */
    func widgetBuilderHeaderContentView() -> UIView?

    /**
        Body view
     */
    func widgetBuilderBodyContentView() -> UIView?

    /**
        Footer view
     */
    func widgetBuilderFooterContentView() -> UIView?

    /**
        Insight view
     */
    @objc
    optional func widgetBuilderInsightContentView() -> UIView?

    /**
        AnimatedBetContent is a themable view that if present,
        replaces the body and footer view in the widgets content view.
     */
    @objc
    optional func widgetBuilderAnimatedBetContentView() -> UIView?

    /**
        Property that determines the visibility of the timer view
     */
    @objc
    optional func widgetBuilderIsTimerViewHidden() -> Bool

    /**
        Property that determines the visibility of the close button
     */
    @objc
    optional func widgetBuilderIsCloseButtonHidden() -> Bool

    /**
        Called before the widget views are layed out.
     */
    @objc
    optional func widgetBuilderWillSetupWidgetView()

    /**
        Called right after the widget views are layed out.
     */
    @objc
    optional func widgetBuilderDidSetupWidgetView(animatedView: UIView?)

    /**
        Called if the widget is prepared to be shown in the timeline.
     */
    @objc
    optional func widgetBuilderPrepareWidgetForTimeline()

    /**
        The top-most view for the widget
     */
    @objc
    optional func widgetBuilderCeilingView() -> LLComplementaryView?
    /**
        The bottom-most view for the widget
     */
    @objc
    optional func widgetBuilderComplementaryFooterView() -> LLComplementaryView?
}

internal class LLComplementaryView: NSObject {
    let view: UIView
    let themeType: LLThemableType

    init(view: UIView, themeType: LLThemableType) {
        self.view = view
        self.themeType = themeType
    }
}

private enum CustomDataError: Error {
    case invalidData
}

/**
    Base class for a widget. New widgets whould use the base class as base to build upon.
 */
internal class LLBaseWidget: Widget {
    /**
        Parsed custom widget data from custom_data
     */
    var customDataPayload: LLBaseWidgetPayload!

    /**
        Custom widget theme
     */
    internal var customWidgetTheme: LLThemeWidgetModel?

    /**
        The root view of the widget
     */
    internal weak var rootView: UIView!

    /**
        Theme builder object
     */
    internal lazy var themeBuilder = LLThemeBuilder(themeModel: customWidgetTheme ?? LLThemeWidgetModel())

    /**
        The countdownView of the widget
     */
    private weak var countdownView: UIView?

    private weak var timerAnimationView: UIView?

    private var closeButtonAction: (() -> Void)?

    weak var interactionDelegate: EmBetWidgetInteractionDelegate?

    var displayType: EmBetWidgetDisplayType = .regular

    var eventManger: EmBetSDKInternalEventManagerProtocol?

    // MARK: - Internal references

    // Content View references, allows for subclasses to have references
    // to individuals view
    /**
        Widget HeaderContentView reference
     */
    internal weak var header: UIView?
    /**
        Widget BodyContentView reference
     */
    internal weak var body: UIView?
    /**
        Widget Footer Content View reference
     */
    internal weak var footer: UIView?
    /**
        Widget Complementary Footer Content View reference
     */
    internal weak var complementaryFooter: UIView?

    /**
        Footer colapsed height constraint
     */
    internal var complementaryFooterCollapsedConstraint: NSLayoutConstraint?

    /**
        Initial height of the expanded constraint
     */
    internal var complementaryFooterInitalTopConstraint: NSLayoutConstraint?

    /**
        Maximum height of the expanded constraint
     */
    internal var complementaryFooterFinishedTopConstraint: NSLayoutConstraint?

    /**
        Timestamp when the view has finished loading
     */
    private var widgetShownTimestamp: Date?

    // MARK: - Lifecycle functions

    override func viewDidLoad() {
        super.viewDidLoad()

        guard let widgetBuildable = self as? LLWidgetBuildable else { return }

        if currentState != .finished {
            let isTimerVisible = widgetBuildable.widgetBuilderIsTimerViewHidden?()
            if isTimerVisible == nil || isTimerVisible == false {
                addTimerView(duration: interactionTimeInterval)
            }

            let isCloseButtonHidden = widgetBuildable.widgetBuilderIsCloseButtonHidden?()
            if isCloseButtonHidden == nil || isCloseButtonHidden == false {
                addCloseButton()
            }
        } else {
            // If view is loaded in finished state, add timeline timer
            addTimerView(duration: widgetModel?.getSecondsUntilInteractivityExpiration(), finishedState: true)
        }

        // Configure suspended state
        if let isSuspended = widgetModel?.isSuspended, isSuspended == true {
            setupSuspendedState(isSuspended: isSuspended)
        }

        // Updates the widget shown timestamp to get more
        // accurate reading we do this at the end of view did load.
        widgetShownTimestamp = Date()
    }

    override func loadView() {
        view = setupSelf()
        viewRespectsSystemMinimumLayoutMargins = false
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        updateWidgetViewingTimeEvent()
    }

    // MARK: Initializers

    init?(safelyWith model: PredictionWidgetModel) {
        do {
            super.init(model: model)
            if model.containsImages {
                try parseData(type: LLBetWidgetPayload.self)
                if let customWidgetTheme = customDataPayload.theme?.widgets?.prediction {
                    self.customWidgetTheme = customWidgetTheme
                }
            } else {
                try parseData(type: LLOddsWidgetPayload.self)
                if let customWidgetTheme = customDataPayload.theme?.widgets?.odds {
                    self.customWidgetTheme = customWidgetTheme
                }
            }
        } catch {
            log.error("Unable to parse 'custom_data' for Bet Widget: \(error)")
            return nil
        }
    }

    init?(predictionSafelyWith model: PredictionWidgetModel) {
        do {
            super.init(model: model)
            try parseData(type: LLPredictionWidgetPayload.self)
            if let customWidgetTheme = customDataPayload.theme?.widgets?.prediction {
                self.customWidgetTheme = customWidgetTheme
            }
        } catch {
            log.error("Unable to parse 'custom_data' for Prediction Widget: \(error)")
            return nil
        }
    }

    init?(safelyWith model: PredictionFollowUpWidgetModel) {
        do {
            super.init(model: model)
            try parseData(type: LLPredictionWidgetPayload.self)
            if let customWidgetTheme = customDataPayload.theme?.widgets?.prediction {
                self.customWidgetTheme = customWidgetTheme
            }
        } catch {
            log.error("Unable to parse 'custom_data' for Prediction Follow-up Widget: \(error)")
            return nil
        }
    }

    init?(safelyWith model: AlertWidgetModel) {
        do {
            super.init(model: model)
            try parseData(type: LLAlertWidgetPayload.self)
            if let customWidgetTheme = customDataPayload.theme?.widgets?.alert {
                self.customWidgetTheme = customWidgetTheme
            }
        } catch {
            log.error("Unable to parse 'custom_data' for Alert Widget: \(error)")
            return nil
        }
    }

    init?(safelyWith model: QuizWidgetModel) {
        do {
            super.init(model: model)
            try parseData(type: LLQuizWidgetPayload.self)
            if let customWidgetTheme = customDataPayload.theme?.widgets?.quiz {
                self.customWidgetTheme = customWidgetTheme
            }
        } catch {
            log.error("Unable to parse 'custom_data' for Alert Widget: \(error)")
            return nil
        }
    }

    init?(advertisimentSafelyWith model: AlertWidgetModel) {
        do {
            super.init(model: model)
            try parseData(type: LLAdvertisingWidgetPayload.self)
            if let customWidgetTheme = customDataPayload.theme?.widgets?.alert {
                self.customWidgetTheme = customWidgetTheme
            }
        } catch {
            log.error("Unable to parse 'custom_data' for Alert Widget: \(error)")
            return nil
        }
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /*
        Default states changes
     */
    override func moveToNextState() {
        switch currentState {
        case .ready:
            currentState = .interacting
        case .interacting:
            currentState = .results
        case .results:
            currentState = .finished
        case .finished:
            break
        }
    }

    /**
        Prepare the custom data object.
     */
    private func parseData<Element: Decodable>(type: Element.Type) throws {
        do {
            if let data = customData?.data(using: .utf8) {
                let json = try JSONDecoder().decode(type, from: data)
                customDataPayload = json as? LLBaseWidgetPayload
            } else {
                throw CustomDataError.invalidData
            }

        } catch {
            throw error
        }
    }

    // MARK: Widget building

    private func setupSelf() -> UIView {
        var widgetViews: [UIView] = []
        var animationView: UIView?

        guard let widgetBuildable = self as? LLWidgetBuildable else { return UIView() }

        widgetBuildable.widgetBuilderWillSetupWidgetView?()

        if let ceilingView = widgetBuildable.widgetBuilderCeilingView?() {
            widgetViews.append(ceilingView.view)
        }

        if let headerView = widgetBuildable.widgetBuilderHeaderContentView() {
            let header = themeBuilder.embedInHolder(themeType: .header, view: headerView)
            widgetViews.append(header)
            self.header = header
        }

        if let insightsView = widgetBuildable.widgetBuilderInsightContentView?() {
            widgetViews.append(themeBuilder.embedInHolder(themeType: .insights, view: insightsView))
        }

        if let animatedBetContentView = widgetBuildable.widgetBuilderAnimatedBetContentView?() {
            let embededAnimatedBet = themeBuilder.embedInHolder(themeType: .animatedBet, view: animatedBetContentView)
            widgetViews.append(embededAnimatedBet)
            animationView = embededAnimatedBet
        } else {
            if let bodyView = widgetBuildable.widgetBuilderBodyContentView() {
                let body = themeBuilder.embedInHolder(themeType: .body, view: bodyView)
                widgetViews.append(body)
                self.body = body
            }

            if let footerView = widgetBuildable.widgetBuilderFooterContentView() {
                let footer = themeBuilder.embedInHolder(themeType: .footer, view: footerView)
                widgetViews.append(footer)
                self.footer = footer
            }
        }

        let sv = UIStackView(arrangedSubviews: widgetViews)
        sv.translatesAutoresizingMaskIntoConstraints = false
        // Overlapping will cover any root view animation visual
        // artifacts between stackview subviews
        sv.spacing = -1
        sv.axis = .vertical

        let embededRootView: UIView!

        if let floorPackedView = widgetBuildable.widgetBuilderComplementaryFooterView?() {
            let widgetHolder = UIView()
            widgetHolder.translatesAutoresizingMaskIntoConstraints = false
            widgetHolder.addSubview(sv)

            NSLayoutConstraint.activate([
                sv.leadingAnchor.constraint(equalTo: widgetHolder.leadingAnchor),
                sv.trailingAnchor.constraint(equalTo: widgetHolder.trailingAnchor),
                sv.topAnchor.constraint(equalTo: widgetHolder.topAnchor),
            ])

            let baseView = UIView()
            baseView.translatesAutoresizingMaskIntoConstraints = false
            widgetHolder.addSubview(baseView)

            NSLayoutConstraint.activate([
                baseView.topAnchor.constraint(equalTo: sv.bottomAnchor),
                baseView.leadingAnchor.constraint(equalTo: widgetHolder.leadingAnchor),
                baseView.trailingAnchor.constraint(equalTo: widgetHolder.trailingAnchor),
                baseView.bottomAnchor.constraint(equalTo: widgetHolder.bottomAnchor),
            ])

            complementaryFooter = baseView

            let rootView = themeBuilder.embedInHolder(themeType: .root, view: widgetHolder, embedsInMarginView: false)
            self.rootView = rootView
            embededRootView = themeBuilder.embedInMarginView(themeType: .root, view: rootView)

            let floorView = themeBuilder.embedInMarginView(themeType: .footer, view: floorPackedView.view)
            floorView.translatesAutoresizingMaskIntoConstraints = false
            embededRootView.addSubview(floorView)

            // Initial height of the expanded constraint
            let complementaryFooterExpandedConstraint = floorView.topAnchor.constraint(equalTo: baseView.topAnchor)
            complementaryFooterInitalTopConstraint = complementaryFooterExpandedConstraint

            // Maximum height of the expanded constraint

            let complementaryFooterFinishedTopConstraint = floorView.topAnchor.constraint(greaterThanOrEqualTo: header?.bottomAnchor ?? baseView.topAnchor)
            self.complementaryFooterFinishedTopConstraint = complementaryFooterFinishedTopConstraint

            NSLayoutConstraint.activate([
                floorView.leadingAnchor.constraint(equalTo: baseView.leadingAnchor),
                floorView.trailingAnchor.constraint(equalTo: baseView.trailingAnchor),
                floorView.bottomAnchor.constraint(equalTo: baseView.bottomAnchor),
                complementaryFooterExpandedConstraint,
            ])

        } else {
            let rootView = themeBuilder.embedInHolder(themeType: .root, view: sv, embedsInMarginView: false)
            self.rootView = rootView
            embededRootView = themeBuilder.embedInMarginView(themeType: .root, view: rootView)
        }

        widgetBuildable.widgetBuilderDidSetupWidgetView?(animatedView: animationView)

        applyTheme(rootView: embededRootView)

        embededRootView.translatesAutoresizingMaskIntoConstraints = true
        return embededRootView
    }

    // MARK: Timer animations and Countdown label

    private func addTimerView(duration: TimeInterval?, finishedState: Bool = false) {
        // Only show the timer in timeline if interactivity expires in more that 0 seconds
        guard let interactivityDuration = duration,
              interactivityDuration > 0 else { return }

        // If the timer property is missing, set as bar timer
        switch customDataPayload.timer {
        case .round:
            addRoundTimerAnimation(duration: interactivityDuration, showsTrailing: finishedState)
        default:
            addBarTimerAnimation(duration: interactivityDuration)
        }
    }

    /**
        Adds the countdown label and round timer animation.
        - Parameters:
            - duration: The duration of the timer
            - showsTrailing: Decides the horizontal alighment of the timer.
     */
    private func addRoundTimerAnimation(duration: TimeInterval, showsTrailing: Bool) {
        guard let timer = themeBuilder.themeModel.roundTimer else { return }
        guard let timerBarWidth = themeBuilder.themeModel.roundTimer?.barWidth else { return }
        guard let timerBackground = themeBuilder.themeModel.roundTimer?.background else { return }

        let countdownTimer = LLCountdownLabel(duration: duration, themeType: .roundTimer)
        countdownTimer.startCountdownTimer()
        countdownTimer.translatesAutoresizingMaskIntoConstraints = false
        rootView.addSubview(countdownTimer)
        countdownView = countdownTimer

        // Instantiates the horizontal constraint
        var horizontalContraint = countdownTimer.leadingAnchor.constraint(equalTo: rootView.layoutMarginsGuide.leadingAnchor, constant: LLUIConstants.FloatingElementsRootLeadingPadding)
        if showsTrailing {
            horizontalContraint = countdownTimer.trailingAnchor.constraint(equalTo: rootView.layoutMarginsGuide.trailingAnchor, constant: -LLUIConstants.FloatingElementsRootLeadingPadding)
        }

        NSLayoutConstraint.activate([
            countdownTimer.heightAnchor.constraint(equalToConstant: LLUIConstants.CountdownTimerViewSize.height),
            countdownTimer.widthAnchor.constraint(equalToConstant: LLUIConstants.CountdownTimerViewSize.width),
            countdownTimer.topAnchor.constraint(equalTo: rootView.layoutMarginsGuide.topAnchor, constant: LLUIConstants.FloatingElementsRootPadding),
            horizontalContraint,
        ])

        let timerDiameter = (timer.height ?? LLUIConstants.DefaultTimerHeight)

        let circularTimer = LLTimerAnimatableView(barWidth: timerBarWidth, frame: CGRect(x: 0, y: 0, width: timerDiameter, height: timerDiameter))
        circularTimer.duration = duration
        circularTimer.animationType = .round
        circularTimer.barWidth = timerBarWidth
        circularTimer.timerColor = timerBackground

        circularTimer.translatesAutoresizingMaskIntoConstraints = false
        rootView.addSubview(circularTimer)
        timerAnimationView = circularTimer

        NSLayoutConstraint.activate([
            circularTimer.heightAnchor.constraint(equalToConstant: timerDiameter),
            circularTimer.widthAnchor.constraint(equalToConstant: timerDiameter),
            circularTimer.centerXAnchor.constraint(equalTo: countdownTimer.centerXAnchor, constant: 0),
            circularTimer.centerYAnchor.constraint(equalTo: countdownTimer.centerYAnchor, constant: 0),
        ])

        DispatchQueue.main.async {
            circularTimer.startAnimating()
        }
    }

    /**
        Adds the countdown label and bar timer animation
     */
    private func addBarTimerAnimation(duration: TimeInterval) {
        guard let timerBarHeight = themeBuilder.themeModel.timer?.height else { return }
        guard let timerBackground = themeBuilder.themeModel.timer?.background else { return }

        let barTimer = LLTimerAnimatableView(barWidth: CGFloat(timerBarHeight), frame: CGRect(x: 0, y: 0, width: rootView.bounds.width, height: CGFloat(timerBarHeight)))
        barTimer.duration = duration
        barTimer.animationType = .bar
        barTimer.barWidth = CGFloat(timerBarHeight)
        barTimer.timerColor = timerBackground

        barTimer.translatesAutoresizingMaskIntoConstraints = false
        rootView.addSubview(barTimer)
        timerAnimationView = barTimer

        NSLayoutConstraint.activate([
            barTimer.heightAnchor.constraint(equalToConstant: timerBarHeight),
            barTimer.leadingAnchor.constraint(equalTo: rootView.leadingAnchor),
            barTimer.trailingAnchor.constraint(equalTo: rootView.trailingAnchor),
            barTimer.bottomAnchor.constraint(equalTo: rootView.bottomAnchor),
        ])

        DispatchQueue.main.async {
            barTimer.startAnimating()
        }
    }

    // MARK: Close Button

    /**
        Adds the close button action
     */
    override func addCloseButton(_ completion: @escaping (WidgetViewModel) -> Void) {
        closeButtonAction = { [weak self] in
            guard let strongSelf = self else { return }
            completion(strongSelf)
        }
    }

    private func addCloseButton() {
        if displayType == .forceHideCloseButton { return }
        guard let dismissSize = customWidgetTheme?.dismiss?.size else { return }

        let closeButton = LLThemableDismissButton.makeDismissButton()
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        closeButton.addTarget(self, action: #selector(closePressed), for: .primaryActionTriggered)

        let closeMarginView = themeBuilder.embedInMarginView(themeType: .dismiss, view: closeButton)
        rootView.addSubview(closeMarginView)

        NSLayoutConstraint.activate([
            closeButton.heightAnchor.constraint(equalToConstant: CGFloat(dismissSize)),
            closeButton.widthAnchor.constraint(equalToConstant: CGFloat(dismissSize)),
            closeMarginView.topAnchor.constraint(equalTo: rootView.topAnchor),
            closeMarginView.trailingAnchor.constraint(equalTo: rootView.trailingAnchor),
        ])

        themeBuilder.applyThemeTo(superview: closeButton)
    }

    @objc func closePressed() {
        closeButtonAction?()
    }

    func submitPressed(payload: EmBetCTAEvent, eventPaylaod: [String: Any]? = nil) {
        addPressedCTAButtonEvent(payload: eventPaylaod)
        // If no delegate is present, the url should be opened
        guard let interactionDelegate = interactionDelegate else {
            if let placeBetUrl = payload.placeBetURL {
                UIApplication.shared.open(placeBetUrl)
            }
            return
        }
        interactionDelegate.widget?(widget: self, didPressCTAWith: payload)
    }

    internal func hideCountdownAndTimer() {
        timerAnimationView?.removeFromSuperview()
        countdownView?.removeFromSuperview()
    }

    override func addTimer(seconds: TimeInterval, completion: @escaping (WidgetViewModel) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) { [weak self] in
            guard let self = self else { return }
            completion(self)
        }
    }

    // MARK: Theme

    internal func applyTheme(rootView: UIView) {
        themeBuilder.applyThemeTo(superview: rootView)
    }

    /**
        Changes the interaction state of the widget
        - Parameters:
            - isSuspended: A bool value that configures the widget acording to suspended flag.
     */
    internal func setupSuspendedState(isSuspended: Bool) {
        let superviews = [header, body, footer].compactMap { $0 }
        let buttonAlpha = isSuspended ? 0.4 : 1.0
        // Buttons should be dissabled only on iOS
        #if os(iOS)
            for superview in superviews {
                DispatchQueue.main.async { [weak self] in
                    self?.getAllButtonFromViewRecursion(view: superview, alpha: buttonAlpha)
                }
            }
        #endif
    }

    private func getAllButtonFromViewRecursion(view: UIView, alpha: CGFloat) {
        for insideView in view.subviews {
            if insideView.isKind(of: LLThemableButton.self) {
                insideView.alpha = alpha
                insideView.isUserInteractionEnabled = false
            } else if insideView.subviews.count > 0 {
                getAllButtonFromViewRecursion(view: insideView, alpha: alpha)
            }
        }
    }

    @objc
    func widgetBuilderComplementaryFooterView() -> LLComplementaryView? {
        if customDataPayload.labels?.disclaimerTitle == nil,
           customDataPayload?.labels?.disclaimerDescription == nil
        {
            return nil
        }

        let footerView = LLOddsFooterView(themeType: .disclaimer,
                                          title: customDataPayload.labels?.disclaimerTitle,
                                          description: customDataPayload.labels?.disclaimerDescription,
                                          descriptionCollapsed: customDataPayload.labels?.disclaimerDescriptionCollapsed)

        // Animates the footer view to expand or colapse
        footerView.didPressExpandHandler = { [weak self] shouldExpand in
            guard let self = self else { return }

            // Initial point when the initial size of the footer is defined
            // Here we can be sure that we always take the collapsed size
            if self.complementaryFooterCollapsedConstraint == nil {
                if let complementaryFooter = self.complementaryFooter {
                    self.complementaryFooterCollapsedConstraint = complementaryFooter.heightAnchor.constraint(equalToConstant: complementaryFooter.bounds.height)
                    self.complementaryFooterCollapsedConstraint?.isActive = true
                }

                // Maximum height constraint
                footerView.heightAnchor.constraint(lessThanOrEqualToConstant: complementaryFooter!.frame.maxY - header!.frame.maxY).isActive = true
            }

            if shouldExpand {
                addDisclaimerExpandedEvent()
                self.complementaryFooterInitalTopConstraint?.isActive = false
                self.complementaryFooterFinishedTopConstraint?.isActive = true
            } else {
                addDisclaimerCollapsedEvent()
                self.complementaryFooterFinishedTopConstraint?.isActive = false
                self.complementaryFooterInitalTopConstraint?.isActive = true
            }
        }

        let complementaryView = LLComplementaryView(view: footerView, themeType: .footer)
        return complementaryView
    }

    internal func hideWidgetViews() {
        let views = [header, body, footer, complementaryFooter]
        UIView.animate(withDuration: 1.0, animations: {
            views.forEach { $0?.alpha = 0 }
        })
    }

    /**
        Takes difference between the timestamp when the widget is shown and this moment
     */
    private func widgetShownDuration() -> Int {
        guard let widgetShownTimestamp else { return 0 }
        return abs(Int(widgetShownTimestamp.timeIntervalSinceNow * 1000))
    }

    /**
        Sends `widget_viewing_time` event
     */
    private func updateWidgetViewingTimeEvent() {
        let duration = widgetShownDuration()
        addWidgetViewingTimeEvent(duration: duration)
    }
}

// MARK: - Events

extension LLBaseWidget {
    func addOptionSelectedEvent(payload: [String: Any]) {
        eventManger?.newEvent(event: EmBetEvent.widgetInteraction(
            EmBetWidgetInteractionEvent(eventName: EmBetWidgetInteractionName.WidgetOptionSelected,
                                        widgetID: widgetID,
                                        payload: payload))
        )
    }

    func addOptionDeselectedEvent(payload: [String: Any]) {
        eventManger?.newEvent(event: EmBetEvent.widgetInteraction(
            EmBetWidgetInteractionEvent(eventName: EmBetWidgetInteractionName.WidgetOptionUnselected,
                                        widgetID: widgetID,
                                        payload: payload))
        )
    }

    func addQRViewDismissedEvent(payload: [String: Any]) {
        eventManger?.newEvent(event: EmBetEvent.widgetInteraction(
            EmBetWidgetInteractionEvent(eventName: EmBetWidgetInteractionName.WidgetQRViewDismissed,
                                        widgetID: widgetID,
                                        payload: payload))
        )
    }

    func addPressedCTAButtonEvent(payload: [String: Any]?) {
        eventManger?.newEvent(event: EmBetEvent.widgetInteraction(
            EmBetWidgetInteractionEvent(eventName: EmBetWidgetInteractionName.WidgetCtaPressed,
                                        widgetID: widgetID,
                                        payload: payload))
        )
    }

    func addDisclaimerCollapsedEvent() {
        eventManger?.newEvent(event: EmBetEvent.widgetInteraction(
            EmBetWidgetInteractionEvent(eventName: EmBetWidgetInteractionName.WidgetDisclaimerCollapsed,
                                        widgetID: widgetID,
                                        payload: nil))
        )
    }

    func addDisclaimerExpandedEvent() {
        eventManger?.newEvent(event: EmBetEvent.widgetInteraction(
            EmBetWidgetInteractionEvent(eventName: EmBetWidgetInteractionName.WidgetDisclaimerExpanded,
                                        widgetID: widgetID,
                                        payload: nil))
        )
    }

    func addWidgetViewingTimeEvent(duration: Int) {
        eventManger?.newEvent(event: EmBetEvent.widgetInteraction(
            EmBetWidgetInteractionEvent(eventName: EmBetWidgetInteractionName.WidgetViewingTime,
                                        widgetID: widgetID,
                                        payload: [EmBetWidgetViewingTimeKeys.Duration: duration]))
        )
    }
}

// MARK: - Public protocol

extension LLBaseWidget: EmBetWidgetPublicProtocol {
    var widgetID: String {
        return id
    }

    func prepareAsTimelineWidget() {
        guard let widgetBuildable = self as? LLWidgetBuildable else { return }
        widgetBuildable.widgetBuilderPrepareWidgetForTimeline?()
    }
}
