//
//  LLQuizBodyViewController.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 24.2.22.
//

import LiveLikeSwift
import UIKit

protocol LLQuizBodyViewControllerDelegate: AnyObject {
    func widgetBodyDidSelectChoiseWithID(id: String)
}

/**
    LLWidgetBodyViewController represents the "body" of a widget.
    It draws its views acording to the data provided in initialization.
 */
internal class LLQuizBodyViewController: UIViewController {
    /**
        Selection delegate
     */
    weak var delegate: LLQuizBodyViewControllerDelegate?

    private let choises: [QuizWidgetModel.Choice]

    private let numberOfVotesText: String?

    var selectionCoordinator = LLQuizSelectionCoordinator()

    init(
        choises: [QuizWidgetModel.Choice],
        numberOfVotesText: String?
    ) {
        self.choises = choises
        self.numberOfVotesText = numberOfVotesText
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        getContentView()
    }

    private func getContentView() {
        let contentView: UIStackView = preparedContentViewAsBarVariation()
        contentView.setContentHuggingPriority(.defaultHigh, for: .vertical)

        view.addSubview(contentView)

        NSLayoutConstraint.activate([
            contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: LLUIConstants.BodyItemsHorizontalPadding),
            contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -LLUIConstants.BodyItemsHorizontalPadding),
            contentView.topAnchor.constraint(equalTo: view.topAnchor),
            contentView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
    }

    /**
        Prepares the content view as a bar variation and returns it.
     */
    private func preparedContentViewAsBarVariation() -> UIStackView {
        var subviews: [LLSelectableView] = []
        for choise in choises {
            let optionView = LLQuizSelectableView(option: choise, betDetails: nil, selectedThemeType: .selectedOption, unselectedThemeType: .unselectedOption, numberOfVotesText: numberOfVotesText)
            optionView.translatesAutoresizingMaskIntoConstraints = false
            optionView.delegate = self
            subviews.append(optionView)
        }
        selectionCoordinator.views = subviews
        let holderStackView = UIStackView(arrangedSubviews: subviews)
        holderStackView.translatesAutoresizingMaskIntoConstraints = false
        holderStackView.axis = .vertical
        holderStackView.distribution = .fillEqually
        holderStackView.spacing = LLUIConstants.SelectableOptionsSpacing
        return holderStackView
    }
}

extension LLQuizBodyViewController: LLSelectableViewDelegate {
    func didSelectOptionWithID(id: String) {
        delegate?.widgetBodyDidSelectChoiseWithID(id: id)
    }
}
