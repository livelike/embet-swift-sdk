//
//  LLQuizWidgetViewController.swift
//
//
//  Created by Ljupco Nastevski on 20.4.23.
//

import LiveLikeSwift
import UIKit

class LLQuizWidgetViewController: LLWidget {
    var betWidgetCustomData: LLQuizWidgetPayload! {
        return (customDataPayload as? LLQuizWidgetPayload)
    }

    private let model: QuizWidgetModel

    private weak var bodyContentView: UIView?

    private weak var selectionCoordinator: LLQuizSelectionCoordinator?

    private var selectedChoise: QuizWidgetModel.Choice?

    override required init?(safelyWith model: QuizWidgetModel) {
        self.model = model
        super.init(safelyWith: model)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override var currentState: WidgetState {
        willSet {
            previousState = currentState
        }
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.delegate?.widgetDidEnterState(widget: self, state: self.currentState)
                switch self.currentState {
                case .ready:
                    break
                case .interacting:
                    self.enterInteractingState()
                case .results:
                    self.enterResultsState()
                case .finished:
                    self.enterFinishedState()
                }
            }
        }
    }

    private var selectedOptionID: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        model.delegate = self
        model.registerImpression()
        model.markAsInteractive()
    }

    private func enterInteractingState() {
        delegate?.widgetStateCanComplete(widget: self, state: .interacting)
    }

    private func enterResultsState() {
        // Dissable user interaction
        bodyContentView?.isUserInteractionEnabled = false
        selectionCoordinator?.performInitialResultsReveal()
        hideCountdownAndTimer()

        delegate?.widgetStateCanComplete(widget: self, state: .results)
    }

    private func enterFinishedState() {
        delegate?.widgetStateCanComplete(widget: self, state: .finished)
    }

    private func loadLastInteraction() {
        if let userVote = model.mostRecentAnswer {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                for choise in self.model.choices {
                    self.selectionCoordinator?.didReceiveUpdateForOption(optionID: choise.id, votes: choise.voteCount, totalVotes: self.model.totalAnswerCount)
                }
                self.selectionCoordinator?.didSelect(id: userVote.choiceID)
                self.selectionCoordinator?.performInstantResultsReveal()
            }
        } else {
            bodyContentView?.isUserInteractionEnabled = true
        }
    }
}

// MARK: Widget protocol conformation

extension LLQuizWidgetViewController {
    func widgetBuilderHeaderContentView() -> UIView? {
        let widgetTitle = customDataPayload.labels?.widgetTitle ?? "Quiz Widget"

        let headerStackView = UIStackView()
        headerStackView.translatesAutoresizingMaskIntoConstraints = false
        headerStackView.spacing = LLUIConstants.HeaderElementsSpacing
        headerStackView.axis = .vertical

        let headerLbl = LLThemableLabel(themeType: .header)
        headerLbl.numberOfLines = 0
        headerLbl.textAlignment = .center
        headerLbl.text = widgetTitle
        headerStackView.addArrangedSubview(headerLbl)

        return headerStackView
    }

    func widgetBuilderBodyContentView() -> UIView? {
        let bodyStackView = UIStackView()
        bodyStackView.translatesAutoresizingMaskIntoConstraints = false
        bodyStackView.spacing = LLUIConstants.BodyElementsSpacing
        bodyStackView.axis = .vertical

        let titleLbl = LLThemableLabel(themeType: .title)
        titleLbl.numberOfLines = 0
        titleLbl.textAlignment = .center
        titleLbl.text = model.question

        bodyStackView.addArrangedSubview(themeBuilder.embedInHolder(themeType: .title, view: titleLbl))

        let bodyVC = LLQuizBodyViewController(choises: model.choices, numberOfVotesText: customDataPayload.labels?.numberOfVotes)
        selectionCoordinator = bodyVC.selectionCoordinator
        selectionCoordinator?.themeBulder = themeBuilder
        bodyVC.delegate = self

        addChild(bodyVC)

        bodyVC.didMove(toParent: self)
        bodyStackView.addArrangedSubview(bodyVC.view)
        bodyContentView = bodyStackView

        return bodyStackView
    }

    func widgetBuilderFooterContentView() -> UIView? {
        let footerContent = LLWidgetFooter(customData: customDataPayload, themeBuilder: themeBuilder)
        footerContent?.interactionAction = { [weak self] in
            guard let self = self else { return }
            // Submit payload
            let payload = EmBetCTAEvent(url: self.betWidgetCustomData.placeBetURL, selectedOptionID: self.selectedOptionID)
            self.submitPressed(payload: payload)
        }

        return footerContent
    }

    func widgetBuilderPrepareWidgetForTimeline() {
        currentState = .finished
        model.loadInteractionHistory { [weak self] _ in
            self?.loadLastInteraction()
        }
    }

    func widgetBuilderWillSetupWidgetView() {
        LLBaseWidgetProperties.applyDefaultThemeProperties(to: &themeBuilder.themeModel)
    }
}

extension LLQuizWidgetViewController: LLQuizBodyViewControllerDelegate {
    func widgetBodyDidSelectChoiseWithID(id: String) {
        // Allow only one answer per lock in
        if model.mostRecentAnswer != nil {
            bodyContentView?.isUserInteractionEnabled = false
            return
        }

        // On selection check if interactivity period has expired
        if model.isInteractivityExpired() {
            // Dissable further interactions
            bodyContentView?.isUserInteractionEnabled = false
            return
        }

        // Send event for selection
        let payload: [String: Any?] = [
            EmBetWidgetOptionEventKeys.Description: options?.first(where: { $0.id == id })?.text,
            EmBetWidgetOptionEventKeys.OptionID: id,
        ]
        super.addOptionSelectedEvent(payload: payload.compactMapValues { $0 })

        // Mark user interacted
        userDidInteract = true

        selectionCoordinator?.didSelect(id: id)
        selectedOptionID = id

        delegate?.userDidInteract(self)

        selectedChoise = model.choices.first(where: { $0.id == id })

        if let selectedID = selectedChoise?.id {
            model.lockInAnswer(choiceID: selectedID) { _ in }
        }

        moveToNextState()
    }
}

extension LLQuizWidgetViewController: QuizWidgetModelDelegate {
    func quizWidgetModel(_: QuizWidgetModel, answerCountDidChange answerCount: Int, forChoice choiceID: String) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.selectionCoordinator?.didReceiveUpdateForOption(optionID: choiceID, votes: answerCount, totalVotes: self.model.totalAnswerCount)
        }
    }
}
