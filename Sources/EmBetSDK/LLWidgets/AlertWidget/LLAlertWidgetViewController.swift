//
//  LLAlertWidgetViewController.swift
//
//
//  Created by Ljupco Nastevski on 18.3.22.
//

import LiveLikeSwift
import UIKit

/**
    EmBet Alert Widget.
 */
internal class LLAlertWidgetViewController: LLWidget {
    var alertWidgetCustomData: LLAlertWidgetPayload! {
        return (customDataPayload as? LLAlertWidgetPayload)
    }

    // MARK: - View controller

    let model: AlertWidgetModel

    override func moveToNextState() {
        switch currentState {
        case .ready:
            currentState = .interacting
        case .interacting:
            currentState = .finished
        case .results:
            break
        case .finished:
            break
        }
    }

    override var currentState: WidgetState {
        willSet {
            previousState = self.currentState
        }
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.delegate?.widgetDidEnterState(widget: self, state: self.currentState)
                switch self.currentState {
                case .interacting:
                    self.enterInteractingState()
                case .finished:
                    self.enterFinishedState()
                default:
                    break
                }
            }
        }
    }

    override required init?(safelyWith model: AlertWidgetModel) {
        self.model = model
        super.init(safelyWith: model)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @objc func buttonPressed() {
        if let placeBetURL = alertWidgetCustomData.placeBetURL {
            UIApplication.shared.open(placeBetURL)
        }
        delegate?.userDidInteract(self)
    }

    private func enterInteractingState() {
        if alertWidgetCustomData.placeBetURL != nil {
            model.markAsInteractive()
        }
        delegate?.widgetStateCanComplete(widget: self, state: .interacting)
    }

    private func enterFinishedState() {
        hideCountdownAndTimer()
        delegate?.widgetStateCanComplete(widget: self, state: .finished)
    }

    /**
        Returns the body text label.
     */
    private func getBodyLbl() -> UIView? {
        if let bodyText = model.text {
            let bodyTextLbl = LLThemableLabel(themeType: .body)
            bodyTextLbl.numberOfLines = 0
            bodyTextLbl.textAlignment = .center
            bodyTextLbl.text = bodyText
            return bodyTextLbl
        }
        return nil
    }

    /**
        Returns the correct media view for the media type.
     */
    private func getMediaView() -> UIView? {
        if let imageUrl = model.imageURL {
            switch alertWidgetCustomData.mediaType {
            case .image:
                let imgView = LLAsyncImageView()
                imgView.translatesAutoresizingMaskIntoConstraints = false
                imgView.loadAsyncImage(url: imageUrl)
                return imgView
            case .gif:
                let imgView = LLAsyncImageView()
                DispatchQueue.global(qos: .background).async {
                    let image = UIImage.gifImageWithURL(gifUrl: imageUrl.absoluteString)
                    DispatchQueue.main.async {
                        imgView.image = image
                    }
                }
                return imgView
            case .video:
                let videoView = VideoPlaybackView(videoUrl: imageUrl)
                return videoView
            }
        }

        return nil
    }
}

// MARK: Widget protocol conformation

extension LLAlertWidgetViewController {
    func widgetBuilderHeaderContentView() -> UIView? {
        let lbl = LLThemableLabel(themeType: .header)
        lbl.textAlignment = .center
        lbl.text = model.title
        return lbl
    }

    func widgetBuilderBodyContentView() -> UIView? {
        let contentView = UIStackView()
        contentView.spacing = LLUIConstants.AlertBodyElementsSpacing
        contentView.axis = .vertical

        switch alertWidgetCustomData.textPosition {
        case .left:
            contentView.axis = .horizontal
            contentView.spacing = 0
            fallthrough
        case .top:
            if let bodyLabel = getBodyLbl() {
                contentView.addArrangedSubview(bodyLabel)
            }
            if let mediaView = getMediaView() {
                contentView.addArrangedSubview(mediaView)
                if alertWidgetCustomData.textPosition == .left, model.text != nil {
                    mediaView.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: LLUIConstants.DefaultAlertHorizontalPositionMediaWidth).isActive = true
                }
            }
        case .right:
            contentView.axis = .horizontal
            contentView.spacing = 0
            fallthrough
        case .bottom:
            if let mediaView = getMediaView() {
                contentView.addArrangedSubview(mediaView)
                if alertWidgetCustomData.textPosition == .right, model.text != nil {
                    mediaView.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: LLUIConstants.DefaultAlertHorizontalPositionMediaWidth).isActive = true
                }
            }
            if let bodyLabel = getBodyLbl() {
                contentView.addArrangedSubview(bodyLabel)
            }
        }

        return contentView
    }

    func widgetBuilderFooterContentView() -> UIView? {
        let footerContent = LLWidgetFooter(customData: customDataPayload, themeBuilder: themeBuilder)
        footerContent?.interactionAction = { [weak self] in
            let payload = EmBetCTAEvent(url: self?.alertWidgetCustomData.placeBetURL, selectedOptionID: nil)
            self?.submitPressed(payload: payload)
        }
        return footerContent
    }

    func widgetBuilderWillSetupWidgetView() {
        LLBaseWidgetProperties.applyDefaultAlertProperties(to: &themeBuilder.themeModel)
    }

    func widgetBuilderPrepareWidgetForTimeline() {
        currentState = .finished
    }
}
