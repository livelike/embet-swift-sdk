//
//  LLBaseWidgetProperties.swift
//
//
//  Created by Ljupco Nastevski on 18.4.22.
//

import Foundation
import UIKit

struct LLBaseWidgetProperties {
    /**
        Prediction follow-up widget default properties
     */
    static func applyDefaultPredictionFollowUpWidgetProperties(to theme: inout LLThemeWidgetModel) {
        if theme.predictionCorrect == nil {
            theme.predictionCorrect = LLThemeViewModel()
        }

        if theme.predictionCorrect?.fontSize == nil {
            theme.predictionCorrect?.fontSize = 18.0
        }

        if theme.predictionCorrect?.fontWeight == nil {
            theme.predictionCorrect?.fontWeight = .normal
        }

        if theme.predictionCorrect?.fontColor == nil {
            theme.predictionCorrect?.fontColor = .white
        }

        if theme.predictionCorrect?.textAlign == nil {
            theme.predictionCorrect?.textAlign = .center
        }

        if theme.predictionIncorrect == nil {
            theme.predictionIncorrect = LLThemeViewModel()
        }

        if theme.predictionIncorrect?.fontSize == nil {
            theme.predictionIncorrect?.fontSize = 18.0
        }

        if theme.predictionIncorrect?.fontWeight == nil {
            theme.predictionIncorrect?.fontWeight = .normal
        }

        if theme.predictionIncorrect?.fontColor == nil {
            theme.predictionIncorrect?.fontColor = .white
        }

        if theme.predictionIncorrect?.textAlign == nil {
            theme.predictionIncorrect?.textAlign = .center
        }

        if theme.predictionUnresolved == nil {
            theme.predictionUnresolved = LLThemeViewModel()
        }

        if theme.predictionUnresolved?.fontSize == nil {
            theme.predictionUnresolved?.fontSize = 18.0
        }

        if theme.predictionUnresolved?.fontWeight == nil {
            theme.predictionUnresolved?.fontWeight = .normal
        }

        if theme.predictionUnresolved?.fontColor == nil {
            theme.predictionUnresolved?.fontColor = .white
        }

        if theme.predictionUnresolved?.textAlign == nil {
            theme.predictionUnresolved?.textAlign = .center
        }

        // OPTIONS
        // Selected Correct Options
        if theme.selectedCorrectOption == nil {
            theme.selectedCorrectOption = LLThemeViewModel()
        }

        if theme.selectedCorrectOption?.borderRadius == nil {
            theme.selectedCorrectOption?.borderRadius = LLUIConstants.DefaultSelectableOptionBorderRadius
        }

        if theme.selectedCorrectOption?.borderColor == nil {
            theme.selectedCorrectOption?.borderColor = LLUIConstants.DefaultAccentColor
        }

        if theme.selectedCorrectOption?.borderWidth == nil {
            theme.selectedCorrectOption?.borderWidth = LLUIConstants.DefaultSelectableOptionBorderWidth
        }

        if theme.selectedCorrectOptionPercentage == nil {
            theme.selectedCorrectOptionPercentage = LLThemeViewModel()
        }

        if theme.selectedCorrectOptionPercentage?.fontColor == nil {
            theme.selectedCorrectOptionPercentage?.fontColor = LLUIConstants.DefaultTextFontColor
        }

        if theme.selectedCorrectOptionBar == nil {
            theme.selectedCorrectOptionBar = LLThemeViewModel()
        }

        if theme.selectedCorrectOptionBar?.background == nil {
            theme.selectedCorrectOptionBar?.background = LLUIConstants.SelectedBarBackground
        }

        // Selected Incorrect Options
        if theme.selectedIncorrectOption == nil {
            theme.selectedIncorrectOption = LLThemeViewModel()
        }

        if theme.selectedIncorrectOption?.borderRadius == nil {
            theme.selectedIncorrectOption?.borderRadius = LLUIConstants.DefaultSelectableOptionBorderRadius
        }

        if theme.selectedIncorrectOption?.borderColor == nil {
            theme.selectedIncorrectOption?.borderColor = .red
        }

        if theme.selectedIncorrectOption?.borderWidth == nil {
            theme.selectedIncorrectOption?.borderWidth = LLUIConstants.DefaultSelectableOptionBorderWidth
        }

        if theme.selectedIncorrectOptionPercentage == nil {
            theme.selectedIncorrectOptionPercentage = LLThemeViewModel()
        }

        if theme.selectedIncorrectOptionPercentage?.fontColor == nil {
            theme.selectedIncorrectOptionPercentage?.fontColor = LLUIConstants.DefaultTextFontColor
        }

        if theme.selectedIncorrectOptionBar == nil {
            theme.selectedIncorrectOptionBar = LLThemeViewModel()
        }

        if theme.selectedIncorrectOptionBar?.background == nil {
            theme.selectedIncorrectOptionBar?.background = .fill(.init(color: .red))
        }

        // Unselected Correct Options
        if theme.unselectedCorrectOption == nil {
            theme.unselectedCorrectOption = LLThemeViewModel()
        }

        if theme.unselectedCorrectOption?.borderRadius == nil {
            theme.unselectedCorrectOption?.borderRadius = LLUIConstants.DefaultSelectableOptionBorderRadius
        }

        if theme.unselectedCorrectOption?.borderColor == nil {
            theme.unselectedCorrectOption?.borderColor = LLUIConstants.SelectedColorBorder
        }

        if theme.unselectedCorrectOption?.borderWidth == nil {
            theme.unselectedCorrectOption?.borderWidth = LLUIConstants.DefaultSelectableOptionBorderWidth
        }

        if theme.unselectedCorrectOptionPercentage == nil {
            theme.unselectedCorrectOptionPercentage = LLThemeViewModel()
        }

        if theme.unselectedCorrectOptionPercentage?.fontColor == nil {
            theme.unselectedCorrectOptionPercentage?.fontColor = LLUIConstants.DefaultTextFontColor
        }

        if theme.unselectedCorrectOptionBar == nil {
            theme.unselectedCorrectOptionBar = LLThemeViewModel()
        }

        if theme.unselectedCorrectOptionBar?.background == nil {
            theme.unselectedCorrectOptionBar?.background = LLUIConstants.SelectedBarBackground
        }

        // Unselected Incorrect Options
        if theme.unselectedIncorrectOption == nil {
            theme.unselectedIncorrectOption = LLThemeViewModel()
        }

        if theme.unselectedIncorrectOption?.borderRadius == nil {
            theme.unselectedIncorrectOption?.borderRadius = LLUIConstants.DefaultSelectableOptionBorderRadius
        }

        if theme.unselectedIncorrectOption?.borderColor == nil {
            theme.unselectedIncorrectOption?.borderColor = LLUIConstants.UnselectedColorBorder
        }

        if theme.unselectedIncorrectOption?.borderWidth == nil {
            theme.unselectedIncorrectOption?.borderWidth = LLUIConstants.DefaultSelectableOptionBorderWidth
        }

        if theme.unselectedIncorrectOptionPercentage == nil {
            theme.unselectedIncorrectOptionPercentage = LLThemeViewModel()
        }

        if theme.unselectedIncorrectOptionPercentage?.fontColor == nil {
            theme.unselectedIncorrectOptionPercentage?.fontColor = LLUIConstants.DefaultTextFontColor
        }

        if theme.unselectedIncorrectOptionBar == nil {
            theme.unselectedIncorrectOptionBar = LLThemeViewModel()
        }

        if theme.unselectedIncorrectOptionBar?.background == nil {
            theme.unselectedIncorrectOptionBar?.background = LLUIConstants.UnselectedBarBackground
        }

        LLBaseWidgetProperties.applyDefaultThemeProperties(to: &theme)
    }

    /**
        Prediction widget default properties
     */
    static func applyDefaultPredictionWidgetProperties(to theme: inout LLThemeWidgetModel) {
        if theme.information == nil {
            theme.information = LLThemeViewModel()
        }

        if theme.information?.fontSize == nil {
            theme.information?.fontSize = 18.0
        }

        if theme.information?.fontWeight == nil {
            theme.information?.fontWeight = .normal
        }

        if theme.information?.fontColor == nil {
            theme.information?.fontColor = .white
        }

        if theme.information?.textAlign == nil {
            theme.information?.textAlign = .center
        }

        LLBaseWidgetProperties.applyDefaultThemeProperties(to: &theme)
    }

    /**
        Odds widget default properties
     */
    static func applyDefaultOddsWidgetProperties(to theme: inout LLThemeWidgetModel) {
        if theme.title == nil {
            theme.title = LLThemeViewModel()
        }

        if theme.title?.textAlign == nil {
            #if os(tvOS)
                theme.title?.textAlign = .left
            #else
                theme.title?.textAlign = .center
            #endif
        }

        // Lock Icon
        if theme.lockIcon == nil {
            theme.lockIcon = LLThemeColorModel()
        }

        if theme.lockIcon?.color == nil {
            theme.lockIcon?.color = .init(hexaRGBA: "#FFFFFF")
        }

        // Root Defaults
        if theme.root == nil {
            theme.root = LLThemeViewModel()
        }

        if theme.root?.background == nil {
            theme.root?.background = LLUIConstants.DefaultBackground
        }

        // Separator defaults

        if theme.separator == nil {
            theme.separator = LLThemeColorModel()
        }

        if theme.separator?.color == nil {
            #if os(tvOS)
                theme.separator?.color = .init(hexaRGBA: "#FFFFFFFF")
            #else
                theme.separator?.color = .init(hexaRGBA: "#FFFFFF")
            #endif
        }

        // Header
        if theme.header == nil {
            theme.header = LLThemeViewModel()
        }

        if theme.header?.fontSize == nil {
            theme.header?.fontSize = 14.0
        }

        if theme.header?.fontSize == nil {
            theme.header?.fontSize = 14.0
        }

        if theme.header?.padding == nil {
            theme.header?.padding = .init(doubleArray: [16, 16, 0, 16])
        }

        // Header
        if theme.body == nil {
            theme.body = LLThemeViewModel()
        }

        if theme.body?.background == nil {
            theme.body?.background = LLUIConstants.DefaultBackground
        }

        if theme.body?.padding == nil {
            theme.body?.padding = .init(doubleArray: [8, 16, 0, 16])
        }

        if theme.footer == nil {
            theme.footer = LLThemeViewModel()
        }

        if theme.footer?.padding == nil {
            theme.footer?.padding = .init(doubleArray: [8, 16, 8, 16])
        }

        // Results description

        if theme.resultDescription == nil {
            theme.resultDescription = LLThemeViewModel()
        }

        if theme.resultDescription?.fontSize == nil {
            theme.resultDescription?.fontSize = 12
        }

        if theme.resultDescription?.textAlign == nil {
            theme.resultDescription?.textAlign = .center
        }

        if theme.resultDescription?.fontColor == nil {
            theme.resultDescription?.fontColor = .white
        }

        // Team Score

        if theme.teamScore == nil {
            theme.teamScore = LLThemeViewModel()
        }

        if theme.teamScore?.fontSize == nil {
            theme.teamScore?.fontSize = 18
        }

        if theme.teamScore?.fontWeight == nil {
            theme.teamScore?.fontWeight = .bold
        }

        if theme.teamScore?.fontColor == nil {
            theme.teamScore?.fontColor = .white.withAlphaComponent(0.7)
        }

        // Team Score Winner

        if theme.teamScoreWinner == nil {
            theme.teamScoreWinner = LLThemeViewModel()
        }

        if theme.teamScoreWinner?.fontSize == nil {
            theme.teamScoreWinner?.fontSize = 18
        }

        if theme.teamScoreWinner?.fontWeight == nil {
            theme.teamScoreWinner?.fontWeight = .bold
        }

        if theme.teamScoreWinner?.fontColor == nil {
            theme.teamScoreWinner?.fontColor = .white
        }

        // Option row

        if theme.optionRow == nil {
            theme.optionRow = LLThemeViewModel()
        }

        if theme.optionRow?.fontWeight == nil {
            theme.optionRow?.fontWeight = .bold
        }

        if theme.optionRow?.padding == nil {
            theme.optionRow?.padding = Padding(top: 8, right: 0, bottom: 8, left: 0)
        }

        // Option row vertical

        if theme.optionRowVertical == nil {
            theme.optionRowVertical = LLThemeViewModel()
        }

        if theme.optionRowVertical?.fontWeight == nil {
            theme.optionRowVertical?.fontWeight = .bold
        }

        if theme.optionRowVertical?.padding == nil {
            theme.optionRowVertical?.padding = Padding(top: 8, right: 0, bottom: 8, left: 0)
        }

        // Team name
        if theme.teamName == nil {
            theme.teamName = LLThemeViewModel()
        }

        if theme.teamName?.fontSize == nil {
            theme.teamName?.fontSize = 12.0
        }

        if theme.teamName?.fontColor == nil {
            theme.teamName?.fontColor = .init(hexaRGBA: "#FFFFFF")
        }

        // Unselected option defaults

        if theme.unselectedOption == nil {
            theme.unselectedOption = LLThemeViewModel()
        }

        if theme.unselectedOption?.fontSize == nil {
            theme.unselectedOption?.fontSize = 12.0
        }

        if theme.unselectedOption?.padding == nil {
            theme.unselectedOption?.padding = Padding(top: 8, right: 8, bottom: 8, left: 8)
        }

        if theme.unselectedOption?.borderColor == nil {
            theme.unselectedOption?.borderColor = .init(hexaRGBA: "FFFFFF")
        }

        if theme.unselectedOption?.fontColor == nil {
            theme.unselectedOption?.fontColor = .init(hexaRGBA: "FFFFFF")
        }

        // Unselected option description

        if theme.unselectedOptionDescription == nil {
            theme.unselectedOptionDescription = LLThemeViewModel()
        }

        if theme.unselectedOptionDescription?.fontSize == nil {
            theme.unselectedOptionDescription?.fontSize = 10.0
        }

        if theme.unselectedOptionDescription?.fontColor == nil {
            theme.unselectedOptionDescription?.fontColor = .init(hexaRGBA: "FFFFFF")
        }

        // Selected option defaults

        if theme.selectedOption == nil {
            theme.selectedOption = LLThemeViewModel()
        }

        if theme.selectedOption?.fontSize == nil {
            theme.selectedOption?.fontSize = 12.0
        }

        if theme.selectedOption?.padding == nil {
            theme.selectedOption?.padding = Padding(top: 8, right: 8, bottom: 8, left: 8)
        }

        if theme.selectedOption?.fontColor == nil {
            theme.selectedOption?.fontColor = .init(hexaRGBA: "FFFFFF")
        }

        // Unselected option description

        if theme.selectedOptionDescription == nil {
            theme.selectedOptionDescription = LLThemeViewModel()
        }

        if theme.selectedOptionDescription?.fontSize == nil {
            theme.selectedOptionDescription?.fontSize = 10.0
        }

        if theme.selectedOptionDescription?.fontColor == nil {
            theme.selectedOptionDescription?.fontColor = .init(hexaRGBA: "FFFFFF")
        }

        // Bet Button defaults

        if theme.betButton == nil {
            theme.betButton = LLThemeViewModel()
        }

        if theme.betButton?.background == nil {
            theme.betButton?.background = .fill(.init(color: .clear))
        }

        if theme.betButton?.fontColor == nil {
            theme.betButton?.fontColor = .white
        }

        #if os(tvOS)
            theme.betButton?.background = .fill(.init(color: .init(hexaRGBA: "#333333") ?? .clear))

            if theme.betButton?.borderRadius == nil {
                theme.betButton?.borderRadius = .init(topLeft: 6, topRight: 6, bottomLeft: 6, bottomRight: 6)
            }

        #endif

        // Bet Button defaults

        if theme.backButton == nil {
            theme.backButton = LLThemeViewModel()
        }

        if theme.backButton?.background == nil {
            theme.backButton?.background = .fill(.init(color: .clear))
        }

        if theme.backButton?.fontColor == nil {
            theme.backButton?.fontColor = .white
        }

        if theme.backButton?.fontSize == nil {
            theme.backButton?.fontSize = 14.0
        }

        if theme.backButton?.fontWeight == nil {
            theme.backButton?.fontWeight = .bold
        }

        #if os(tvOS)
            theme.backButton?.background = .fill(.init(color: .init(hexaRGBA: "#333333") ?? .clear))

            if theme.backButton?.borderRadius == nil {
                theme.backButton?.borderRadius = .init(topLeft: 6, topRight: 6, bottomLeft: 6, bottomRight: 6)
            }

            if theme.backButton?.padding == nil {
                theme.backButton?.padding = .init(top: 8.0, right: 16.0, bottom: 8.0, left: 16.0)
            }
        #endif
        // Footer

        if theme.footer == nil {
            theme.footer = LLThemeViewModel()
        }

        if theme.footer?.padding == nil {
            theme.footer?.padding = .init(doubleArray: [8, 16, 8, 16])
        }

        LLBaseWidgetProperties.applyDefaultThemeProperties(to: &theme)
    }

    /**
        Alert Widget default properties
     */
    static func applyDefaultAlertProperties(to theme: inout LLThemeWidgetModel) {
        if theme.body == nil {
            theme.body = LLThemeViewModel()
        }

        if theme.body?.padding == nil {
            theme.body?.padding = LLUIConstants.DefaultAlertBodyPadding
        }

        if theme.footer == nil {
            theme.footer = LLThemeViewModel()
        }

        if theme.footer?.padding == nil {
            theme.footer?.padding = LLUIConstants.DefaultAlertFooterPadding
        }

        LLBaseWidgetProperties.applyDefaultThemeProperties(to: &theme)
    }

    static func applyDefaultThemeProperties(to theme: inout LLThemeWidgetModel) {
        // Dismiss button properties

        if theme.dismiss == nil {
            theme.dismiss = LLThemeDismissModel()
        }

        if theme.dismiss?.margin == nil {
            theme.dismiss?.margin = LLUIConstants.DismissButtonDefaultMargin
        }

        if theme.dismiss?.color == nil {
            theme.dismiss?.color = LLUIConstants.DefaultDismissButtonColor
        }

        // Countdown timer defaults
        if theme.timer == nil {
            theme.timer = LLThemeTimerModel()
        }

        if theme.timer?.height == nil {
            theme.timer?.height = LLUIConstants.DefaultTimerBarWidth
        }

        if theme.timer?.background == nil {
            theme.timer?.background = LLUIConstants.DefaultTimerColor
        }

        // Timer defaults

        if theme.roundTimer == nil {
            theme.roundTimer = LLThemeTimerModel()
        }

        if theme.roundTimer?.height == nil {
            theme.roundTimer?.height = LLUIConstants.DefaultTimerHeight
        }

        if theme.roundTimer?.barWidth == nil {
            theme.roundTimer?.barWidth = LLUIConstants.DefaultTimerBarWidth
        }

        if theme.roundTimer?.background == nil {
            theme.roundTimer?.background = LLUIConstants.DefaultTimerColor
        }

        if theme.timer?.fontColor == nil {
            theme.timer?.fontColor = LLUIConstants.DefaultTextFontColor
        }

        // Root defaults

        if theme.root == nil {
            theme.root = LLThemeViewModel()
        }

        if theme.root?.borderRadius == nil {
            theme.root?.borderRadius = LLUIConstants.DefaultRootBorderRadius
        }

        // Title defaults

        if theme.title == nil {
            theme.title = LLThemeViewModel()
        }

        if theme.title?.fontColor == nil {
            theme.title?.fontColor = LLUIConstants.DefaultTextFontColor
        }

        if theme.title?.fontWeight == nil {
            theme.title?.fontWeight = .bold
        }

        // Header defaults

        if theme.header == nil {
            theme.header = LLThemeViewModel()
        }

        if theme.header?.fontColor == nil {
            theme.header?.fontColor = LLUIConstants.DefaultTextFontColor
        }

        if theme.header?.fontWeight == nil {
            theme.header?.fontWeight = .bold
        }

        if theme.header?.fontSize == nil {
            theme.header?.fontSize = LLUIConstants.DefaultHeaderFontSize
        }

        if theme.header?.background == nil {
            theme.header?.background = LLUIConstants.DefaultBackground
        }

        if theme.header?.padding == nil {
            theme.header?.padding = LLUIConstants.DefaultHeaderPadding
        }

        // Body defaults

        if theme.body == nil {
            theme.body = LLThemeViewModel()
        }

        if theme.body?.background == nil {
            theme.body?.background = LLUIConstants.DefaultBackground
        }

        if theme.body?.padding == nil {
            theme.body?.padding = LLUIConstants.DefaultBodyPadding
        }

        // Insights defaults

        if theme.animatedBet == nil {
            theme.animatedBet = LLThemeViewModel()
        }

        if theme.insights == nil {
            theme.insights = LLThemeViewModel()
        }

        if theme.insightBodyText == nil {
            theme.insightBodyText = LLThemeViewModel()
        }

        if theme.insightBodyText?.padding == nil {
            theme.insightBodyText?.padding = LLUIConstants.InsightComponentsPadding
        }

        if theme.insights?.background == nil {
            theme.insights?.background = LLUIConstants.DefaultBackground
        }

        if theme.insights?.fontColor == nil {
            theme.insights?.fontColor = LLUIConstants.DefaultTextFontColor
        }

        if theme.insights?.padding == nil {
            theme.insights?.padding = LLUIConstants.DefaultInsightsPadding
        }

        if theme.insightTable == nil {
            theme.insightTable = LLThemeViewModel()
        }
        if theme.insightTable?.padding == nil {
            theme.insightTable?.padding = LLUIConstants.InsightComponentsPadding
        }

        if theme.insightTableCell == nil {
            theme.insightTableCell = LLThemeViewModel()
        }

        if theme.insightTableCell?.padding == nil {
            theme.insightTableCell?.padding = LLUIConstants.InsightTableCellPadding
        }

        if theme.insightTableCell == nil {
            theme.insightTableCell?.fontColor = LLUIConstants.DefaultTextFontColor
        }

        if theme.insightTableCellHighlighted == nil {
            theme.insightTableCellHighlighted = LLThemeViewModel()
        }

        if theme.insightTableCellHighlighted?.padding == nil {
            theme.insightTableCellHighlighted?.padding = LLUIConstants.InsightTableCellPadding
        }

        if theme.insightTableCellHighlighted?.fontColor == nil {
            theme.insightTableCellHighlighted?.fontColor = LLUIConstants.DefaultTextFontColor
        }

        if theme.insightTableCellHighlighted?.background == nil {
            theme.insightTableCellHighlighted?.background = LLUIConstants.DefaultInsightsBackground
        }

        // Unselected option bar defaults

        if theme.unselectedOption == nil {
            theme.unselectedOption = LLThemeViewModel()
        }

        if theme.unselectedOption?.borderRadius == nil {
            theme.unselectedOption?.borderRadius = LLUIConstants.DefaultSelectableOptionBorderRadius
        }

        if theme.unselectedOption?.borderColor == nil {
            theme.unselectedOption?.borderColor = LLUIConstants.UnselectedColorBorder
        }

        if theme.unselectedOption?.borderWidth == nil {
            theme.unselectedOption?.borderWidth = LLUIConstants.DefaultSelectableOptionBorderWidth
        }

        // Selected option bar defaults

        if theme.selectedOption == nil {
            theme.selectedOption = LLThemeViewModel()
        }

        if theme.selectedOption?.borderRadius == nil {
            theme.selectedOption?.borderRadius = LLUIConstants.DefaultSelectableOptionBorderRadius
        }

        if theme.selectedOption?.borderColor == nil {
            theme.selectedOption?.borderColor = LLUIConstants.DefaultAccentColor
        }

        if theme.selectedOption?.borderWidth == nil {
            theme.selectedOption?.borderWidth = LLUIConstants.DefaultSelectableOptionBorderWidth
        }

        // Selected option bar

        if theme.selectedOptionBar == nil {
            theme.selectedOptionBar = LLThemeViewModel()
        }

        if theme.selectedOptionBar?.background == nil {
            theme.selectedOptionBar?.background = LLUIConstants.SelectedBarBackground
        }

        // Selected option bar

        if theme.unselectedOptionBar == nil {
            theme.unselectedOptionBar = LLThemeViewModel()
        }

        if theme.unselectedOptionBar?.background == nil {
            theme.unselectedOptionBar?.background = LLUIConstants.UnselectedBarBackground
        }

        // Footer defaults

        if theme.footer == nil {
            theme.footer = LLThemeViewModel()
        }

        if theme.footer?.background == nil {
            theme.footer?.background = LLUIConstants.DefaultBackground
        }

        if theme.footer?.fontColor == nil {
            theme.footer?.fontColor = LLUIConstants.DefaultTextFontColor
        }

        if theme.footer?.fontWeight == nil {
            theme.footer?.fontWeight = .bold
        }

        if theme.footer?.padding == nil {
            theme.footer?.padding = LLUIConstants.DefaultFooterPadding
        }

        // Bet Button defaults

        if theme.betButton == nil {
            theme.betButton = LLThemeViewModel()
        }

        if theme.betButton?.background == nil {
            theme.betButton?.background = LLUIConstants.DefaultBetButtonBackground
        }

        if theme.betButton?.padding == nil {
            theme.betButton?.padding = LLUIConstants.DefaultBetButtonPadding
        }

        if theme.betButton?.borderRadius == nil {
            theme.betButton?.borderRadius = LLUIConstants.DefaultBetButtonBorderRadius
        }

        if theme.betButton?.fontColor == nil {
            theme.betButton?.fontColor = LLUIConstants.DefaultBetButtonFontColor
        }

        if theme.betButton?.fontSize == nil {
            theme.betButton?.fontSize = 16.0
        }

        if theme.betButton?.fontWeight == nil {
            theme.betButton?.fontWeight = LLUIConstants.BetButonFontWeight
        }

        // Sponsor text defaults

        if theme.sponsorText == nil {
            theme.sponsorText = LLThemeViewModel()
        }

        if theme.sponsorText?.fontSize == nil {
            theme.sponsorText?.fontSize = LLUIConstants.DefaultSponsoredByLabelFontSize
        }

        if theme.sponsorText?.fontColor == nil {
            theme.sponsorText?.fontColor = LLUIConstants.DefaultSponsoredByLabelFontColor
        }

        // Sponsor area defaults

        if theme.sponsor == nil {
            theme.sponsor = LLThemeViewModel()
        }

        if theme.sponsor?.padding == nil {
            theme.sponsor?.padding = LLUIConstants.DefaultSponsoredImagePadding
        }

        if theme.sponsor?.borderWidth == nil {
            theme.sponsor?.borderWidth = LLUIConstants.DefaultSponsoredImageBorderWidth
        }

        if theme.sponsor?.borderColor == nil {
            theme.sponsor?.borderColor = LLUIConstants.DefaultSponsoredImageBorderColor
        }

        // Option image overwriter
        if theme.optionImageOverwriter == nil {
            theme.optionImageOverwriter = LLThemeViewModel()
        }

        if theme.optionImageOverwriter?.fontSize == nil {
            theme.optionImageOverwriter?.fontSize = LLUIConstants.DefaultImageOverwriterFontSize
        }

        // Disclaimer
        if theme.disclaimer == nil {
            theme.disclaimer = LLThemeViewModel()
        }

        if theme.disclaimer?.background == nil {
            theme.disclaimer?.background = LLUIConstants.DefaultBackground
        }

        if theme.disclaimer?.padding == nil {
            theme.disclaimer?.padding = LLUIConstants.DefaultFooterPadding
        }

        if theme.disclaimerTitle == nil {
            theme.disclaimerTitle = LLThemeViewModel()
        }

        if theme.disclaimerTitle?.fontSize == nil {
            theme.disclaimerTitle?.fontSize = 14.0
        }

        if theme.disclaimerTitle?.fontWeight == nil {
            theme.disclaimerTitle?.fontWeight = .bold
        }

        // Disclaimer details

        if theme.disclaimerDescription == nil {
            theme.disclaimerDescription = LLThemeViewModel()
        }

        if theme.disclaimerDescription?.fontSize == nil {
            theme.disclaimerDescription?.fontSize = 12.0
        }

        // Lock Icon
        if theme.accessoryButton == nil {
            theme.accessoryButton = LLThemeDismissModel()
        }

        if theme.accessoryButton?.color == nil {
            theme.accessoryButton?.color = .init(hexaRGBA: "#FFFFFF")
        }
    }
}
