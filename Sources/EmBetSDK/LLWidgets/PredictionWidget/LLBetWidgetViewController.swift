//
//  LLBetWidgetViewController.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 23.2.22.
//

import LiveLikeSwift
import UIKit

/**
 Container view controller (widget), that prepares itself acording to data.
 */
internal class LLBetWidgetViewController: LLWidget {
    var betWidgetCustomData: LLBetWidgetPayload! {
        return (customDataPayload as? LLBetWidgetPayload)
    }

    private let model: PredictionWidgetModel

    private var selectionCoordinator: LLSelectableViewCoordinator?

    /// The body content view of the widget
    private weak var bodyContentView: UIView?

    override var currentState: WidgetState {
        willSet {
            previousState = currentState
        }
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.delegate?.widgetDidEnterState(widget: self, state: self.currentState)
                switch self.currentState {
                case .ready:
                    break
                case .interacting:
                    self.enterInteractingState()
                case .results:
                    self.enterResultsState()
                case .finished:
                    self.enterFinishedState()
                }
            }
        }
    }

    private var selectedOptionID: String?

    override required init?(safelyWith model: PredictionWidgetModel) {
        self.model = model
        super.init(safelyWith: model)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        model.delegate = self
        model.registerImpression()
        // On load check if interactivity period has expired
        if model.isInteractivityExpired() {
            bodyContentView?.isUserInteractionEnabled = false
        } else {
            model.markAsInteractive()
        }
    }

    /**
     Animates the insight view if present
     */
    private func animateInsight(animatedView: UIView?) {
        guard let animatedBetHolderView = animatedView else { return }
        animatedBetHolderView.isHidden = true

        // On null duration, keep the insight hidden
        guard let duration = betWidgetCustomData.animationDelay else { return }

        // On 0 duration, show the animated bet right away
        if duration == 0 {
            animatedBetHolderView.isHidden = false
            return
        }

        // Show animated bet part after delay
        DispatchQueue.main.asyncAfter(deadline: .now() + duration / 1000) {
            animatedBetHolderView.alpha = 0
            animatedBetHolderView.isHidden = false

            UIView.animate(withDuration: 1.3, delay: 0, options: UIView.AnimationOptions.layoutSubviews) {
                animatedBetHolderView.alpha = 1
            } completion: { finished in
                if finished {}
            }
        }
    }

    private func enterInteractingState() {
        model.markAsInteractive()
        delegate?.widgetStateCanComplete(widget: self, state: .interacting)
    }

    private func enterResultsState() {
        // Dissable user interaction
        bodyContentView?.isUserInteractionEnabled = false
        selectionCoordinator?.performInitialResultsReveal()
        hideCountdownAndTimer()

        delegate?.widgetStateCanComplete(widget: self, state: .results)
    }

    private func enterFinishedState() {
        delegate?.widgetStateCanComplete(widget: self, state: .finished)
    }
}

// MARK: - Widget body delegate

extension LLBetWidgetViewController: LLWidgetBodyViewDelegate {
    func widgetBodyDidSelectOptionWithID(id: String) {
        // On selection check if interactivity period has expired
        if model.isInteractivityExpired() {
            // Dissable further interactions
            bodyContentView?.isUserInteractionEnabled = false
            return
        }

        // Send event for selection
        let payload: [String: Any?] = [
            EmBetWidgetOptionEventKeys.Description: options?.first(where: { $0.id == id })?.text,
            EmBetWidgetOptionEventKeys.OptionID: id,
        ]
        super.addOptionSelectedEvent(payload: payload.compactMapValues { $0 })

        // Mark user interacted
        userDidInteract = true

        selectionCoordinator?.didSelect(id: id)
        selectedOptionID = id

        delegate?.userDidInteract(self)

        model.submitVote(optionID: id) { res in
            switch res {
            case let .failure(error):
                log.info("Error submitting vote: \(error.localizedDescription)")
            default:
                break
            }
        }
    }
}

extension LLBetWidgetViewController: PredictionWidgetModelDelegate {
    func predictionWidgetModel(_: PredictionWidgetModel, voteCountDidChange voteCount: Int, forOption optionID: String) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.selectionCoordinator?.didReceiveUpdateForOption(optionID: optionID, votes: voteCount, totalVotes: self.model.totalVoteCount)
        }
    }

    // No followup
    func predictionWidgetModel(_: PredictionWidgetModel, didReceiveFollowUp _: String, kind _: WidgetKind) {}
}

// MARK: Widget protocol conformation

extension LLBetWidgetViewController {
    func widgetBuilderInsightContentView() -> UIView? {
        if betWidgetCustomData.widgetType.presentationType == .insight {
            let insightView = LLInsightWidgetView(bodyText: betWidgetCustomData.bodyText, table: betWidgetCustomData.table, themeBuilder: themeBuilder)
            return insightView
        }
        return nil
    }

    func widgetBuilderHeaderContentView() -> UIView? {
        let widgetTitle = customDataPayload.labels?.widgetTitle ?? betWidgetCustomData.widgetType.descriptive

        let headerStackView = UIStackView()
        headerStackView.translatesAutoresizingMaskIntoConstraints = false
        headerStackView.spacing = LLUIConstants.HeaderElementsSpacing
        headerStackView.axis = .vertical

        let headerLbl = LLThemableLabel(themeType: .header)
        headerLbl.numberOfLines = 0
        headerLbl.textAlignment = .center
        headerLbl.text = widgetTitle.uppercased()
        headerStackView.addArrangedSubview(headerLbl)

        return headerStackView
    }

    func widgetBuilderAnimatedBetContentView() -> UIView? {
        if betWidgetCustomData.widgetType.presentationType == .insight {
            var animatableViews: [UIView] = []

            if let bodyView = widgetBuilderBodyContentView() {
                animatableViews.append(themeBuilder.embedInHolder(themeType: .body, view: bodyView))
            }
            if let footerView = widgetBuilderFooterContentView() {
                animatableViews.append(themeBuilder.embedInHolder(themeType: .footer, view: footerView))
            }

            let contentStackView = UIStackView(arrangedSubviews: animatableViews)
            contentStackView.axis = .vertical

            return contentStackView
        }

        return nil
    }

    func widgetBuilderBodyContentView() -> UIView? {
        let bodyStackView = UIStackView()
        bodyStackView.translatesAutoresizingMaskIntoConstraints = false
        bodyStackView.spacing = LLUIConstants.BodyElementsSpacing
        bodyStackView.axis = .vertical

        let titleLbl = LLThemableLabel(themeType: .title)
        titleLbl.numberOfLines = 0
        titleLbl.textAlignment = .center
        titleLbl.text = model.question
        bodyStackView.addArrangedSubview(themeBuilder.embedInHolder(themeType: .title, view: titleLbl))

        let bodyVC = LLWidgetBodyViewController(variation: betWidgetCustomData.widgetVariation, options: model.options, betDetails: betWidgetCustomData.betDetails, numberOfVotesText: customDataPayload.labels?.numberOfVotes)
        selectionCoordinator = bodyVC.selectionCoordinator
        selectionCoordinator?.themeBulder = themeBuilder
        bodyVC.delegate = self
        addChild(bodyVC)
        bodyVC.didMove(toParent: self)

        bodyStackView.addArrangedSubview(bodyVC.view)

        bodyContentView = bodyStackView

        return bodyStackView
    }

    func widgetBuilderFooterContentView() -> UIView? {
        let footerContent = LLWidgetFooter(customData: customDataPayload, themeBuilder: themeBuilder)
        footerContent?.interactionAction = { [weak self] in
            guard let self = self else { return }
            // Submit payload
            let payload = EmBetCTAEvent(url: self.betWidgetCustomData.placeBetURL, selectedOptionID: self.selectedOptionID)
            self.submitPressed(payload: payload)
        }

        return footerContent
    }

    func widgetBuilderWillSetupWidgetView() {
        LLBaseWidgetProperties.applyDefaultThemeProperties(to: &themeBuilder.themeModel)
    }

    func widgetBuilderDidSetupWidgetView(animatedView: UIView?) {
        animateInsight(animatedView: animatedView)
    }

    func widgetBuilderPrepareWidgetForTimeline() {
        currentState = .finished
        model.loadInteractionHistory { [weak self] _ in
            self?.loadLastVote()
        }
    }

    /**
        Loads the last vote
     */
    private func loadLastVote() {
        if let userVote = model.mostRecentVote {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                // Show instant results
                self.selectionCoordinator?.shouldShowInstantResults = true

                for option in self.model.options {
                    self.selectionCoordinator?.didReceiveUpdateForOption(optionID: option.id, votes: option.voteCount, totalVotes: self.model.totalVoteCount)
                }
                self.selectedOptionID = userVote.optionID
                self.selectionCoordinator?.didSelect(id: userVote.optionID)
            }
        }
    }
}
