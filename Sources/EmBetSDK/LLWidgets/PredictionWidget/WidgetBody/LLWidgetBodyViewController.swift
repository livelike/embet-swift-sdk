//
//  LLWidgetBodyViewController.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 24.2.22.
//

import LiveLikeSwift
import UIKit

protocol LLWidgetBodyViewDelegate: AnyObject {
    func widgetBodyDidSelectOptionWithID(id: String)
}

/**
    LLWidgetBodyViewController represents the "body" of a widget.
    It draws its views acording to the data provided in initialization.
 */
internal class LLWidgetBodyViewController: UIViewController {
    /**
        Selection delegate
     */
    weak var delegate: LLWidgetBodyViewDelegate?

    private let options: [EBWidgetOption]

    private let betDetails: [LLWidgetBetDetailsModel]

    private let variation: LLWidgetBodyVariation

    private let numberOfVotesText: String?

    var selectionCoordinator = LLSelectableViewCoordinator()

    init(
        variation: LLWidgetBodyVariation,
        options: [EBWidgetOption],
        betDetails: [LLWidgetBetDetailsModel],
        numberOfVotesText: String?
    ) {
        self.variation = variation
        self.options = options
        self.betDetails = betDetails
        self.numberOfVotesText = numberOfVotesText

        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        getContentView()
    }

    private func getContentView() {
        var contentView: UIStackView!

        switch variation {
        case .square:
            contentView = preparedContentViewAsSquareVariation()
        case .inline:
            contentView = preparedContentViewAsInlineVariation()
        case .bar:
            contentView = preparedContentViewAsBarVariation()
        case .duel:
            contentView = preparedContentViewAsDuelVariation()
        case .squareFollowUp:
            contentView = preparedContentViewAsSquareFollowUpVariation()
        }

        contentView.setContentHuggingPriority(.defaultHigh, for: .vertical)

        view.addSubview(contentView)

        NSLayoutConstraint.activate([
            contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: LLUIConstants.BodyItemsHorizontalPadding),
            contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -LLUIConstants.BodyItemsHorizontalPadding),
            contentView.topAnchor.constraint(equalTo: view.topAnchor),
            contentView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
    }

    /**
        Prepares the content view as a square variation and returns it.
     */
    private func preparedContentViewAsSquareVariation() -> UIStackView {
        var subviews: [LLSelectableView] = []

        for option in options {
            let betDetail = betDetails.first(where: { $0.description == option.text })

            let optionView = LLSquareSelectableView(option: option, betDetails: betDetail, selectedThemeType: .selectedOption, unselectedThemeType: .unselectedOption, numberOfVotesText: numberOfVotesText)
            optionView.translatesAutoresizingMaskIntoConstraints = false
            optionView.delegate = self
            subviews.append(optionView)
        }

        selectionCoordinator.views = subviews

        let holderStackView = UIStackView(arrangedSubviews: subviews)
        holderStackView.translatesAutoresizingMaskIntoConstraints = false
        holderStackView.axis = .horizontal
        holderStackView.distribution = .fillEqually
        holderStackView.spacing = LLUIConstants.SelectableOptionsSpacing
        return holderStackView
    }

    /**
        Prepares the content view as a bar variation and returns it.
     */
    private func preparedContentViewAsBarVariation() -> UIStackView {
        var subviews: [LLSelectableView] = []

        for option in options {
            let betDetail = betDetails.first(where: { $0.description == option.text })

            let optionView = LLBarSelectableView(option: option, betDetails: betDetail, selectedThemeType: .selectedOption, unselectedThemeType: .unselectedOption, numberOfVotesText: numberOfVotesText)
            optionView.translatesAutoresizingMaskIntoConstraints = false
            optionView.delegate = self
            subviews.append(optionView)
        }

        selectionCoordinator.views = subviews

        let holderStackView = UIStackView(arrangedSubviews: subviews)
        holderStackView.translatesAutoresizingMaskIntoConstraints = false
        holderStackView.axis = .vertical
        holderStackView.distribution = .fillEqually
        holderStackView.spacing = LLUIConstants.SelectableOptionsSpacing

        return holderStackView
    }

    /**
        Prepares the content view as a inline variation and returns it.
     */
    private func preparedContentViewAsInlineVariation() -> UIStackView {
        var subviews: [LLSelectableView] = []

        let leftItemBetDetails = betDetails.first(where: { $0.description == options.first?.text })
        let leftImageView = ImageOverwriter.overwriteImage(imageUrl: options.first?.imageURL, text: leftItemBetDetails?.imageOverwriter)
        leftImageView.translatesAutoresizingMaskIntoConstraints = false

        let leftImageViewHolder = UIView()
        leftImageViewHolder.translatesAutoresizingMaskIntoConstraints = false
        leftImageViewHolder.addSubview(leftImageView)

        NSLayoutConstraint.activate([
            leftImageView.leadingAnchor.constraint(equalTo: leftImageViewHolder.leadingAnchor),
            leftImageView.trailingAnchor.constraint(equalTo: leftImageViewHolder.trailingAnchor),
            leftImageView.bottomAnchor.constraint(lessThanOrEqualTo: leftImageViewHolder.bottomAnchor),
            leftImageView.topAnchor.constraint(greaterThanOrEqualTo: leftImageViewHolder.topAnchor),
            leftImageView.centerYAnchor.constraint(equalTo: leftImageViewHolder.centerYAnchor),
        ])

        let rightItemBetDetails = betDetails.first(where: { $0.description == options.last?.text })
        let rightImageView = ImageOverwriter.overwriteImage(imageUrl: options.last?.imageURL, text: rightItemBetDetails?.imageOverwriter)
        rightImageView.translatesAutoresizingMaskIntoConstraints = false

        let rightImageViewHolder = UIView()
        rightImageViewHolder.translatesAutoresizingMaskIntoConstraints = false
        rightImageViewHolder.addSubview(rightImageView)

        NSLayoutConstraint.activate([
            rightImageView.leadingAnchor.constraint(equalTo: rightImageViewHolder.leadingAnchor),
            rightImageView.trailingAnchor.constraint(equalTo: rightImageViewHolder.trailingAnchor),
            rightImageView.bottomAnchor.constraint(lessThanOrEqualTo: rightImageViewHolder.bottomAnchor),
            rightImageView.topAnchor.constraint(greaterThanOrEqualTo: rightImageViewHolder.topAnchor),
            rightImageView.centerYAnchor.constraint(equalTo: rightImageViewHolder.centerYAnchor),
        ])

        for option in options {
            let betDetail = betDetails.first(where: { $0.description == option.text })

            let optionView = LLInlineSelectableView(option: option, betDetails: betDetail, selectedThemeType: .selectedOption, unselectedThemeType: .unselectedOption, numberOfVotesText: numberOfVotesText)
            optionView.translatesAutoresizingMaskIntoConstraints = false
            optionView.delegate = self
            subviews.append(optionView)
        }

        selectionCoordinator.views = subviews

        let holderStackView = UIStackView(arrangedSubviews: subviews)
        holderStackView.translatesAutoresizingMaskIntoConstraints = false
        holderStackView.spacing = LLUIConstants.SelectableOptionsSpacing
        holderStackView.axis = .vertical
        holderStackView.distribution = .fillEqually

        let retStackView = UIStackView(arrangedSubviews: [leftImageViewHolder, holderStackView, rightImageViewHolder])
        retStackView.translatesAutoresizingMaskIntoConstraints = false
        retStackView.spacing = LLUIConstants.BodyInlineImageSpacing
        retStackView.axis = .horizontal

        NSLayoutConstraint.activate([
            rightImageView.heightAnchor.constraint(equalToConstant: LLUIConstants.InlineSelectableViewImageSize.height),
            rightImageView.widthAnchor.constraint(equalToConstant: LLUIConstants.InlineSelectableViewImageSize.width),
            leftImageView.heightAnchor.constraint(equalToConstant: LLUIConstants.InlineSelectableViewImageSize.height),
            leftImageView.widthAnchor.constraint(equalToConstant: LLUIConstants.InlineSelectableViewImageSize.width),
        ])

        return retStackView
    }

    /**
        Prepares the content view as a duel variation and returns it.
     */
    private func preparedContentViewAsDuelVariation() -> UIStackView {
        var subviews: [LLSelectableView] = []

        let firstOptionText = options.first?.text ?? ""
        let lastOptionText = options.last?.text ?? ""

        // Holds the first option graphic
        let leftOptionHolder = DuelGraphic(teamName: firstOptionText, position: .left)
        leftOptionHolder.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)

        // Holds the second option graphic
        let rightOptionHolder = DuelGraphic(teamName: lastOptionText, position: .right)
        rightOptionHolder.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)

        for option in options {
            let betDetail = betDetails.first(where: { $0.description == option.text })
            let optionView = LLDuelSelectableView(option: option, betDetails: betDetail, selectedThemeType: .selectedOption, unselectedThemeType: .unselectedOption, numberOfVotesText: numberOfVotesText)
            optionView.translatesAutoresizingMaskIntoConstraints = false
            optionView.delegate = self
            subviews.append(optionView)
        }

        selectionCoordinator.views = subviews

        // Add the options views into a stack view
        let holderStackView = UIStackView(arrangedSubviews: subviews)
        holderStackView.translatesAutoresizingMaskIntoConstraints = false
        holderStackView.spacing = LLUIConstants.SelectableOptionsSpacing
        holderStackView.axis = .horizontal
        holderStackView.distribution = .fillEqually

        // Label between the two team names
        let vsLabel = UILabel()
        vsLabel.translatesAutoresizingMaskIntoConstraints = false
        vsLabel.text = LLUIConstants.DuelVariationVSLabelText
        vsLabel.textColor = .white
        vsLabel.textAlignment = .center
        vsLabel.font = .systemFont(ofSize: LLUIConstants.DuelVariationVSLabelFontSize)

        let betDetailsHolder = UIStackView(arrangedSubviews: [leftOptionHolder, vsLabel, rightOptionHolder])
        betDetailsHolder.distribution = .fillProportionally
        betDetailsHolder.translatesAutoresizingMaskIntoConstraints = false
        betDetailsHolder.spacing = LLUIConstants.BodyInlineImageSpacing
        betDetailsHolder.axis = .horizontal

        let bodyStackView = UIStackView(arrangedSubviews: [betDetailsHolder, holderStackView])
        bodyStackView.distribution = .fill
        bodyStackView.translatesAutoresizingMaskIntoConstraints = false
        bodyStackView.spacing = LLUIConstants.DuelVariationBodyElementSpacing
        bodyStackView.axis = .vertical

        NSLayoutConstraint.activate([
            leftOptionHolder.widthAnchor.constraint(equalTo: rightOptionHolder.widthAnchor),
        ])

        return bodyStackView
    }

    /**
        Prepares the content view as a square followup variation and returns it.
     */
    private func preparedContentViewAsSquareFollowUpVariation() -> UIStackView {
        var subviews: [LLSelectableView] = []

        for option in options {
            let betDetail = betDetails.first(where: { $0.description == option.text })

            let optionView = LLSquareFollowUpSelectableView(option: option, betDetails: betDetail, selectedThemeType: .selectedOption, unselectedThemeType: .unselectedOption, numberOfVotesText: numberOfVotesText)
            optionView.translatesAutoresizingMaskIntoConstraints = false
            optionView.delegate = self
            subviews.append(optionView)
        }

        selectionCoordinator.views = subviews

        let holderStackView = UIStackView(arrangedSubviews: subviews)
        holderStackView.translatesAutoresizingMaskIntoConstraints = false
        holderStackView.axis = .horizontal
        holderStackView.distribution = .fillEqually
        holderStackView.spacing = LLUIConstants.SelectableOptionsSpacing
        return holderStackView
    }
}

extension LLWidgetBodyViewController: LLSelectableViewDelegate {
    func didSelectOptionWithID(id: String) {
        delegate?.widgetBodyDidSelectOptionWithID(id: id)
    }
}
