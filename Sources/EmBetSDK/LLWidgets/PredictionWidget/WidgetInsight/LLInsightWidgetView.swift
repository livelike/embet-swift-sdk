//
//  LLInsightWidgetView.swift
//  EmBetSDK
//
//  Created by Ljupco Nastevski on 3.3.22.
//

import UIKit

class LLInsightWidgetView: UIView {
    private var themeBuilder: LLThemeBuilder

    required init(bodyText: String?, table: [LLWidgetTableElementModel]?, themeBuilder: LLThemeBuilder) {
        self.themeBuilder = themeBuilder
        super.init(frame: .zero)
        initialSetup(bodyText: bodyText, table: table)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func initialSetup(bodyText: String?, table: [LLWidgetTableElementModel]?) {
        var mainViews: [UIView] = []

        if let bodyText = bodyText {
            let bodyTextLbl = LLThemableLabel(themeType: .insights)
            bodyTextLbl.textAlignment = .center
            bodyTextLbl.text = bodyText
            bodyTextLbl.numberOfLines = 0
            mainViews.append(themeBuilder.embedInHolder(themeType: .insightBodyText, view: bodyTextLbl))
        }

        if let table = table, table.count > 0 {
            let tableView = LLInsightTableView(elements: table, themeBuilder: themeBuilder)
            mainViews.append(themeBuilder.embedInMarginView(themeType: .insightTable, view: tableView))
        }

        let mainStackView = UIStackView(arrangedSubviews: mainViews)
        mainStackView.translatesAutoresizingMaskIntoConstraints = false
        mainStackView.axis = .vertical
        addSubview(mainStackView)

        NSLayoutConstraint.activate([
            mainStackView.topAnchor.constraint(equalTo: topAnchor),
            mainStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            mainStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            mainStackView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
}
