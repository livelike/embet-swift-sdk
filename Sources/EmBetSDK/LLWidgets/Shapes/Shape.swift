//
//  Shape.swift
//
//
//  Created by Ljupco Nastevski on 13.2.23.
//

import UIKit

internal struct ShapeShadow {
    let color: CGColor
    let opacity: Float
    let offset: CGSize
    let radius: CGFloat

    static func defaultShadow() -> ShapeShadow {
        return ShapeShadow(color: UIColor.black.cgColor,
                           opacity: 1,
                           offset: .zero,
                           radius: 10)
    }
}

internal class Shape: UIView {
    /**
        The fill color of the shape
     */
    var fillColor: UIColor = .white {
        didSet { setNeedsDisplay() }
    }

    /**
        The shadow of the shape
     */
    var shadow: ShapeShadow = .defaultShadow() {
        didSet { setNeedsDisplay() }
    }

    override init(frame _: CGRect) {
        super.init(frame: .zero)
        backgroundColor = .clear
        setupShadow()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupShadow() {
        layer.shadowColor = shadow.color
        layer.shadowOpacity = shadow.opacity
        layer.shadowOffset = shadow.offset
        layer.shadowRadius = shadow.radius
    }
}

class CircleView: Shape {
    override func draw(_ rect: CGRect) {
        fillColor.set()
        let graphic = UIBezierPath(arcCenter: CGPoint(x: rect.midX, y: rect.midY),
                                   radius: rect.width / 2,
                                   startAngle: 0,
                                   endAngle: 2 * Double.pi, clockwise: false)
        graphic.fill()
        graphic.stroke()
    }
}

class SquareView: Shape {
    override func draw(_ rect: CGRect) {
        fillColor.set()
        let graphic = UIBezierPath()
        graphic.move(to: .zero)
        graphic.addLine(to: CGPoint(x: rect.maxX, y: 0))
        graphic.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        graphic.addLine(to: CGPoint(x: 0, y: rect.maxY))
        graphic.move(to: .zero)

        graphic.fill()
        graphic.stroke()
    }
}
