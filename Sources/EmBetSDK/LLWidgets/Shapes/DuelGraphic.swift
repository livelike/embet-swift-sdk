//
//  DuelGraphic.swift
//
//
//  Created by Ljupco Nastevski on 13.2.23.
//

import UIKit

/**
    Position of the drawn shape.
 */
internal enum DuelGrapicPosition {
    case left
    case right
}

internal class DuelGraphic: UIView {
    let teamName: String
    let position: DuelGrapicPosition

    lazy var vsLabel: UILabel = {
        let vsLabel = UILabel()
        vsLabel.setContentHuggingPriority(.defaultLow, for: .vertical)
        vsLabel.text = teamName.uppercased()
        vsLabel.textColor = .black
        vsLabel.textAlignment = position == .left ? .left : .right
        vsLabel.font = .boldSystemFont(ofSize: 12)
        vsLabel.heightAnchor.constraint(equalToConstant: 33).isActive = true
        return vsLabel
    }()

    required init(teamName: String, position: DuelGrapicPosition) {
        self.teamName = teamName
        self.position = position
        super.init(frame: .zero)
        setupSelf()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupSelf() {
        let stackView = position == .left ?UIStackView(arrangedSubviews: [vsLabel]) : UIStackView(arrangedSubviews: [vsLabel])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.distribution = .fill
        stackView.spacing = 8
        stackView.axis = .horizontal

        let animationView = HolderAnimator()
        animationView.position = position
        animationView.translatesAutoresizingMaskIntoConstraints = false
        animationView.addSubview(stackView)
        addSubview(animationView)

        NSLayoutConstraint.activate([
            animationView.widthAnchor.constraint(equalTo: widthAnchor),
            animationView.heightAnchor.constraint(equalTo: heightAnchor),
            animationView.centerYAnchor.constraint(equalTo: centerYAnchor),
            animationView.centerXAnchor.constraint(equalTo: centerXAnchor),
        ])

        backgroundColor = .clear
        NSLayoutConstraint.activate([
            stackView.centerXAnchor.constraint(equalTo: animationView.centerXAnchor),
            stackView.centerYAnchor.constraint(equalTo: animationView.centerYAnchor),
            stackView.leadingAnchor.constraint(equalTo: animationView.leadingAnchor, constant: 8),
            stackView.trailingAnchor.constraint(lessThanOrEqualTo: animationView.trailingAnchor, constant: 8),
            stackView.topAnchor.constraint(greaterThanOrEqualTo: animationView.topAnchor),
            stackView.bottomAnchor.constraint(lessThanOrEqualTo: animationView.bottomAnchor),
        ])
    }
}

internal class HolderAnimator: UIView {
    var position: DuelGrapicPosition = .left

    override init(frame _: CGRect) {
        super.init(frame: .zero)
        backgroundColor = .clear
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func draw(_ rect: CGRect) {
        let verticalPadding = 2.0
        let widthPercent: CGFloat = rect.size.width / 100

        UIColor.white.set()

        let graphic = UIBezierPath()

        if position == .left {
            graphic.move(to: CGPoint(x: 0, y: verticalPadding))
            // Add 10% padding on top right side
            graphic.addLine(to: CGPoint(x: rect.width - (10 * widthPercent), y: 0 + verticalPadding))
            graphic.addLine(to: CGPoint(x: rect.width, y: rect.height - verticalPadding))
            graphic.addLine(to: CGPoint(x: 0, y: rect.height - verticalPadding))
            graphic.move(to: CGPoint(x: 0, y: verticalPadding))
        } else if position == .right {
            graphic.move(to: CGPoint(x: rect.width, y: verticalPadding))
            graphic.addLine(to: CGPoint(x: rect.width, y: rect.height - verticalPadding))
            graphic.addLine(to: CGPoint(x: 0, y: rect.height - verticalPadding))
            graphic.addLine(to: CGPoint(x: 10 * widthPercent, y: verticalPadding))
            graphic.addLine(to: CGPoint(x: rect.width, y: verticalPadding))
        }

        graphic.fill()
        graphic.stroke()
    }
}
