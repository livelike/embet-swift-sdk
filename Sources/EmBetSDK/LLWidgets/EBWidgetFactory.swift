//
//  EBWidgetFactory.swift
//
//
//  Created by Ljupco Nastevski on 3.8.22.
//

import Foundation
import LiveLikeSwift

/**
    Widget factory.
 */
internal enum EBWidgetFactory {
    enum Error: LocalizedError {
        case failedToBuildUI
    }

    /**
        Creates an emBet widget from the given model.
     */
    static func defaultWidgetFor(widgetModel: EmBetWidgetModel, eventManger: EmBetSDKInternalEventManagerProtocol? = nil) -> EmBetWidget? {
        guard let engagementModel = widgetModel as? WidgetModel else { return nil }

        var emBetWidget: LLBaseWidget?

        switch widgetModel.widgetKind {
        case .liveAtBat, .liveStatsOdds, .liveSpread, .liveMoneyline, .liveTotal:

            switch engagementModel {
            case let .prediction(predictionWidgetModel):
                emBetWidget = LLBetWidgetViewController(safelyWith: predictionWidgetModel)
            default:
                break
            }

        case .alert:

            switch engagementModel {
            case let .alert(alertWidgetModel):
                emBetWidget = LLAlertWidgetViewController(safelyWith: alertWidgetModel)
            default:
                break
            }

        case .odds:

            switch engagementModel {
            case let .prediction(predictionWidgetModel):
                emBetWidget = LLOddsWidgetViewController(safelyWith: predictionWidgetModel)
            default:
                break
            }

        case .quiz:

            switch engagementModel {
            case let .quiz(quizWidgetModel):
                if !quizWidgetModel.containsImages {
                    emBetWidget = LLQuizWidgetViewController(safelyWith: quizWidgetModel)
                }
            default:
                break
            }

        case .advertisement:

            switch engagementModel {
            case let .alert(alertWidgetModel):
                emBetWidget = LLAdvertisementWidget(safelyWith: alertWidgetModel)
            default:
                break
            }

        case .prediction:

            switch engagementModel {
            case let .prediction(predictionWidgetModel):
                emBetWidget = LLPredictionWidgetViewController(safelyWith: predictionWidgetModel)
            default:
                break
            }

        case .predictionFollowUp:

            switch engagementModel {
            case let .predictionFollowUp(predictionFollowUpModel):
                // Follow up should be shown only to the user that has voted on the prediction
                if predictionFollowUpModel.mostRecentVote != nil {
                    emBetWidget = LLPredictionFollowUpWidgetViewController(safelyWith: predictionFollowUpModel)
                }
            default:
                break
            }

        case .unsupported:
            break
        }

        emBetWidget?.eventManger = eventManger
        return emBetWidget
    }
}
