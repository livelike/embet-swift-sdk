//
//  EmBetAttribute.swift
//
//
//  Created by Ljupco Nastevski on 23.1.24.
//

import LiveLikeSwift

/**
    EmBet widget attributes, object containing a string- key and string- value.
 */
internal typealias EmBetWidgetAttribute = Attribute

internal enum EmBetWidgetAttributeKeys {
    static let isSuspended = "isSuspended"
    static let isHidden = "isHidden"
    static let isPersistent = "isPersistent"
}
