//
//  File.swift
//
//
//  Created by Ljupco Nastevski on 22.6.22.
//
import UIKit

internal enum FooterType {
    case horizontal
    case vertical
}

internal class LLWidgetFooter: UIStackView {
    /**
        Theme builder for applying custom theme inside.
     */
    let themeBuilder: LLThemeBuilder

    /**
        Custom data for seting up footer from the widget data
     */
    let customData: LLBaseWidgetPayload

    /**
        Button interaction action
     */
    public var interactionAction: (() -> Void)?

    required init?(customData: LLBaseWidgetPayload, themeBuilder: LLThemeBuilder) {
        // If it has no content return nil
        if customData.sponsors == nil, customData.placeBetURL == nil || customData.placeBetLabel == nil, customData.legalLogoURL == nil {
            return nil
        }

        self.customData = customData
        self.themeBuilder = themeBuilder
        super.init(frame: .zero)
        axis = .vertical
        initialSetup()
    }

    @available(*, unavailable)
    required init(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func hasContent() -> Bool {
        return true
    }

    private func initialSetup() {
        let contentStackView = UIStackView()
        contentStackView.spacing = LLUIConstants.FooterElementsSpacing
        contentStackView.distribution = .fill
        contentStackView.alignment = .bottom
        contentStackView.axis = .horizontal

        addArrangedSubview(contentStackView)
        spacing = LLUIConstants.FooterElementsSpacing + 2
        axis = .vertical
        alignment = .center

        // Sponsor section
        if let sponsorView = getSponsorView() {
            contentStackView.addArrangedSubview(sponsorView)
        }

        // Place Bet button
        if let buttonView = getButtonView() {
            contentStackView.addArrangedSubview(buttonView)
        }

        if let solo = contentStackView.subviews.first, contentStackView.subviews.count == 1 {
            // Single element in horizontal content axis should be 50% of width
            solo.widthAnchor.constraint(equalTo: widthAnchor, multiplier: LLUIConstants.SingleFooterTopElementRatio).isActive = true
        } else if contentStackView.subviews.count == 2 {
            // if button and sponsor should be shown
            let first = contentStackView.subviews[0]
            let second = contentStackView.subviews[1]
            first.widthAnchor.constraint(equalTo: second.widthAnchor).isActive = true

            contentStackView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        }

        if let legalLogoURL = customData.legalLogoURL {
            let legalImgView = LLAsyncImageView()
            legalImgView.translatesAutoresizingMaskIntoConstraints = false
            legalImgView.loadAsyncImage(url: legalLogoURL)

            addArrangedSubview(legalImgView)
            legalImgView.heightAnchor.constraint(equalToConstant: LLUIConstants.LegalAdviceImageViewHeight).isActive = true
            legalImgView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: LLUIConstants.LegalAdviceImageEmbededWidthRatio).isActive = true
        }
    }

    private func getSponsorView() -> UIView? {
        if let sponsor = customData.sponsors?.first {
            let sponsoredByLbl = LLThemableLabel(themeType: .sponsorText)
            sponsoredByLbl.textAlignment = .center
            sponsoredByLbl.numberOfLines = 0
            sponsoredByLbl.text = customData.labels?.sponsoredBy
            let embededSponsoredBy = themeBuilder.embedInHolder(themeType: .sponsorText, view: sponsoredByLbl)

            let sponsorLogoImgView = LLAsyncImageView()
            sponsorLogoImgView.translatesAutoresizingMaskIntoConstraints = false
            sponsorLogoImgView.loadAsyncImage(string: sponsor.logoUrl)

            let sponsor = LLThemableView(themeType: .sponsor)
            let sponsorImageViewHolder = themeBuilder.embedInMarginView(themeType: .sponsor, view: sponsor)
            sponsorImageViewHolder.translatesAutoresizingMaskIntoConstraints = false
            sponsor.addSubview(sponsorLogoImgView)

            let sponsorStackView = UIStackView(arrangedSubviews: [embededSponsoredBy, sponsorImageViewHolder])
            sponsorStackView.translatesAutoresizingMaskIntoConstraints = false
            sponsorStackView.distribution = .fill
            sponsorStackView.spacing = LLUIConstants.FooterElementsSpacing
            sponsorStackView.alignment = .fill
            sponsorStackView.axis = .vertical

            NSLayoutConstraint.activate([
                sponsorLogoImgView.topAnchor.constraint(equalTo: sponsor.layoutMarginsGuide.topAnchor),
                sponsorLogoImgView.bottomAnchor.constraint(equalTo: sponsor.layoutMarginsGuide.bottomAnchor),
                sponsorLogoImgView.leadingAnchor.constraint(equalTo: sponsor.layoutMarginsGuide.leadingAnchor),
                sponsorLogoImgView.trailingAnchor.constraint(equalTo: sponsor.layoutMarginsGuide.trailingAnchor),

                sponsorImageViewHolder.heightAnchor.constraint(equalToConstant: LLUIConstants.SponsorImageHeight),

            ])

            return sponsorStackView
        }

        return nil
    }

    /**
        CTA button view
         - Returns: CTA button as view, if there should be any. *Always* returns nil if the target is `tvOS`
     */
    private func getButtonView() -> UIView? {
        // Place bet button should only be shown on iOS
        #if os(tvOS)
            return nil
        #else
            if let placeBetLabel = customData.placeBetLabel,
               customData.placeBetURL != nil
            {
                let betButton = LLThemableButton(themeType: .betButton)
                betButton.setTitle(placeBetLabel, for: .normal)
                betButton.addTarget(self, action: #selector(buttonPressed), for: .primaryActionTriggered)

                return themeBuilder.embedInMarginView(themeType: .betButton, view: betButton)
            }

            return nil
        #endif
    }

    @objc func buttonPressed() {
        interactionAction?()
    }
}
