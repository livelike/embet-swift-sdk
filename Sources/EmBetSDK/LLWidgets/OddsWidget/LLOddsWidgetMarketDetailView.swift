//
//  LLOddsWidgetMarketDetailView.swift
//
//
//  Created by Ljupco Nastevski on 27.8.24.
//

import UIKit

@available(tvOS 15, *)
internal class LLOddsWidgetMarketDetailView: LLThemableView {
    /**
        Adds QR Code View.
     */
    private let qrCodeView = LLQRView()

    /**
        Label below the QR code view that gives information about code.
     */
    private let qrLabel = LLThemableLabel(themeType: .qrLabel)
    /**
        The market view reference.
     */
    weak var marketView: LLVerticalOddsTableCell?
    /**
        The selection object.
     */
    var selection: LLOddsSelection

    /**
        The selection object.
     */
    let contentView: UIView = .init(frame: .zero)

    /**
        QR code content view.
     */
    private lazy var qrStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [qrCodeView, qrLabel])
        stackView.spacing = 8.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.distribution = .equalCentering
        stackView.axis = .vertical
        return stackView
    }()

    required init(selection: LLOddsSelection) {
        self.selection = selection
        super.init(themeType: .root)
        setupSelf()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupSelf() {
        contentView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(contentView)

        qrLabel.text = "Scan to place bet"
        qrLabel.textAlignment = .center

        qrStackView.translatesAutoresizingMaskIntoConstraints = false
        qrCodeView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(qrStackView)

        let detail = LLOddsBetDetailsDiffModel(marketName: selection.market.marketName, marketID: selection.market.marketID, marketPresentation: selection.market.marketPresentation, currentOptions: [selection.option], previousOptions: nil)

        let marketView = LLVerticalOddsTableCell(betDetails: detail,
                                                 selectionCoordinator: nil)
        marketView.separatorView.isHidden = true
        marketView.isUserInteractionEnabled = false
        marketView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(marketView)
        self.marketView = marketView

        NSLayoutConstraint.activate([
            contentView.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: layoutMarginsGuide.trailingAnchor),
            contentView.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor),
            contentView.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor),

            marketView.leadingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor),
            marketView.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor),
            marketView.bottomAnchor.constraint(equalTo: contentView.layoutMarginsGuide.bottomAnchor),

            qrStackView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            qrStackView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            qrStackView.widthAnchor.constraint(lessThanOrEqualTo: contentView.widthAnchor),
            qrStackView.widthAnchor.constraint(greaterThanOrEqualTo: contentView.widthAnchor, multiplier: 0.8),

            qrCodeView.widthAnchor.constraint(equalTo: qrCodeView.heightAnchor),
        ])

        updateQRCode()
    }

    /**
        Updates the QR code view
     */
    private func updateQRCode() {
        DispatchQueue.main.async { [weak self] in
            // Makes sure the view is layed out correctly
            // Before doing calculations for the size of the QR code
            self?.setNeedsLayout()
            self?.layoutIfNeeded()
            self?.qrCodeView.generateCode(string: self?.selection.option.url?.absoluteString)
        }
    }

    /**
        Sets the QR label text
     */
    public func setQRLabelText(text: String?) {
        qrLabel.text = text
    }

    /**
        Sets bet details for the market view
     */
    public func setBetDetails(betDetails: [LLOddsBetDetailsModel]?) {
        if let market = betDetails?.first(where: { $0.marketID == marketView?.getMarketID() }),
           let selection = market.marketOptions.first(where: { $0.outcomeID == self.selection.option.outcomeID })
        {
            let prevoiusMarketModel = LLMarketOptionModel(imageURL: self.selection.option.imageURL, description: self.selection.option.description, odds: self.selection.option.odds, outcomeID: self.selection.option.outcomeID, url: self.selection.option.url, state: self.selection.option.state)

            let detail = LLOddsBetDetailsDiffModel(marketName: market.marketName,
                                                   marketID: market.marketID,
                                                   marketPresentation: market.marketPresentation,
                                                   currentOptions: [selection],
                                                   previousOptions: [prevoiusMarketModel])

            // Updates the selection
            self.selection = LLOddsSelection(option: selection, market: market, uniqueID: self.selection.uniqueID)

            DispatchQueue.main.async { [weak self] in
                self?.marketView?.setModel(model: detail)
                self?.qrCodeView.generateCode(string: self?.selection.option.url?.absoluteString)
            }
        }
    }

    public func setForceSuspension(isSuspended: Bool, completion: @escaping () -> Void) {
        marketView?.forceSuspension = isSuspended

        let detail = LLOddsBetDetailsDiffModel(marketName: selection.market.marketName, marketID: selection.market.marketID, marketPresentation: selection.market.marketPresentation, currentOptions: [selection.option], previousOptions: nil)

        DispatchQueue.main.async { [weak self] in
            self?.marketView?.setModel(model: detail)
            completion()
        }
    }
}
