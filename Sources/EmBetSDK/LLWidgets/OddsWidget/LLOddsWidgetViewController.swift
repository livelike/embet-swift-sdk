//
//  LLOddsWidgetViewController.swift
//
//
//  Created by Ljupco Nastevski on 7.11.23.
//

import LiveLikeSwift
import UIKit

internal class LLOddsWidgetViewController: LLWidget {
    var oddsWidgetCustomData: LLOddsWidgetPayload? {
        return (customDataPayload as? LLOddsWidgetPayload)
    }

    /**
        Pointer to the odds tableview
     */
    private weak var oddsBodyView: LLOddsBodyView?

    // MARK: - View controller

    let model: PredictionWidgetModel

    override func moveToNextState() {
        switch currentState {
        case .ready:
            currentState = .interacting
        case .interacting:
            currentState = .finished
        case .results:
            break
        case .finished:
            break
        }
    }

    override var currentState: WidgetState {
        willSet {
            previousState = currentState
        }
        didSet {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.delegate?.widgetDidEnterState(widget: self, state: self.currentState)
                switch self.currentState {
                case .interacting:
                    break
                case .finished:
                    break
                default:
                    break
                }
            }
        }
    }

    #if os(tvOS)
        /**
            Ref to details view
         */
        private weak var detailsView: LLOddsWidgetMarketDetailView? {
            didSet {
                goBackButton?.isHidden = detailsView == nil
            }
        }

        /**
            Stores the last focused view
         */
        weak var viewToFocus: UIView?
        /**
            Ref to the go back button
         */
        weak var goBackButton: UIView?
    #endif
    override required init?(safelyWith model: PredictionWidgetModel) {
        self.model = model
        super.init(safelyWith: model)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        model.delegate = self
        model.registerImpression()
        model.markAsInteractive()
        // Update with final result if present
        setFinalResult(finalResult: oddsWidgetCustomData?.finalResult)
    }

    /**
        Sets the bet details for the widget
     */
    func setBetDetails(betDetails: [LLOddsBetDetailsModel]) {
        oddsBodyView?.setBetDetails(betDetails: betDetails)
        #if os(tvOS)

            if let detailsView = detailsView {
                detailsView.setBetDetails(betDetails: betDetails)
            }
        #endif
    }

    func setFinalResult(finalResult: LLFinalResultModel?) {
        oddsBodyView?.setFinalResult(finalResult: finalResult)
    }

    override func setupSuspendedState(isSuspended: Bool) {
        super.setupSuspendedState(isSuspended: isSuspended)
        oddsBodyView?.setupSuspendedState(isSuspended: isSuspended)
        #if os(tvOS)
            detailsView?.setForceSuspension(isSuspended: isSuspended, completion: { [weak self] in
                if let detailsView = self?.detailsView {
                    self?.themeBuilder.applyThemeTo(superview: detailsView)
                }
            })
        #endif
    }

    /**
        Prepares the widget and sets the closing odds
     */
    func setupLabels(labels _: LLWidgetLabels, teams _: LLFinalResultTeamsModel) {}

    @objc
    private func ctaButtonPressed() {
        // Extract selection and convert to array with single element
        let selectionPayload = convertSelectionToEventPayload(selection: oddsBodyView?.activeSelection)

        if let oddsBodyView = oddsBodyView,
           let selection = oddsBodyView.getSelectedMarketOption()
        {
            let payload = EmBetCTAEvent(url: selection.url, selectedOptionID: selection.outcomeID)
            submitPressed(payload: payload, eventPaylaod: selectionPayload)
            return
        }
        // Submit payload
        if let placeBetUrl = oddsWidgetCustomData?.placeBetURL {
            let payload = EmBetCTAEvent(url: placeBetUrl, selectedOptionID: nil)
            submitPressed(payload: payload)
        }
    }

    #if os(tvOS)
        override var preferredFocusEnvironments: [UIFocusEnvironment] {
            if let detailsView = detailsView {
                return [detailsView]
            }
            if let viewToFocus = viewToFocus {
                return [viewToFocus]
            } else {
                return super.preferredFocusEnvironments
            }
        }

        override func pressesBegan(_ presses: Set<UIPress>, with event: UIPressesEvent?) {
            guard let buttonPress = presses.first?.type else { return }

            switch buttonPress {
            case .menu:
                closeDetailsView()
            default:
                super.pressesBegan(presses, with: event)
            }
        }

        @objc
        private func closeDetailsView() {
            viewToFocus = oddsBodyView?.selectionCoordinator.getSelected
            oddsBodyView?.forceDeselect()
            detailsView?.removeFromSuperview()
            detailsView = nil
            setNeedsFocusUpdate()
        }

    #endif
}

extension LLOddsWidgetViewController {
    func widgetBuilderHeaderContentView() -> UIView? {
        // Holder for the imageview and button
        let headerTopStackView = UIStackView()
        headerTopStackView.translatesAutoresizingMaskIntoConstraints = false
        headerTopStackView.heightAnchor.constraint(equalToConstant: 44.0).isActive = true
        headerTopStackView.axis = .horizontal
        headerTopStackView.distribution = .fillEqually
        headerTopStackView.alignment = .trailing

        // Sportsbook imageview
        if let sportsbookLogo = oddsWidgetCustomData?.sportsbookLogoURL {
            let sportsbookImageView = LLAsyncImageView()
            sportsbookImageView.translatesAutoresizingMaskIntoConstraints = false
            sportsbookImageView.loadAsyncImage(url: sportsbookLogo)
            headerTopStackView.addArrangedSubview(sportsbookImageView)
            sportsbookImageView.widthAnchor.constraint(equalTo: headerTopStackView.widthAnchor, multiplier: 0.33).isActive = true
        }

        // Creates space between sportsbook and place bet button
        let spacerView = UIView()
        spacerView.setContentHuggingPriority(.defaultLow, for: .horizontal)
        headerTopStackView.addArrangedSubview(spacerView)

        // Place bet button should only be shown on iOS
        #if os(iOS)
            // Place bet button
            if let placeBetLabel = oddsWidgetCustomData?.placeBetLabel,
               oddsWidgetCustomData?.placeBetURL != nil
            {
                let placeBetButton = LLThemableButton(themeType: .betButton)
                placeBetButton.setTitle(placeBetLabel, for: .normal)
                placeBetButton.addTarget(self, action: #selector(ctaButtonPressed), for: .touchUpInside)
                let embededPlaceBetView = themeBuilder.embedInMarginView(themeType: .betButton, view: placeBetButton)
                headerTopStackView.addArrangedSubview(embededPlaceBetView)
                embededPlaceBetView.widthAnchor.constraint(equalTo: headerTopStackView.widthAnchor, multiplier: 0.33).isActive = true
            }
        #elseif os(tvOS)
            /**
                tvOS has a static button at top that is only visible when the details overlay is shown
             */
            let goBackButton = LLThemableButton(themeType: .backButton)
            goBackButton.isHidden = true
            goBackButton.setTitle("Go Back", for: .normal)
            goBackButton.addTarget(self, action: #selector(closeDetailsView), for: .primaryActionTriggered)
            let embededPlaceBetView = themeBuilder.embedInMarginView(themeType: .backButton, view: goBackButton)
            headerTopStackView.addArrangedSubview(embededPlaceBetView)
            self.goBackButton = goBackButton
        #endif

        // Header stack view
        let headerStackView = UIStackView()
        headerStackView.axis = .vertical
        headerStackView.spacing = 8

        if headerTopStackView.arrangedSubviews.count != 1 {
            headerStackView.addArrangedSubview(headerTopStackView)
        }

        return headerStackView.arrangedSubviews.count != 0 ? headerStackView : nil
    }

    func widgetBuilderBodyContentView() -> UIView? {
        let bodyView = LLOddsBodyView(themeBuilder: themeBuilder,
                                      teams: oddsWidgetCustomData?.teams,
                                      widgetLabels: oddsWidgetCustomData?.labels,
                                      placeBetURL: oddsWidgetCustomData?.placeBetURL)
        bodyView.setBetDetails(betDetails: oddsWidgetCustomData?.betDetails)
        bodyView.setContentHuggingPriority(.defaultLow, for: .vertical)
        bodyView.delegate = self
        oddsBodyView = bodyView
        return bodyView
    }

    func widgetBuilderFooterContentView() -> UIView? { return nil }

    func widgetBuilderWillSetupWidgetView() {
        LLBaseWidgetProperties.applyDefaultOddsWidgetProperties(to: &themeBuilder.themeModel)
    }

    func widgetBuilderPrepareWidgetForTimeline() {
        currentState = .finished
        displayType = .forceHideCloseButton
    }

    func widgetBuilderIsTimerViewHidden() -> Bool {
        displayType = .forceHideCloseButton
        return true
    }

    #if os(iOS)
        func widgetBuilderDidSetupWidgetView(animatedView _: UIView?) {
            guard let body = body else { return }

            let separator = LLThemableSeparator(themeType: .separator)
            separator.translatesAutoresizingMaskIntoConstraints = false

            body.addSubview(separator)

            NSLayoutConstraint.activate([
                separator.leadingAnchor.constraint(equalTo: body.leadingAnchor),
                separator.trailingAnchor.constraint(equalTo: body.trailingAnchor),
                separator.bottomAnchor.constraint(equalTo: body.bottomAnchor),
                separator.heightAnchor.constraint(equalToConstant: 1.0),
            ])
        }
    #endif
    private func convertSelectionToEventPayload(selection: LLOddsSelection?) -> [String: Any] {
        guard let selection = selection else { return [:] }

        var teamName: String?
        if selection.market.marketOptions.first?.outcomeID == selection.option.outcomeID {
            teamName = oddsWidgetCustomData?.teams?.first?.description
        } else {
            teamName = oddsWidgetCustomData?.teams?.last?.description
        }

        // Prepare the option payload
        let payload: [String: Any?] = [
            EmBetWidgetOptionEventKeys.Market: selection.market.marketName,
            EmBetWidgetOptionEventKeys.Team: teamName,
            EmBetWidgetOptionEventKeys.Description: selection.option.description,
            EmBetWidgetOptionEventKeys.Odds: selection.option.odds,
            EmBetWidgetOptionEventKeys.OptionID: selection.uniqueID,
            EmBetWidgetOptionEventKeys.OutcomeID: selection.option.outcomeID,
        ]

        return payload.compactMapValues { $0 }
    }
}

extension LLOddsWidgetViewController: PredictionWidgetModelDelegate {
    func predictionWidgetModel(_: PredictionWidgetModel, voteCountDidChange _: Int, forOption _: String) {}
    func predictionWidgetModel(_: PredictionWidgetModel, didReceiveFollowUp _: String, kind _: WidgetKind) {}
}

extension LLOddsWidgetViewController: LLOddsBodyViewSelectionDelegate {
    func oddsTableViewDidSelect(selection: LLOddsSelection) {
        // Vote on the first selected option,
        // If no votes have been done before.
        // This is required for analytics
        if let firstOptionID = model.options.first?.id, model.mostRecentVote == nil {
            model.submitVote(optionID: firstOptionID) { _ in }
        }

        let payload = convertSelectionToEventPayload(selection: selection)
        super.addOptionSelectedEvent(payload: payload)
        #if os(tvOS)

            let betDetails = LLOddsWidgetMarketDetailView(selection: selection)
            betDetails.translatesAutoresizingMaskIntoConstraints = false

            if let message = oddsWidgetCustomData?.labels?.qrBetMessage {
                betDetails.setQRLabelText(text: message)
            }

            themeBuilder.applyThemeTo(superview: betDetails)

            // Offset should be the disclaimer height if a disclaimer is present
            var betDetailsViewBottomOffset: CGFloat = 0.0

            if let complementaryFooter = complementaryFooter {
                betDetailsViewBottomOffset = -complementaryFooter.bounds.height
                view.insertSubview(betDetails, at: 1)
            } else {
                view.addSubview(betDetails)
            }

            // Top TO constraint for the details controller
            let topAnchor = header?.bottomAnchor ?? view.topAnchor

            NSLayoutConstraint.activate([
                betDetails.topAnchor.constraint(equalTo: topAnchor),
                betDetails.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor, constant: betDetailsViewBottomOffset),
                betDetails.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
                betDetails.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor),
            ])
            detailsView = betDetails
            viewToFocus = goBackButton
            setNeedsFocusUpdate()

        #endif
    }

    func oddsTableViewDidDeselect(selection: LLOddsSelection) {
        let payload = convertSelectionToEventPayload(selection: selection)
        #if os(tvOS)
            super.addQRViewDismissedEvent(payload: payload)
        #else
            super.addOptionDeselectedEvent(payload: payload)
        #endif
    }
}
