//
//  EBAccessTokenStorage.swift
//
//
//  Created by Ljupco Nastevski on 26.7.22.
//

import Foundation

/**
    Basic implementation of the AccessTokenStoring protocol.
 */
@objcMembers
public class EBDefaultTokenStorage: NSObject, EBAccessTokenStorage {
    private let defaultAccessTokenKey = "com.embet.EmBetSDK:AccessToken"

    public func fetchAccessToken() -> String? {
        return UserDefaults.standard.string(forKey: defaultAccessTokenKey)
    }

    public func storeAccessToken(accessToken: String) {
        UserDefaults.standard.set(accessToken, forKey: defaultAccessTokenKey)
    }

    public func removeAccessToken() {
        UserDefaults.standard.removeObject(forKey: defaultAccessTokenKey)
    }
}
