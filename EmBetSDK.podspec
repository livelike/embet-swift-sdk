Pod::Spec.new do |spec|

  spec.name             = 'EmBetSDK'
  spec.version          = '0.10.0'
  spec.summary          = 'emBet iOS SDK'
  spec.description      = 'Official emBet iOS SDK'
  spec.homepage         = 'https://embet.readme.io/'
  spec.license          = { :type => 'MIT', :file => 'LICENSE.md' }
  spec.author           = { 'emBet' => 'ljupcho@livelike.com' }
  spec.source           = { :git => 'https://bitbucket.org/livelike/embet-swift-sdk.git', :tag =>  spec.version.to_s}
  
  spec.ios.deployment_target  = '13.0'
  spec.tvos.deployment_target  = '15.0'

  spec.swift_version = '5.0'
  
  spec.source_files = 'Sources/EmBetSDK/**/*.{swift}'
  spec.resource_bundles = {
    'EmBetSDK_EmBetSDK' => [
       'Sources/EmBetSDK/Resources/**/*'
    ]
  }

  spec.pod_target_xcconfig = { 'PRODUCT_BUNDLE_IDENTIFIER': 'com.livelike.embet' }
  
  spec.dependency 'LiveLikeSwift', '2.98'
  spec.dependency 'jsonlogic', '~> 1'
  
  spec.ios.framework  = 'UIKit'
  spec.ios.framework  = 'Foundation'

end
