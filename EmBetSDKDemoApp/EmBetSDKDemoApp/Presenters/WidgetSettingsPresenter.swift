//
//  WidgetSettingsPresenter.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 13.1.23.
//

import UIKit

protocol WidgetSettingsPresenterDelegate {
    var widget: LocalWidgetModel { get }
}

class WidgetSettingsPresenter: SettingsBasePresenter, WidgetSettingsPresenterDelegate {

    let widget = LocalWidgetModel()

    let settingsItems: [SettingsSection] = SettingsFactory.generateWidgetCreationSettings()

    private lazy var initialItems = Array(settingsItems.prefix(3))

    // List of selected indexPaths
    private var selectedIndexPaths: [IndexPath] = []

    required init(viewController: SettingsViewController) {
        super.init(viewController: viewController)
    }

    override func setUp() {
        guard let viewController = viewController else { return }
        viewController.tableView.delegate = self
        viewController.tableView.allowsMultipleSelection = true
        settingsDataSource = SettingsTableViewDataSource(
            settingsItems: initialItems,
            tableView: viewController.tableView,
            dataSource: self
        )

        // Make Initial Theme Selection
        let initialSelection = IndexPath(row: 0, section: 1)
        viewController.tableView.selectRow(at: initialSelection, animated: false, scrollPosition: .none)
        self.tableView(viewController.tableView, didSelectRowAt: initialSelection)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        self.setSectionSelectionForIndex(indexPath: indexPath)

        if indexPath.section == 0 {
            guard let widgetPayload = settingsItems[0].rows[indexPath.row].associatedObject as? LocalWidgetFile else { return }
            widget.widget = widgetPayload
            viewController?.viewControllerShouldPrepareAsReady()
            if indexPath.row <= 3, tableView.numberOfSections == 3 {
                settingsDataSource.settingsItems = settingsItems
                viewController?.viewControllerShouldAddSections(sectionIndex: IndexSet(integersIn: 3 ... 3))
            } else if indexPath.row > 3, tableView.numberOfSections == 4 {
                settingsDataSource.settingsItems = initialItems
                viewController?.viewControllerShouldDeleteSections(sectionIndex: IndexSet(integersIn: 3 ... 3))
            }
        } else if indexPath.section == 1 {
            if let customData = settingsItems[indexPath.section].rows[indexPath.row].associatedObject as? LocalWidgetCustomData {
                widget.customData = customData
            }
        }
    }


    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        // Dissable selection on already selected index and on toggable cells
        if indexPath.section > 1 || selectedIndexPaths.contains(where: {$0 == indexPath}) == true { return false }
        return true
    }

    private func setSectionSelectionForIndex(indexPath: IndexPath) {
        if let hit = selectedIndexPaths.firstIndex(where: {$0.section == indexPath.section}) {
            viewController?.tableView.deselectRow(at: selectedIndexPaths[hit], animated: false)
            selectedIndexPaths.remove(at: hit)
        }
        selectedIndexPaths.append(indexPath)
    }
}

/**
    SettingsDataSource
 */
extension WidgetSettingsPresenter: SettingsDataSource {

    func setObjectForKey(key: SettingsDataSourceKeys, data: Any) {
        switch key {
        case .creteAWidgetKeys(let createWidgetKey):
            switch createWidgetKey {
            case .visibleSponsor:
                guard let boolValue = data as? Bool else { return }
                widget.hasSponsor = boolValue
            case .visibleLegalLogo:
                guard let boolValue = data as? Bool else { return }
                widget.hasLegalLogo = boolValue
            case .visibleCTAButton:
                guard let boolValue = data as? Bool else { return }
                widget.hasCTA = boolValue
            case .isInsight:
                guard let boolValue = data as? Bool else { return }
                widget.isInsight = boolValue
            case .hasDisclaimer:
                guard let boolValue = data as? Bool else { return }
                widget.hasDisclaimer = boolValue
            default:
                return
            }
        default:
            return
        }
    }

    func boolForKey(key: SettingsDataSourceKeys) -> Bool {
        // All options are active by default
        switch key {
        case .creteAWidgetKeys(let createWidgetKey):
            switch createWidgetKey {
            case .visibleCTAButton:
                return widget.hasCTA
            case .visibleLegalLogo:
                return widget.hasLegalLogo
            case .visibleSponsor:
                return widget.hasSponsor
            case .isInsight:
                return widget.isInsight
            case .hasDisclaimer:
                return widget.hasDisclaimer
            default:
                return false
            }
        default:
            return false
        }
    }

    func dataForKey(key: SettingsDataSourceKeys) -> String? {
        return nil
    }
}
