//
//  SegmentationSettingsPresenter.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 13.1.23.
//

import UIKit
import EmBetSDK

class SegmentationSettingsPresenter: SettingsBasePresenter {

    var userProfile: UserProfile = UserProfile()

    var remoteDataLoadingViewController: AsyncDataViewController? {
        return viewController as? AsyncDataViewController ?? nil
    }

    required init(viewController: SettingsViewController) {
        super.init(viewController: viewController)
    }

    override func setUp() {
        guard let viewController = viewController else { return }
        viewController.tableView.allowsSelection = false
        viewController.tableView.delegate = self
        settingsDataSource = SettingsTableViewDataSource(
            settingsItems: SettingsFactory.generateSegmentationSettings(),
            tableView: viewController.tableView,
            dataSource: self
        )
        self.getData()
    }

    override func presenterShouldUpdateDataPressed() {
        self.updateProfileData()
    }

    func updateProfileData() {
        viewController?.view.endEditing(true)
        if let encoded = try? JSONEncoder().encode(userProfile) {
            guard let json = try? JSONSerialization.jsonObject(with: encoded, options: []) as? [String: Any] else { return }
            guard let clientID = EmBetConfigurationManger.shared.clientID else { return }
            let sdk = EmBetSDK(config: EmBetSDKConfiguration(clientID: clientID))
            sdk.profile.updateProfileData(data: json) { (_, error) in

                guard let error = error else {
                    ToastMessenger.showMessage(message: "Successfully updated user")
                    return
                }
                ToastMessenger.showMessage(message: "Failed to update with error \(String(describing: error.localizedDescription))")
            }
        }
    }


    func getData() {
        remoteDataLoadingViewController?.startedLoadingData()
        guard let clientID = EmBetConfigurationManger.shared.clientID else { return }
        let sdk = EmBetSDK(config: EmBetSDKConfiguration(clientID: clientID))
        sdk.profile.getProfileData { [weak self] profileData, error in

            if let error = error {
                self?.remoteDataLoadingViewController?.didFailToLoadData()
                ToastMessenger.showMessage(message: "Failed to get user data: \(error.localizedDescription)")
                return
            }

            if let profileData = profileData {
                guard let jsonData = try? JSONSerialization.data(withJSONObject: profileData, options: .withoutEscapingSlashes) else { return }
                self?.userProfile = try! JSONDecoder().decode(UserProfile.self, from: jsonData)
                self?.viewController?.viewControllerShouldReloadTableView()
                ToastMessenger.showMessage(message: "Did get profile data: \(profileData)")
            }
            self?.remoteDataLoadingViewController?.didLoadData()
        }

    }
}

extension SegmentationSettingsPresenter: SettingsDataSource {

    func setObjectForKey(key: SettingsDataSourceKeys, data: Any) {

        switch key {
        case .segmentationKeys(let segmentationDataSourceItemKey):
            switch segmentationDataSourceItemKey {
            case .age:
                guard let stringValue = data as? String,
                      let intValue = Int(stringValue) else { return }
                userProfile.age = intValue
            case .language:
                userProfile.language = data as? String
            case .country:
                userProfile.country = data as? String
            case .platform:
                userProfile.platform = data as? String
            case .bookmaker:
                userProfile.bookmaker = data as? String
            }
        default:
            return
        }
    }

    func boolForKey(key: SettingsDataSourceKeys) -> Bool {
        return false
    }

    func dataForKey(key: SettingsDataSourceKeys) -> String? {
        switch key {
        case .segmentationKeys(let segmentationDataSourceItemKey):
            switch segmentationDataSourceItemKey {
            case .age:
                return userProfile.age?.description ?? ""
            case .language:
                return userProfile.language
            case .country:
                return userProfile.country
            case .platform:
                return userProfile.platform
            case .bookmaker:
                return userProfile.bookmaker
            }
        default:
            return nil
        }
    }

}
