//
//  SettingsBasePresenter.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 13.1.23.
//

import UIKit

protocol SettingsPresenter {
    /**
        Inits in view controller
     */
    init(viewController: SettingsViewController)
    /**
        Method to prepare the presenter
     */
    func setUp()
    /**
        Signals the presenter to update the data
     */
    func presenterShouldUpdateDataPressed()
}

class SettingsBasePresenter: NSObject, SettingsPresenter {

    var settingsDataSource: SettingsTableViewDataSource!

    weak var viewController: SettingsViewController?

    required init(viewController: SettingsViewController) {
        self.viewController = viewController
        super.init()
    }

    func setUp() {
        guard let viewController = viewController else { return }
        viewController.tableView.delegate = self
    }

    func presenterShouldUpdateDataPressed() { }

}

extension SettingsBasePresenter: UITableViewDelegate {
    func tableView(_: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        return Constants.Application.DefaultSettingsCellHeight
    }
}
