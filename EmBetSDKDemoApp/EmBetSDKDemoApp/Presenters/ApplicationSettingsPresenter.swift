//
//  ApplicationSettingsPresenter.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 13.1.23.
//

import Foundation

class ApplicationSettingsPresenter: SettingsBasePresenter {

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    required init(viewController: SettingsViewController) {
        super.init(viewController: viewController)
    }

    override func setUp() {
        super.setUp()
        guard let viewController = viewController else { return }
        setupNotificationObserver()
        viewController.tableView.allowsSelection = false
        settingsDataSource = SettingsTableViewDataSource(
            settingsItems: SettingsFactory.generateSettingsSections(),
            tableView: viewController.tableView,
            dataSource: EmBetConfigurationManger.shared
        )
    }

    private func setupNotificationObserver() {
        NotificationCenter.default.addObserver(
            forName: ConfigurationManagerNotifications.programIDUpdatedName,
            object: nil, queue: .main
        ) { [weak self] notification in
            if notification.object as? String != nil {
                self?.viewController?.viewControllerShouldReloadTableView()
            }
        }
    }

}
