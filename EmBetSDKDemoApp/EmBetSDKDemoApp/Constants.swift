//
//  UIConstants.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 13.7.22.
//

import UIKit

/**
    Project constants
 */
struct Constants {

    /**
        Application constants
     */
    struct Application {
        static let PrimaryAccentColor: UIColor = UIColor.systemRed
        static let SecondaryAccentColor: UIColor = UIColor.systemRed
        static let PrivaryLabelColor: UIColor = UIColor.systemRed
        static let PrimaryTextInputColor: UIColor = .secondaryLabel
        static let DefaultSettingsCellHeight: CGFloat = 44.0
    }

    /**
        EmBet SDK Constants
     */
    struct SDK {
        static let InitialIsSegmentationEnabled = true
        static let InitialIsPresentationModeEnabled = false
        static let InitalLandscapeScaleFactor: Float = 0.7
    }

    /**
        SDK Widget constants
     */
    struct Widgets {
        static let MaxWidgetWidth: CGFloat = 400
        static let MinWidgetWidth: CGFloat = 340
        static let LandscapePerferedRatio: CGFloat = 0.46
    }
}
