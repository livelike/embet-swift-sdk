//
//  ToastMessenger.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 11.1.23.
//

import Foundation
import Toast_Swift

/**
    Shows message as an adroid toast.
 */
class ToastMessenger {
    /**
        Shows a toast message if event messages are enabled.
     */
    static func showMessage(message: String) {

        if EmBetConfigurationManger.shared.areEventMessagesEnabled == false {
            return
        }

        forceShowMessage(message: message)
    }

    /**
        Shows the toast message immediately on the first scene.
     */
    static func forceShowMessage(message: String) {
        let scene = UIApplication.shared.connectedScenes.first
        if let sd: SceneDelegate = (scene?.delegate as? SceneDelegate) {
            sd.window?.makeToast(message)
        }
    }
}
