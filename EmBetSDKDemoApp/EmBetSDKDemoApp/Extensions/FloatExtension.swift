//
//  FloatExtension.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 27.1.23.
//

import Foundation

extension Float {
    /**
        Creates Float from String
     */
    static func floatFromDecimalString(decimalString string: String) -> Float? {
        // Replace `,` with `.` as some locales show semicolon for decimal separation
        return Float(string.replacingOccurrences(of: ",", with: "."))
    }
}
