//
//  UIViewExtension.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 31.1.23.
//

import UIKit

extension UIView {
    /**
        Changes the views alpha to 1 with UIView animation.
     */
    func fadeIn(duration: TimeInterval = 0.25) {
        UIView.animate(withDuration: duration) {
            self.alpha = 1
        }
    }
}
