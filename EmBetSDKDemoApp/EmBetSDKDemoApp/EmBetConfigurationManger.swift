//
//  EmBetConfigurationManger.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 30.3.22.
//

import Foundation
import EmBetSDK

struct ConfigurationManagerNotifications {
    static let programIDUpdatedName: Notification.Name = .init(rawValue: "widget.program_id.updated")

    static func notifyNewProgramIDReceived(programID: String) {
        NotificationCenter.default.post(name: ConfigurationManagerNotifications.programIDUpdatedName, object: programID)
    }
}

class EmBetConfigurationManger: NSObject {

    static let shared = EmBetConfigurationManger()

    var currentSDK: EmBetSDK?

    var clientID: String? = PersistentStore.getClientID() {
        didSet {
            PersistentStore.setClientID(clientID: clientID)
        }
    }

    var programID: String? = PersistentStore.getProgramID() {
        didSet {
            PersistentStore.setProgramID(programID: programID)
        }
    }

    var customID: String? = PersistentStore.getCustomID() {
        didSet {
            PersistentStore.setCustomID(customID: customID)
            if customID != nil, customID?.isEmpty == false {
                self.updateProgramID()
            }
        }
    }

    var isPresentationModeEnabled: Bool = PersistentStore.getIsPresentationModeEnabled() {
        didSet {
            PersistentStore.setIsPresentationModeEnabled(isEnabled: isPresentationModeEnabled)
        }
    }

    var isSegmentationEnabled: Bool = PersistentStore.getIsSegmentationEnabled() {
        didSet {
            PersistentStore.setIsSegmentationEnabled(isEnabled: isSegmentationEnabled)
        }
    }

    var shouldForceHideCloseButton: Bool = PersistentStore.getShouldHideCloseButton() {
        didSet {
            PersistentStore.setShouldHideCloseButton(isEnabled: shouldForceHideCloseButton)
        }
    }

    var landscapeScaleFactor: Float = PersistentStore.getLandscapeScaleFactor() {
        didSet {
            PersistentStore.setLandscapeScaleFactor(scaleFactor: landscapeScaleFactor)
        }
    }

    var areEventMessagesEnabled: Bool = PersistentStore.getAreEventMessagesEnabled() {
        didSet {
            PersistentStore.setAreEventMessagesEnabled(enabled: areEventMessagesEnabled)
        }
    }

    var isTimelineInHorizontalMode: Bool = PersistentStore.getIsTimelineInHorizontalFlow() {
        didSet {
            PersistentStore.setIsTimelineInHorizontalFlow(enabled: isTimelineInHorizontalMode)
        }
    }

    var isTimelineShowingPersistentWidgetsOnly: Bool = PersistentStore.getIsTimelineShowingPersistentWidgetsOnly() {
        didSet {
            PersistentStore.setIsTimelineShowingPersistentWidgetsOnly(onlyPersistent: isTimelineShowingPersistentWidgetsOnly)
        }
    }

    var emptyTimelineLabel: String? = PersistentStore.getTimelineLabel() {
        didSet {
            PersistentStore.setTimelineLabel(label: emptyTimelineLabel)
        }
    }

    var completeConnectionDetails: (clientID: String, programID: String)? {
        guard let clientID = clientID, !clientID.isEmpty,
              let programID = programID, !programID.isEmpty else { return nil }

        return (clientID, programID)
    }

    var completeProgramSearchDetails: (clientID: String, customID: String)? {
        guard let clientID = clientID, !clientID.isEmpty,
              let customID = customID, !customID.isEmpty else { return nil }

        return (clientID, customID)
    }

    func updateProgramID() {
        if let completeProgramSearchDetails = completeProgramSearchDetails {
            currentSDK = EmBetSDK(config: EmBetSDKConfiguration(clientID: completeProgramSearchDetails.clientID))
            currentSDK?.getProgramID(customID: completeProgramSearchDetails.customID) { prgmID, err in
                if prgmID != nil {
                    self.programID = prgmID
                    ConfigurationManagerNotifications.notifyNewProgramIDReceived(programID: prgmID!)
                    self.currentSDK = nil
                }
            }

        }
    }
}

extension EmBetConfigurationManger: SettingsDataSource {

    func dataForKey(key: SettingsDataSourceKeys) -> String? {

        switch key {
        case .persistentKeys(let persistentKey):
            switch persistentKey {
            case .clientID:
                return clientID
            case .programID:
                return programID
            case .customID:
                return customID
            case .landscapeScaleFactor:
                return "\(landscapeScaleFactor)"
            case .emptyTimelineLabel:
                return emptyTimelineLabel
            default:
                return nil
            }
        default:
            return nil
        }
    }

    func boolForKey(key: SettingsDataSourceKeys) -> Bool {
        switch key {
        case .persistentKeys(let persistentKey):
            switch persistentKey {
            case .enablePresentationMode:
                return isPresentationModeEnabled
            case .segmentationStatus:
                return isSegmentationEnabled
            case .enableEventMessages:
                return areEventMessagesEnabled
            case .forceHideCloseButton:
                return shouldForceHideCloseButton
            case .horizontalFlow:
                return isTimelineInHorizontalMode
            case .timelinePersistentOnly:
                return isTimelineShowingPersistentWidgetsOnly
            default:
                return false
            }
        default:
            return false
        }
    }

    func setObjectForKey(key: SettingsDataSourceKeys, data: Any) {
        switch key {
        case .persistentKeys(let persistentKey):
            switch persistentKey {
            case .clientID:
                clientID = data as? String
            case .programID:
                programID = data as? String
            case .customID:
                customID = data as? String
            case .enablePresentationMode:
                guard let boolValue = data as? Bool else { return }
                isPresentationModeEnabled = boolValue
            case .segmentationStatus:
                guard let boolValue = data as? Bool else { return }
                isSegmentationEnabled = boolValue
            case .enableEventMessages:
                guard let boolValue = data as? Bool else { return }
                areEventMessagesEnabled = boolValue
            case .forceHideCloseButton:
                guard let boolValue = data as? Bool else { return }
                shouldForceHideCloseButton = boolValue
            case .landscapeScaleFactor:
                guard let stringValue = data as? String,
                      let floatValue = Float.floatFromDecimalString(decimalString: stringValue) else { return }
                let clamped = min(max(floatValue, 0.5), 1)
                landscapeScaleFactor = clamped
            case .horizontalFlow:
                guard let boolValue = data as? Bool else { return }
                isTimelineInHorizontalMode = boolValue
            case .timelinePersistentOnly:
                guard let boolValue = data as? Bool else { return }
                isTimelineShowingPersistentWidgetsOnly = boolValue
            case .emptyTimelineLabel:
                emptyTimelineLabel = data as? String
            }
        default:
            return
        }
    }

}
