//
//  SettingsDataSource.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 13.1.23.
//

import UIKit

protocol SettingsDataSource: NSObject {
    func setObjectForKey(key: SettingsDataSourceKeys, data: Any)
    func boolForKey(key: SettingsDataSourceKeys) -> Bool
    func dataForKey(key: SettingsDataSourceKeys) -> String?
}

class SettingsTableViewDataSource: NSObject, UITableViewDataSource {

    var settingsItems: [SettingsSection]

    weak var dataSource: SettingsDataSource?

    private let cellIdentifier = "cell_identifier"
    private let cellIdentifierToggle = "toggle_cell"
    private let cellIdentifierButton = "button_cell"
    private let cellIdentifierBase = "base_cell"

    init(
        settingsItems: [SettingsSection],
        tableView: UITableView,
        dataSource: SettingsDataSource
    ) {
        self.settingsItems = settingsItems
        self.dataSource = dataSource
        super.init()
        tableView.register(BaseInputTableViewTextCell.self, forCellReuseIdentifier: cellIdentifier)
        tableView.register(BaseToggleTableViewCell.self, forCellReuseIdentifier: cellIdentifierToggle)
        tableView.register(BaseTableViewCell.self, forCellReuseIdentifier: cellIdentifierBase)
        tableView.dataSource = self
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return settingsItems[section].header
    }

    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return settingsItems[section].footer
    }
    func numberOfSections(in _: UITableView) -> Int {
        return settingsItems.count
    }

    func tableView(_: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingsItems[section].rows.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let rowModel = settingsItems[indexPath.section].rows[indexPath.row]

        guard let dataSource = dataSource else { return UITableViewCell() }

        if rowModel.interactionType == .toggle {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifierToggle) as? BaseToggleTableViewCell else {
                return UITableViewCell()
            }

            cell.cellLabel.text = rowModel.title

            if let key = rowModel.dataSourceKey {
                cell.key = key
                cell.toggle.isOn = dataSource.boolForKey(key: key)
            }

            cell.delegate = self
            return cell

        } else if rowModel.interactionType == .freeformText {

            guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? BaseInputTableViewTextCell else {
                return UITableViewCell()
            }

            cell.cellLabel.text = rowModel.title

            if let key = rowModel.dataSourceKey {
                cell.key = key
                cell.textField.text = dataSource.dataForKey(key: key)
            }

            cell.delegate = self

            return cell

        } else if rowModel.interactionType == .decimalNumbers {

            guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? BaseInputTableViewTextCell else {
                return UITableViewCell()
            }
            cell.textField.keyboardType = .decimalPad
            cell.cellLabel.text = rowModel.title

            if let key = rowModel.dataSourceKey {
                cell.key = key
                cell.textField.text = dataSource.dataForKey(key: key)
            }

            cell.delegate = self
            return cell

        } else if rowModel.interactionType == .regular {

            guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifierBase) as? BaseTableViewCell else {
                return UITableViewCell()
            }

            cell.cellLabel.text = rowModel.title

            if let key = rowModel.dataSourceKey {
                cell.key = key
            }

            return cell

        }

        return UITableViewCell()
    }

}

extension SettingsTableViewDataSource: InputTableViewTextCellDelegate {
    func inputTableViewDidEndEditing(key: SettingsDataSourceKeys, text: String) {
        dataSource?.setObjectForKey(key: key, data: text)
    }
}

extension SettingsTableViewDataSource: BaseToggleCellDelegate {
    func didToggle(key: SettingsDataSourceKeys, isEnabled: Bool) {
        dataSource?.setObjectForKey(key: key, data: isEnabled)
    }
}
