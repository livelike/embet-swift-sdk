//
//  Icon.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 1.4.22.
//

import UIKit

struct Icon {

    /// Menu Icon
    static var MenuIcon: UIImage? {
        return UIImage(named: "menu_icon")
    }

    /// Home Icon
    static var HomeIcon: UIImage? {
        return UIImage(named: "home")
    }

    /// Calendar Icon
    static var CalendarIcon: UIImage? {
        return UIImage(named: "calendar")
    }

    /// Football Icon
    static var FootballIcon: UIImage? {
        return UIImage(named: "football")
    }
}
