//
//  TimelineViewController.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 4.7.23.
//

import UIKit
import EmBetSDK

class TimelineViewController: UIViewController {

    var timelineViewController: EmBetTimelineViewController!

    override func loadView() {
        self.view = UIView()
        self.view.backgroundColor = UIColor.systemGroupedBackground
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupSelf()
    }

    private func setupSelf() {

        self.title = "Timeline"
        self.setupNavigationController()

        if let connectionDetails = EmBetConfigurationManger.shared.completeConnectionDetails {
            let clientID = connectionDetails.clientID
            let programID = connectionDetails.programID
            let emBetSDK = EmBetSDK(config: .init(clientID: clientID))

            emBetSDK.enableSegmentation(isEnabled: EmBetConfigurationManger.shared.isSegmentationEnabled)
            emBetSDK.configureSession(configuration: .init(programID: programID))

            timelineViewController = EmBetTimelineViewController(sdk: emBetSDK)            

            if EmBetConfigurationManger.shared.isTimelineInHorizontalMode == false {
                timelineViewController.traits.update(with: .usesVerticalFlow)
            }

            if EmBetConfigurationManger.shared.isTimelineShowingPersistentWidgetsOnly == true {
                timelineViewController.traits.update(with: .persistentWidgetsOnly)
            }

            timelineViewController.delegate = self

            addChild(timelineViewController)
            timelineViewController.didMove(toParent: self)
            timelineViewController.view.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(timelineViewController.view)

            NSLayoutConstraint.activate([
                timelineViewController.view.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                timelineViewController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                timelineViewController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                timelineViewController.view.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            ])
        }
    }

    /**
        Sets up the views controller left button items.
     */
    internal func setupNavigationController() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            image: Icon.MenuIcon,
            landscapeImagePhone: nil,
            style: .done, target: self,
            action: #selector(didPressMenu)
        )
    }

    @objc func didPressMenu() {
        sideMenuController?.revealMenu()
    }

}

extension TimelineViewController: EmBetTimelineViewControllerDelegate {
    func timelineEmptyView() -> UIView? {
        guard let customText = PersistentStore.getTimelineLabel(),
              !customText.isEmpty else { return nil }

        let emptyView = UILabel()
        emptyView.translatesAutoresizingMaskIntoConstraints = false
        emptyView.textAlignment = .center
        emptyView.numberOfLines = 0
        emptyView.text = customText
        return emptyView
    }
}
