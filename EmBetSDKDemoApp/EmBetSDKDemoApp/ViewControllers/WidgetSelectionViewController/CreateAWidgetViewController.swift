//
//  CreateAWidgetViewController.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 13.1.23.
//

import UIKit

/**
    Create a widget view controller delegate
 */
protocol CreateAWidgetViewControllerDelegate {
    func didSelectWidget(widget: LocalWidgetModel)
}

class CreateAWidgetViewController: SettingsBaseViewController {

    var delegate: CreateAWidgetViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupSelf()
    }

    override func setupPresenter() {
        presenter = WidgetSettingsPresenter(viewController: self)
        presenter.setUp()
    }

    private func setupSelf() {
        self.title = "Create a widget"
    }

    override func setupNavigationController() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(didPressTrigger))
        navigationItem.rightBarButtonItem?.isEnabled = false
    }

    @objc
    func didPressTrigger() {
        if let widget = (presenter as? WidgetSettingsPresenterDelegate)?.widget {
            delegate?.didSelectWidget(widget: widget)
        }
        self.dismiss(animated: true)
    }

    override func viewControllerShouldPrepareAsReady() {
        if navigationItem.rightBarButtonItem?.isEnabled == false {
            navigationItem.rightBarButtonItem?.isEnabled = true
        }
    }

    override func viewControllerShouldDeleteSections(sectionIndex: IndexSet) {
        tableView.deleteSections(sectionIndex, with: .middle)
    }

    override func viewControllerShouldAddSections(sectionIndex: IndexSet) {
        tableView.insertSections(sectionIndex, with: .middle)
    }
}

