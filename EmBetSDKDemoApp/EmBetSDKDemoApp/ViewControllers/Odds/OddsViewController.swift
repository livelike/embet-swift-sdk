//
//  OddsViewController.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 17.11.23.
//

import UIKit
import EmBetSDK
import SideMenuSwift
import UIKit
import AVKit

class OddsViewController: UIViewController {

    private var widgetHolderView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private weak var sampleUsageView: UIView?

    private var presenter: OddsDemoPresenter
    /**
        Holds the active widget constraints
     */
    private var activeWidgetConstraints: [NSLayoutConstraint] = []

    private var widgetTopConstraintConstant: CGFloat = 0

    private weak var playerVC: AVPlayerViewController?

    private let previewView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .black
        return view
    }()

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    static func initForPresenting() -> OddsViewController {
        return OddsViewController(presenter: OddsDemoPresenter())
    }

    internal init(presenter: OddsDemoPresenter) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupSelf()
        setupNavigationController()
        setupSession()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.removePlayer()
    }

    override func loadView() {
        view = UIView()
        view.backgroundColor = .systemBackground
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }

    private func setupNavigationController() {
        let menuItem = UIBarButtonItem(
            image: Icon.MenuIcon,
            landscapeImagePhone: nil,
            style: .done,
            target: self,
            action: #selector(didPressMenu)
        )
        navigationItem.leftBarButtonItem = menuItem
    }

    private func setupSelf() {
        title = "Odds Tracking"
    }

    private func addVideoPlayer() {
        let videoURL = URL(string: "https://websdk.livelikecdn.com/demo/manchestervid.mp4")!
        let playerItem = AVPlayerItem(url: videoURL)
        let player = AVQueuePlayer(playerItem: playerItem)

        let playerController = AVPlayerViewController()
        playerController.player = player

        addChild(playerController)
        playerController.didMove(toParent: self)
        previewView.addSubview(playerController.view)
        playerController.view.frame = previewView.bounds

        self.playerVC = playerController
        view.addSubview(previewView)

    }

    private func removePlayer() {
        self.playerVC?.player?.pause()
        self.playerVC = nil
    }

    private func setupWidgetViewController(sdkConfiguration: EmBetSDKConfiguration, sessionConfiguration: EmBetSessionConfiguration) {
        presenter.setupPresenter(delegate: self)
        presenter.getWidgetViewController(sdkConfiguration: sdkConfiguration, sessionConfiguration: sessionConfiguration)
    }

    private func setupNotificationObserver() {
        NotificationCenter.default.addObserver(
            forName: ConfigurationManagerNotifications.programIDUpdatedName,
            object: nil, queue: .main
        ) { [weak self] notification in
            if notification.object as? String != nil {
                self?.setupSession()
            }
        }
    }

    private func setupSession() {
        if let connectionDetails = EmBetConfigurationManger.shared.completeConnectionDetails {
            addVideoPlayer()
            view.addSubview(widgetHolderView)
            setupWidgetViewController(sdkConfiguration: EmBetSDKConfiguration(clientID: connectionDetails.clientID), sessionConfiguration: EmBetSessionConfiguration(programID: connectionDetails.programID))
            setupConstraints()
            NotificationCenter.default.removeObserver(self)

        } else {
            setupNotificationObserver()
        }
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.setupConstraints(size: size)
    }

    /**
        Initial constraints setup
     */
    private func setupConstraints(size: CGSize = UIScreen.main.bounds.size) {

        // Main screen bounds
        let maxWidgetWidth = Constants.Widgets.MaxWidgetWidth
        let minWidgetWidth = Constants.Widgets.MinWidgetWidth

        // Max size normalization, should not extend beyond MaxWidgetWidth
        let maxSizeNormalized = size.width > maxWidgetWidth ? maxWidgetWidth : size.width

        // Clear previeous constraints
        // Note: When developing for iOS 8.0 or later, set the constraint’s isActive property to false instead
        // of calling the removeConstraint(_:) method directly. The isActive property automatically adds and
        // removes the constraint from the correct view.
        NSLayoutConstraint.deactivate(activeWidgetConstraints)
        activeWidgetConstraints.removeAll()

        let deviceOrientation = UIDevice.current.orientation
        if deviceOrientation.isPortrait == true || deviceOrientation.isValidInterfaceOrientation == false {

            self.navigationController?.setNavigationBarHidden(false, animated: true)
            // Normalized Width, should not be below MinWidgetSize
            let normalizedWidth = maxSizeNormalized < minWidgetWidth ? minWidgetWidth : maxSizeNormalized

            activeWidgetConstraints = [
                previewView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
                previewView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
                previewView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                previewView.heightAnchor.constraint(equalTo: previewView.widthAnchor, multiplier: 9 / 16),

                widgetHolderView.topAnchor.constraint(equalTo: previewView.bottomAnchor, constant: widgetTopConstraintConstant),
                widgetHolderView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
                widgetHolderView.widthAnchor.constraint(equalToConstant: normalizedWidth - 16),
                widgetHolderView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            ]

            sampleUsageView?.isHidden = false
            widgetHolderView.transform = CGAffineTransformMakeScale(1, 1)

        } else {
            self.navigationController?.setNavigationBarHidden(true, animated: true)

            // Calculates prefered size
            let calculatedSize = size.width * Constants.Widgets.LandscapePerferedRatio > maxWidgetWidth ? maxWidgetWidth : size.width * Constants.Widgets.LandscapePerferedRatio

            // Size should not be lower than predefined minimum width
            let normalizedWidth = calculatedSize < minWidgetWidth ? minWidgetWidth : calculatedSize

            activeWidgetConstraints = [
                previewView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                previewView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                previewView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                previewView.bottomAnchor.constraint(equalTo: view.bottomAnchor),

                widgetHolderView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                widgetHolderView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
                widgetHolderView.widthAnchor.constraint(equalToConstant: normalizedWidth),
                widgetHolderView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            ]

            sampleUsageView?.isHidden = true
        }

        NSLayoutConstraint.activate(activeWidgetConstraints)
    }

    @objc func didPressMenu() {
        sideMenuController?.revealMenu()
    }
}

extension OddsViewController: OddsDemoPresenterDelegate {
    func presenterDelegateDidProvideWidgetViewController(viewController: UIViewController) {
        addChild(viewController)
        viewController.didMove(toParent: self)
        viewController.view.translatesAutoresizingMaskIntoConstraints = false
        widgetHolderView.addSubview(viewController.view)

        NSLayoutConstraint.activate([
            viewController.view.trailingAnchor.constraint(equalTo: widgetHolderView.trailingAnchor),
            viewController.view.leadingAnchor.constraint(equalTo: widgetHolderView.leadingAnchor),
            viewController.view.topAnchor.constraint(equalTo: widgetHolderView.topAnchor),
            viewController.view.bottomAnchor.constraint(equalTo: widgetHolderView.bottomAnchor),
        ])

        (viewController as? EmBetOddsTrackingViewController)?.delegate = self
    }

    func presenterShowPresentationModeControls() {

        let usageView = SampleUsageView()
        usageView.translatesAutoresizingMaskIntoConstraints = false
        self.view.insertSubview(usageView, belowSubview: widgetHolderView)

        usageView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        usageView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        usageView.topAnchor.constraint(equalTo: previewView.bottomAnchor).isActive = true
        usageView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true

        sampleUsageView = usageView
        widgetTopConstraintConstant = usageView.topToolbar.intrinsicContentSize.height
    }

}

extension OddsViewController: EmBetOddsTrackingViewControllerDelegate {
    func emptyDataView() -> UIView? {
        guard let customText = PersistentStore.getTimelineLabel(),
              !customText.isEmpty else { return nil }

        let emptyView = UILabel()
        emptyView.translatesAutoresizingMaskIntoConstraints = false
        emptyView.textAlignment = .center
        emptyView.numberOfLines = 0
        emptyView.text = customText
        return emptyView

    }
}
