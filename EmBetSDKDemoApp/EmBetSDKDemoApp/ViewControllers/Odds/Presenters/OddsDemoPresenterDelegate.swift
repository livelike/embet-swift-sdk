//
//  OddsDemoPresenterDelegate.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 17.11.23.
//

import UIKit

/**
    OddsDemoPresenterDelegate
 */
@objc
protocol OddsDemoPresenterDelegate {
    /**
        Called when the presenter will provide the widget holder view controller
     */
    func presenterDelegateDidProvideWidgetViewController(viewController: UIViewController)
    /**
        Called if the view controller needs to setup for presentation
     */
    func presenterShowPresentationModeControls()
}
