//
//  OddsDemoPresenter.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 17.11.23.
//

import Foundation
import EmBetSDK

class OddsDemoPresenter {

    weak var delegate: OddsDemoPresenterDelegate?

    private var widgetViewController: EBWidgetViewController!

    var isPresenterModeEnabled: Bool { return EmBetConfigurationManger.shared.isPresentationModeEnabled }

    public func setupPresenter(delegate: OddsDemoPresenterDelegate) {
        self.delegate = delegate
        if isPresenterModeEnabled == true {
            self.delegate?.presenterShowPresentationModeControls()
        }
    }

    public func getWidgetViewController(sdkConfiguration: EmBetSDKConfiguration, sessionConfiguration: EmBetSessionConfiguration) {
        let sdk = EmBetSDK(config: sdkConfiguration)
        sdk.configureSession(configuration: sessionConfiguration)
        let embetOddsController = EmBetOddsTrackingViewController(sdk: sdk)

        delegate?.presenterDelegateDidProvideWidgetViewController(viewController: embetOddsController)

    }
}
