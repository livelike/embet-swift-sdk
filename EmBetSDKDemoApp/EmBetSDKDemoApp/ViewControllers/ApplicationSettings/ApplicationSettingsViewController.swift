//
//  ApplicationSettingsViewController.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 30.3.22.
//

import UIKit

class ApplicationSettingsViewController: SettingsBaseViewController {

    override func viewDidLoad() {
        self.setupSelf()
        super.viewDidLoad()
    }

    override func setupPresenter() {
        presenter = ApplicationSettingsPresenter(viewController: self)
        presenter.setUp()
    }

    private func setupSelf() {
        self.title = "Settings"
    }

}
