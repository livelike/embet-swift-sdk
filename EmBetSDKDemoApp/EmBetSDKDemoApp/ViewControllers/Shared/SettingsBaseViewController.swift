//
//  SettingsBaseViewController.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 13.1.23.
//

import UIKit
import EmBetSDK

/**
    Settings view controller protocol
 */
protocol SettingsViewController: UIViewController {

    /**
        The view controllers tableview.
     */
    var tableView: UITableView { get }

    /**
        The view controller should reload the tableView.
     */
    func viewControllerShouldReloadTableView()

    /**
        The view controller should reload specified indexes.
     */
    func viewControllerShouldReload(indexPaths: [IndexPath])

    /**
        The view controller should add sections.
     */
    func viewControllerShouldAddSections(sectionIndex: IndexSet)

    /**
        The view controller should delete sections.
     */
    func viewControllerShouldDeleteSections(sectionIndex: IndexSet)

    /**
        The view controller should prepare as ready.
     */
    func viewControllerShouldPrepareAsReady()
}

/**
    Base view controller class for creating pre-populated TableViewControllers.
 */
class SettingsBaseViewController: UIViewController, SettingsViewController {

    /**
        The presenter attached to this view controller class
     */
    lazy var presenter: SettingsPresenter = SegmentationSettingsPresenter(viewController: self)

    internal lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.keyboardDismissMode = .onDrag
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func loadView() {
        self.view = UIView()
        self.view.backgroundColor = UIColor.systemGroupedBackground
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupKeyboardNotifications()
        setupNavigationController()
        setupTableView()
        setupPresenter()
    }

    /**
        Sets up the keyboard notifications.
     */
    private func setupKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)

        self.tableView.contentInsetAdjustmentBehavior = .always
    }

    /**
        Starts setting up the presenter.
     */
    internal func setupPresenter() {
        presenter.setUp()
    }

    /**
        Sets up the views controller left button items.
     */
    internal func setupNavigationController() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            image: Icon.MenuIcon,
            landscapeImagePhone: nil,
            style: .done, target: self,
            action: #selector(didPressMenu)
        )
    }

    /**
        Sets up the view controllers table view.
     */
    private func setupTableView() {
        view.addSubview(tableView)

        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])

        tableView.reloadData()
    }

    @objc func didPressMenu() {
        view.endEditing(true)
        sideMenuController?.revealMenu()
    }

    func viewControllerShouldPrepareAsReady() {}

    func viewControllerShouldReloadTableView() {
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }

    func viewControllerShouldReload(indexPaths: [IndexPath]) {
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadRows(at: indexPaths, with: .automatic)
        }
    }

    func viewControllerShouldAddSections(sectionIndex: IndexSet) {}

    func viewControllerShouldDeleteSections(sectionIndex: IndexSet) {}

}

/**
    Keyboard events
 */
extension SettingsBaseViewController {

    /**
        Keyboard will show
     */
    @objc
    private func keyboardWillShow(_ notification: NSNotification) {
        if let newFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.tableView.contentInset.bottom == 0 {
                let insets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: newFrame.height, right: 0)
                self.tableView.contentInset = insets
                self.tableView.scrollIndicatorInsets = insets
                UIView.animate(withDuration: 0.1) {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }

    /**
        Keyboard will hide
     */
    @objc
    private func keyboardWillHide(_ notification: NSNotification) {
        if self.tableView.contentInset.bottom != 0 {
            self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            self.tableView.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            UIView.animate(withDuration: 0.1) {
                self.view.layoutIfNeeded()
            }
        }
    }

}
