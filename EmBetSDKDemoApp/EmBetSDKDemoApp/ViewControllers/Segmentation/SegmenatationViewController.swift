//
//  SegmenatationViewController.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 27.7.22.
//

import UIKit

/**
    Async view controller protocol.
    Should be adopted by views that load data.
 */
protocol AsyncDataViewController {
    func startedLoadingData()
    func didLoadData()
    func didFailToLoadData()
}

class SegmenatationViewController: SettingsBaseViewController {

    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView()
        activityView.translatesAutoresizingMaskIntoConstraints = false
        activityView.hidesWhenStopped = true
        return activityView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupSelf()
    }

    override func setupPresenter() {
        presenter = SegmentationSettingsPresenter(viewController: self)
        presenter.setUp()
    }

    private func setupSelf() {
        self.title = "Segmentation"
    }

    override func setupNavigationController() {
        super.setupNavigationController()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Update data", style: .plain, target: self, action: #selector(didPressUpdate))
    }

    @objc
    func didPressUpdate() {
        presenter.presenterShouldUpdateDataPressed()
    }
}

// Async view controller protocol methods implementation
extension SegmenatationViewController: AsyncDataViewController {
    func startedLoadingData() {

        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)

        NSLayoutConstraint.activate([
            activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])

        DispatchQueue.main.async { [weak self] in
            self?.tableView.alpha = 0
            self?.navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }

    func didLoadData() {
        DispatchQueue.main.async { [weak self] in
            self?.activityIndicator.stopAnimating()
            self?.navigationItem.rightBarButtonItem?.isEnabled = true
            self?.tableView.fadeIn()
        }
    }

    func didFailToLoadData() {
        DispatchQueue.main.async { [weak self] in
            self?.activityIndicator.stopAnimating()
        }
    }


}
