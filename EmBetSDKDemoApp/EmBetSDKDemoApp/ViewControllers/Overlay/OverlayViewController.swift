//
//  OverlayViewController.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 12.4.24.
//

import AVKit
import UIKit
import EmBetSDK

class OverlayViewController: UIViewController {

    private weak var playerVC: AVPlayerViewController?

    private var activeWidgetConstraints: [NSLayoutConstraint] = []

    private let previewView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .black
        return view
    }()

    private let widgetContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationController()
        self.addVideoPlayer()
        self.setupSelf()
    }

    override func loadView() {
        view = UIView()
        view.backgroundColor = .systemBackground
    }

    private func addVideoPlayer() {
        let videoURL = URL(string: "https://websdk.livelikecdn.com/demo/manchestervid.mp4")!
        let playerItem = AVPlayerItem(url: videoURL)
        let player = AVQueuePlayer(playerItem: playerItem)

        let playerController = AVPlayerViewController()
        playerController.player = player

        addChild(playerController)
        playerController.didMove(toParent: self)
        previewView.addSubview(playerController.view)
        playerController.view.frame = previewView.bounds

        self.playerVC = playerController
        view.addSubview(previewView)

    }

    private func setupSelf() {
        title = "Overlay"

        if let connectionDetails = EmBetConfigurationManger.shared.completeConnectionDetails {
            let clientID = connectionDetails.clientID
            let programID = connectionDetails.programID

            let config = EmBetSDKConfiguration(clientID: clientID)
            let emBetSDK = EmBetSDK(config: config)

            emBetSDK.enableSegmentation(isEnabled: EmBetConfigurationManger.shared.isSegmentationEnabled)
            emBetSDK.configureSession(configuration: .init(programID: programID))

            emBetSDK.eventManager.eventListener = { event in
                ToastMessenger.showMessage(message: event.toDictionary().map({ "\($0.key)=\($0.value)"}).joined(separator: ","))
            }

            let widgetOverlayViewController = EmBetWidgetOverlayViewController(sdk: emBetSDK)
            let advertisementOverlayViewController = EmBetAdvertisementOverlayViewController(sdk: emBetSDK)

            widgetOverlayViewController.delegate = self
            advertisementOverlayViewController.delegate = self

            self.addChild(widgetOverlayViewController)
            widgetOverlayViewController.willMove(toParent: self)

            self.addChild(advertisementOverlayViewController)
            advertisementOverlayViewController.willMove(toParent: self)

            widgetContainer.addSubview(widgetOverlayViewController.view)
            widgetContainer.addSubview(advertisementOverlayViewController.view)

            widgetOverlayViewController.view.translatesAutoresizingMaskIntoConstraints = false
            advertisementOverlayViewController.view.translatesAutoresizingMaskIntoConstraints = false

            NSLayoutConstraint.activate([
                widgetOverlayViewController.view.topAnchor.constraint(equalTo: widgetContainer.safeAreaLayoutGuide.topAnchor),
                widgetOverlayViewController.view.leadingAnchor.constraint(equalTo: widgetContainer.safeAreaLayoutGuide.leadingAnchor),
                widgetOverlayViewController.view.trailingAnchor.constraint(equalTo: widgetContainer.safeAreaLayoutGuide.trailingAnchor),
                widgetOverlayViewController.view.bottomAnchor.constraint(equalTo: widgetContainer.safeAreaLayoutGuide.bottomAnchor),

                advertisementOverlayViewController.view.topAnchor.constraint(equalTo: widgetContainer.safeAreaLayoutGuide.topAnchor),
                advertisementOverlayViewController.view.leadingAnchor.constraint(equalTo: widgetContainer.safeAreaLayoutGuide.leadingAnchor),
                advertisementOverlayViewController.view.trailingAnchor.constraint(equalTo: widgetContainer.safeAreaLayoutGuide.trailingAnchor),
                advertisementOverlayViewController.view.bottomAnchor.constraint(equalTo: widgetContainer.safeAreaLayoutGuide.bottomAnchor),
            ])

            self.view.addSubview(widgetContainer)

            setupConstraints()
        }

    }

    private func setupNavigationController() {
        let menuItem = UIBarButtonItem(
            image: Icon.MenuIcon,
            landscapeImagePhone: nil,
            style: .done,
            target: self,
            action: #selector(didPressMenu)
        )
        navigationItem.leftBarButtonItem = menuItem
    }

    @objc func didPressMenu() {
        sideMenuController?.revealMenu()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.setupConstraints(size: size)
    }

    /**
        Initial constraints setup
     */
    private func setupConstraints(size: CGSize = UIScreen.main.bounds.size) {
        // Clear previeous constraints
        // Note: When developing for iOS 8.0 or later, set the constraint’s isActive property to false instead
        // of calling the removeConstraint(_:) method directly. The isActive property automatically adds and
        // removes the constraint from the correct view.
        NSLayoutConstraint.deactivate(activeWidgetConstraints)
        activeWidgetConstraints.removeAll()

        let deviceOrientation = UIDevice.current.orientation
        if deviceOrientation.isPortrait == true || deviceOrientation.isValidInterfaceOrientation == false {

            self.navigationController?.setNavigationBarHidden(false, animated: true)

            let hToWRatio = size.width / size.height
            // Transforms subviews to width to height ratio
            widgetContainer.subviews.forEach({ $0.transform = CGAffineTransformMakeScale(hToWRatio, hToWRatio) })

            activeWidgetConstraints = [
                previewView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
                previewView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
                previewView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                previewView.heightAnchor.constraint(equalTo: previewView.widthAnchor, multiplier: 9 / 16),

                widgetContainer.centerXAnchor.constraint(equalTo: previewView.centerXAnchor),
                widgetContainer.centerYAnchor.constraint(equalTo: previewView.centerYAnchor),
                widgetContainer.widthAnchor.constraint(equalTo: previewView.widthAnchor, constant: (1 - hToWRatio) * size.height),
                widgetContainer.heightAnchor.constraint(equalTo: widgetContainer.widthAnchor, multiplier: 9 / 16),
            ]

        } else {
            self.navigationController?.setNavigationBarHidden(true, animated: true)

            activeWidgetConstraints = [
                previewView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                previewView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                previewView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                previewView.bottomAnchor.constraint(equalTo: view.bottomAnchor),

                widgetContainer.topAnchor.constraint(equalTo: previewView.topAnchor),
                widgetContainer.rightAnchor.constraint(equalTo: previewView.rightAnchor),
                widgetContainer.leftAnchor.constraint(equalTo: previewView.leftAnchor),
                widgetContainer.bottomAnchor.constraint(equalTo: previewView.bottomAnchor),
            ]
            // Transforms subview to default scale
            widgetContainer.subviews.forEach({ $0.transform = CGAffineTransformMakeScale(1, 1) })
        }

        NSLayoutConstraint.activate(activeWidgetConstraints)
    }

}

extension OverlayViewController: EmBetWidgetOverlayDelegate {

    func widgetOverlay(willDisplay widget: EmBetWidget) {
        ToastMessenger.showMessage(message: "Will display widget: \(widget.widgetID)")
    }

    func widgetOverlay(didDisplay widget: EmBetWidget) {
        ToastMessenger.showMessage(message: "Did display widget: \(widget.widgetID)")
    }

    func widgetOverlay(willDismiss widget: EmBetWidget) {
        ToastMessenger.showMessage(message: "Will dismiss widget: \(widget.widgetID)")
    }

    func widgetOverlay(didDismiss widget: EmBetWidget) {
        ToastMessenger.showMessage(message: "Did dismiss widget: \(widget.widgetID)")
    }


}
