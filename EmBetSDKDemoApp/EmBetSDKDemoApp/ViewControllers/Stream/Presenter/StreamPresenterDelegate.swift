//
//  StreamPresenterDelegate.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 3.8.22.
//

import UIKit

/**
    Stream presenter delegate
 */
@objc
protocol StreamPresenterDelegate {

    /**
        Called when the presenter will provide the widget holder view controller
     */
    func presenterDelegateDidProvideWidgetViewController(viewController: UIViewController)

    /**
        Called when the view controller should show the "select widget" button
     */
    func presenterShowWidgetSelectionButton()

    /**
        Called if the view controller needs to setup for presentation
     */
    func presenterShowPresentationModeControls()

}
