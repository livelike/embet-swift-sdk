//
//  StreamLivePresenter.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 3.8.22.
//

import Foundation
import EmBetSDK

/**
    Presenter for the Default 'live' stream view controller.
 */
class StreamLivePresenter: StreamPresenter {

    weak var delegate: StreamPresenterDelegate?

    public func setupPresenter(delegate: StreamPresenterDelegate) {
        self.delegate = delegate
        if isPresenterModeEnabled == true {
            self.delegate?.presenterShowPresentationModeControls()
        }
    }

    public func getWidgetViewController(sdkConfiguration: EmBetSDKConfiguration, sessionConfiguration: EmBetSessionConfiguration) {
        let sdk = EmBetSDK(config: sdkConfiguration)
        sdk.configureSession(configuration: sessionConfiguration)
        sdk.enableSegmentation(isEnabled: EmBetConfigurationManger.shared.isSegmentationEnabled)
        sdk.setupDelegate = self
        let widgetViewController = EBWidgetPopupViewController(sdk: sdk)
        widgetViewController.delegate = self
        delegate?.presenterDelegateDidProvideWidgetViewController(viewController: widgetViewController)
    }

}

extension StreamLivePresenter: EmBetSDKSetupDelegate {
    func didFinishSetupWithSuccess(sdk: EmBetSDK) {
        ToastMessenger.showMessage(message: "SDK successfully initialized")
    }

    func didFinishSetupWithError(sdk: EmBetSDK, error: NSError) {
        ToastMessenger.showMessage(message: "SDK failed to initialize: \(error.localizedDescription)")
    }
}

extension StreamLivePresenter: EBWidgetPopupViewControllerDelegate {
    func widgetViewController(didDismiss widget: EmBetWidget) {
        ToastMessenger.showMessage(message: "Did dismiss widget")
    }

    func widgetViewController(didDisplay widget: EmBetWidget) {
        print("did Display widget \(widget.widgetID)")
    }

    func widgetViewController(willDismiss widget: EmBetWidget) {
        print("will Dismiss widget \(widget.widgetID)")
    }

    func widgetViewController(willDisplay widget: EmBetWidget) {
        if EmBetConfigurationManger.shared.shouldForceHideCloseButton == true {
            widget.displayType = .forceHideCloseButton
        }
        ToastMessenger.showMessage(message: "Will display widget")
    }

}
