//
//  StreamDemoPresenter.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 3.8.22.
//

import Foundation
import EmBetSDK

/**
    Presenter for the StreamViewController that allows for loading local files.
 */
class StreamDemoPresenter: StreamPresenter {
        
    var widget: LocalWidgetModel = LocalWidgetModel() {
        didSet {
            didSelectWidget()
        }
    }
    
    weak var delegate: StreamPresenterDelegate?
            
    private var widgetViewController: EBWidgetViewController!
        
    public func setupPresenter(delegate: StreamPresenterDelegate) {
        self.delegate = delegate
        if isPresenterModeEnabled == true {
            self.delegate?.presenterShowPresentationModeControls()
        }
        self.delegate?.presenterShowWidgetSelectionButton()
    }
    
    public func getWidgetViewController(sdkConfiguration: EmBetSDKConfiguration, sessionConfiguration: EmBetSessionConfiguration) {
        widgetViewController = EBWidgetViewController(clientID: sdkConfiguration.clientID)
        
        if EmBetConfigurationManger.shared.shouldForceHideCloseButton == true {
            widgetViewController.widgetDisplayType = .forceHideCloseButton
        }
        
        delegate?.presenterDelegateDidProvideWidgetViewController(viewController: widgetViewController)
    }
    
    func didSelectWidget() {
        
        guard let localWidgetFile = widget.widget else { return }
        
        let path = localWidgetFile.bundleJsonFileName
        
        if let path = Bundle.main.path(forResource: path, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path))
                let j = try JSONSerialization.jsonObject(with: data, options: .fragmentsAllowed)
                
                guard var json = j as? [String: Any] else { return }
                
                // Append the custom data
                json["custom_data"] = customData()
                
                // Generate a random id for the widget
                json["id"] = UUID().uuidString
                
                // Match the clientID with the current clientID
                json["client_id"] = EmBetConfigurationManger.shared.clientID
                
                widgetViewController.loadWidgetFromJsonObject(jsonObject: json)
                
            } catch (let error) {
                print(error)
            }
            
        }
        
        
    }
    
    func customData() -> String? {
        
        guard let localWidget = widget.widget else { return nil }
        
        if let path = Bundle.main.path(forResource: localWidget.bundleCustomDataJsonFileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path))
                let j = try JSONSerialization.jsonObject(with: data, options: .fragmentsAllowed)
                
                guard var json = j as? [String: Any] else { return nil }
                json["widgetVariation"] = localWidget.variation
                
                if widget.hasSponsor == false {
                    json["sponsors"] = nil
                }
                
                if widget.hasLegalLogo == false {
                    json["legalLogoSrc"] = nil
                }

                if widget.hasCTA == false {
                    json["placeBetUrl"] = nil
                    json["placeBetLabel"] = nil
                }
                
                if widget.isInsight == true {
                    json["widgetType"] = "live-at-bat"
                }

                if widget.hasDisclaimer == true {
                    var labels: [String: Any] = json["labels"] as! [String: Any]
                    labels["disclaimer"] = "Responsible gaming:"
                    labels["disclaimerDescription"] = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non ligula non purus posuere fringilla in in erat. Donec tempus, augue in ultricies cursus, mauris libero tincidunt neque, in accumsan nulla felis sed elit. Suspendisse suscipit convallis finibus. Vivamus nec ullamcorper metus, eu rhoncus ex. Sed fringilla eleifend nisl, nec pulvinar mauris porta quis. Nullam nec dapibus dolor. Phasellus nisl orci, suscipit sit amet arcu nec, pulvinar auctor augue. Sed ornare pulvinar sem at aliquam. Aenean metus nisi, dapibus quis arcu et, tristique bibendum dui. Integer aliquam erat ac tortor congue, quis commodo metus laoreet. Suspendisse eget dolor non ipsum fermentum lacinia id quis diam. Mauris arcu purus, iaculis quis orci vitae, dapibus tempus enim. Nam cursus condimentum est a faucibus. Maecenas mi elit, cursus non nulla consequat, pharetra pharetra arcu. Pellentesque scelerisque pretium massa eget eleifend. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non ligula non purus posuere fringilla in in erat. Donec tempus, augue in ultricies cursus, mauris libero tincidunt neque, in accumsan nulla felis sed elit. Suspendisse suscipit convallis finibus. Vivamus nec ullamcorper metus, eu rhoncus ex. Sed fringilla eleifend nisl, nec pulvinar mauris porta quis. Nullam nec dapibus dolor. Phasellus nisl orci, suscipit sit amet arcu nec, pulvinar auctor augue. Sed ornare pulvinar sem at aliquam. Aenean metus nisi, dapibus quis arcu et, tristique bibendum dui. Integer aliquam erat ac tortor congue, quis commodo metus laoreet. Suspendisse eget dolor non ipsum fermentum lacinia id quis diam. Mauris arcu purus, iaculis quis orci vitae, dapibus tempus enim. Nam cursus condimentum est a faucibus. Maecenas mi elit, cursus non nulla consequat, pharetra pharetra arcu. Pellentesque scelerisque pretium massa eget eleifend"
                    labels["disclaimerDescriptionCollapsed"] = "This is a collapsed description. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non ligula non purus posuere fringilla in in erat."
                    json["labels"] = labels
                }

                if let theme = getTheme() {
                    json["theme"] = theme
                }

                let jsonData = try JSONSerialization.data(withJSONObject: json, options: .withoutEscapingSlashes)
                return String(data: jsonData, encoding: .utf8)

            } catch (let error) {
                print(error)
            }
        }

        return nil
    }
    
    func getTheme() -> [String: Any]? {
        
        guard let customData = widget.customData else { return nil }
        
        if let path = Bundle.main.path(forResource: customData.bundleJsonFileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path))
                let j = try JSONSerialization.jsonObject(with: data, options: .fragmentsAllowed)
                
                return j as? [String: Any]

            } catch (let error) {
                print(error)
            }
        }

        return nil
    }


}

extension StreamDemoPresenter: CreateAWidgetViewControllerDelegate {
    
    func didSelectWidget(widget: LocalWidgetModel) {
        self.widget = widget
    }
}
