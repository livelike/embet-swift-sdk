//
//  StreamPresenter.swift
//  
//
//  Created by Ljupco Nastevski on 1.8.22.
//

import UIKit
import EmBetSDK

/**
    Collections of methods that the Stream presenter should implement.
 */
@objc
protocol StreamPresenter {
    /**
        Sets up the presenter
     */
    func setupPresenter(delegate: StreamPresenterDelegate)

    /**
        Start creating the widget view
     */
    func getWidgetViewController(sdkConfiguration: EmBetSDKConfiguration, sessionConfiguration: EmBetSessionConfiguration)

    /**
        Did make a selection
     */
    @objc optional func didSelectOption(title: String)
}

extension StreamPresenter {

    var isPresenterModeEnabled: Bool { return EmBetConfigurationManger.shared.isPresentationModeEnabled }

}
