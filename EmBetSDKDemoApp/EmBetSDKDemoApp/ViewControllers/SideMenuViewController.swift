//
//  SideMenuViewController.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 30.3.22.
//

import UIKit

enum SideMenuItems: String, CaseIterable {
    case overlay = "Overlay"
    case stream = "Stream"
    case demoViewController = "Demo"
    case timeline = "Timeline"
    case oddsTracking = "Odds Tracking"
    case segmentation = "Segmentation"
    case settings = "Settings"
}

class SideMenuViewController: UIViewController {
    private let sideMenuItems = SideMenuItems.allCases

    private let cellIdentifier = "cell_identifier"

    override func viewDidLoad() {
        super.viewDidLoad()
        setupSelf()
        setupTableView()
    }

    override func loadView() {
        view = UIView()
        view.backgroundColor = .systemBackground
    }

    private func setupSelf() {
        title = "Menu"
    }

    private func setupTableView() {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.tableFooterView = UIView()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        view.addSubview(tableView)

        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])

        tableView.reloadData()
    }
}

extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)! as UITableViewCell

        if #available(iOS 15, *) {
            var config = cell.defaultContentConfiguration()
            config.text = sideMenuItems[indexPath.row].rawValue
            cell.contentConfiguration = config
        } else {
            cell.textLabel?.text = sideMenuItems[indexPath.row].rawValue
        }

        return cell
    }

    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return sideMenuItems.count
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let contentViewController: UIViewController!

        if sideMenuItems[indexPath.row] == .overlay {
            contentViewController = UINavigationController(rootViewController: OverlayViewController())
        } else if sideMenuItems[indexPath.row] == .stream {
            contentViewController = UINavigationController(rootViewController: StreamViewController.initForPresenting())
        } else if sideMenuItems[indexPath.row] == .settings {
            contentViewController = UINavigationController(rootViewController: ApplicationSettingsViewController())
        } else if sideMenuItems[indexPath.row] == .segmentation {
            contentViewController = UINavigationController(rootViewController: SegmenatationViewController())
        } else if sideMenuItems[indexPath.row] == .timeline {
            contentViewController = UINavigationController(rootViewController: TimelineViewController())
        } else if sideMenuItems[indexPath.row] == .oddsTracking {
            contentViewController = UINavigationController(rootViewController: OddsViewController.initForPresenting())
        } else {
            contentViewController = UINavigationController(rootViewController: StreamViewController.initForDemo())
        }

        sideMenuController?.setContentViewController(to: contentViewController)
        sideMenuController?.hideMenu()
    }
}
