//
//  PersistentStore.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 20.1.23.
//

import Foundation

class PersistentStore {

    // MARK: - UserDefault keys

    private static let HasInitialSetupKey = "connection.embet_SKD.demo_project.initialSetup"
    private static let ClientIDKey = "connection.embet_SKD.demo_project.client_id_key"
    private static let ProgramIDKey = "connection.embet_SKD.demo_project.program_id_key"
    private static let CustomIDKey = "connection.embet_SKD.demo_project.custom_id_key"
    private static let PresentationModeKey = "connection.embet_SKD.demo_project.presentation_mode"
    private static let SegmentationStatusKey = "connection.embet_SKD.demo_project.segmentation_status"
    private static let HidesCloseButtonKey = "connection.embet_SKD.demo_project.close_button_hide_key"
    private static let LandscapeScaleFactorKey = "connection.embet_SKD.demo_project.landscape_scale_factor"
    private static let EventMessagesStatusKey = "connection.embet_SKD.demo_project.event_messages_status"
    private static let IsTimelineFlowDirectionHorizontalKey = "connection.embet_SKD.demo_project.timeline_flow_direction_horizontal"
    private static let IsTimelineShowingPersistentWidgetsOnlyKey = "connection.embet_SKD.demo_project.timeline_persistent_widgets_only"
    private static let EmptyTimelineLabel = "connection.embet_SKD.demo_project.empty_timeline_label"

    // MARK: - Initial Setup

    static func hasInitialSetup() -> Bool {
        return UserDefaults.standard.bool(forKey: HasInitialSetupKey)
    }

    static func setHasInitialSetup(hasInitialSetup: Bool) {
        set(value: hasInitialSetup, forKey: HasInitialSetupKey)
    }

    // MARK: - Client ID

    static func getClientID() -> String? {
        return UserDefaults.standard.string(forKey: ClientIDKey)
    }

    static func setClientID(clientID: String?) {
        set(value: clientID, forKey: ClientIDKey)
    }

    // MARK: - Program ID

    static func getProgramID() -> String? {
        return UserDefaults.standard.string(forKey: ProgramIDKey)
    }

    static func setProgramID(programID: String?) {
        set(value: programID, forKey: ProgramIDKey)
    }

    // MARK: - Custom ID

    static func getCustomID() -> String? {
        return UserDefaults.standard.string(forKey: CustomIDKey)
    }

    static func setCustomID(customID: String?) {
        set(value: customID, forKey: CustomIDKey)
    }

    // MARK: - Presentation Mode

    static func getIsPresentationModeEnabled() -> Bool {
        return UserDefaults.standard.bool(forKey: PresentationModeKey)
    }

    static func setIsPresentationModeEnabled(isEnabled: Bool) {
        set(value: isEnabled, forKey: PresentationModeKey)
    }

    // MARK: - Segmentation

    static func getIsSegmentationEnabled() -> Bool {
        return UserDefaults.standard.bool(forKey: SegmentationStatusKey)
    }

    static func setIsSegmentationEnabled(isEnabled: Bool) {
        set(value: isEnabled, forKey: SegmentationStatusKey)
    }

    // MARK: - Close Button

    static func getShouldHideCloseButton() -> Bool {
        return UserDefaults.standard.bool(forKey: HidesCloseButtonKey)
    }

    static func setShouldHideCloseButton(isEnabled: Bool) {
        set(value: isEnabled, forKey: HidesCloseButtonKey)
    }

    // MARK: - Landscape factor

    static func getLandscapeScaleFactor() -> Float {
        return UserDefaults.standard.float(forKey: LandscapeScaleFactorKey)
    }

    static func setLandscapeScaleFactor(scaleFactor: Float) {
        set(value: scaleFactor, forKey: LandscapeScaleFactorKey)
    }

    // MARK: - Event

    static func getAreEventMessagesEnabled() -> Bool {
        return UserDefaults.standard.bool(forKey: EventMessagesStatusKey)
    }

    static func setAreEventMessagesEnabled(enabled: Bool) {
        set(value: enabled, forKey: EventMessagesStatusKey)
    }

    // MARK: - Timeline horizontal flow

    static func getIsTimelineInHorizontalFlow() -> Bool {
        return UserDefaults.standard.bool(forKey: IsTimelineFlowDirectionHorizontalKey)
    }

    static func setIsTimelineInHorizontalFlow(enabled: Bool) {
        set(value: enabled, forKey: IsTimelineFlowDirectionHorizontalKey)
    }

    static func getIsTimelineShowingPersistentWidgetsOnly() -> Bool {
        return UserDefaults.standard.bool(forKey: IsTimelineShowingPersistentWidgetsOnlyKey)
    }

    static func setIsTimelineShowingPersistentWidgetsOnly(onlyPersistent: Bool) {
        set(value: onlyPersistent, forKey: IsTimelineShowingPersistentWidgetsOnlyKey)
    }

    // MARK: - Empty timeline label

    static func getTimelineLabel() -> String? {
        return UserDefaults.standard.string(forKey: EmptyTimelineLabel)
    }

    static func setTimelineLabel(label: String?) {
        set(value: label, forKey: EmptyTimelineLabel)
    }

    // MARK: - Private

    /**
        Sets value for key in userdef
     */
    private static func set(value: Any?, forKey key: String) {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
}
