//
//  SettingsSection.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 11.1.23.
//

import Foundation

struct SettingsSection {
    let header: String?
    let footer: String?
    let rows: [SettingsRow]

    init(header: String? = nil, footer: String? = nil, rows: [SettingsRow]) {
        self.header = header
        self.footer = footer
        self.rows = rows
    }
}
