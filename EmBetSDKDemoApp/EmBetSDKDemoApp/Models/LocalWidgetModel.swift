//
//  LocalWidgetModel.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 25.1.23.
//

import Foundation

/**
    Local widget model
 */
class LocalWidgetModel {
    
    /**
        The assiciated local widget file
     */
    public var widget: LocalWidgetFile?
    
    /**
        Local widget theme
     */
    public var customData: LocalWidgetCustomData?
    
    /**
        Determines if the widget should have sponsor
     */
    public var hasSponsor: Bool = true
    
    /**
        Determines if the widget should have a clickable button
     */
    public var hasCTA: Bool = true
    
    /**
        Determines if the widget should have a legal logo
     */
    public var hasLegalLogo: Bool = false
    
    /**
        Determines if the widget is insight
     */
    public var isInsight: Bool = false
    
    /**
        Determines if the widget should have a disclaimer
     */
    public var hasDisclaimer: Bool = false
    
}
