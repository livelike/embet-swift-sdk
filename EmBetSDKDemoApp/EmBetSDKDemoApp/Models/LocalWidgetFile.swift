//
//  LocalWidgetFile.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 3.8.22.
//

import Foundation

/**
    Local widget model used for local widgets.
 */
struct LocalWidgetFile {
    /**
        Display name of the widget.
     */
    let name: String
    /**
        Updates the variation of the bet widget.
     */
    let variation: String
    /**
        Filename of the local widget file.
     */
    let bundleJsonFileName: String
    /**
        Custom data filename.
     */
    let bundleCustomDataJsonFileName: String
    /**
        Custom data - Theme filename.
     */
    let bundleThemeJsonFileName: String? = nil
}
