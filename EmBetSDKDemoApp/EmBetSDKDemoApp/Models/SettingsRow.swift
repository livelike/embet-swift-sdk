//
//  SettingsRow.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 24.1.23.
//

import Foundation

enum RowInteractionType {
    case regular
    case freeformText
    case decimalNumbers
    case toggle
}

struct SettingsRow {
    let title: String
    let interactionType: RowInteractionType
    let dataSourceKey: SettingsDataSourceKeys?
    let associatedObject: Any?

    init(
        title: String,
        interactionType: RowInteractionType,
        dataSourceKey: SettingsDataSourceKeys? = nil,
        associatedObject: Any? = nil
    ) {
        self.title = title
        self.interactionType = interactionType
        self.dataSourceKey = dataSourceKey
        self.associatedObject = associatedObject
    }
}
