//
//  UserProfile.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 27.7.22.
//

import Foundation

/**
    User profile model - segmenation
 */
class UserProfile: Codable {
    var age: Int?
    var language: String?
    var country: String?
    var platform: String?
    var bookmaker: String?
}
