//
//  LocalWidgetCustomData.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 10.3.23.
//

import Foundation

/**
    Local custom data model.
 */
struct LocalWidgetCustomData {
    /**
        Display name of the custom data.
     */
    let name: String

    /**
        Filename of the local widget file.
     */
    let bundleJsonFileName: String
}
