//
//  SettingsSectionFactory.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 2.2.23.
//

import Foundation

/**
    Generates settings sections and populates them with adequate rows.
 */
class SettingsFactory {

    /**
        Generates `Application` settings sections.
     */
    static func generateSettingsSections() -> [SettingsSection] {

        return [
            SettingsSection(
                header: "REQUIRED: Client Configuration",
                footer: "To successfully configure the SDK, please make sure to have a valid Client ID.",
                rows:
                [
                    SettingsRow(title: "Client ID", interactionType: .freeformText, dataSourceKey: .persistentKeys(.clientID))
                ]
            ),
            SettingsSection(
                header: "REQUIRED: Program Configuration",
                footer: "To successfully connect to a specific program, please provide a valid ProgramID or CustomID",
                rows:
                [
                    SettingsRow(title: "Program ID", interactionType: .freeformText, dataSourceKey: .persistentKeys(.programID)),
                    SettingsRow(title: "Custom ID", interactionType: .freeformText, dataSourceKey: .persistentKeys(.customID))
                ]
            ),
            SettingsSection(
                header: "Presentation Mode",
                footer: "Changes the demo application state to `Presentation` - adds additional UI elements, simulating an example integration.",
                rows:
                [
                    SettingsRow(title: "Enabled", interactionType: .toggle, dataSourceKey: .persistentKeys(.enablePresentationMode))
                ]
            ),
            SettingsSection(
                header: "Intergrator Options",
                rows:
                [
                    SettingsRow(title: "Segmentation Feature", interactionType: .toggle, dataSourceKey: .persistentKeys(.segmentationStatus)),
                    SettingsRow(title: "Hides close button", interactionType: .toggle, dataSourceKey: .persistentKeys(.forceHideCloseButton)),
                ]
            ),
            SettingsSection(
                footer: "Integrator options are options that the integrator can change and apply directly to the widget, bypassing any settings sent from the CMS.",
                rows:
                [
                    SettingsRow(title: "LandscapeScale", interactionType: .decimalNumbers, dataSourceKey: .persistentKeys(.landscapeScaleFactor)),
                ]
            ),
            SettingsSection(
                header: "Timeline Settings",
                footer: "Changes the look of the timeline.",
                rows:
                [
                    SettingsRow(title: "Horizontal Flow", interactionType: .toggle, dataSourceKey: .persistentKeys(.horizontalFlow)),
                    SettingsRow(title: "Only persistent widgets", interactionType: .toggle, dataSourceKey: .persistentKeys(.timelinePersistentOnly)),
                    SettingsRow(title: "Empty timeline text", interactionType: .freeformText, dataSourceKey: .persistentKeys(.emptyTimelineLabel))
                ]
            ),
            SettingsSection(
                header: "Event Messages",
                footer: "Wheather the event messages should be shown or not.",
                rows:
                [
                    SettingsRow(title: "Enabled", interactionType: .toggle, dataSourceKey: .persistentKeys(.enableEventMessages))
                ]
            )
        ]
    }

    /**
        Generates `Segmentations` settings sections.
     */
    static func generateSegmentationSettings() -> [SettingsSection] {

        return [
            SettingsSection(
                header: "User data",
                footer: "In order for upcoming widgets to be shown, the data must abide by the widget rules. NOTE: Changing ClientID will generate a new user, so the segmentation properties need to be updated again.",
                rows:
                [
                    SettingsRow(title: "Age", interactionType: .freeformText, dataSourceKey: .segmentationKeys(.age)),
                    SettingsRow(title: "Country", interactionType: .freeformText, dataSourceKey: .segmentationKeys(.country)),
                    SettingsRow(title: "Language", interactionType: .freeformText, dataSourceKey: .segmentationKeys(.language)),
                    SettingsRow(title: "Platform", interactionType: .freeformText, dataSourceKey: .segmentationKeys(.platform)),
                    SettingsRow(title: "Bookmaker", interactionType: .freeformText, dataSourceKey: .segmentationKeys(.bookmaker)),
                ]
            )
        ]
    }


    /**
        Generates `Create a Widget` settings sections.
     */
    static func generateWidgetCreationSettings() -> [SettingsSection] {
        return [
            SettingsSection(
                header: "Widgets and variations",
                rows:
                [
                    SettingsRow(
                        title: "Bet Widget - Square",
                        interactionType: .regular,
                        dataSourceKey: .creteAWidgetKeys(.square),
                        associatedObject: LocalWidgetFile(name: "Square", variation: "square", bundleJsonFileName: "bet_widget", bundleCustomDataJsonFileName: "bet_custom_data")
                    ),
                    SettingsRow(
                        title: "Bet Widget - Bar",
                        interactionType: .regular,
                        dataSourceKey: .creteAWidgetKeys(.bar),
                        associatedObject: LocalWidgetFile(name: "Bar", variation: "bar", bundleJsonFileName: "bet_widget", bundleCustomDataJsonFileName: "bet_custom_data")
                    ),
                    SettingsRow(
                        title: "Bet Widget - Inline",
                        interactionType: .regular,
                        dataSourceKey: .creteAWidgetKeys(.inline),
                        associatedObject: LocalWidgetFile(name: "Inline", variation: "inline", bundleJsonFileName: "bet_widget", bundleCustomDataJsonFileName: "bet_custom_data")
                    ),
                    SettingsRow(
                        title: "Bet Widget - Duel",
                        interactionType: .regular,
                        dataSourceKey: .creteAWidgetKeys(.duel),
                        associatedObject: LocalWidgetFile(name: "Duel", variation: "duel", bundleJsonFileName: "bet_widget", bundleCustomDataJsonFileName: "bet_custom_data")
                    ),
                    // TODO: widget payload should be correct
                    SettingsRow(
                        title: "Quiz Widget",
                        interactionType: .regular,
                        dataSourceKey: .creteAWidgetKeys(.duel),
                        associatedObject: LocalWidgetFile(name: "Quiz", variation: "bar", bundleJsonFileName: "quiz_widget", bundleCustomDataJsonFileName: "quiz_custom_data")
                    ),
                    SettingsRow(
                        title: "Odds Widget",
                        interactionType: .regular,
                        dataSourceKey: .creteAWidgetKeys(.square),
                        associatedObject: LocalWidgetFile(name: "Odds Widget", variation: "", bundleJsonFileName: "odds_widget", bundleCustomDataJsonFileName: "odds_custom_data")
                    ),
                    SettingsRow(
                        title: "Alert Widget",
                        interactionType: .regular,
                        dataSourceKey: .creteAWidgetKeys(.alert),
                        associatedObject: LocalWidgetFile(name: "Alert", variation: "", bundleJsonFileName: "alert_widget", bundleCustomDataJsonFileName: "alert_custom_data")
                    ),
                    SettingsRow(
                        title: "Prediction Widget",
                        interactionType: .regular,
                        dataSourceKey: .creteAWidgetKeys(.prediction),
                        associatedObject: LocalWidgetFile(name: "Prediction", variation: "", bundleJsonFileName: "prediction_widget", bundleCustomDataJsonFileName: "prediction_custom_data")
                    ),
                    SettingsRow(
                        title: "Prediction Follow-Up Widget",
                        interactionType: .regular,
                        dataSourceKey: .creteAWidgetKeys(.prediction),
                        associatedObject: LocalWidgetFile(name: "PredictionFolowUp", variation: "", bundleJsonFileName: "prediction_followup_widget", bundleCustomDataJsonFileName: "prediction_followup_custom_data")
                    )
                ]
            ),
            SettingsSection(
                header: "Theme",
                footer: "Determines what theme will the widget have.",
                rows:
                [
                    SettingsRow(
                        title: "Regular",
                        interactionType: .regular,
                        dataSourceKey: .selectAWidgetThemeKeys(.regular),
                        associatedObject: LocalWidgetCustomData(name: "Regular", bundleJsonFileName: "sportradar_theme")
                    ),
                    SettingsRow(
                        title: "Neon",
                        interactionType: .regular,
                        dataSourceKey: .selectAWidgetThemeKeys(.neon),
                        associatedObject: LocalWidgetCustomData(name: "Neon", bundleJsonFileName: "neon_theme")
                    ),
                    SettingsRow(
                        title: "Dark Gray",
                        interactionType: .regular,
                        dataSourceKey: .selectAWidgetThemeKeys(.neon),
                        associatedObject: LocalWidgetCustomData(name: "Dark Gray", bundleJsonFileName: "dark_gray_theme")
                    )
                ]
            ),
            SettingsSection(
                header: "Footer",
                footer: "Defines what elements will be shown in the widgets footer.",
                rows:
                [
                    SettingsRow(title: "CTA Button", interactionType: .toggle, dataSourceKey: .creteAWidgetKeys(.visibleCTAButton)),
                    SettingsRow(title: "Sponsor", interactionType: .toggle, dataSourceKey: .creteAWidgetKeys(.visibleSponsor)),
                    SettingsRow(title: "Legal Logo", interactionType: .toggle, dataSourceKey: .creteAWidgetKeys(.visibleLegalLogo)),
                    SettingsRow(title: "Disclaimer", interactionType: .toggle, dataSourceKey: .creteAWidgetKeys(.hasDisclaimer)),
                ]
            ),
            SettingsSection(
                header: "Widget Type",
                footer: "Defines the type of the bet widget.",
                rows:
                [
                    SettingsRow(title: "Is Insight", interactionType: .toggle, dataSourceKey: .creteAWidgetKeys(.isInsight)),
                ]
            )

        ]
    }

}
