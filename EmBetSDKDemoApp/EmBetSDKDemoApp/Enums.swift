//
//  Enums.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 27.1.23.
//

import Foundation

/**
    Data source item keys
 */
enum SettingsDataSourceKeys {
    case persistentKeys(PersistentKey)
    case segmentationKeys(SegmentationDataSourceItemKey)
    case creteAWidgetKeys(CreateAWidgetDataSourceItemKey)
    case selectAWidgetThemeKeys(SelectAWidgetThemeDataSourceItemKey)
}

/**
    Keys for applications settings
 */
enum PersistentKey {
    case clientID
    case programID
    case customID
    case enablePresentationMode
    case segmentationStatus
    case forceHideCloseButton
    case landscapeScaleFactor
    case enableEventMessages
    case horizontalFlow
    case timelinePersistentOnly
    case emptyTimelineLabel
}

/**
    Keys for segmentation settings
 */
enum SegmentationDataSourceItemKey {
    case age
    case country
    case language
    case platform
    case bookmaker
}

/**
    Keys for create widget
 */
enum CreateAWidgetDataSourceItemKey {
    case square
    case bar
    case inline
    case duel
    case alert
    case prediction
    case visibleCTAButton
    case visibleSponsor
    case visibleLegalLogo
    case isInsight
    case hasDisclaimer
}

/**
    Keys for create widget
 */
enum SelectAWidgetThemeDataSourceItemKey {
    case regular
    case neon
}
