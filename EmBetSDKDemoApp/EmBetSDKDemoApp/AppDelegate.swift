//
//  AppDelegate.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 30.3.22.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    func application(_: UIApplication, didFinishLaunchingWithOptions _: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        if PersistentStore.hasInitialSetup() == false {
            self.initialApplicationSetup()
        }
        return true
    }

    /**
        Initial application properties setup
     */
    private func initialApplicationSetup() {
        PersistentStore.setHasInitialSetup(hasInitialSetup: true)
        PersistentStore.setIsSegmentationEnabled(
            isEnabled: Constants.SDK.InitialIsSegmentationEnabled
        )
        PersistentStore.setIsPresentationModeEnabled(
            isEnabled: Constants.SDK.InitialIsPresentationModeEnabled
        )
        PersistentStore.setLandscapeScaleFactor(
            scaleFactor: Constants.SDK.InitalLandscapeScaleFactor
        )
    }

    // MARK: UISceneSession Lifecycle

    func application(_: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options _: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
}
