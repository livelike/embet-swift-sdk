//
//  SampleUsageView.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 25.1.23.
//

import UIKit

/**
    Sample usage view.
    Draws a view that would look like a simple implementation.
    No other functional usage usage.
 */
class SampleUsageView: UIView {

    lazy var topToolbar: UIToolbar = {
        let topBar = UIToolbar()
        topBar.translatesAutoresizingMaskIntoConstraints = false
        let button1 = UIBarButtonItem(title: "BETTING", style: .done, target: nil, action: nil)
        button1.isEnabled = false
        let button2 = UIBarButtonItem(title: "INTERACT", style: .done, target: nil, action: nil)
        let button3 = UIBarButtonItem(title: "STATS", style: .done, target: nil, action: nil)
        button3.isEnabled = false

        topBar.setItems(
            [
                UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil),
                button1,
                UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
                button2,
                UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
                button3
            ],
            animated: false
        )

        return topBar
    }()

    lazy var bottomToolbar: UITabBar = {
        let bottomBar = UITabBar()
        bottomBar.translatesAutoresizingMaskIntoConstraints = false
        bottomBar.setItems(
            [
                UITabBarItem(title: "Home", image: Icon.HomeIcon, tag: 0),
                UITabBarItem(title: "Scedule", image: Icon.CalendarIcon, tag: 1),
                UITabBarItem(title: "Sports", image: Icon.FootballIcon, tag: 2),
            ], animated: false
        )

        bottomBar.selectedItem = bottomBar.items?.last
        return bottomBar
    }()

    lazy var label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.numberOfLines = 0
        label.text = "Interactive widgets\nwill appear here!"
        return label
    }()

    required init() {
        super.init(frame: .zero)
        setupSelf()
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupSelf() {

        addSubview(topToolbar)
        addSubview(label)
        addSubview(bottomToolbar)

        NSLayoutConstraint.activate([
            topToolbar.leadingAnchor.constraint(equalTo: leadingAnchor),
            topToolbar.trailingAnchor.constraint(equalTo: trailingAnchor),
            topToolbar.topAnchor.constraint(equalTo: topAnchor),

            label.leadingAnchor.constraint(equalTo: leadingAnchor),
            label.trailingAnchor.constraint(equalTo: trailingAnchor),
            label.topAnchor.constraint(equalTo: topToolbar.bottomAnchor),
            label.bottomAnchor.constraint(equalTo: bottomToolbar.topAnchor),

            bottomToolbar.leadingAnchor.constraint(equalTo: leadingAnchor),
            bottomToolbar.trailingAnchor.constraint(equalTo: trailingAnchor),
            bottomToolbar.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)

        ])

    }
}
