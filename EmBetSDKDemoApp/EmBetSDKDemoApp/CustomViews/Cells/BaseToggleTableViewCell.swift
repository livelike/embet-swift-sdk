//
//  BaseToggleTableViewCell.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 11.1.23.
//

import UIKit

/**
    BaseToggleTableViewCell delegate
 */
protocol BaseToggleCellDelegate: AnyObject {
    func didToggle(key: SettingsDataSourceKeys, isEnabled: Bool)
}

/**
    Basic cell including a toggle
 */
class BaseToggleTableViewCell: BaseTableViewCell {

    weak var delegate: BaseToggleCellDelegate?

    lazy var toggle: UISwitch = {
        let retToggle = UISwitch()
        retToggle.addTarget(self, action: #selector(didToggle(_:)), for: .valueChanged)
        return retToggle
    }()

    override func commonInit() {
        super.commonInit()
        self.accessoryView = toggle
    }

    @objc
    func didToggle(_ sender: UISwitch) {
        if let key = key {
            delegate?.didToggle(key: key, isEnabled: sender.isOn)
        }

    }
}
