//
//  BaseInputTableViewCell.swift
//  EmBetSDKDemoApp
//
//  Created by Ljupco Nastevski on 28.7.22.
//

import UIKit

/**
    InputTableViewTextCell delegate
 */
protocol InputTableViewTextCellDelegate: AnyObject {
    func inputTableViewDidEndEditing(key: SettingsDataSourceKeys, text: String)
}

/**
    Basic table view cell including a text field  text input
 */
class BaseInputTableViewTextCell: BaseTableViewCell {
    weak var delegate: InputTableViewTextCellDelegate?

    lazy var textField: UITextField = {
        let textField = UITextField()
        textField.textAlignment = .right
        textField.textColor = Constants.Application.PrimaryTextInputColor
        textField.autocapitalizationType = .none
        textField.autocorrectionType = .no
        textField.returnKeyType = .done
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.delegate = self
        return textField
    }()

    override func commonInit() {
        super.commonInit()
        contentStackView.addArrangedSubview(textField)
    }

}

// MARK: - TextFieldDelegate
extension BaseInputTableViewTextCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text, let key = key {
            delegate?.inputTableViewDidEndEditing(key: key, text: text)
        }
    }
}
