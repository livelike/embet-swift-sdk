//
//  SegmentationTests.swift
//  
//
//  Created by Ljupco Nastevski on 5.8.22.
//

import XCTest
@testable import EmBetSDK

class SegmentationTests: XCTestCase {

    let skd: EmBetSDKInternal = EmBetSDKInternal(config: EmBetSDKConfiguration(clientID: "RuHpZaHvgP252o0oMw75cmaZAQ1pjG34UdyyqDF2"))
    
    override func setUpWithError() throws {
        EmBetSDK.logLevel = .verbose
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testUserDataNull() throws {
        
        let segmentator = UserSegmentator(internalSDK: self.skd)
        segmentator.rule = """
            {"segmentation" :{"and":[{">":[{"var":"age"},18]},{"==":[{"var":"country"},"mk"]}]}}
        """
        segmentator.userData = nil
                
        XCTAssertFalse(segmentator.applyRuleToData())

    }
    
    func testSegmentationMissing() throws {
        
        let segmentator = UserSegmentator(internalSDK: self.skd)
        segmentator.rule = nil
        
        segmentator.userData = """
            { "segmentation" : {"age":33,"lang":"en","country":"us"}}
        """

        XCTAssertTrue(segmentator.applyRuleToData())

    }

    
    func testRuleANDGreaterThan() throws {
        
        let segmentator = UserSegmentator(internalSDK: self.skd)
        
        segmentator.rule = """
            {"segmentation" :{"and":[{"in":[{"var":"lang"},"en"]},{"in":[{"var":"country"},"us"]}, {">=":[{"var":"age"},"18"]}]}}
        """
        
        segmentator.userData = """
            { "segmentation" : {"age":33,"lang":"en","country":"us"}}
        """
        
        XCTAssertTrue(segmentator.applyRuleToData())

    }

    func testRuleANDEquals() throws {
        
        let segmentator = UserSegmentator(internalSDK: self.skd)
        
        segmentator.rule = """
            {"segmentation" :{"and":[{"==":[{"var":"lang"},"en"]},{"==":[{"var":"country"},"mk"]}]}}
        """
        
        segmentator.userData = """
            {"segmentation" :{"age":33,"lang":"en","country":"mk"}}
        """

        XCTAssertTrue(segmentator.applyRuleToData())

    }
    
    func testRuleANDEqualsGreater() throws {
        
        let segmentator = UserSegmentator(internalSDK: self.skd)
        
        segmentator.rule = """
            {"segmentation" :{"and":[{">":[{"var":"age"},18]},{"==":[{"var":"country"},"mk"]}]}}
        """
        
        segmentator.userData = """
            {"segmentation" :{"age":33,"lang":"en","country":"mk"}}
        """

        XCTAssertTrue(segmentator.applyRuleToData())

    }

    func testRuleOREquals() throws {
        
        let segmentator = UserSegmentator(internalSDK: self.skd)
        
        segmentator.rule = """
            {"segmentation" :{"or":[{"==":[{"var":"age"},"33"]},{"==":[{"var":"country"},"us"]}]}}
        """
        
        segmentator.userData = """
            {"segmentation" :{"age":33,"lang":"en","country":"mk"}}
        """

        XCTAssertTrue(segmentator.applyRuleToData())

    }

    func testRuleANDIN() throws {
        
        let segmentator = UserSegmentator(internalSDK: self.skd)
        
        segmentator.rule = """
            {"segmentation" :{"and":[{"in":[{"var":"lang"},"es"]},{"==":[{"var":"country"},"de"]}]}}
        """
        
        segmentator.userData = """
            {"segmentation" :{"age":33,"lang":"en","country":"mk"}}
        """

        XCTAssertFalse(segmentator.applyRuleToData())

    }

    func testRuleEqualsNested() throws {
        
        let segmentator = UserSegmentator(internalSDK: self.skd)
        
        segmentator.rule = """
            {"segmentation" :{"==":[{"var":"country.info.phone"},"070238219"]}}
        """
        
        segmentator.userData = """
            { "segmentation" : {"age":33,"lang":"en","country": {"info" : {"phone":"070238219"}}}}
        """

        XCTAssertTrue(segmentator.applyRuleToData())

    }

    func testRuleInvalidData() throws {
        
        let segmentator = UserSegmentator(internalSDK: self.skd)
        
        segmentator.rule = """
            {"segmentation" : wrong_segmentation_data }
        """
        
        segmentator.userData = """
            { "segmentation" : {"age":33,"lang":"en","country": {"info" : {"phone":"070238219"}}}}
        """

        XCTAssertFalse(segmentator.applyRuleToData())

    }

    func testRuleInvalidUserData() throws {
        
        let segmentator = UserSegmentator(internalSDK: self.skd)
        
        segmentator.rule = """
            {"segmentation" :{"==":[{"var":"country.info.phone"},"070238219"]}}
        """

        segmentator.userData = """
            { "segmentation" : wrong_user_data }
        """

        XCTAssertFalse(segmentator.applyRuleToData())

    }

    func testUserInvalidData() throws {
        
        let segmentator = UserSegmentator(internalSDK: self.skd)
        
        segmentator.rule = """
            {"segmentation" :{"==":[{"var":"country.info.phone"},"070238219"]}}
        """

        segmentator.userData = """
            segmentator.userData
        """

        XCTAssertFalse(segmentator.applyRuleToData())

    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
